// // Side Menu Component
// import { SideMenuSettings } from '../shared/side-menu-content/models/side-menu-settings';
// import { SideMenuOption } from '../shared/side-menu-content/models/side-menu-option';
// import { SideMenuContentComponent } from './../shared/side-menu-content/side-menu-content.component';
// import { AuthService } from '../providers/auth-provider';

// export class menuSettingPage {
//     // Options to show in the SideMenuComponent
//     public options: Array<SideMenuOption>;
//     // Settings for the SideMenuContentComponent
//     // public sideMenuSettings: SideMenuSettings = {
//     //     accordionMode: true,
//     //     showSelectedOption: true,
//     //     selectedOptionClass: 'active-side-menu-option'
//     // };
//     constructor(
//         private authService: AuthService
//     ) {
//         if (this.authService.UserType == "guest") {
//             this.options.push({
//                 displayText: 'Dashboard',
//                 component: 'dashboardPage'
//             });
//         } else {
//             this.options.push({
//                 displayText: 'Dashboard',
//                 component: 'dashboardPage',
//                 // This option is already selected
//                 selected: true
//             });
//         }

//         this.options.push({
//             displayText: 'Events/Entertainment',
//             component: 'HomePage'

//             // This option is already selected
//             //  selected: true
//         });
//         this.options.push({
//             displayText: 'The Fishing Exchange',
//             component: 'FishingExchangPage'
//         });
//         // this.options.push({
//         //   //		iconName: 'analytics',
//         //   displayText: 'Get Boat',
//         //   component: 'GeoLocationTrackPage'
//         // });
//         this.options.push({
//             displayText: 'Local Weather',
//             component: 'HomeWeatherPage'
//         });
//         if (this.authService.UserType == "guest") {
//             this.options.push({
//                 displayText: 'Marine Weather',
//                 component: 'MapPage',
//                 selected: true
//             });
//         } else {
//             this.options.push({
//                 displayText: 'Marine Weather',
//                 component: 'MapPage'
//             });
//         }

//         this.options.push({
//             displayText: 'Live Buoys',
//             component: 'MapPage'
//         });
//         this.options.push({
//             displayText: 'What’s Hot?',
//             component: 'WHEventPage'
//         });

//         this.options.push({
//             displayText: 'Tides',
//             component: 'MapPage'
//         });
//         this.options.push({
//             displayText: 'Currents',
//             component: 'MapPage'
//         });
//         this.options.push({
//             displayText: 'Fish Counts',
//             component: 'FishCountsPage'
//         });
//         this.options.push({
//             displayText: 'Today\'s Catch - Photos',
//             component: 'ShoutOutMainPage'
//         });
//         this.options.push({
//             displayText: 'News & Fishing Reports',
//             component: 'FishingNewsPage'
//         });
//     }

//     menuIntialize() {

//         // Load simple menu options
//         // ------------------------------------------
//         // if (this.authService.UserType == "guest") {
//         //   this.options.push({
//         //     displayText: 'Dashboard',
//         //     component: 'dashboardPage'
//         //   });
//         //   this.options.push({
//         //     displayText: 'Marine Weather',
//         //     component: 'MapPage',
//         //     selected: true
//         //   });
//         // }else {
//         //   this.options.push({
//         //     displayText: 'Dashboard',
//         //     component: 'dashboardPage',
//         //     // This option is already selected
//         //     selected: true
//         //   });
//         //   this.options.push({
//         //     displayText: 'Marine Weather',
//         //     component: 'MapPage'
//         //   });
//         // }


//     }
// }