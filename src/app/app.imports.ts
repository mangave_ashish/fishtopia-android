// Global state (used for theming)
import { AppState } from './app.global';
// Providers
import { ToastService } from '../providers/util/toast.service';
import { AlertService } from '../providers/util/alert.service';
import { Diagnostic } from '@ionic-native/diagnostic';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { AuthService } from '../providers/auth-provider';
import { Calendar } from '@ionic-native/calendar';
import { UserData } from '../providers/user-data-local';
import { SocialSharing } from '@ionic-native/social-sharing';
import { DatabaseService, ForecastService, Sql, UtilService } from '../providers/weather';
import { Keyboard } from '@ionic-native/keyboard';
import { File } from '@ionic-native/file';
import { Transfer } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
import { Stripe } from '@ionic-native/stripe';
import { CardIO } from '@ionic-native/card-io';
import { AppVersion } from '@ionic-native/app-version';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Network } from '@ionic-native/network';
import { Geolocation } from '@ionic-native/geolocation';
import { Device } from '@ionic-native/device';
import { FCM } from '@ionic-native/fcm';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { GooglePlus } from '@ionic-native/google-plus';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links/ngx';


export const MODULES = [

];

export const PROVIDERS = [
  Geolocation,
  NativeGeocoder,
  Network,
  AppVersion,
  AlertService,
  ToastService,
  AppState,
  SocialSharing,
  Calendar,
  Diagnostic,
  StatusBar,
  SplashScreen,
  AuthService,
  UserData,
  Sql,
  DatabaseService,
  UtilService,
  ForecastService,
  Keyboard,
  File,
  Transfer,
  Camera,
  FilePath,
  Stripe,
  CardIO,
  Device,
  FCM,
  PhotoViewer,
  BarcodeScanner,
  // InAppPurchase2,
  GooglePlus,
  LocationAccuracy,
  FirebaseDynamicLinks
];
