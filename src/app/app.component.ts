import { Component, NgZone, ViewChild } from '@angular/core';
import { Facebook } from '@ionic-native/facebook';
import { FCM } from '@ionic-native/fcm';
import { File } from '@ionic-native/file';
import { Geolocation } from '@ionic-native/geolocation';
import { GooglePlus } from '@ionic-native/google-plus';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { LocationAccuracy } from '@ionic-native/location-accuracy';
import { NativeGeocoder } from '@ionic-native/native-geocoder';
import { Network } from '@ionic-native/network';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';
import { Transfer } from '@ionic-native/transfer';
import { Storage } from '@ionic/storage';
import { AlertController, LoadingController, MenuController, ModalController, Nav, Platform } from 'ionic-angular';
import { IonicApp } from 'ionic-angular/components/app/app-root';
import * as _ from 'lodash';
import { Subject } from 'rxjs/Subject';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../providers/auth-provider';
import { UserData } from '../providers/user-data-local';
import { AlertService } from '../providers/util/alert.service';
import { DatabaseService, DataPoint, Forecast, ForecastService, Location, Metrics, UtilService } from '../providers/weather';
import { SideMenuOption } from './../shared/side-menu-content/models/side-menu-option';
// Side Menu Component
import { SideMenuSettings } from './../shared/side-menu-content/models/side-menu-settings';
import { SideMenuContentComponent } from './../shared/side-menu-content/side-menu-content.component';
import { AppState } from './app.global';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;
  // Get the instance to call the public methods
  @ViewChild(SideMenuContentComponent) sideMenu: SideMenuContentComponent;
  // Options to show in the SideMenuComponent
  public options: Array<SideMenuOption>;
  // Settings for the SideMenuContentComponent
  public sideMenuSettings: SideMenuSettings = {
    accordionMode: true,
    showSelectedOption: true,
    selectedOptionClass: 'active-side-menu-option'
  };
  alert: any;
  rootPage: any;
  activePage = new Subject();
  authToken: any;
  pages: Array<{ title: string, component: any, active: boolean }>;
  rightMenuItems: Array<{ icon: string, active: boolean }>;
  state: any;
  placeholder = 'assets/img/avatar/girl-avatar.png';
  chosenPicture: any;
  Subpages: Array<{ title: string, component: any, active: boolean }>;
  userDetails: any;
  firstName: any;
  UserType: any;
  MdleName: any;
  lastName: any;
  Profileimg: any;
  alertcheck: boolean = false;
  MembershipText: any;
  is_featured: any;
  BuoyTble: any[] = [];
  public watch: any;
  isExitAlertOpen: boolean = false;
  storageDirectory: string = '';
  heading: string;
  autocompleteItems: Array<{ description: string, place_id: string }>;
  query: string;
  acService: any;
  locationObj: Location;
  showCancel: boolean;
  lat: number = 61.217381;
  lng: number = -149.863129;
  data: any = "Anchorage Alaska";

  location: Location;
  forecast: Forecast;
  metrics: Metrics;
  todayForecast: DataPoint;
  forecastSubscriber: Subscription;
  hourlyArray: Array<{
    time: number,
    icon: string,
    temperature: number
  }> = [];
  constructor(
    public modalCtrl: ModalController,
    public platform: Platform,
    public statusBar: StatusBar,
    public splashscreen: SplashScreen,
    public global: AppState,
    public menuCtrl: MenuController,
    public authService: AuthService,
    public storage: Storage,
    public alertService: AlertService,
    public userData: UserData,
    public alertCtrl: AlertController,
    public network: Network,
    public _ionicApp: IonicApp,
    public fcm: FCM,
    public geolocation: Geolocation,
    private nativeGeocoder: NativeGeocoder,
    public zone: NgZone,
    public forecastService: ForecastService,
    public databaseService: DatabaseService,
    public utilService: UtilService,
    private locationAccuracy: LocationAccuracy,
    private transfer: Transfer,
    private file: File,
    private googlePlus: GooglePlus,
    private fb: Facebook,
    private iap: InAppPurchase,
    private loadingCtrl: LoadingController
  ) {
    this.initializeApp();
    this.activePage.subscribe((selectedPage: any) => {
      this.pages.map(page => {
        page.active = page.title === selectedPage.title;
      });
    });
  }
  ionViewWillEnter() {
   
  }
  initializeApp() {
    this.platform.ready().then(() => {
      this.global.set('theme', 'theme-dark');
      this.statusBar.styleDefault();
      this.splashscreen.hide();
      this.menuCtrl.enable(false, 'right');
      this.NetworkCheck();
      this.initializeOptions();
      this.HardwareBackbutton();
      // this.GetData();
      this.is_featured = localStorage.getItem("is_featured");
      let img = localStorage.getItem("Profileimg");
      if (img == "undefined") {
        this.authService.Profileimg = "assets/img/icon/placeholder.png";
      } else {
        this.authService.Profileimg = localStorage.getItem("Profileimg");
      }
      //  this.locationAccuracyOn();
      //  this.authService.Profileimg=localStorage.getItem("Profileimg"); 
      this.authService.MdleName = localStorage.getItem("UserName");
      this.authToken = localStorage.getItem("token");
      this.authService.UserType = localStorage.getItem("UserType");


      this.authService.getsinguserpoints(null).subscribe((result: any) => {
        console.log("PONTS", result)
        this.authService.points = result.data[0].sum_of_points
      })
      try {
        //main try
        this.storage.get('auth').then(auth => {
          this.authToken = auth;
          if (this.authToken === '' || this.authToken === null || this.authToken === undefined) {
            this.menuCtrl.swipeEnable(false);
            this.rootPage = 'LoginListPage';
            // this.locationAccuracyOn();
          } else {
            this.InvalidToken();
            this.menuCtrl.swipeEnable(true);
            if (localStorage.getItem("UserType") == "guest") {
              this.nav.setRoot('marineWeatherPage');
              this.authService.Profileimg = localStorage.getItem("Profileimg");
              //this.BuoyTble = JSON.parse(localStorage.getItem('tbl_BuoyData'));
            } else {
              try {
               // alert('LOCAL STORAGE OTP')
                let original_otp = JSON.parse(localStorage.getItem("original_otp"));
                //let original_otp = localStorage.getItem("original_otp");
                if (original_otp != null) {
                  if (original_otp == true) {
                 //   alert("IN original_otp true")
                    this.authService.getProfileDetails();
                    this.authService.Profileimg = localStorage.getItem("Profileimg");
                    this.rootPage = 'dashboardPage';

                    // this.BuoyTble = JSON.parse(localStorage.getItem('tbl_BuoyData'));
                  } else {
                 //   alert("IN original_otp false")
                    try {
                 //     alert("Local storage subscription try block")

                      let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))
                      if (subscriptionDate) {
                        console.log("Locally subscription saved data", JSON.stringify(subscriptionDate));
                        if (subscriptionDate != null) {
                          try {
                            var todaysdate = new Date()
                            var expdate = new Date(subscriptionDate.expirationDate)
                            console.log("today's date", todaysdate);
                            console.log("expiry date", expdate);
                            if (todaysdate <= expdate) {
                         //     alert("valid subcription");

                              // this.authService.getProfileDetails();
                              this.authService.Profileimg = localStorage.getItem("Profileimg");
                              if (this.authService.Profileimg == undefined) {
                                this.authService.Profileimg = "assets/img/icon/placeholder.png";
                              }
                              this.nav.setRoot('dashboardPage');
                              //this.BuoyTble = JSON.parse(localStorage.getItem('tbl_BuoyData'));
                            }
                            else {
                        //      alert("invalid subcription");
                              this.menuCtrl.swipeEnable(false);
                              this.rootPage = 'LoginListPage';
                            }
                          } catch (er) {
                        //    alert("subscription data expiry date parse error")
                            this.menuCtrl.swipeEnable(false);
                            this.rootPage = 'LoginListPage';
                          }
                        } else {
                     //     alert("subscription data null ")
                          this.menuCtrl.swipeEnable(false);
                          this.rootPage = 'LoginListPage';
                        }
                      } else {
                       // alert("subscription data undefined ")

                        this.menuCtrl.swipeEnable(false);
                        this.rootPage = 'LoginListPage';
                      }
                    } catch (err) {
                   //   alert("LOCAL STORAGE subscription err " + err)
                      this.rootPage = 'LoginListPage';

                    }
                  }
                } else {
                 // alert("IN original_otp null")

                  this.menuCtrl.swipeEnable(false);
                  this.rootPage = 'LoginListPage';
                }
              } catch (err) {
              //  alert("LOCAL STORAGE OTP catch" + err)
                this.rootPage = 'LoginListPage';

              }
            }
          }
        });
      } catch (er) {
     //   alert("MAIN catch" + er)
        this.rootPage = 'LoginListPage';

      }
    });
  }

  // locationAccuracyOn() {
  //   this.startTracking();
  //   // this.locationAccuracy.canRequest().then((canRequest: any) => {

  //   //   if (canRequest) {
  //   //     // the accuracy option will be ignored by iOS
  //   //     this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
  //   //       (res) => console.log('Request successful', JSON.stringify(res)),
  //   //       (error) => console.log('Error requesting location permissions', JSON.stringify(error))
  //   //     );
  //   //   }
  //   // });
  // }

  NetworkCheck() {
    this.network.onDisconnect().subscribe(() => {
      if (this.alert) {
        this.alert.dismiss();
        this.alert = null;
      } else {
        this.authService.INTERNETCHECK = true;
        if (this.alertcheck == false) {
          var title = 'NO INTERNET';
          var msg = 'Please check your network connection.';
          this.showAlertNetwork(title, msg);
        }
      }
    });
    this.network.onConnect().subscribe(() => {
      this.authService.INTERNETCHECK = false;
      if (this.alert) {
        this.alert.dismiss();
        this.alert = null;
      }
    });
  }
  private getLatestPoint() {
    let loading = this.loadingCtrl.create({
      spinner: 'crescent',
      content: 'Getting points data. <br> Please wait...',
      duration: 3000
    });
    loading.present()
    let data = {
      "description": "In Subscription-dialog page",
    }
    this.authService.getsinguserpoints(null).subscribe((result: any) => {
      loading.dismiss()
      console.log("PONTS", result)
      this.authService.points = result.data[0].sum_of_points
    })
  }
  HardwareBackbutton() {
    this.platform.registerBackButtonAction(() => {
      // Close any active modals or overlays
      let activePortal = this._ionicApp._loadingPortal.getActive() ||
        this._ionicApp._modalPortal.getActive() ||
        this._ionicApp._toastPortal.getActive() ||
        this._ionicApp._overlayPortal.getActive();
      if (activePortal) {
        activePortal.dismiss();
      } else if (this.nav.canGoBack()) {
        this.nav.pop();
      } else if (this.menuCtrl.isOpen()) {
        this.menuCtrl.close();
      } else {
        if (this.isExitAlertOpen) return;
        this.isExitAlertOpen = true;
        this.showExitAlert();
      }
    });
  }

  showExitAlert() {
    this.alertCtrl.create({
      title: 'Exit',
      message: 'Are you sure you want to exit the app?',
      enableBackdropDismiss: false,
      buttons: [
        {
          text: 'Yes',
          handler: () => {
            this.isExitAlertOpen = false;
            this.platform.exitApp();
          }
        }, {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.isExitAlertOpen = false;
          }
        }
      ]
    }).present();
  }

  showAlertNetwork(title: any, msg: any) {
    this.alertcheck = true;
    let alert = this.alertCtrl.create({
      title: title,
      message: msg,
      buttons: [
        {
          text: 'Ok',
          role: 'Ok',
          handler: () => {
            this.alertcheck = false;
            this.alert = null
          }
        }
      ]
    });
    alert.present();
    return true;
  }


  showAlert(title: any, msg: any) {
    this.alertcheck = true;
    let alert = this.alertCtrl.create({
      title: title,
      message: msg,
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: () => {
            this.alertcheck = false;
            this.alert = null
          }
        },
        {
          text: 'Exit',
          handler: () => {
            this.alertcheck = false;
            this.platform.exitApp();
          }
        }
      ]
    });

    alert.present();
    return true;
  }

  private initializeOptions(): void {
    this.options = new Array<SideMenuOption>();
    this.options.push({
      displayText: 'Dashboard',
      component: 'dashboardPage',
      // This option is already selected
      selected: true
    });
    this.options.push({
      displayText: 'Events/Entertainment',
      component: 'HomePage'

      // This option is already selected
      //  selected: true
    });
    this.options.push({
      displayText: 'The Fishing Exchange',
      component: 'FishingExchangPage'
    });
    this.options.push({
      displayText: 'Local Weather',
      component: 'HomeWeatherPage'
    });

    this.options.push({
      displayText: 'Marine Weather',
      component: 'marineWeatherPage'
    });

    this.options.push({
      displayText: 'Live Buoys',
      component: 'buoyWeatherPage'
    });
    this.options.push({
      displayText: 'What’s Hot?',
      component: 'WHEventPage'
    });

    this.options.push({
      displayText: 'Tides',
      component: 'TidesPage'
    });
    this.options.push({
      displayText: 'Currents',
      component: 'currentsPage'
    });
    this.options.push({
      displayText: 'River Flows',
      component: 'USGSFlowsPage'
    });
    this.options.push({
      displayText: 'Fish Counts',
      component: 'FishCountsPage'
    });
    this.options.push({
      displayText: 'Today\'s Catch - Photos',
      component: 'ShoutOutMainPage'
    });
    this.options.push({
      displayText: 'News & Fishing Reports',
      component: 'FishingNewsPage'
    });

    this.options.push({
      displayText: 'Regulations',
      component: 'regulationsList'
    });
    this.options.push({
      displayText: 'Commercial Fishing info',
      component: 'CommercialFishingInfoPage'
    });
    // this.options.push({
    //   displayText: 'Fishing License',
    //   component: 'FishingLicenformPage'
    // });
  }

  msg: any;
  public onOptionSelected(option: SideMenuOption): void {
    this.InvalidToken();
    this.authService.getNotificationsCount();
    // console.log('option', option);
    this.menuCtrl.close().then(() => {
      if (option.custom && option.custom.isLogin) {
        this.presentAlert('You\'ve clicked the login option!');
      } else if (option.custom && option.custom.isLogout) {
        this.presentAlert('You\'ve clicked the logout option!');
      } else if (option.custom && option.custom.isExternalLink) {
        let url = option.custom.externalUrl;
        window.open(url, '_blank');
      } else {
        // Get the params if any
        if (localStorage.getItem("UserType") == "guest") {
          if (option.displayText == 'Marine Weather') {
            this.nav.setRoot('marineWeatherPage', { 'title': option.displayText });
          } else if (option.displayText == 'Regulations') {
            this.nav.setRoot('regulationsList', { 'title': option.displayText });
          } else {
            this.nav.push('GuestUserErrorPage');
          }
        } else {
          // this.authService.getMap(option.displayText);
          this.nav.setRoot(option.component || 'dashboardPage', { 'title': option.displayText });
        }
      }
    });
  }

  InvalidToken() {
    this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
      return res;
    }, (err) => {
      var json = JSON.parse(err.text());
      if (json.success === false && json.error.status === 401 ||
        json.error.status === 500 || json.message === "Access token is missing" ||
        json.message === "Invalid access token" || localStorage.getItem("token") === undefined) {
        localStorage.removeItem('token');
        localStorage.removeItem('UserType');
        localStorage.removeItem('UserName');
        localStorage.removeItem('Profileimg');
        localStorage.removeItem('userId');
        localStorage.removeItem('is_featured');
        localStorage.removeItem('fcmToken');
        this.userData.removeAuthToken();
        this.nav.setRoot('LoginListPage');
      }
      return err;
    });

  }

  public collapseMenuOptions(): void {
    this.sideMenu.collapseAllOptions();
  }
  public presentAlert(message: string): void {
  }

  openPage(page) {
    // Reset the content nav to have just this page
    // we wouldn't want the back button to show in this scenario
    this.nav.setRoot(page.component);
    this.activePage.next(page);
  }

  rightMenuClick(item) {
    this.rightMenuItems.map(menuItem => menuItem.active = false);
    item.active = true;
  }

  LogOut() {
    this.alertService.presentAlertWithCallback('Log Out of FishTopia?',
      'Are you sure you want to log out.').then((yes) => {
        if (yes) {
          // console.log('Log Out');
          localStorage.removeItem('UserType');
          localStorage.removeItem('UserName');
          localStorage.removeItem('Profileimg');
          localStorage.removeItem('userId');
          localStorage.removeItem('is_featured');
          localStorage.removeItem('fcmToken');
          localStorage.setItem("old_token", localStorage.getItem("token"));
          localStorage.removeItem('token');
          this.userData.removeAuthToken();
          this.nav.setRoot('LoginListPage');
          this.authService.logout(localStorage.getItem("token")).subscribe(
            res => this.logoutSuccessfully(res),
            error => error
          )
        }
      });
  }
  Notification() {
    this.nav.push('NotificationsPage');
  }
  logoutSuccessfully(res) {
    this.options = [];
    localStorage.removeItem('token');
    this.userData.removeAuthToken();
    localStorage.removeItem('old_token');
    this.googlePlus.logout().then(res_ => {
      // console.log(JSON.stringify(res_));
    });
    this.fb.logout().then(res_ => {
      // alert(JSON.stringify(res_));
      // console.log(JSON.stringify(res_));
    });
    this.initializeOptions();
  }

  Settings() {
    this.authService.getNotificationsCount();
    this.nav.push('ProfileSettings');
  }
  Subcribe() {
    this.authService.getNotificationsCount();
    if (this.authService.UserType === "guide") {
      if (this.authService.is_featured === 'yes') {
        this.nav.push('BusinessformPage');
      } else {
        this.nav.push('Guide_DirectPage');
      }
    } else if (this.authService.is_featured === 'yes') {
      this.nav.push('PrimeOfferPage');
    } else {
      this.nav.push('Guide_DirectPage');
    }
  }


  // public startTracking() {
  //   let options = {
  //     frequency: 1000,
  //     enableHighAccuracy: true
  //   };
  //   this.geolocation.getCurrentPosition(options).then((resp) => {
  //     this.GeoLocationNameTrack(resp.coords.latitude, resp.coords.longitude);
  //   }).catch((error) => {
  //     console.log('Error getting location', error);
  //   });
  //   this.geolocation.watchPosition(options).subscribe(position => {
  //     if ((position as Geoposition).coords != undefined) {
  //       var geoposition = (position as Geoposition);
  //       this.Trackuser(geoposition.coords.latitude, geoposition.coords.longitude, geoposition.coords.speed);
  //       // console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude + 'Speed' + geoposition.coords.speed);
  //     }
  //   });
  // }


  // Trackuser(lat: any, lng: any, speed: any) {
  //   var data = {
  //     latitude: lat,
  //     longitude: lng,
  //     speed: speed
  //   }
  //   this.authService.GeoTracklat = lat;
  //   this.authService.GeoTracklng = lng;
  //   this.authService.getUserLocation(data).subscribe(res => {
  //     // console.log('track', res);
  //   })
  // }


  // GeoLocationNameTrack(lat: any, lng: any) {
  //   localStorage.removeItem('GeoLat');
  //   localStorage.removeItem('GeoLong');
  //   localStorage.removeItem('SubGeoLocation');
  //   localStorage.removeItem('GeoLocation');
  //   // /61.071544, -148.981580
  //   this.nativeGeocoder.reverseGeocode(lat, lng)
  //     .then((result: NativeGeocoderReverseResult[]) => {
  //       let nameLoc = result[0].subAdministrativeArea;
  //       let locationData = result[0].administrativeArea;
  //       localStorage.setItem('GeoLat', JSON.stringify(lat));
  //       localStorage.setItem('GeoLong', JSON.stringify(lng));
  //       localStorage.setItem('SubGeoLocation', nameLoc);
  //       localStorage.setItem('GeoLocation', locationData);
  //     })
  //     .catch((error: any) => console.log(error));
  // }

  guestLogout() {
    this.menuCtrl.close();
    this.authService.UserType = null;
    localStorage.removeItem('UserType');
    localStorage.removeItem('UserName');
    localStorage.removeItem('Profileimg');
    localStorage.removeItem('userId');
    localStorage.removeItem('is_featured');
    localStorage.removeItem('fcmToken');
    localStorage.setItem("old_token", localStorage.getItem("token"));
    localStorage.removeItem('token');
    this.userData.removeAuthToken();
    this.nav.setRoot('LoginListPage');
    this.authService.logout(localStorage.getItem("token")).subscribe(
      res => this.logoutSuccessfully(res),
      error => error
    )
  }

  // GetData() {
  //   let self = this;
  //   var data = {
  //     "lat": this.lat,
  //     "lng": this.lng,
  //     "name": this.data
  //   }
  //   if (data === null) {
  //     let modal = self.modalCtrl.create('LocationPage', { heading: 'Enter Home City', showCancel: false });
  //     modal.onDidDismiss((data: Location) => {
  //       // console.debug('page > modal dismissed > data > ', data);
  //       if (data) {
  //         self.databaseService.setJson(CONFIG.HOME_LOCATION, data);
  //         // console.log(CONFIG.HOME_LOCATION, data);
  //         self.location = data;
  //         //self.init(self.location);
  //         //self.emitInit();
  //       }
  //     });
  //     modal.present();
  //   } else {
  //     self.location = data;
  //    // self.init(self.location);
  //   }

  // }

  // init(data) {
  //   let self = this;
  //   if (data) {
  //     self.getForecast(self.location);
  //   }
  //   this.databaseService.getJson(CONFIG.METRICS).then(data => {
  //     if (data === null) {
  //       self.databaseService.setJson(CONFIG.METRICS, DEFAULT_METRICS);
  //       self.metrics = DEFAULT_METRICS;
  //     } else {
  //       self.metrics = data;
  //     }
  //   });
  // }



  getForecast(location: Location) {
    let self = this;
    self.forecastSubscriber = self.forecastService.getForecast(location)
      .subscribe((data: Forecast) => {
        self.forecast = data;
        if (self.forecast && self.forecast.daily && self.forecast.daily.data) {
          self.todayForecast = self.forecast.daily.data[0];
        }
        self.hourlyArray = [];
        let currentHour = self.utilService.getCurrentHour(self.forecast.timezone);
        let flag = false;
        _.forEach(self.forecast.hourly.data, (obj: DataPoint) => {
          if (!flag && self.utilService.epochToHour(obj.time, self.forecast.timezone) < currentHour) {
            return;
          }
          flag = true;
          self.hourlyArray.push({
            time: obj.time,
            icon: obj.icon,
            temperature: obj.temperature
          });
          if (self.hourlyArray.length > 10) {
            return false;
          }
        });
      }, err => {
        console.error(err);
      });
  }

  invitReferral() {
    this.nav.push('Invite_referralPage');
  }
}
