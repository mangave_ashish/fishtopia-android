import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { marineWeatherPage } from './marineWeather';
import { AgmCoreModule ,LatLngLiteral} from '@agm/core';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    marineWeatherPage,
  ],
  imports: [
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en   
      apiKey: 'AIzaSyD0xzjjJldzIvAEokvMJ4EKJaxOoJqq57k'
    }),
    IonicPageModule.forChild(marineWeatherPage),
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class marineWeatherPageModule { }
