import { Component, NgZone } from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, LoadingController, NavController, Platform } from 'ionic-angular';
import * as _ from 'lodash';
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-provider';
import { DatabaseService } from '../../providers/weather';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

@SideMenuDisplayText('Marine Weather')

@IonicPage({
  priority: 'high'
})

@Component({
  selector: 'page-Marine-Weather',
  templateUrl: 'marineWeather.html',
})

export class marineWeatherPage {
  title: string;
  iconUrl: any;
  lat: number = 61.217381;;
  lng: number = -149.863129;
  latlong: any[] = [];
  markers: any[] = [];
  infoWindowOpened = null;
  map: any;
  regionals: any = [];
  currentregional: any;
  selectedDay: any;
  selectedMonth: any;
  tmwDate: any;
  YearMonth: any;
  SelectDate: any;
  mainEventList: any;
  dateList: Date[] = [];
  eventListData: any[] = [];
  ChartdateList: Date[] = [];
  GeoLat: any;
  GeoLong: any;
  pinsDataList: any;
  currentDateData: any[] = [];
  MapEnabled: boolean = false;
  data_DB: any[] = [];
  dataDB: any[] = [];
  TideStationlist: any[] = [];
  stationId: any;
  DataSet: any[] = [];
  Currents_Pin: any[] = [];
  EndDate: any;
  StartDate: any;
  PrevPage: any;
  ploypath: any[] = [];
  marinePins: any[] = [];
  pinCnvrt: any[] = [];

  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public authService: AuthService,
    public platform: Platform,
    public databaseService: DatabaseService,
    public zone: NgZone,
    public http: Http
  ) {
    var v = new Date();
    this.StartDate = v;
    this.selectedDay = v.getDate();
    this.selectedMonth = v.getMonth();
    this.YearMonth = v.getFullYear();
    this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
    this.getGeolocationPosition();
    this.ploypath = [];
    this.http.get('assets/data/akmarine_2.json').subscribe(data => {
      var json = JSON.parse(data.text());
      this.marinePins = json.features;
      this.marinePins.forEach(element => {
        this.Currents_Pin = [];
        element.geometry.coordinates.forEach(item => {
          this.pinCnvrt = [];
          item.forEach(pin => {
            this.pinCnvrt.push({ lng: pin[0], lat: pin[1] });
          });
          this.Currents_Pin.push(this.pinCnvrt);
        });
        this.ploypath.push({ name: element.properties.ZONE, point: this.Currents_Pin })
      });
    });
    this.authService.pinsDataList.forEach(element => {
      element.Label = element.id;
    });
    console.log(this.authService.pinsDataList)
    // let loading = this.loadingCtrl.create({
    //   spinner: 'hide',
    //   content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
    //   duration: 3500
    // });
    // loading.present();
  }

  markerDragEnd(event: any) {
  }

  zoomChange(event: any) {

    if (event >= 5) {
      this.authService.pinsDataList.forEach(element => {
        element.Label = element.name;
      });
    } else {
      this.authService.pinsDataList.forEach(element => {
        element.Label = '';
      });
    }
  }

  polyClicked(i: any, data: any) {
    var marker = {
      id: data.name
    }
    this.navCtrl.push('MarineWeatherinfoPage', { MarineWeatherDetails: marker });
    console.log('data', data);
  }

  AreaName: any;
  polyMouseOver(i: any, data: any) {
    this.AreaName = data.name;
    console.log('data', data);
  }


  ionViewWillEnter() {
    this.PrevPage = this.authService.title;
  }

  ionViewWillLeave() {
    this.EndDate = new Date();
    this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
    this.authService.audit_Page(this.PrevPage);
  }

  getGeolocationPosition() {
    let self = this;
    self.GeoLat = localStorage.getItem('GeoLat');
    self.GeoLong = localStorage.getItem('GeoLong');
    let locationData = localStorage.getItem('GeoLocation');
    let Sub_Location = localStorage.getItem('SubGeoLocation');
    if (locationData === "Alaska") {
      self.lat = JSON.parse(self.GeoLat);
      self.lng = JSON.parse(self.GeoLong);
      self.getMarine();
    } else {
      self.lat = 61.217381;
      self.lng = -149.863129;
      self.getMarine();
    }
  }
  MarineData: any[] = [];
  getMarine() {
    // debugger
    let self = this;
    this.MarineData = [];
    self.databaseService.getMarinePins('Marine_DB')
      .then(data => {
        if (data != undefined) {
          let Data = JSON.parse(data.data);
          Data.forEach(element => {
            if (element.name != '')
              this.MarineData.push(element);
          });
          if (this.MarineData.length < 0 || _.isEmpty(data.data)) {
            self.authService.getMarinelistinfo();
          } else {
            self.authService.getMarineLoclistinfo(this.MarineData);
          }
        } else {
          self.authService.getMarinelistinfo();
        }
      })
  }


  getKnots(mphvalu: any) {
    if(mphvalu!='Not Available'){
    let test = (parseInt(mphvalu, 10) / 1.151).toFixed(2)
    return "/ "+test + "kts"
    }else return ""
  }
  MarkerDetails(marker: any, infoWindow, index: number) {
    if (marker.marine_weather_location != undefined && marker.marine_weather_location != null) {
      if (this.infoWindowOpened === infoWindow)
        return;

      if (this.infoWindowOpened !== null) {
        this.infoWindowOpened.close();

      }
      this.infoWindowOpened = infoWindow;
    } else {
      this.navCtrl.push('MarineWeatherinfoPage', { MarineWeatherDetails: marker });
    }
  }
}



