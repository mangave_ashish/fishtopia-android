import { Component } from '@angular/core';
import Highcharts from 'highcharts/highstock';
import { IonicPage, Loading, LoadingController, NavController, NavParams } from 'ionic-angular';
import * as _ from 'lodash';
import moment from 'moment';
import { AuthService } from '../../providers/auth-provider';

// import { flatten } from '@angular/compiler';
/**
 * Generated class for the TidesDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-FishCountChart',
  templateUrl: 'FishCountChart.html',
})
export class FishCountChartPage {
  WsRecords: any[];
  WsChartData: any[];
  loading: Loading;
  area: any;
  randomColor = ['#000000', '#0000FF', '#FF00FF', '#D2691E', '#008000', '#8d44ad'];
  activeCheckboxes: number;
  year = [];
  showCum: boolean = false;
  sum: number = 0;
  AvgSum: number = 0;
  StartDate: any;
  EndDate: any;
  constructor(
    public navCtrl: NavController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public authService: AuthService
  ) {
    this.activeCheckboxes = 2;
    this.area = navParams.get('area');
    console.log('area', this.area);
    var v = new Date();
    this.StartDate = v;
    this.year.push({ label: 'Last 3 Year Average', color: "#ff0000", checked: true });
    let j = 0;
    for (let i = new Date().getFullYear(); i >= new Date().getFullYear() - 4; i--) {
      const chartColor = this.randomColor[(j++) % 5];
      this.year.push({ label: i, color: chartColor, checked: i == v.getFullYear() ? true : false });
    }
    this.loadFishingCountData();

  }

  ionViewWillLeave() {
    this.EndDate = new Date();
    this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
    //console.log('this.authService.TimeSpan', this.authService.TimeSpan);
  }


  loadFishingCountData() {
    const self = this;
    self.authService.getfishDataByYear(self.area._id).subscribe(res => {
      console.log("Fishing data ", res.data);

      self.WsRecords = res.data;
      self.WsRecords.forEach(function (item, index) {
        const date = new Date();
        date.setMonth((item.month - 1), item.day);
        item['timestamp'] = Date.parse(date.toDateString());
        self.WsRecords[index] = item;
      });
      self.WsRecords = _.sortBy(self.WsRecords, 'timestamp');
      self.prepareWsCharData();
    });
  }

  prepareWsCharData() {
    const self = this;
    console.log('self.showCum', self.showCum);
    if (self.showCum) {
      self.Cum_prepareWsCharData();
    } else {
      this.loading = this.loadingCtrl.create({
        spinner: 'hide',
        content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
        duration: 3500
      });
      this.loading.present();
      self.activeCheckboxes = 0;
      self.WsChartData = [];
      self.year.forEach(function (y) {
        if (y.checked == true) {
          self.activeCheckboxes++;
        }
        if (y.label == 'Last 3 Year Average' && y.checked == true) {
          let avgData = [];
          self.WsRecords.forEach(function (item) {
            const date = new Date();
            date.setMonth((item.month - 1), item.day);
            let avgRoundup = Math.round(item.last_3_year_avg * 100) / 100;
            avgData.push([Date.parse(date.toDateString()), avgRoundup]);
          });
          self.WsChartData.push({ name: 'Average Count', color: "#ff0000", data: avgData, visible: true });
        } else if (y.label != 'Last 3 Year Average' && y.checked == true) {
          let newData = [];
          self.WsRecords.forEach(function (item) {
            const date = new Date();
            date.setMonth((item.month - 1), item.day);
            let key = y.label;
            if (item[key] !== undefined && item[key] !== 0) {
              newData.push([Date.parse(date.toDateString()), item[key] * 1]);
            } else {
             // newData.push([Date.parse(date.toDateString()), JSON.parse('0')]);
               newData.push([Date.parse(date.toDateString()), null]);

            }
          });
          console.log("GETSET", y.label.toString())
          self.WsChartData.push({ name: y.label.toString(), color: y.color, data: newData, visible: true });
        }
      });
      self.plotChart();
    }
  }

  toggleButton(data: any) {
    this.showCum = !this.showCum;
    this.prepareWsCharData();
  }

  Min: number = 0;
  Max: number = 0;
  Cum_prepareWsCharData() {
    const self = this;
    console.log('self.showCum', self.showCum);
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    this.loading.present();
    self.activeCheckboxes = 0;
    self.WsChartData = [];
    var early_run_start_date = moment(self.area.early_run_start_date).format('MM-DD-YYYY');
    var early_run_end_date = moment(self.area.early_run_end_date).subtract(1, "days").format('MM-DD-YYYY');
    var late_run_start_date = moment(self.area.late_run_start_date).format('MM-DD-YYYY');
    var late_run_end_date = moment(self.area.late_run_end_date).format('MM-DD-YYYY');
    self.year.forEach(function (y) {
      if (y.checked == true) {
        self.activeCheckboxes++;
      }
      if (y.label == 'Last 3 Year Average' && y.checked == true) {
        let avgData = [];
        self.AvgSum = 0;
        self.WsRecords.forEach(function (item) {
          const date = new Date();
          date.setMonth((item.month - 1), item.day);
          self.AvgSum = self.AvgSum + item.last_3_year_avg;
          let avgRoundup = Math.round(self.AvgSum * 100) / 100;
          let date_1 = moment(date).format('MM-DD-YYYY');
          if (date_1 >= early_run_end_date && date_1 <= late_run_start_date) {
            if (date_1 == early_run_end_date) {
              self.AvgSum = 0;
              avgData.push([Date.parse(date.toDateString()), null]);
              avgData.push([Date.parse(date.toDateString()), null]);
            } else {
              self.AvgSum = 0;
              avgData.push([Date.parse(date.toDateString()), null]);
              avgData.push([Date.parse(date.toDateString()), null]);
            }
          } else {
            avgData.push([Date.parse(date.toDateString()), avgRoundup]);
          }
        });
        self.WsChartData.push({ name: 'Average Count', color: "#ff0000", data: avgData, visible: true });
      } else if (y.label != 'Last 3 Year Average' && y.checked == true) {
        let newData = [];
        self.sum = 0;
        self.WsRecords.forEach(function (item) {
          const date = new Date();
          date.setMonth((item.month - 1), item.day);
          let key = y.label;
          if (item[key] !== undefined && item[key] !== 0) {
            let date_1 = moment(date).format('MM-DD-YYYY');
            self.sum = self.sum + item[key] * 1;
            if (date_1 >= early_run_end_date && date_1 <= late_run_start_date) {
              if (date_1 == early_run_end_date) {
                self.sum = 0
                newData.push([Date.parse(date.toDateString()), null]);
                newData.push([Date.parse(date.toDateString()), null]);
              } else {
                self.sum = 0
                newData.push([Date.parse(date.toDateString()), null]);
                newData.push([Date.parse(date.toDateString()), null]);
              }
            } else {
              newData.push([Date.parse(date.toDateString()), self.sum]);
            }
          } else {
          //  newData.push([Date.parse(date.toDateString()), self.sum]);
          newData.push([Date.parse(date.toDateString()),null]);

          }
        });
        console.log("SETGET", y.label.toString())

        self.WsChartData.push({ name: y.label.toString(), color: y.color, data: newData, visible: true });
      }
      if (self.area.minimum_escapement_number !== null && self.area.minimum_escapement_number !== undefined && y.label != "Minimum Escapement Goal") {
        // && self.area.minimum_escapement_number != 0 
        let minData = [];
        let maxData = [];
        let earlyData = [];
        let lateData = [];
        // self.area.early_run_minimum_number = 0;
        // self.area.late_run_maximum_number = 0;
        self.WsRecords.forEach(function (item) {
          let key = y.label;
          const date = new Date();
          date.setMonth((item.month - 1), item.day);
          var date_1 = moment(date).format('MM-DD-YYYY');
          if (y.label == "Last 3 Year Average" && item.name != 'Minimum Escapement Goal') {
            if (date_1 >= early_run_start_date && early_run_start_date <= early_run_end_date && date_1 <= early_run_end_date || early_run_start_date == "Invalid date" && date_1 <= early_run_end_date || early_run_end_date == "Invalid date" && date_1 >= early_run_start_date) {
              minData.push([Date.parse(date.toDateString()), JSON.parse(self.area.minimum_escapement_number)]);
              maxData.push([Date.parse(date.toDateString()), JSON.parse(self.area.maximum_escapement_number)]);
            } else if (late_run_start_date <= date_1 && late_run_start_date <= late_run_end_date && date_1 <= late_run_end_date || late_run_start_date == "Invalid date" && late_run_end_date <= date_1 || late_run_end_date == "Invalid date" && late_run_start_date <= date_1) {
              minData.push([Date.parse(date.toDateString()), JSON.parse(self.area.late_run_minimum_escapement_number)]);
              maxData.push([Date.parse(date.toDateString()), JSON.parse(self.area.late_run_maximum_escapement_number)]);
            } else {
              minData.push([Date.parse(date.toDateString()), null]);
              // JSON.parse('0')]);
              maxData.push([Date.parse(date.toDateString()), null]);
              //JSON.parse('0')]);
            }
          }
        });
        self.WsChartData.push({ name: 'Minimum Escapement Goal', color: "#FF1454", data: minData, visible: true });
        self.WsChartData.push({ name: 'Maximum Escapement Goal', color: "#FC9500", data: maxData, visible: true });
      }
    });
    self.plotChart();
  }



  handleMinMax(data: any) {
    let self = this;
    console.log(data);
    if (self.Min === 0) {
      self.Min = data;
    } else if (self.Min > data) {
      self.Min = data;
    }

    if (self.Max < data) {
      self.Max = data;
    }
    // console.log(self.Min, self.Max);

  }


  plotChart() {
    console.log("plotChart", this.WsChartData);
    Highcharts.setOptions({
      lang: {
        decimalPoint: '.',
        thousandsSep: ','
      }
    });
    Highcharts.stockChart('container', {
      chart: {
        type: 'line',
        zoomType: 'x'
      },
      //  title: {
      //               text: 'Fish count over years'
      //             },
      tooltip: {
        xDateFormat: '%A, %b %e, %Y',
        pointFormat: '<span style="color:{point.color}">{series.name}: <b>{point.y:,.f}</b></span>'
      },

      xAxis: {
        type: 'datetime',
        // dateTimeLabelFormats: { // don't display the dummy year
        //   month: '%e. %b',
        //   year: '%b'
        // },
      },
      yAxis: {
        title: {
          text: 'Fish Count'
        }
      },
      //  legend: {
      //       enabled: true,
      //       layout: 'vertical'
      //   },
      rangeSelector: false,
      series: this.WsChartData
    });
    this.loaderDismiss();
  }

  Notification() {
    this.navCtrl.push('NotificationsPage');
  }


  loaderDismiss() {
    this.loading.dismiss();
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      duration: 3500,
    });
    this.loading.present();
  }


}