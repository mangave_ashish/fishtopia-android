import { Component } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AuthService, Event } from '../../providers/auth-provider';

@IonicPage()
@Component({
    selector: 'page-PrimeOffer',
    templateUrl: 'PrimeOffer.html'
})
export class PrimeOfferPage {
    drawerOptions: any;
    eventList: Event[] = [];
    TmweventList: Event[] = [];
    eventSource;
    viewTitle;
    monthNames: any = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    isToday: boolean;
    lockSwipeToPrev: boolean;
    month: string;
    mainEventList: Event[] = [];
    mainEventFillterList: any[] = [];
    EventType: any[] = [];
    dateList: Date[] = [];
    IPushMessage: any[];
    showPageView: boolean = false;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
    filterobject: any[] = [];
    public filteredItems: any;
    public issearch = false;
    public i: any;
    public keywords: any[];
    inputName: any = '';
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;
    FXdata: any;
    EndDate: any;
    StartDate: any;
    public product: any = {
        name: 'Alaska FishTopia',
        appleProductId: 'AKFISHTOPIA001',
        googleProductId: 'akfishtopiayearlysubscription'
    };
    constructor(public navParams: NavParams,
        public viewCtrl: ViewController,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public menuCtrl: MenuController,
        // private store: InAppPurchase2,
        public Platform: Platform,
        public authService: AuthService) {
        this.FXdata = navParams.get('FishingExchnage');
        this.menuCtrl.swipeEnable(false);
        this.page = 1;
        var v = new Date();
        this.StartDate = v;
        this.selectedDay = ("0" + (v.getDate())).slice(-2);
        this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
        this.YearMonth = v.getFullYear();
        this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
        this.getEventDetails(this.page);
        //this.selectedDay = v.getDate();       
        console.log('this.SelectDate', this.SelectDate);
    }

    ionViewWillLeave() {
        this.EndDate = new Date();
        this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
        this.authService.audit_Page('PrimeOffer');
        this.menuCtrl.swipeEnable(true);
    }
    Notification(){
        this.navCtrl.push("dashboardPage")
      }

    change() {
        this.issearch = true;
    }
    change1() {
        this.issearch = false;
    }

    cancelSearch() {
        this.issearch = false;
    }
    ngOnInit() {
        //called after the constructor and called  after the first ngOnChanges() 
        // this.menuCtrl.enable(true, 'menu-right');    
    }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        setTimeout(() => {
            this.page = 1;
            this.mainEventFillterList = [];
            this.dateList = [];
            this.getEventDetails(this.page);
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }

    doInfinite(infiniteScroll) {
        if (this.page < this.totalPage) {
            console.log('Begin async operation', infiniteScroll);
            setTimeout(() => {
                this.page++;
                this.getEventDetails(this.page);
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 1000);
        }
        else {
            infiniteScroll.enabled(false);
        }
    }

    getEventDetails(Page) {
        //this.dateList=[];   
        var data = {
            page: Page,
            sort: "asc",
        }
        this.authService.getCouponlistData(data).subscribe(res => {
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;
            for (let d of res) {
                try {
                    if (this.SelectDate <= d.coupon_expiry_date) {
                        this.dateList.push(d.coupon_expiry_date);
                    }
                } catch (e) { }
            }
            var seriesValues = this.dateList;
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
            console.log(seriesValues);
            this.dateList = seriesValues;
        //    console.log('dateList', this.dateList);
            this.dateList.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });
            //  this.mainEventList = res;
            console.log(this.mainEventList);
            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.mainEventList = this.mainEventFillterList;
           // console.log('mainlist', this.mainEventFillterList)
          //  console.log('mainEventList', this.mainEventList);
            if (this.mainEventList.length > 0) {
                //this.currentDay = date[0];
            }
            //   this.filterEvent();
            // this.NextDayfilterEvent();
            //this.eventList = res;
            // console.log('getEventByDate-->', res);

        });


        // }
    }




    filterEvent() {
        try {
            if (this.mainEventList.length > 0) {

                // let dt: Date = new Date();
                var d = new Date();
                let today: Date = new Date(d);
                for (let mel of this.mainEventList) {
                    if (today.toDateString() == new Date(mel.event_date).toDateString())
                        this.eventList.push(mel);
                }
                console.log(this.eventList);

            }
        }
        catch (e) {
            console.log(e);
        }
    }

    // Notification() {
    //     this.navCtrl.push('NotificationsPage');
    // }


    openEventDetail(item) {
        this.navCtrl.push('PrimeOfferDetailPage', { event: item });
    }

    dismiss() {
        //this.navCtrl.setRoot('HomePage');
        this.viewCtrl.dismiss();
    }

    ionViewDidLeave() {
        this.menuCtrl.swipeEnable(true);
    }


    // async configureRestore() {
    //     // Only works with an emulator or real device
    //     if (!this.Platform.is('cordova')) { return; }
    //     let productId;
    //     try {
    //         if (this.Platform.is('ios')) {
    //             productId = this.product.appleProductId;
    //         } else if (this.Platform.is('android')) {
    //             productId = this.product.googleProductId;
    //         }

    //         // Register Product
    //         // Set Debug High
    //         this.store.verbosity = this.store.DEBUG;
    //         // Register the product with the store
    //         this.store.register({
    //             id: productId,
    //             alias: productId,
    //             type: this.store.NON_CONSUMABLE
    //         });

    //         this.registerHandlers(productId);

    //         // Errors On The Specific Product
    //         this.store.when(productId).error((error) => {
    //             alert('An Error Occured' + JSON.stringify(error));
    //         });
    //         // Refresh Always
    //         console.log('Refresh Store');
    //         this.store.refresh();
    //     } catch (err) {
    //         console.log('Error On Store Issues' + JSON.stringify(err));
    //     }

    // }

    // registerHandlers(productId) {
    //     // Handlers
    //     //
    //     this.store.when(productId).owned((product: IAPProduct) => {
    //         //Place code to activate what happens when your user already owns the product here
    //         product.finish();
    //     });

    //     this.store.when(productId).registered((product: IAPProduct) => {
    //         console.log('Registered: ' + JSON.stringify(product));
    //     });

    //     this.store.when(productId).updated((product: IAPProduct) => {
    //         console.log('Loaded' + JSON.stringify(product));
    //     });

    //     this.store.when(productId).cancelled((product) => {
    //         alert('Purchase was Cancelled');
    //     });

    //     // Overall Store Error
    //     this.store.error((err) => {
    //         alert('Store Error ' + JSON.stringify(err));
    //     });
    // }

    // async restorepurchase() {
    //     if (!this.Platform.is('cordova')) { return };
    //     this.configureRestore();

    //     let productId;

    //     if (this.Platform.is('ios')) {
    //         productId = this.product.appleProductId;
    //     } else if (this.Platform.is('android')) {
    //         productId = this.product.googleProductId;
    //     }

    //     console.log('Products: ' + JSON.stringify(this.store.products));
    //     console.log('Refreshing Store: ' + productId);
    //     try {
    //         let product = await this.store.get(productId);
    //         console.log('Product Info: ' + JSON.stringify(product));
    //         this.store.refresh();
    //         console.log('Finished Restore');
    //     } catch (err) {
    //         console.log('Error Ordering ' + JSON.stringify(err));
    //     }
    // }

}
