import { PrimeOfferPage } from './PrimeOffer';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module';


@NgModule({
  declarations: [
    PrimeOfferPage,
  ],
  imports: [
    IonicPageModule.forChild(PrimeOfferPage),
    SharedModule
  ],
  exports: [
    PrimeOfferPage,
    
  ]
})

export class HomePageModule { }
