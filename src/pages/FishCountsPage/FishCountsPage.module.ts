import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishCountsPage } from './FishCountsPage';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    FishCountsPage,
  ],
  imports: [
    IonicPageModule.forChild(FishCountsPage),
    SharedModule
  ],
})
export class FishCountsPageModule {}
