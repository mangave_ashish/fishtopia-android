import { Component } from '@angular/core';
import { IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import * as _ from 'lodash';
import { AuthService } from '../../providers/auth-provider';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

/**
 * Generated class for the TidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-FishCountsPage',
	templateUrl: 'FishCountsPage.html',
})
@SideMenuDisplayText('Fish Counts')
export class FishCountsPage {
	FishCountLocList: any[] = [];
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	FavLocationList: any[] = [];
	FishCountOption: any;
	tabType: any;
	status: boolean;
	GeoLat: any;
	GeoLong: any;
	lat: number = 61.217381;
	lng: number = -149.863129;
	image_url = "assets/img/salamanfish.jpg";
	StartDate: any;
	EndDate: any;
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public authService: AuthService,
		public loadingCtrl: LoadingController
	) {
		this.tabType = "FavLocation";
		this.getGeolocationPosition();
		this.StartDate = new Date();
	}




	ionViewWillLeave() {
		this.EndDate = new Date();
		this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		this.authService.audit_Page('Fish Counts');
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}



	getGeolocationPosition() {
		let self = this;
		self.GeoLat = localStorage.getItem('GeoLat');
		self.GeoLong = localStorage.getItem('GeoLong');
		let locationData = localStorage.getItem('GeoLocation');
		let Sub_Location = localStorage.getItem('SubGeoLocation');
		console.log('Geolat');
		if (locationData === "Alaska") {
			self.lat = self.GeoLat;
			self.lng = self.GeoLat;
			// self.zoom = 8;
			self.getfishingcountFavList(1);
		} else {
			self.lat = 61.217381;
			self.lng = -149.863129;
			self.getfishingcountFavList(1);
		}
	}
	arrayLength: number;
	Output: any[] = [];
	getfishingcountlocationlist(page_no: any) {

		let self = this;
		let loading = self.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 2500
		});
		loading.present();
		var token = localStorage.getItem("token");
		var data = {
			page: page_no,
			latitude: self.GeoLat,
			longitude: self.GeoLong,
			sort: "asc"
		}
		//alert(JSON.stringify(data));
		self.authService.getfishingcountlocation(data).subscribe(res => {
			let obj = res.data.items.docs;
			self.perPage = self.authService.perPage;
			self.totalData = self.authService.totalData;
			self.totalPage = self.authService.totalPage;
			var id = localStorage.getItem('userId');
			obj.forEach(item => {
				if (item.last_updated_count !== undefined && item.last_updated_count !== null) {
					item.Count_Date = item.last_updated_count.count_date;
					item.Fish_Count = item.last_updated_count.fish_count;
				} else {
					item.Count_Date = 'empty';
					item.Fish_Count = 'empty';
				}
				for (let fav of item.favourites) {
					if (fav.user_id) {
						if (fav.user_id._id) {
							if (fav.user_id._id == id) {
								item.favouriteStatus = true;
							} else {
								item.favouriteStatus = false;
							}
						} else {
							item.favouriteStatus = false;

						}
					} else {
						item.favouriteStatus = false;

					}
				}
				self.FavLocationList = item.favourites
				//item.favouriteStatus = false;
				this.Output = [];
				let split_string = item.title.split(/(\d+)/);
				item.titleSplit = split_string[0];
				this.Output = split_string.slice(1, split_string.length);
				item.fromDate = this.Output.join(" ");
				self.FishCountLocList.push(item);
				if (obj.length == self.FishCountLocList.length)
				loading.dismiss();
			})
			
		});


	}
	doRefresh(refresher) {
		console.log('Begin async operation', refresher);
		setTimeout(() => {
			this.page = 1;
			this.FishCountLocList = [];
			if (this.tabType == "AllLocation") {
				this.getfishingcountlocationlist(this.page);
			} else if (this.tabType == "FavLocation") {
				this.getfishingcountFavList(1);
				//this.FishCountLocList = this.FavLocationList;
			}
			console.log('Async operation has ended');
			refresher.complete();
		}, 2000);
	}

	doInfinite(infiniteScroll) {
		//if (this.tabType == "AllLocation") {
		if (this.page < this.totalPage) {
			console.log('Begin async operation', infiniteScroll);
			setTimeout(() => {
				this.page++;
				if (this.tabType == 'FavLocation') {
					this.getfishingcountFavList(this.page);
				} else {
					this.getfishingcountlocationlist(this.page);
				}
				console.log('Async operation has ended');
				infiniteScroll.complete();
			}, 1000);
		}
		else {
			infiniteScroll.enabled(false);
		}
	}

	getfishingcountFavList(page_no: any) {
		let self = this;
		this.FishCountLocList = [];
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 3000
		});
		loading.present();
		var token = localStorage.getItem("token");
		var data = {
			page: page_no,
			latitude: self.GeoLat,
			longitude: self.GeoLong,
			sort: "asc"
		}
		self.authService.getFavFishCountlist(data).subscribe(res => {
			console.log('data', res);
			let obj = res.data.items.docs;
			self.perPage = self.authService.perPage;
			self.totalData = self.authService.totalData;
			self.totalPage = self.authService.totalPage;
			var id = localStorage.getItem('userId');
			_.forEach(obj, (item: any) => {
				if (item.last_updated_count !== undefined && item.last_updated_count !== null) {
					item.Count_Date = item.last_updated_count.count_date;
					item.Fish_Count = item.last_updated_count.fish_count;
				} else {
					item.Count_Date = 'empty';
					item.Fish_Count = 'empty';
				}
				item.favouriteStatus = true;
				this.Output = [];
				let split_string = item.title.split(/(\d+)/);
				item.titleSplit = split_string[0];
				this.Output = split_string.slice(1, split_string.length);
				item.fromDate = this.Output.join(" ");
				self.FishCountLocList.push(item);
			});
			loading.dismiss();
			console.log('getfishingcountlocation', self.FishCountLocList);
		});
	}


	openAreaDetail(area) {
		console.log('area', area);
		this.navCtrl.push('FishCountChartPage', { area: area });
	}
	Notification() {
		this.navCtrl.push('NotificationsPage');
	}

	addTofavouriteLocList(value: any) {
		if (!value.favouriteStatus) {
			this.status = true;
		} else {
			this.status = false;
		}
		var data = {
			fish_count_location_id: value._id,
			status: this.status
		};
		value.favouriteStatus = !value.favouriteStatus;
		this.authService.addToFavouriteFishCount(data).subscribe(res => {
			console.log('res', res);
			// added as a favourite.
			//this.getfishingcountlocationlist(1);
		});
	}

	addTofavouriteList(value: any) {
		this.FishCountLocList = [];
		if (!value.favouriteStatus) {
			this.status = true;
		} else {
			this.status = false;
		}
		var data = {
			fish_count_location_id: value._id,
			status: this.status
		};
		value.favouriteStatus = !value.favouriteStatus;
		this.authService.addToFavouriteFishCount(data).subscribe(res => {
			// added as a favourite.
			console.log('res', res);
			if (res.success) {
				//value.favouriteStatus = !value.favouriteStatus;
				// this.getfishingcountFavList(1);
				// console.log('value', value);
				// var i = this.FishCountLocList.indexOf(value._id);
				// console.log('id', i)
				// this.FishCountLocList.splice(i, 1);
			}
		});
	}

	btnSegmentCall(data: any) {
		this.FishCountLocList = [];
		this.page = 1;
		this.tabType = data;
		if (data == "AllLocation") {
			this.getfishingcountlocationlist(this.page);
		} else if (data == "FavLocation") {
			this.getfishingcountFavList(this.page);
			//this.FishCountLocList = this.FavLocationList;
		}

	}
}
