import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService, Event } from '../../providers/auth-provider';
import moment from 'moment';

/**
 * Generated class for the EventDetailsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-event-details',
  templateUrl: 'event-details.html',
})
export class EventDetailsPage {
  eventItem: any;
eventDetails:any={};
eventDay: any;
eventDate: string;
  constructor(public navCtrl: NavController, public authService: AuthService, public navParams: NavParams) {
    this.eventItem = navParams.get('event');
    var date = moment(this.eventItem.event_date);
    this.eventDay = date.date();
    this.getEventDetails()
  }

  ionViewDidLoad() {
   

    console.log('ionViewDidLoad EventDetailsPage');
  }
  openUrl(url:any){
    window.open(url, '_system');
  }
  getEventDetails() {
    this.authService.getEventDataSingle(this.eventItem._id).subscribe(res => {
      this.eventDetails=res.data
      console.log("GET DATA ",this.eventDetails)

    });
  }
}
