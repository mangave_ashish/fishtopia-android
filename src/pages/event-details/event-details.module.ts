import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { EventDetailsPage } from './event-details';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    EventDetailsPage,
  ],
  imports: [
    SharedModule,
    IonicPageModule.forChild(EventDetailsPage),
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class EventDetailsPageModule {}
