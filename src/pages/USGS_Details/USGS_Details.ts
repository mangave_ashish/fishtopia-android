import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, NavParams, Content, Platform, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { DateTime } from 'ionic-angular';
import chartJs from 'chart.js';
import Highcharts from 'highcharts/highstock';
import * as moment from 'moment';
import { MatTabChangeEvent } from '@angular/material';
import * as _ from 'lodash';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';


@IonicPage()
@Component({
    selector: 'page-USGS_Details',
    templateUrl: 'USGS_Details.html'
})
export class USGS_DetailsPage {
    @ViewChild('leftCats') leftItems;
    @ViewChild(Content) content: Content;
    @ViewChild('lineCanvas') lineCanvas;
    @ViewChild('slider') slider: Slides;
    dateArray: any = [];
    monthArray: any[] = ["January", "February ", "March ", "April ", "May ", "June ", "July", "August", "September", "October", "November", "December"];
    yearArray: any[];
    doneLoading = false;
    messages: any[] = [];
    USGS_Details: any;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
    mainEventList: any[] = [];
    dateList: Date[] = [];
    eventList: Event[] = [];
    ChartdateList: Date[] = [];
    ChartdateTimeList: DateTime[] = [];
    dataft: any;
    lineChart: any;
    title: any;
    TideStationlist: any[] = [];
    TideStationlistfilter: any[] = [];
    dataDB: any;
    WsChartData: any[];
    WsRecords: any[];
    tabCheck: any;
    graphEnabled: boolean = false;
    timeSpan: boolean = false;
    GraphOn: boolean = false;
    imageUrlDC: any = '';
    imageUrlGH: any = '';
    USGSOption: any;
    range: number = 0;
    img: any;
    fromDate: any = new Date().toISOString();
    toDate: any = new Date().toISOString();
    constructor(public navParams: NavParams,
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public authService: AuthService,
        public photoViewer: PhotoViewer,
    ) {
        this.USGSOption = 'Calendar';
        this.USGS_Details = navParams.get('USGS_Details');
        var v = new Date();
        this.yearArray = [v.getFullYear(), (v.getFullYear() + 1)];
        this.selectedDay = v.getDate();
        this.selectedMonth = v.getMonth();
        this.YearMonth = v.getFullYear();
        this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
        this.allDates();
    }
    errorEnabled: boolean = false;
    getDatefilter(event: any, data: any) {
        if (data == "fromDate") {
            this.fromDate = moment(event).format("MM/DD/YYYY");
        } else {
            this.toDate = moment(event).format("MM/DD/YYYY");
        }
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3000
        });
        loading.present();
        console.log('this.SelectDate', event + "" + this.fromDate + " " + this.toDate);
        this.getUSGSFLowGraphDetails();

    }

    onLinkClick(event: any) {
        var v = new Date();
        if (event == "Graph") {
            this.graphEnabled = true;
            let loading = this.loadingCtrl.create({
                spinner: 'hide',
                content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
                duration: 3000
            });
            loading.present();
            this.fromDate = new Date().toISOString();
            this.toDate = new Date().toISOString();
            // console.log('this.SelectDate', this.fromDate + " " + this.toDate);
            this.getUSGSFLowGraphDetails();
        } else {
            this.graphEnabled = false;
            this.errorEnabled = false;
        }
    }

    getYear(YearMonth: any) {
        console.log('this.SelectDate', YearMonth);
        this.range = 0;
        if (YearMonth == new Date().getFullYear()) {
            this.YearMonth = YearMonth;
            this.selectedMonth = new Date().getMonth();
            this.selectedDay = new Date().getDate();
            this.allDates();
        } else {
            this.YearMonth = YearMonth;
            this.selectedMonth = 1;
            this.selectedDay = 1;
            this.allDates();
        }
    }


    onDateClick(date: any, i: any) {
        this.selectedDay = date;
        this.fromDate = new Date(this.YearMonth, this.selectedMonth, date);
        console.log('this.fromDate', this.fromDate)
        this.allDates();
    }


    getUSGSFLowDetails() {
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3000
        });
        loading.present();
        this.dateList = [];
        this.mainEventList = [];
        var data = {
            fromDate: moment(this.fromDate).format("YYYY-MM-DD"),
            //    new Date(this.fromDate),
            toDate: moment(this.fromDate).format("YYYY-MM-DD"),
            //new Date(this.fromDate),
            stationNumber: this.USGS_Details.stationNumber
        }
        this.authService.getwaterlocationDetails(data).subscribe(res => {
            console.log('res', res.data);
            let dt = res.data;
            // loading.dismiss();
            dt.forEach(item => {
                item.water_temp = Math.round(JSON.parse(item.water_temp) * 100) / 100;
                //JSON.parse(item.water_temp);
                item.air_temp = Math.round(JSON.parse(item.air_temp) * 100) / 100;
                //JSON.parse(item.air_temp);
                item.gage_height = Math.round(JSON.parse(item.gage_height) * 100) / 100;
                // JSON.parse(item.gage_height);
                item.discharge = Math.round(JSON.parse(item.discharge) * 100) / 100;
                // JSON.parse(item.discharge);
                // item.water_temp = JSON.parse(item.water_temp);
                // item.air_temp = JSON.parse(item.air_temp);
                // item.gage_height = JSON.parse(item.gage_height);
                // item.discharge = JSON.parse(item.discharge);

                if (item.water_temp < 1 || item.water_temp == null) {
                    item.water_temp = 0;
                }

                if (item.air_temp < 1 || item.air_temp == null) {
                    item.air_temp = 0;
                }

                if (item.discharge < 1 || item.discharge == null) {
                    item.discharge = 0;

                }

                if (item.gage_height < 1 || item.gage_height == null) {
                    item.gage_height = 0;

                }

                this.mainEventList.push(item);
            })
        });
    }

    getUSGSFLowGraphDetails() {
        console.log(this.fromDate, this.toDate);
        var fromDate = this.fromDate;
        var toDate = this.toDate;
        console.log(fromDate, toDate);
        if (moment(fromDate).format("YYYY-MM-DD") <= moment(toDate).format("YYYY-MM-DD")) {
            this.errorEnabled = false;
            var data = {
                fromDate: moment(fromDate).format("YYYY-MM-DD"),
                toDate: moment(toDate).format("YYYY-MM-DD"),
                stationNumber: this.USGS_Details.stationNumber
            }
            console.log('data', data);
            this.authService.getwaterDSGraphDetails(data).subscribe(res => {
                var protocol1 = res.data.split("//");
                if (protocol1[0] == "http:") {
                    this.imageUrlDC = res.data;
                } else if (protocol1[0] == "https:") {
                    this.imageUrlDC = res.data;
                } else {
                    this.imageUrlDC = 'assets/img/error.png';
                }
                // loading.dismiss();
                // let parser = new DOMParser();
                // let parsedHtml = parser.parseFromString(res.data, 'text/html');
                // var t = parsedHtml.getElementsByTagName("img")[0].children;
                // var d = t[1].innerHTML;
            });
            this.authService.getwaterGHGraphDetails(data).subscribe(res => {
                var protocol1 = res.data.split("//");
                if (protocol1[0] == "http:") {
                    this.imageUrlGH = res.data;
                } else if (protocol1[0] == "https:") {
                    this.imageUrlGH = res.data;
                } else {
                    this.imageUrlGH = 'assets/img/error.png';
                }

                // let parser = new DOMParser();
                // let parsedHtml = parser.parseFromString(res.data, 'text/html');
                // this.imageUrlGH = res.data;
                // var t = parsedHtml.getElementsByTagName("img")[0].children;
                // var d = t[1].innerHTML;
            });
        } else {
            this.errorEnabled = true;
        }
    }



    allDates() { // get all days in selected month
        let date: Date = new Date(new Date(this.YearMonth, this.selectedMonth, 1));
        console.log('dataDate', date);
        var names = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        this.authService.dateArray = [];
        while (date.getMonth() == this.selectedMonth) {
            this.authService.dateArray.push({ day: date.getDate(), name: names[date.getDay()] });
            date.setDate(date.getDate() + 1);
        }
        if (this.selectedDay > this.authService.dateArray.length)
            this.selectedDay = 1;
        this.getUSGSFLowDetails();
        setTimeout(() => {
            try {
                // var temp=document.getElementById('temp');
                // var numOfElement= Math.floor(temp.clientWidth/50);
                // var scroll= Math.floor(this.selectedDay/numOfElement);
                this.leftItems._scrollContent.nativeElement.scrollLeft = (50 * this.selectedDay) - 200;//(temp.clientWidth*(scroll))-50;
            } catch (ex) {

            }
        }, 1000);
    }
    get dateString() {// concat date,month and year to create date string
        return new Date().getFullYear() + "-" + this.selectedMonth + "-" + this.selectedDay;
    }


    PhotoView(text: any) {
        if (text == 'DC') {
            this.photoViewer.show(this.imageUrlDC, 'Discharge, cubic feet per second', { share: false });
        } else {
            this.photoViewer.show(this.imageUrlGH, 'Gage height, feet', { share: false });
        }
    }
}
