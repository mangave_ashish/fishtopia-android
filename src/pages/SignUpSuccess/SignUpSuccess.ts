// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, App, IonicPage, LoadingController, MenuController, ModalController, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
  selector: 'page-SignUpSuccess',
  templateUrl: 'SignUpSuccess.html',
})
export class SignUpSuccessPage {
  public loginForm: any;
  // public backgroundImage = 'assets/img/background/background-6.jpg';
  isenabledEmail: any;
  isenabledPhone: any;
  RegisterForm: FormGroup;
  FirstNo: AbstractControl;
  SecondNo: AbstractControl;
  ThirdNo: AbstractControl;
  ForthNo: AbstractControl;
  submitAttempt: boolean = false;
  SubmitBtn: boolean = false;
  public otp1 = [/\d/];
  public otp2 = [/\d/];
  public otp3 = [/\d/];
  public otp4 = [/\d/];
  UserId: any;
  phone: any;
  phoneCode: any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public modalCtrl: ModalController,
    public navParams: NavParams
  ) {
    this.UserId = navParams.get('UserId');
    this.phone = navParams.get('phone');
    this.phoneCode = navParams.get('phoneCode');
    console.log(this.UserId + this.phone + this.phoneCode);
    this.isenabledEmail = true;
    this.isenabledPhone = false;
    this.RegisterForm = this.formBuilder.group({
      'FirstNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
      'SecondNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
      'ThirdNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
      'ForthNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
    });
    this.FirstNo = this.RegisterForm.controls['FirstNo'];
    this.SecondNo = this.RegisterForm.controls['SecondNo'];
    this.ThirdNo = this.RegisterForm.controls['ThirdNo'];
    this.ForthNo = this.RegisterForm.controls['ForthNo'];

  }


  otpverify() {
    var FirstNo = this.RegisterForm.controls['FirstNo'].value;
    var SecondNo = this.RegisterForm.controls['SecondNo'].value;
    var ThirdNo = this.RegisterForm.controls['ThirdNo'].value;
    var ForthNo = this.RegisterForm.controls['ForthNo'].value;
    let otpno = FirstNo + SecondNo + ThirdNo + ForthNo;
    var data = {
      otp_code: otpno
    }
    this.authService.otpVerifed(data).subscribe(res => {
      if (res.success) {
        let alert = this.alertCtrl.create({
          //title: 'Exit?',
          message: 'You one-time use passcode has been accepted and your registration is complete. Thank you from Alaska FishTopia!',
          buttons: [
            {
              text: 'OK',
              role: 'OK',
              handler: () => {
                //  this.alert = null
              }
            }
          ]
        });
        alert.present();
        console.log('Output for signupdata-->');
        this.navCtrl.push('LoginListPage');
        // this.navCtrl.push('HomePage');
        //   this.navCtrl.setRoot(LoginPage);

      }
      else {
        this.submitAttempt = true;
      }
    })

  }



  next(el) {
    el.setFocus();
  }


  Email() {
    this.isenabledEmail = true;
    this.isenabledPhone = false;

  }
  Phone() {
    this.isenabledPhone = true;
    this.isenabledEmail = false;

  }

  goToSignup() {
    //this.slider.slideTo(2);
    this.navCtrl.push('signup');
  }

  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }

  login() {
    this.presentLoading('Thanks for signing up!');
    this.navCtrl.setRoot('HomePage');
  }

  signup() {
    this.presentLoading('Thanks for signing up!');
    this.navCtrl.setRoot('signup');
  }

  resetPassword() {
    this.navCtrl.setRoot('HomePage');
    this.presentLoading('An e-mail was sent with your new password.');

  }
  ResendOTP() {
    
    this.navCtrl.push('resendotpPage', { Userdata: this.UserId, phone: this.phoneCode+""+this.phone });
   
  }
}
