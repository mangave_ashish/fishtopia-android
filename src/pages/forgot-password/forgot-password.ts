
import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AlertController, App, IonicPage, LoadingController, MenuController, NavController, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';



@IonicPage()
@Component({
  selector: 'page-forgot-password',
  templateUrl: 'forgot-password.html',
})
export class forgotPasswordPage {
  public loginForm: any;
  // public backgroundImage = 'assets/img/background/background-6.jpg';
  isenabledEmail: any;
  isenabledPhone: any;
  ForgotForm: FormGroup;
  userEmail: AbstractControl;
  //  PhoneNumber:  AbstractControl;
  //  userType:  AbstractControl;
  //  emailPlaceholder:any;
  //  phonePlaceholder:any;
  userType: string;
  // userEmail:string;
  // userPhone:string;
  EmailExist: boolean = false;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public toastCtrl: ToastController,
  ) {
    this.userType = "email";
    this.isenabledEmail = true;
    this.isenabledPhone = false;
    this.ForgotForm = this.formBuilder.group({
      'userEmail': ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-z0-9.-]+[.]{1}[a-zA-Z]{2,}$')])],
      // 'PhoneNumber': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9][0-9 ]*')])],
      // 'userType': ['']
    });
    this.userEmail = this.ForgotForm.controls['userEmail'];
    // this.PhoneNumber = this.ForgotForm.controls['PhoneNumber'];
    // this.userType = this.ForgotForm.controls['userType']; 
  }





  Email() {
    this.isenabledEmail = true;
    this.isenabledPhone = false;

  }
  Phone() {
    this.isenabledPhone = true;
    this.isenabledEmail = false;

  }

  goToSignup() {
    //this.slider.slideTo(2);
    this.navCtrl.push('signup');
  }


  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }

  login() {
    //  this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('HomePage');
  }

  signup() {
    //  this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('signup');
  }

  resetPassword() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    loading.present();
    //    if(this.userEmail !=undefined && this.userType=="phone"){
    //     var data = {
    //       type:this.userType,
    //       identity: this.userPhone
    //   }
    // }else{
    var data = {
      type: this.userType,
      identity: this.userEmail.value.toLowerCase()
    }
    //}
    console.log('data', data);
    this.authService.forgotPassword(data).subscribe(res => {
      loading.dismiss();
      if (res.success != false) {
        this.navCtrl.push('resetPasswordPage');
      } else {
        this.showToast(res.message);
      }
    });
  }

  showToast(msg) {
    this.EmailExist = true
    let toast = this.toastCtrl.create({
      message: "Email ID does not exist",
      duration: 3000
    });
    toast.onDidDismiss(() => {
      //console.log('Dismissed toast');
    });
    toast.present();
  }
}
