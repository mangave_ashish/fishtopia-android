import { Component, ElementRef, Input, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { Camera, CameraOptions } from '@ionic-native/camera';
import { CardIO } from '@ionic-native/card-io';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { Stripe } from '@ionic-native/stripe';
import { Transfer } from '@ionic-native/transfer';
import { ActionSheetController, IonicPage, Loading, LoadingController, NavController, NavParams, Platform, ToastController, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AuthService } from '../../providers/auth-provider';


declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-businessform',
  templateUrl: 'Businessform.html',
})
export class BusinessformPage {


  @ViewChild('editor') editor: ElementRef;
  @ViewChild('decorate') decorate: ElementRef;
  @ViewChild('styler') styler: ElementRef;

  @Input() formControlItem: FormControl;

  @Input() placeholderText: string;

  loading: Loading;
  tripItem: any;
  tripDay: string;
  tripDate: string;
  // item: FormControl;
  BusinessForm: FormGroup;
  BusinessName: AbstractControl;
  // BusinessDesp: AbstractControl;
  contNumber: AbstractControl;
  EmailId: AbstractControl;
  FirstURL: AbstractControl;
  SecondURL: AbstractControl;
  ThirdURL: AbstractControl;
  Business_Name: any;
  Business_Desp: any;
  cont_Number: any;
  Email_Id: any;
  ErrorMsg: any;
  userDetails: any;
  imgPreview: any;
  ImageEncoded: string;
  FURL: any;
  SURL: any;
  TURL: any;
  FXdata: any;
  constructor(
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public platform: Platform,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public stripe: Stripe,
    public cardIO: CardIO,
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    private file: File,
    private filePath: FilePath,
    private camera: Camera,
    private transfer: Transfer,
    public actionSheetCtrl: ActionSheetController,
    private sanitizer: DomSanitizer,
  ) {
    this.FXdata = navParams.get('FishingExchnage');
    this.menuCtrl.swipeEnable(false);
    this.getProfileDetails();
    this.BusinessForm = this.formBuilder.group({
      'BusinessName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      // 'BusinessDesp': ['', Validators.compose([Validators.required])],
      'contNumber': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(14), Validators.pattern('[0-9][0-9 ]*')])],
      'EmailId': ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-z0-9.-]+[.]{1}[a-zA-Z]{2,}$')])],
      'FirstURL': ['', Validators.compose([Validators.pattern("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")])],
      'SecondURL': ['', Validators.compose([Validators.pattern("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")])],
      'ThirdURL': ['', Validators.compose([Validators.pattern("^(http[s]?:\\/\\/(www\\.)?|ftp:\\/\\/(www\\.)?|www\\.){1}([0-9A-Za-z-\\.@:%_\+~#=]+)+((\\.[a-zA-Z]{2,3})+)(/(.)*)?(\\?(.)*)?")])],
    });
  }


  Uploadimg() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }


  public takePicture(sourceType) {
    const options: CameraOptions = {
      quality: 70,
      allowEdit: true,
      sourceType: sourceType,
      destinationType: this.camera.DestinationType.DATA_URL,
      targetWidth: 1024,
      targetHeight: 1024,
    };
    let loading = this.loadingCtrl.create({
      content: 'Uploading...',
      duration: 3500
    });
    loading.present();
    this.camera.getPicture(options).then((imagePath) => {
      // Special handling for Android library
      this.ImageEncoded = imagePath;
      this.imgPreview = 'data:image/jpeg;base64,' + imagePath;
    })
  }
  getProfileDetails() {
    this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
      this.userDetails = res.Details;
      this.Business_Name = this.userDetails.name;
      const element = this.editor.nativeElement as HTMLDivElement;
      element.innerHTML = this.userDetails.BusinessDesp;;
      this.cont_Number = this.userDetails.Contact_No;
      this.Email_Id = this.userDetails.Email_Id;

      let split_string = this.userDetails.image.split(',');
      if (split_string[0] == 'data:image/*;charset=utf-8;base64') {
        this.imgPreview = this.sanitizer.bypassSecurityTrustResourceUrl(this.userDetails.image);
      } else if (split_string[0] == "data:image/png;base64") {
        this.imgPreview = this.userDetails.image;
      } else {
        this.imgPreview = 'data:image/jpeg;base64,' + this.userDetails.image;
      }
      // this.imgPreview = 'data:image/jpeg;base64,' + this.userDetails.image;
      // this..va = this.userDetails.BusinessDesp;
      // this.sanitizer.bypassSecurityTrustResourceUrl(this.userDetails.image);
      // var head = 'data:image/jpeg;base64,';
      // this.imgPreview = ((this.userDetails.image.length - head.length) * 3 / 4 / (611 * 600)).toFixed(4);
      if (this.userDetails.URL1 === "http://" || this.userDetails.URL1 === "https://") {
        this.FURL = '';
      } else {
        this.FURL = this.userDetails.URL1;
      }
      if (this.userDetails.URL2 === "http://" || this.userDetails.URL2 === "https://") {
        this.SURL = '';
      } else {
        this.SURL = this.userDetails.URL2;
      }
      if (this.userDetails.URL3 === "http://" || this.userDetails.URL3 === "https://") {
        this.TURL = '';
      } else {
        this.TURL = this.userDetails.URL3;
      }
    });
  }
  ionViewWillLeave() {
    this.fabGone = true;
    this.menuCtrl.swipeEnable(true);
  }
  ionViewDidLeave() {
    this.menuCtrl.swipeEnable(true);
  }

  loaderDismiss() {
    this.loading.dismiss();
  }

  validateCard() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    loading.present();
  }

  // Just to animate the fab
  fabGone = false;
  ionViewWillEnter() {
    this.fabGone = false;
  }

  ngAfterContentInit() {
    const element = this.editor.nativeElement as HTMLDivElement;
    element.innerHTML = '';
    // this.formControlItem.setValue(null);
    this.updateItem();
    this.wireupButtons();

  }


  dismiss() {
    //this.navCtrl.setRoot('HomePage');
    this.viewCtrl.dismiss();
  }
  data: any;
  First_Url: string;
  Second_Url: string;
  Third_Url: string;
  saveBusinessinfo() {
    const element = this.editor.nativeElement as HTMLDivElement;
    console.log('element', element.innerHTML);
    this.First_Url = this.BusinessForm.controls['FirstURL'].value;
    this.Second_Url = this.BusinessForm.controls['SecondURL'].value;
    this.Third_Url = this.BusinessForm.controls['ThirdURL'].value;
    //console.log(this.First_Url + this.Second_Url + this.Third_Url)


    // URL1
    if (this.First_Url != undefined) {
      var protocol1 = this.First_Url.split("www");
      if (protocol1[0] !== "http://" && protocol1.length > 1 && protocol1[0] !== "https://" &&
        this.BusinessForm.controls['FirstURL'].value !== undefined && this.BusinessForm.controls['FirstURL'].value !== null) {
        var CnverttUrl1 = "http://" + this.BusinessForm.controls['FirstURL'].value;
      } else {
        CnverttUrl1 = this.BusinessForm.controls['FirstURL'].value;
      }
    }
    //URL2
    if (this.Second_Url != undefined) {
      var protocol2 = this.Second_Url.split("www");
      if (protocol2[0] !== "http://" && protocol2.length > 1 && protocol2[0] !== "https://" &&
        this.BusinessForm.controls['SecondURL'].value !== undefined && this.BusinessForm.controls['SecondURL'].value !== null) {
        var CnverttUrl2 = "http://" + this.BusinessForm.controls['SecondURL'].value;
      } else {
        CnverttUrl2 = this.BusinessForm.controls['SecondURL'].value;
      }
    }
    //URL3
    if (this.Third_Url != undefined) {
      var protocol3 = this.Third_Url.split("www");
      if (protocol3[0] !== "http://" && protocol3.length > 1 && protocol3[0] !== "https://" &&
        this.BusinessForm.controls['ThirdURL'].value !== undefined && this.BusinessForm.controls['ThirdURL'].value !== null) {
        var CnverttUrl3 = "http://" + this.BusinessForm.controls['ThirdURL'].value;
      } else {
        CnverttUrl3 = this.BusinessForm.controls['ThirdURL'].value;
      }
    }

    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 7500
    });
    loading.present();
    console.log('this.ImageEncoded', this.ImageEncoded)
    if (this.ImageEncoded !== undefined) {
      this.data = {
        details: {
          // BusinessDesp: this.BusinessForm.controls['BusinessDesp'].value,
          BusinessDesp: element.outerHTML,
          Contact_No: this.BusinessForm.controls['contNumber'].value,
          Email_Id: this.BusinessForm.controls['EmailId'].value,
          URL1: CnverttUrl1,
          URL2: CnverttUrl2,
          URL3: CnverttUrl3,
          image: this.ImageEncoded,
          name: this.BusinessForm.controls['BusinessName'].value
        }
      }
    } else {
      this.data = {
        details: {
          //   BusinessDesp: this.BusinessForm.controls['BusinessDesp'].value,
          BusinessDesp: element.outerHTML,
          Contact_No: this.BusinessForm.controls['contNumber'].value,
          Email_Id: this.BusinessForm.controls['EmailId'].value,
          URL1: CnverttUrl1,
          URL2: CnverttUrl2,
          URL3: CnverttUrl3,
          image: this.userDetails.image,
          name: this.BusinessForm.controls['BusinessName'].value
        }
      }
    }
    //var ddt = element.innerHTML;
    //console.log('data', ddt);
    //if (ddt != '') {
    this.authService.postBusinessDetails(this.data).subscribe(res => {
      if (res.success) {
        this.getProfileDetails();
        let alert = this.alertCtrl.create({
          //title: 'Exit?',
          message: res.message,
          buttons: [
            {
              text: 'OK',
              role: 'OK',
              handler: () => {
                loading.dismiss();
                this.authService.getProfileDetails();
                //  this.alert = null
              }
            }
          ]
        });
        alert.present();
        this.viewCtrl.dismiss();
        this.navCtrl.setRoot('FishingExchangPage');
        // this.navCtrl.setRoot('HomePage');
        //this.presentToast(res.message);
      } else {
        console.log(res.message);
        this.ErrorMsg = res.message;
      }
    }, (error) => {
      var json = JSON.parse(error.text());
      console.log(error);
    })
    // } else {
    //   this.ErrorMsg = " * Please Let Us Know Something About Your Business.";
    // }
  }
  Notification(){
    this.navCtrl.push("dashboardPage")
  }

  getPlaceholderText() {
    if (this.placeholderText !== undefined) {
      return this.placeholderText
    }
    return '';
  }

  uniqueId = `editor${Math.floor(Math.random() * 1000000)}`;

  private stringTools = {
    isNullOrWhiteSpace: (value: string) => {
      if (value == null || value == undefined) {
        return true;
      }
      value = value.replace(/[\n\r]/g, '');
      value = value.split(' ').join('');

      return value.length === 0;
    }
  };

  private updateItem() {
    const element = this.editor.nativeElement as HTMLDivElement;
    element.innerHTML = '<div></div>';
    //  this.formControlItem.value;

    // if (element.innerHTML === null || element.innerHTML === '') {
    //   element.innerHTML = '<div></div>';
    // }

    const reactToChangeEvent = () => {

      // if (this.stringTools.isNullOrWhiteSpace(element.innerText)) {
      //   element.innerHTML = '<div></div>';
      //   this.formControlItem.setValue(null);
      // } else {
      //   this.formControlItem.setValue(element.innerHTML);
      // }
    };

    element.onchange = () => reactToChangeEvent();
    element.onkeyup = () => reactToChangeEvent();
    element.onpaste = () => reactToChangeEvent();
    element.oninput = () => reactToChangeEvent();
  }

  private wireupButtons() {
    let buttons = (this.decorate.nativeElement as HTMLDivElement).getElementsByTagName('button');
    for (let i = 0; i < buttons.length; i++) {
      let button = buttons[i];

      let command = button.getAttribute('data-command');

      if (command.includes('|')) {
        let parameter = command.split('|')[1];
        command = command.split('|')[0];

        button.addEventListener('click', () => {
          document.execCommand(command, false, parameter);
        });
      } else {
        button.addEventListener('click', () => {
          document.execCommand(command);
        });
      }
    }

  }









}
