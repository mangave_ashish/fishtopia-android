import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ToastController, LoadingController, ModalController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
/**
 * Generated class for the GuideReviewPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-Paymentlist',
    templateUrl: 'Paymentlist.html',
})
export class PaymentlistPage {
    guideReviews: any[] = [];
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;
    ReviewAll: any;
    userDetails: any;
    Paymentlist: any;
    checked: boolean = true;
    DataNA: boolean;
    MemberDetails: any;
    title: any;
    constructor(
        public menuCtrl: MenuController, public modalCtrl: ModalController,
        public loadingCtrl: LoadingController, public toastCtrl: ToastController,
        public authService: AuthService, public navCtrl: NavController,
        public navParams: NavParams, public alertCtrl: AlertController
    ) {
        this.getAllMyCard();
        this.MemberDetails = navParams.get('MemberDetails');
        this.title = navParams.get('Title');
        console.log(this.MemberDetails + this.title);
    }

    getAllMyCard() {
        console.log('call');
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3500
        });
        loading.present();
        this.authService.Paymentlist = [];
        this.authService.getAllStripePost().subscribe(res => {
            console.log('res', res);
            loading.dismiss();
            if (res.success) {
                if (res.data.data.length !== 0) {
                    for (let item of res.data.data) {
                        item.checked = false;
                        this.authService.Paymentlist.push(item);
                    }
                    this.authService.Paymentlist[0].checked = true;
                    this.authService.DataNA = false;
                    console.log('data-->', this.authService.Paymentlist);
                } else {
                    this.authService.DataNA = true;
                    if (this.title == "Guide Direct" || this.title == "FishTopia Prime") {
                        this.modalCtrl.create('PaymentPage', { MemberDetails: this.MemberDetails, Title: this.title }).present();
                    } else {
                        this.modalCtrl.create('PaymentPage').present();
                    }

                    //   this.navCtrl.push('PaymentPage');
                }
            } else {
                this.authService.DataNA = true;
                //this.modalCtrl.create('PaymentPage').present();
            }
        });
    }

    // DeleteCard(card: any) {
    //     console.log('data-->', card);
    //     this.authService.getAllStripePost().subscribe(res => {
    //         this.Paymentlist = res.data.data;
    //         console.log('data-->', this.Paymentlist);
    //     });
    // }
    AddPayment() {
        if (this.title == "Guide Direct" || this.title == "FishTopia Prime") {
            console.log('Hello')
            this.modalCtrl.create('PaymentPage', { MemberDetails: this.MemberDetails, Title: this.title }).present();
        } else {
            this.modalCtrl.create('PaymentPage').present();
        }
        //   this.modalCtrl.create('PaymentPage').present();;
    }
    alert: any;
    DeleteCard(card: any) {
        console.log('card', card);
        let alert = this.alertCtrl.create({
            // title: 'Delete?',
            message: 'Do you want to delete the card?',
            buttons: [
                {
                    text: 'Cancel',
                    role: 'cancel',
                    handler: () => {
                        this.alert = null
                    }
                },
                {
                    text: 'Ok',
                    handler: () => {
                        this.Delete(card.id);
                    }
                }
            ]
        });
        alert.present();
        return true;
    }
    Delete(card: any) {
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 5500
        });
        loading.present();
        this.authService.DeleteCardPost(card.id).subscribe(res => {
            loading.dismiss();
            this.authService.Paymentlist = [];
            console.log('res', res);
            this.getAllMyCard();
            this.showToast('Card Deleted Successfully.');
        })
    }

    showToast(msg: any) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });
        toast.onDidDismiss(() => {
            this.getAllMyCard();
            //////console.log'Dismissed toast');
        });
        toast.present();
    }
    ionViewWillLeave() {
        this.menuCtrl.swipeEnable(true);
    }
    ionViewDidLeave() {
        this.menuCtrl.swipeEnable(true);
    }
}
