import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PaymentlistPage } from './Paymentlist';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    PaymentlistPage,
  ],
  imports: [
    IonicPageModule.forChild(PaymentlistPage),
    SharedModule
    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class PaymentlistPageModule {}
