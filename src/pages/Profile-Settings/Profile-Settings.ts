import { Component } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version';
import { AlertController, IonicPage, ModalController, NavController, Platform } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AuthService } from '../../providers/auth-provider';
import { UserData } from '../../providers/user-data-local';


@IonicPage()
@Component({
  selector: 'Profile-Settings',
  templateUrl: 'Profile-Settings.html'
})
export class ProfileSettings {
  rootPage: any;
  items: Array<{ title: string, page: any }>;
  AppVersion: any;
  AppName: any;
  expand: boolean = false;
 
  productId: any;
  constructor(
    public menuCtrl: MenuController,
    public modalCtrl: ModalController,
    public app: AppVersion,
    public navCtrl: NavController,
    public authService: AuthService,
    public alertCtrl: AlertController,
    public platform: Platform,
    public userData: UserData,

  ) {
    this.authService.getProfileDetails();
    app.getVersionNumber().then((res) => {
      var val = JSON.stringify(res);
      console.log('val', val, res);
      this.AppVersion = res;
    });
  }




  EditProfile() {
    this.navCtrl.push('EditProfilePage');
  }
  ChangePwd() {
    this.navCtrl.push('ChangePasswordPage');
  }

  Notification() {
    this.navCtrl.push('NotificationsPage');
  }

  invitReferral() {
    this.navCtrl.push('Invite_referralPage');
  }

  ionViewWillLeave() {
    this.menuCtrl.swipeEnable(true);
  }
  ionViewDidLeave() {
    this.menuCtrl.swipeEnable(true);
  }
  PayNow() {
    //this.modalCtrl.create('PaymentPage').present();;
    this.navCtrl.push('PaymentlistPage');
  }

  NotificationSettings() {
    //this.modalCtrl.create('PaymentPage').present();;
    this.navCtrl.push('notificationsettingsPage');
  }

  toggleGroup(group: any) {
    console.log('group', group)
    this.expand = !this.expand;
  }

  isGroupShown(group: any) {
    // console.log('group', group)
    return this.expand;
  }

  AboutUs(data: any) {
    this.navCtrl.push('AboutUsPage', { AboutData: data });
  }

  LicenseSettings() {
    this.navCtrl.push('addLicensePage');
  }

  FishingLicenselist() {
    this.navCtrl.push('FishingLicenListPage');
  }

  SubDetails() {
    var dat = localStorage.getItem('SubsDetails');
    // console.log(dat)
    if (dat)
      this.navCtrl.push('subscribeDetailsPage');
    else {
      let alert = this.alertCtrl.create({
        message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
        enableBackdropDismiss: false,
        buttons: [
          {
            text: 'Not interested',
            handler: () => {
              this.navCtrl.setRoot('dashboardPage');

            }
          },
          {
            text: 'Subscribe',
            handler: () => {
              this.authService.UserType = null;
              localStorage.removeItem('UserType');
              localStorage.removeItem('UserName');
              localStorage.removeItem('Profileimg');
              localStorage.removeItem('userId');
              localStorage.removeItem('is_featured');
              localStorage.removeItem('fcmToken');
              localStorage.setItem("old_token", localStorage.getItem("token"));
              localStorage.removeItem('token');
              this.userData.removeAuthToken();
              this.navCtrl.setRoot('LoginListPage');
            }
          }
        ]

      });
      alert.present()
    }
  }
}
