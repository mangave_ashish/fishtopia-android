import { Component, NgZone, ViewChild } from '@angular/core';
import { Keyboard } from '@ionic-native/keyboard';
import { IonicPage, LoadingController, ModalController, NavController, NavParams, ViewController } from 'ionic-angular';
import * as _ from 'lodash';
import { Subscription } from 'rxjs/Subscription';
import { AuthService } from '../../../providers/auth-provider';
import { CONFIG, DatabaseService, DataPoint, DEFAULT_METRICS, Forecast, ForecastService, Location, Metrics, UtilService } from '../../../providers/weather';
import { SideMenuDisplayText } from '../../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';
declare let google: any;

@IonicPage({
  priority: 'high'
})
@Component({
  selector: 'page-home-weather',
  templateUrl: 'home-weather.html'
})
@SideMenuDisplayText('Local Weather')
export class HomeWeatherPage {
  @ViewChild('searchInput') searchInput;
  heading: string;
  autocompleteItems: Array<{ description: string, place_id: string }>;
  query: string;
  acService: any;
  locationObj: Location;
  showCancel: boolean;
  lat: number = 61.217381;
  lng: number = -149.863129;
  data: any = "Anchorage Alaska";
  location: Location;
  forecast: Forecast;
  metrics: Metrics;
  todayForecast: DataPoint;
  forecastSubscriber: Subscription;
  hourlyArray: Array<{
    time: number,
    icon: string,
    temperature: number
  }> = [];
  GeoLat: any;
  GeoLong: any;
  EndDate: any;
  StartDate: any;
  constructor(public navCtrl: NavController,
    public forecastService: ForecastService,
    public databaseService: DatabaseService,
    public utilService: UtilService,
    public modalCtrl: ModalController,
    public navParams: NavParams,
    public viewCtrl: ViewController,
    public keyboard: Keyboard,
    public zone: NgZone,
    public loadingCtrl: LoadingController,
    public authService: AuthService
  ) {
    this.StartDate = new Date();
    this.acService = new google.maps.places.AutocompleteService();
    this.getGeolocationPosition();
    this.ionViewWillEnter();
  }

  ionViewDidEnter() {
    console.log('ionViewDidEnter');
    this.zone.run(() => {
      this.searchInput.setFocus();
      this.keyboard.show();
    });
  }
  ionViewWillLeave() {
    this.EndDate = new Date();
    this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
    this.authService.audit_Page('Local Weather');
  }

  ionViewWillEnter() {
    console.log('ionViewWillEnter');
    this.acService = new google.maps.places.AutocompleteService();
    this.autocompleteItems = [];
    this.query = '';
    this.locationObj = {
      name: null,
      lat: null,
      lng: null
    };
  }
  updateSearch() {
    console.debug('modal > updateSearch > query ', this.query);
    if (this.query.trim() == '') {
      this.autocompleteItems = [];
      return;
    }
    let self = this;
    let config = {
      types: ['(cities)'],
      input: this.query
    };
    this.acService.getPlacePredictions(config, (predictions, status) => {
      console.debug('modal > getPlacePredictions > status > ', status);
      self.zone.run(() => {
        self.autocompleteItems = predictions ? predictions : [];
      });
    });
  }

  // ionViewWillLeave() {
  //   console.log('ionViewWillLeave');    
  //   this.keyboard.close();
  // }


  getGeolocationPosition() {
    this.GeoLat = localStorage.getItem('GeoLat');
    this.GeoLong = localStorage.getItem('GeoLong');
    let locationData = localStorage.getItem('GeoLocation');
    let Sub_Location = localStorage.getItem('SubGeoLocation');
    console.log(this.GeoLat, this.GeoLong);
    console.log('Location', locationData, Sub_Location);
    if (locationData === "Alaska") {
      this.lat = this.GeoLat;
      this.lng = this.GeoLong;
      this.data = Sub_Location;
    } else {
      
      this.lat = this.GeoLat;
      this.lng = this.GeoLong;
      this.data = Sub_Location;
      
      // this.lat = 61.217381;
      // this.lng = -149.863129;
      // this.data = "Anchorage Alaska";
    }
    var data = {
      "lat": this.lat,
      "lng": this.lng,
      "name": this.data
    }
    //  console.log(JSON.stringify(data));
    this.location = data;
    this.GetData(this.location);
  }


  loading: any;
  chooseItem(item) {
    this.loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      // duration: 3000
    });
    this.query = '';
    this.autocompleteItems = [];
    let self = this;
    let request = {
      placeId: item.place_id
    };
    let response: Location;
    let placesService = new google.maps.places.PlacesService(document.createElement('div'));
    placesService.getDetails(request, (place, status) => {
      if (status == google.maps.places.PlacesServiceStatus.OK) {
        self.locationObj.lat = place.geometry.location.lat();
        self.locationObj.lng = place.geometry.location.lng();
        let obj = _.find(place.address_components, ['types[0]', 'locality']);
        if (obj) {
          self.locationObj.name = obj['short_name'];
        }
        response = self.locationObj;
        this.GetData(response);
        console.debug('page > getPlaceDetail > status > ', response);
      } else {
        console.debug('page > getPlaceDetail > status > ', status);
      }
      // self.zone.run(() => {
      //   self.viewCtrl.dismiss(response);
      // });
    });
  }

  GetData(dataloc) {
    let self = this;
    var data = dataloc;
    console.log('dataloc', dataloc);
    if (data === null) {
      let modal = self.modalCtrl.create('LocationPage', { heading: 'Enter Home City', showCancel: false });
      modal.onDidDismiss((data: Location) => {
        console.debug('page > modal dismissed > data > ', data);
        if (data) {

          self.databaseService.setJson(CONFIG.HOME_LOCATION, data);
          console.log(CONFIG.HOME_LOCATION, data);
          this.location = data;
          this.init(this.location);
          //self.emitInit();
        }
      });
      modal.present();
    } else {
      this.location = data;
      this.init(this.location);
    }

  }

  init(data) {
    let self = this;
    if (data) {
      self.getForecast(this.location);
    }
    this.databaseService.getJson(CONFIG.METRICS).then(data => {
      if (data === null) {
        self.databaseService.setJson(CONFIG.METRICS, DEFAULT_METRICS);
        self.metrics = DEFAULT_METRICS;
      } else {
        self.metrics = data;
      }
    });
  }

  getForecast(location: Location) {
    let self = this;
    this.forecastSubscriber = self.forecastService.getForecast(location)
      .subscribe((data: Forecast) => {
        self.forecast = data;
        if (self.forecast && self.forecast.daily && self.forecast.daily.data) {
          self.todayForecast = self.forecast.daily.data[0];
        }
        self.hourlyArray = [];
        let currentHour = self.utilService.getCurrentHour(self.forecast.timezone);
        let flag = false;
        _.forEach(self.forecast.hourly.data, (obj: DataPoint) => {
          if (!flag && self.utilService.epochToHour(obj.time, self.forecast.timezone) < currentHour) {
            return;
          }
          flag = true;
          self.hourlyArray.push({
            time: obj.time,
            icon: obj.icon,
            temperature: obj.temperature
          });
          if (self.hourlyArray.length > 10) {
            return false;
          }
        });
      }, err => {
        console.error(err);
      });
  }

  itemClicked(item: any) {
    this.navCtrl.push('WeatherDetailPage', {
      forecast: this.forecast,
      currentForecast: item,
      currentLocation: this.location,
      metrics: this.metrics
    });
  }
}
