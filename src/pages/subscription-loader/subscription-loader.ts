import { Component } from '@angular/core';
import { Device } from '@ionic-native/device';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { Storage } from '@ionic/storage';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, Platform } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { UserData } from '../../providers/user-data-local';

/**
 * Generated class for the SubscriptionLoaderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-subscription-loader',
  templateUrl: 'subscription-loader.html',
})
export class SubscriptionLoaderPage {
  productId: any;
  public product: any = {
    name: 'Alaska FishTopia',
    appleProductId: 'com.FishTopia.alaskaFishTopia.oneyearauto',
    googleProductId: 'akfishtopiayearlysubscription'
  };
  authToken: any;
  BuoyTble: any[] = [];

  constructor(public navCtrl: NavController,
    private loadingCtrl: LoadingController,
    public platform: Platform,
    private iap: InAppPurchase,
    public authService: AuthService,
    public storage: Storage,
    public userData: UserData,
    private device: Device,

    public alertCtrl: AlertController,
    public navParams: NavParams) {
    let data = {
      "description": "In Subscription loader page",
      "response": "Subscription page started",
      "app_time": new Date().toUTCString(),
      "device_id": this.device.uuid
    }
    // this.authService.postLog(data).subscribe(res => {

    // });
    // this.authToken = localStorage.getItem("token");
    if (this.platform.is('ios')) {
      this.productId = this.product.appleProductId;
    } else if (this.platform.is('android')) {
      this.productId = this.product.googleProductId;
    }
    this.platform.ready().then(() => {
      this.iapCheck();
    });


  }
  iapCheck() {
    this.iap
      .restorePurchases()
      .then((data: any) => {
        if (data.length > 0) {
          for (var i = 0; i < data.length; ++i) {
            if (data[i].productId == this.productId) {
              let subscriptiondata: any = JSON.parse(data[i].receipt)
              console.log(subscriptiondata)
              try {
               // let tempdate = moment.unix(subscriptiondata.purchaseTime).format('dddd, MMMM, YYYY h:mm:ss A')
                let expirationDate = new Date(subscriptiondata.purchaseTime)
                console.log("before date add" + expirationDate)
                expirationDate.setFullYear(expirationDate.getFullYear() +1)
                console.log("after date add" + expirationDate)
                subscriptiondata.expirationDate = expirationDate
                console.log(subscriptiondata)
                localStorage.setItem('SubsDetails', JSON.stringify(subscriptiondata));
                this.navCtrl.setRoot('dashboardPage');
                this.authService.getProfileDetails();
              } catch (er) {
                console.log(er)
                this.navCtrl.setRoot('subscribeDialogPage');

              }
            } else {
              this.navCtrl.setRoot('subscribeDialogPage');
            }
          }
        } else {
          this.navCtrl.setRoot('subscribeDialogPage');
        }
      }).catch((err) => {
        console.log(JSON.stringify(err));
      });
  }

  ionViewDidLoad() {
    // this.iapCheck()

    console.log('ionViewDidLoad SubscriptionLoaderPage');
  }
  public loading: any


  InvalidToken() {
    this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
      return res;
    }, (err) => {
      var json = JSON.parse(err.text());
      if (json.success === false && json.error.status === 401 ||
        json.error.status === 500 || json.message === "Access token is missing" ||
        json.message === "Invalid access token" || localStorage.getItem("token") === undefined) {
        localStorage.removeItem('token');
        localStorage.removeItem('UserType');
        localStorage.removeItem('UserName');
        localStorage.removeItem('Profileimg');
        localStorage.removeItem('userId');
        localStorage.removeItem('is_featured');
        localStorage.removeItem('fcmToken');
        this.userData.removeAuthToken();
        this.navCtrl.setRoot('LoginListPage');
      }
      return err;
    });

  }

}
