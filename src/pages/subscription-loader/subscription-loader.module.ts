import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SubscriptionLoaderPage } from './subscription-loader';

@NgModule({
  declarations: [
    SubscriptionLoaderPage,
  ],
  imports: [
    IonicPageModule.forChild(SubscriptionLoaderPage),
  ],
})
export class SubscriptionLoaderPageModule {}
