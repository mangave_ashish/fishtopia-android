import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AuthService } from '../../providers/auth-provider';
import { LocationAccuracy } from '@ionic-native/location-accuracy';

import * as _ from 'lodash';
import moment from 'moment';
@IonicPage()
@Component({
    selector: 'GPS_On',
    templateUrl: 'GPS_On.html'
})
export class GPS_OnPage {
    constructor(public platform: Platform,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public menuCtrl: MenuController,
        public authService: AuthService,
        private locationAccuracy: LocationAccuracy
    ) {
    }

    gpsOn() {
        this.platform.ready().then(() => {
            this.locationAccuracy.canRequest().then((canRequest: any) => {
                console.log(canRequest);
                if (canRequest) {
                    // the accuracy option will be ignored by iOS
                    this.locationAccuracy.request(this.locationAccuracy.REQUEST_PRIORITY_HIGH_ACCURACY).then(
                        (res) => console.log('Request successful', JSON.stringify(res)),
                        error => console.log('Error requesting location permissions', JSON.stringify(error))
                    );
                }
            });
        });
    }

    // guestLogout() {
    //     console.log('Log Out');
    //     localStorage.removeItem('UserType');
    //     localStorage.removeItem('UserName');
    //     localStorage.removeItem('Profileimg');
    //     localStorage.removeItem('userId');
    //     localStorage.removeItem('is_featured');
    //     localStorage.removeItem('fcmToken');
    //     localStorage.setItem("old_token", localStorage.getItem("token"));
    //     localStorage.removeItem('token');
    //     // this.userData.removeAuthToken();
    //     this.navCtrl.setRoot('LoginListPage');
    // }

}
