import { GPS_OnPage } from './GPS_On';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module';


@NgModule({
  declarations: [
    GPS_OnPage,
  ],
  imports: [
    IonicPageModule.forChild(GPS_OnPage),
    SharedModule
  ],
  exports: [
    GPS_OnPage,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

export class GPS_OnPageModule { }
