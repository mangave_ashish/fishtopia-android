import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommercialInfoPage } from './commercial-info';

@NgModule({
  declarations: [
    CommercialInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(CommercialInfoPage),
  ],
})
export class CommercialInfoPageModule {}
