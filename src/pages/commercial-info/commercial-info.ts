import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

/**
 * Generated class for the CommercialInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commercial-info',
  templateUrl: 'commercial-info.html',
})
export class CommercialInfoPage {
  com_object: any;
  constructor(public navCtrl: NavController, public navParams: NavParams) {

  }
  getstring(data: any) {
    if (data != null) {
      if (data.length > 0)
        return data.join()
      else
        return 'N/A'
    } else {
      return 'N/A'
    }
  }
  getstatus(color: any) {
    if (color == '#f44336')
      return 'Zone is actively fishing'
    if (color == '#ff9800')
      return 'Zone will be active in the next 48 hours'
    if (color == '#e1f5fe')
      return 'Zone will not be active'

  }
  gettime(color: any, fishingdate: any) {
    if (fishingdate != null) {
      if (fishingdate.length > 0) {
        if (color == "#f44336" || color == "#ff9800") {
          return this.timeampm(fishingdate[0].start_time) + " To " + this.timeampm(fishingdate[0].end_time)
        }
        else{
          return " N/A "
        }
      }else{
        return " N/A "
      }
    }else{
      return " N/A "
    }
  }
  timeampm(timeString: any) {
    if (timeString) {
      var hourEnd = timeString.indexOf(":");
      var H = +timeString.substr(0, hourEnd);
      var h = H % 12 || 12;
      var ampm = (H < 12 || H === 24) ? " AM" : " PM";
      return timeString = h + timeString.substr(hourEnd, 3) + ampm;
    }else{
      return timeString
    }
  }
  ionViewDidLoad() {
    this.com_object = this.navParams.get('data')
    console.log('data', JSON.stringify(this.com_object))
    console.log('ionViewDidLoad CommercialInfoPage');
  }

}
