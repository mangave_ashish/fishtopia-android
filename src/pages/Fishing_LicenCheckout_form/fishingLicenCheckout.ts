import { Component, NgZone, Input } from '@angular/core';
import { Platform, ToastController, AlertController, Loading, LoadingController, ModalController, NavController, NavParams, IonicPage, ActionSheetController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../providers/auth-provider';
import * as moment from 'moment';
import { Transfer, TransferObject, FileUploadOptions } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import {
    CONFIG,
    DatabaseService,
    DataPoint,
    DEFAULT_METRICS,
    Forecast,
    ForecastService,
    Location,
    Metrics,
    UtilService
} from '../../providers/weather';


@IonicPage()
@Component({
    selector: 'fishingLicenCheckout',
    templateUrl: 'fishingLicenCheckout.html',
})
export class fishingLicenCheckoutPage {
    @Input('progress') progress: number = 0;

    public loginForm: any;
    User: any;
    Countrycode: any;
    CountryName: any;
    countries: any[] = [];
    PhoneCode: any;
    show = false;
    userDetails: any;
    ConfirmForm: FormGroup;
    FirstName: AbstractControl;
    MiddleName: AbstractControl;
    LastName: AbstractControl;
    StrtAddr1: AbstractControl;
    StrtAddr2: AbstractControl;
    Email: AbstractControl;
    State: AbstractControl;
    City: AbstractControl;
    Countryoption: AbstractControl;
    ZipCode: AbstractControl;
    PhoneNumber: AbstractControl;
    LicenState: AbstractControl;
    LicenseNo: AbstractControl;
    Month: AbstractControl;
    Year: AbstractControl;
    mnth: any[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
    SuffixData: any[] = [
        {
            id: 1,
            Suffix_name: 'None'
        },
        {
            id: 2,
            Suffix_name: 'Jr'
        },
        {
            id: 3,
            Suffix_name: 'II'
        },
        {
            id: 4,
            Suffix_name: 'Sr.'
        },
        {
            id: 5,
            Suffix_name: 'III'
        },
        {
            id: 6,
            Suffix_name: 'IV'
        },
        {
            id: 7,
            Suffix_name: 'V'
        }
    ];

    statelist: any[] = [
        {
            id: 1,
            title: 'None'
        },
        {
            id: 2,
            title: 'Alaska'
        },
        {
            id: 3,
            title: 'Arkansas'
        },
        {
            id: 4,
            title: 'Arizona'
        },
        {
            id: 5,
            title: 'California'
        },
        {
            id: 6,
            title: 'Colorado'
        },
        {
            id: 7,
            title: 'Connecticut'
        },
        {
            id: 8,
            title: 'District of Columbia'
        },
        {
            id: 9,
            title: 'Delaware'
        },
        {
            id: 10,
            title: 'Florida'
        },
        {
            id: 11,
            title: 'Georgia'
        },
        {
            id: 12,
            title: 'Hawaii'
        },
        {
            id: 13,
            title: 'Iowa'
        },
        {
            id: 14,
            title: 'Idaho'
        },
        {
            id: 15,
            title: 'Illinois'
        },
        {
            id: 16,
            title: 'Indiana'
        },
        {
            id: 17,
            title: 'Kansas'
        },
        {
            id: 18,
            title: 'Kentucky'
        },
        {
            id: 19,
            title: 'Louisiana'
        },
        {
            id: 20,
            title: 'Massachusetts'
        },
        {
            id: 21,
            title: 'Maryland'
        },
        {
            id: 22,
            title: 'Maine'
        },
        {
            id: 23,
            title: 'Michigan'
        },
        {
            id: 24,
            title: 'Minnesota'
        },
        {
            id: 25,
            title: 'Missouri'
        },
        {
            id: 26,
            title: 'Mississippi'
        },
        {
            id: 27,
            title: 'Montana'
        },
        {
            id: 28,
            title: 'North Carolina'
        },
        {
            id: 29,
            title: 'North Dakota'
        },
        {
            id: 30,
            title: 'Nebraska'
        },
        {
            id: 31,
            title: 'New Hampshire'
        },
        {
            id: 32,
            title: 'New Jersey'
        },
        {
            id: 33,
            title: 'New York'
        },
        {
            id: 34,
            title: 'Ohio'
        },
        {
            id: 35,
            title: 'Oklahoma'
        },
        {
            id: 36,
            title: 'Oregon'
        },
        {
            id: 37,
            title: 'Pennsylvania'
        },
        {
            id: 38,
            title: 'Rhode Island'
        },
        {
            id: 39,
            title: 'South Carolina'
        },
        {
            id: 40,
            title: 'South Dakota'
        },
        {
            id: 41,
            title: 'Tennessee'
        },
        {
            id: 42,
            title: 'Texas'
        },
        {
            id: 43,
            title: 'Utah'
        },
        {
            id: 44,
            title: 'Virginia'
        },
        {
            id: 45,
            title: 'Vermont'
        },
        {
            id: 46,
            title: 'Washington'
        },
        {
            id: 47,
            title: 'Wisconsin'
        },
        {
            id: 48,
            title: 'West Virginia'
        },
        {
            id: 49,
            title: 'Wyoming'
        },
        {
            id: 50,
            title: 'Armed Forces Americas'
        },
        {
            id: 51,
            title: 'Armed Forces Europe'
        },
        {
            id: 52,
            title: 'Armed Forces Pacific'
        },
        {
            id: 53,
            title: 'Alberta'
        },
        {
            id: 54,
            title: 'British Columbia'
        },
        {
            id: 55,
            title: 'Manitoba'
        },
        {
            id: 56,
            title: 'Newfoundland and Labrador'
        },
        {
            id: 57,
            title: 'Northwest Territories'
        },
        {
            id: 58,
            title: 'Nova Scotia'
        },
        {
            id: 59,
            title: 'Nunavut'
        },
        {
            id: 60,
            title: 'Ontario'
        },
        {
            id: 61,
            title: 'Prince Edward Island'
        },
        {
            id: 62,
            title: 'Quebec'
        },
        {
            id: 63,
            title: 'Saskatchewan'
        },
        {
            id: 64,
            title: 'Yukon'
        }
    ];
    chosenItem: any;
    ResidencyMnthEnabled: boolean = false;
    chosenSuffix: any;
    PersonInfo: any;

    UserPhonecode: any;
    loading: Loading;
    firstName: string;
    MdleName: string;
    lastName: string;
    Licen_State: any;
    License_No: string;
    Mnth: string;
    YearRes: string;
    Suffix: any;
    Residency: any;
    Gender: any;
    DOB: any;
    FishingType: any;
    Genderbind: any;
    storageDirectory: string = '';
    State_: string;
    Country: string;
    Zip_Code: string;
    Country1: string;
    countryname: any;
    ProdList: any;
    progressBarEnabled: boolean = false;
    Statelist: any[] = [
        'Alaska'
        , 'Alabama'
        , 'Alberta'
        , 'American Samoa'
        , 'Arizona'
        , 'Arkansas'
        , 'Armed Forces Americas'
        , 'Armed Forces Europe'
        , 'Armed Forces Pacific'
        , 'British Columbia'
        , 'California'
        , 'Colorado'
        , 'Connecticut'
        , 'Delaware'
        , 'District of Columbia'
        , 'Federated States of Micronesia'
        , 'Florida'
        , 'Georgia'
        , 'Guam'
        , 'Hawaii'
        , 'Idaho'
        , 'Illinois'
        , 'Indiana'
        , 'Iowa'
        , 'Kansas'
        , 'Kentucky'
        , 'Louisiana'
        , 'Maine'
        , 'Manitoba'
        , 'Mariana Islands'
        , 'Marshall Islands'
        , 'Maryland'
        , 'Massachusetts'
        , 'Michigan'
        , 'Minnesota'
        , 'Mississippi'
        , 'Missouri'
        , 'Montana'
        , 'Nebraska'
        , 'Nevada'
        , 'New Brunswick'
        , 'New Hampshire'
        , 'New Jersey'
        , 'New Mexico'
        , 'New York'
        , 'Newfoundland and Labrador'
        , 'North Carolina'
        , 'North Dakota'
        , 'Northwest Territories'
        , 'Nova Scotia'
        , 'Nunavut'
        , 'Ohio'
        , 'Oklahoma'
        , 'Ontario'
        , 'Oregon'
        , 'Palau'
        , 'Pennsylvania'
        , 'Prince Edward Island'
        , 'Puerto Rico'
        , 'Quebec'
        , 'Rhode Island'
        , 'Saskatchewan'
        , 'South Carolina'
        , 'South Dakota'
        , 'Tennessee'
        , 'Texas'
        , 'US Virgin Islands'
        , 'Utah'
        , 'Vermont'
        , 'Virginia'
        , 'Washington'
        , 'West Virginia'
        , 'Wisconsin'
        , 'Wyoming'
        , 'Yukon'
    ];
    constructor(
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public http: Http,
        private formBuilder: FormBuilder,
        public authService: AuthService,
        public actionSheetCtrl: ActionSheetController,
        public platform: Platform,
        public toastCtrl: ToastController,
        public navParams: NavParams,
        public modalCtrl: ModalController,
        private transfer: Transfer,
        private file: File,
        public zone: NgZone,
        public forecastService: ForecastService,
        public databaseService: DatabaseService,
        public utilService: UtilService
    ) {
        this.getAllMyCard();
        this.PersonInfo = navParams.get('PersonInfo');
        this.ProdList = navParams.get('Productlist');
        console.log('test', this.PersonInfo, this.ProdList);
        this.ConfirmForm = this.formBuilder.group({
            'FirstName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
            'MiddleName': ['', Validators.compose([Validators.minLength(1), Validators.maxLength(1), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
            'LastName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
            'Email': ['', Validators.compose([Validators.pattern('[a-zA-Z0-9._%+-]+@[a-z0-9.-]+[.]{1}[a-zA-Z]{2,}$')])],
            'PhoneNumber': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9][0-9 ]*')])],
            'StrtAddr1': ['', Validators.compose([Validators.required])],
            'StrtAddr2': [''],
            'State': ['', Validators.compose([Validators.required])],
            'City': ['', Validators.compose([Validators.required])],
            'Countryoption': ['', Validators.compose([Validators.required])],
            'ZipCode': ['', Validators.compose([Validators.required, Validators.minLength(5), Validators.maxLength(5), Validators.pattern('[0-9][0-9 ]*')])],
            'Gender': [''],
            'LicenState': ['', Validators.compose([Validators.required])],
            'LicenseNo': [''],
            'Month': ['', Validators.compose([Validators.required])],
            'Year': ['', Validators.compose([Validators.required])]
        });
        this.FirstName = this.ConfirmForm.controls['FirstName'];
        this.MiddleName = this.ConfirmForm.controls['MiddleName'];
        this.LastName = this.ConfirmForm.controls['LastName'];
        this.Email = this.ConfirmForm.controls['Email'];
        this.StrtAddr1 = this.ConfirmForm.controls['StrtAddr1'];
        this.StrtAddr2 = this.ConfirmForm.controls['StrtAddr2'];
        this.State = this.ConfirmForm.controls['State'];
        this.City = this.ConfirmForm.controls['City'];
        this.Countryoption = this.ConfirmForm.controls['Countryoption'];
        this.ZipCode = this.ConfirmForm.controls['ZipCode'];
        this.Gender = this.ConfirmForm.controls['Gender'];
        this.PhoneNumber = this.ConfirmForm.controls['PhoneNumber'];
        this.LicenState = this.ConfirmForm.controls['LicenState'];
        this.LicenseNo = this.ConfirmForm.controls['LicenseNo'];
        this.Month = this.ConfirmForm.controls['Month'];
        this.Year = this.ConfirmForm.controls['Year'];

        this.http.get('assets/data/locations.json').subscribe(data => {
            var json = JSON.parse(data.text());
            this.countries = json.locations;
        });
        this.getProfileDetails();
        this.ResidencyMnthEnabled = true;
    }

    getAllMyCard() {
        console.log('call');
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3500
        });
        loading.present();
        this.authService.Paymentlist = [];
        this.authService.getAllStripePost().subscribe(res => {
            console.log('res', res);
            loading.dismiss();
            if (res.success) {
                if (res.data.data.length !== 0) {
                    for (let item of res.data.data) {
                        item.checked = false;
                        this.authService.Paymentlist.push(item);
                    }
                    this.authService.Paymentlist[0].checked = true;
                    this.authService.DataNA = false;
                    console.log('data-->', this.authService.Paymentlist);
                } else {
                    this.authService.DataNA = true;
                    this.modalCtrl.create('PaymentPage').present();
                }
            } else {
                this.authService.DataNA = true;
                //this.modalCtrl.create('PaymentPage').present();
            }
        });
    }


    onLocationChange() {
        var testname = this.ConfirmForm.controls['Countryoption'].value;
        console.log(testname);
        this.CountryName = testname.name;
        this.Countrycode = testname.dial_code;

    }

    getProfileDetails() {
        this.userDetails = this.PersonInfo;
        this.firstName = this.userDetails.FirstName;
        this.MdleName = this.userDetails.MiddleName;
        this.lastName = this.userDetails.LastName;
        this.State_ = this.userDetails.LicenState;
        // this.phoneNo = this.userDetails.phoneNo;
        // this.email = this.userDetails.email;
        // this.StreetAddr1 = this.userDetails.StreetAddr1;
        // this.StreetAddr2 = this.userDetails.StreetAddr2;
        // this.City_ = this.userDetails.City;
        this.Country = this.userDetails.Country;
        this.Zip_Code = this.userDetails.ZipCode;
        this.Mnth = this.userDetails.Month;
        this.YearRes = this.userDetails.Year;
        this.Licen_State = this.userDetails.LicenState;
        this.License_No = this.userDetails.LicenseNo;
        this.Suffix = this.userDetails.Suffix;
        this.Residency = this.userDetails.Residency;
        this.Gender = this.userDetails.Gender;
        this.DOB = moment(this.userDetails.DOB).format('L');
        // moment(this.userDetails.DOB).format('mm/dd/yyyy');
        this.FishingType = this.userDetails.FishingType;
        this.Genderbind = this.userDetails.Gender;
        let i = 0;
        for (let item of this.countries) {
            if (item.name == this.Country) {
                this.UserPhonecode = item;
                this.Countrycode = item.dial_code;
                this.CountryName = item.name;
            }
            else {
                // console.log("data not found");
            }

        }
    }
    licenError: boolean = false;
    StateError: boolean = false;
    CityError: boolean = false;
    confirm() {
        if (this.ConfirmForm.controls['LicenState'].value != "None" && this.ConfirmForm.controls['LicenseNo'].value == "") {
            this.licenError = true;
        } else {
            this.licenError = false;
        }
        if (this.ConfirmForm.controls['State'].value == "") {
            this.StateError = true;
        } else if (this.ConfirmForm.controls['Countryoption'].value == "" || this.ConfirmForm.controls['Countryoption'].value == undefined) {
            this.CityError = true;
            this.StateError = false;
        } else {
            this.CityError = false;
        }
        if (this.ConfirmForm.valid) {
            let alert = this.alertCtrl.create({
                message: 'I certify that to the best of my knowledge the information provided is true and correct.',
                buttons: [
                    {
                        text: 'Cancel',
                        role: 'cancel',
                        handler: () => {
                            console.log('Cancel clicked');
                        }
                    },
                    {
                        text: 'Ok',
                        handler: (data) => {
                            //  this.save();
                            this.paylicense();
                        }
                    }
                ]
            });
            alert.present();
        }
    }


    paylicense() {
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<div>Fishing for data. Please wait...</div>`,
            //duration: 3500
        });
        loading.present();
        var mobNo = this.ConfirmForm.controls['PhoneNumber'].value;
        var PhCode = this.Countrycode;
        var resident_sport_fishing_license = '';
        var resident_sport_fishing_license_amount = '';
        var resident_sport_fishing_license_title = '';
        var resident_king_salmon_stamp = '';
        var resident_king_salmon_stamp_amount = '';
        this.ProdList.forEach(element => {
            if (element.title == "Sport Fishing License") {
                resident_sport_fishing_license = element.Checked;
                resident_sport_fishing_license_amount = element.Price;
                resident_sport_fishing_license_title = element.title;
            } else if (element.title == "King Salmon Stamp") {
                resident_king_salmon_stamp = element.Checked;
                resident_king_salmon_stamp_amount = element.Price;
                //  resident_king_salmon_stamp_amount = element.Price;
            }

        });


        let data = {
            firstname: this.ConfirmForm.controls['FirstName'].value,
            middleinitial: this.ConfirmForm.controls['MiddleName'].value,
            lastname: this.ConfirmForm.controls['LastName'].value,
            suffix: this.Suffix,
            dateofbirth: this.DOB,
            gender: this.ConfirmForm.controls['Gender'].value,
            email: this.ConfirmForm.controls['Email'].value,
            mailingaddress_addressline_1: this.ConfirmForm.controls['StrtAddr1'].value,
            mailingaddress_addressline_2: this.ConfirmForm.controls['StrtAddr2'].value,
            mailingaddress_stateprovince: 'Alaska',
            // this.ConfirmForm.controls['State'].value,
            mailingaddress_city: this.ConfirmForm.controls['City'].value,
            mailingaddress_country: this.CountryName,
            mailingaddress_zipcode: this.ConfirmForm.controls['ZipCode'].value,
            phone: mobNo,
            driverslicensestate: this.ConfirmForm.controls['LicenState'].value,
            driverslicensenumber: this.ConfirmForm.controls['LicenseNo'].value,
            residency: this.Residency,
            residencystartmonth: this.ConfirmForm.controls['Month'].value,
            residencystartyear: this.ConfirmForm.controls['Year'].value,
            checkphysicalsameasmailing: true,
            user_id: localStorage.getItem('userId'),
            resident_sport_fishing_license: resident_sport_fishing_license,
            resident_sport_fishing_license_amount: resident_sport_fishing_license_amount,
            resident_sport_fishing_license_title: resident_sport_fishing_license_title,
            status: 'active'
        }
        this.authService.addfishingLicen(data).subscribe(res => {
            loading.dismiss();
            if (res.success) {
                this.save(res.result._id);
            } else {
                this.presentToast('Please add Payment card.');
                this.navCtrl.push('PaymentlistPage');
            }
        });
    }


    private presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 2500,
            position: 'bottom'
        });
        toast.present();
    }

    save(id: any) {
        // let loading = this.loadingCtrl.create({
        //     spinner: 'hide',
        //     content: `<div>Please wait! The selected Licenses are generated.This process may take several minutes to complete.</div><br><div></div>`,
        //     //duration: 3500
        // });
        // loading.present();
        this.progressBarEnabled = true;
        let data = {
            _id: id
        }
        console.log(data);
        setInterval(() => {
            if (this.progress < 100) {
                this.progress += 1;
            } else {
                clearInterval(this.progress);
                this.progressBarEnabled = false;
            }
        }, 1200);
        this.authService.fishingLicen(data).subscribe(res => {
            // loading.dismiss();
            // console.log(JSON.stringify(res));
            // alert('license Details' + JSON.stringify(res));
            if (res.success) {
                this.progressBarEnabled = false;
                var license_number = res.license_number.split(/(\d+)/);
                var pdfUrl = {
                    title: 'Fishing License Details',
                    displayText: license_number[1],
                    parent_Name: 'fishingLicense',
                    pdfURL: res.license_url
                }
                // alert('license Details' + JSON.stringify(pdfUrl));
                //console.log('license Details' + JSON.stringify(pdfUrl));
                // this.platformcheck(pdfUrl);
                let alertct = this.alertCtrl.create({
                    //title: 'Exit?',
                    message: 'License successfully created.',
                    buttons: [
                        {
                            text: 'OK',
                            role: 'OK',
                            handler: () => {
                                alertct = null;
                                this.navCtrl.push('LicenpdfViewpage', { data: pdfUrl, fishingLicen: true });
                            }
                        }
                    ]
                });
                alertct.present();
            } else {
                this.progress = 0;
                this.progressBarEnabled = false;
                let alertct = this.alertCtrl.create({
                    //title: 'Exit?',
                    message: res.message,
                    buttons: [
                        {
                            text: 'OK',
                            role: 'OK',
                            handler: () => {
                                alertct = null;
                                // this.navCtrl.push('LicenpdfViewpage', { data: pdfUrl, fishingLicen: true });
                            }
                        }
                    ]
                });
                alertct.present();
            }
        });

    }


    platformcheck(data: any) {
        console.log('Fishplatformcheck', JSON.stringify(data));
        this.platform.ready().then(() => {
            // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
            if (!this.platform.is('cordova')) {
                return false;
            }

            if (this.platform.is('ios')) {
                this.storageDirectory = this.file.documentsDirectory;
                this.downloadImage(data);
                //this.retrieveImage();
            }
            else if (this.platform.is('android')) {
                this.storageDirectory = this.file.dataDirectory;
                this.downloadImage(data);
                // this.retrieveImage();
            }
            else {
                // exit otherwise, but you could add further types here e.g. Windows
                return false;
            }
        });
    }

    downloadImage(element: any) {
        this.platform.ready().then(() => {
            const fileTransfer: TransferObject = this.transfer.create();
            fileTransfer.download(element.pdfURL, this.storageDirectory + element.displayText + '.pdf').then((entry) => {
                this.file.readAsDataURL(this.storageDirectory, element.displayText + '.pdf').then(result => {
                    this.dbStoreregualtions(result, element);
                })
            }, (error) => {
                //alert(JSON.stringify(error));
            });
        });
    }


    dbStoreregualtions(result: any, element: any) {
        this.databaseService.addRegulations_Data(result, element);
    }

}
