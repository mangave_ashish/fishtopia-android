import { NgModule ,CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PrimeOfferDetailPage } from './PrimeOffer-details';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    PrimeOfferDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(PrimeOfferDetailPage),
    SharedModule    
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class PrimeOfferDetailPageModule {}
