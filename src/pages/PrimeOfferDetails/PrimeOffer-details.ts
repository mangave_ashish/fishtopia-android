import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import moment from 'moment';
import { AuthService } from '../../providers/auth-provider';


/**
 * Generated class for the EventDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-PrimeOffer-Details',
  templateUrl: 'PrimeOffer-Details.html',
})
export class PrimeOfferDetailPage {
	eventItem: any;
	eventDay: any;
	eventDate: string;
  constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService) {
    this.eventItem = navParams.get('event');
    console.log('this.eventItem',this.eventItem);
   // this.eventDay=(new Date(this.eventItem.event_date).getDate()).toString();
   var date = moment(this.eventItem.coupon_expiry_date);
   this.eventDay = date.date();
    console.log('this.eventDay',this.eventDay);
  	// this.eventDay = this.eventItem.event_date_time;
  	// this.eventDate = this.eventItem.event_date_timeevent_date_time.slice(3);
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad EventDetailPage');
  }

}
