import { Component } from '@angular/core';
import { IonicPage, Platform, NavController, ModalController, NavParams, ToastController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { SocialSharing } from '@ionic-native/social-sharing';
import { FirebaseDynamicLinks } from '@ionic-native/firebase-dynamic-links/ngx';

/**
 * Generated class for the GuideDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-Invite_referral',
	templateUrl: 'Invite_referral.html',
})
export class Invite_referralPage {
	guideReviewList: any;
	// firstGuideReviewItem: any;
	UserDetails: any;
	Review: any;
	userReviewDetails: any;
	firstGuideReviewItem: any;
	image_url: any;
	GuideDetailsData: any;
	userDetails: any;
	UserType: any;
	MembershipText: any;
	Title: any;
	Amount: any;
	OffersData: any[] = [];
	month: string;
	mainEventList: Event[] = [];
	mainEventFillterList: any[] = [];
	EventType: any[] = [];
	dateList: Date[] = [];
	IPushMessage: any[];
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	selectedDay: any;
	selectedMonth: any;
	tmwDate: any;
	YearMonth: any;
	SelectDate: any;
	referral_code: any;
	shareURL: string;
	constructor(
		public toastCtrl: ToastController,
		public alertCtrl: AlertController,
		public menuCtrl: MenuController,
		public modalCtrl: ModalController,
		public navCtrl: NavController,
		public navParams: NavParams,
		public authService: AuthService,
		private socialSharing: SocialSharing,
		public platform: Platform,
		public firebaseDynamicLinks: FirebaseDynamicLinks
	) {
		this.menuCtrl.swipeEnable(false);
		this.referral_code = localStorage.getItem('referral_code');
		// if (this.platform.is('ios')) {
		// 	this.shareURL = "https://itunes.apple.com/us/app/alaska-fishtopia/id1384360605#?platform=iphone";
		// }
		// else if (this.platform.is('android')) {
		// 	this.shareURL = "https://play.google.com/store/apps/details?id=com.FishTopia.alaskaFishTopia";
		// }
		//   else {
		// 	return false;
		//   }
	}


	referralShare() {
		// this.firebaseDynamicLinks.onDynamicLink().subscribe((res: any) =>
		// 	alert(JSON.stringify(res)),
		// 	// console.log(res), 
		// 	(error: any) => alert(JSON.stringify(error))
		// );
		this.socialSharing.share("I cannot think of going fishing in Alaska without this app. You should try it too. Download it from here: http://bit.ly/AKFishTopia #" + this.referral_code + ".", "I cannot think of going fishing in Alaska without this app. You should try it too. Download it from here: http://bit.ly/AKFishTopia #" + this.referral_code + ".", "").then(() => {
			// alert("Success");
		}).catch(() => {
			// alert("Error");
		});
	}


	Couponlist(Page: any) {
		var data = {
			page: Page
		}
		this.authService.getCouponlistData(data).subscribe(res => {
			// this.perPage = this.authService.perPage;
			// this.totalData = this.authService.totalData;
			// this.totalPage = this.authService.totalPage;
			if (res.success) {
				for (let item of res.data.items.docs) {
					this.OffersData.push(item);
				}
			}
		});
	}

	ionViewWillLeave() {
		this.menuCtrl.swipeEnable(true);
	}

}
