import { GuestUserErrorPage } from './Guest_User_Error';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module';


@NgModule({
  declarations: [
    GuestUserErrorPage,
  ],
  imports: [
    IonicPageModule.forChild(GuestUserErrorPage),
    SharedModule
  ],
  exports: [
    GuestUserErrorPage,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

export class GuestUserErrorPageModule { }
