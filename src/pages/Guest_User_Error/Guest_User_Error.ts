import { Component } from '@angular/core';
import { IonicPage, NavController, MenuController, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AuthService } from '../../providers/auth-provider';

import * as _ from 'lodash';
import moment from 'moment';
import { UserData } from '../../providers/user-data-local';
@IonicPage()
@Component({
    selector: 'Guest_User_Error',
    templateUrl: 'Guest_User_Error.html'
})
export class GuestUserErrorPage {
    constructor(public platform: Platform,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public menuCtrl: MenuController,
        public authService: AuthService,
        public userData: UserData,

    ) {
    }

    guestLogout() {
        this.menuCtrl.close();
        this.authService.UserType = null;
        localStorage.removeItem('UserType');
        localStorage.removeItem('UserName');
        localStorage.removeItem('Profileimg');
        localStorage.removeItem('userId');
        localStorage.removeItem('is_featured');
        localStorage.removeItem('fcmToken');
        localStorage.setItem("old_token", localStorage.getItem("token"));
        localStorage.removeItem('token');
        this.userData.removeAuthToken();
        this.navCtrl.setRoot('LoginListPage');
        this.authService.logout(localStorage.getItem("token")).subscribe(
            res => this.logoutSuccessfully(res),
            error => error
        )
    }

    logoutSuccessfully(res) {
        localStorage.removeItem('token');
        this.userData.removeAuthToken();
        localStorage.removeItem('old_token');

    }
}
