import { Component, ViewChild, OnInit } from '@angular/core';
import { IonicPage, Slides, NavController, NavParams } from 'ionic-angular';
import { AuthService, WHEvent, FishingExchngEvent } from '../../providers/auth-provider';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

/**
 * Generated class for the WhatHotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-what-hot',
	templateUrl: 'what-hot.html',
})
@SideMenuDisplayText('What’s Hot?')

export class WhatHotPage implements OnInit {
	@ViewChild('slider') slider: Slides;
	topEventListing: any[];
	topTripListing: any[];
	fishingNewslist: any[];
	drawerOptions: any;
	eventList: WHEvent[] = [];
	TmweventList: WHEvent[] = [];
	eventSource;
	viewTitle;
	monthNames: any = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];

	isToday: boolean;
	lockSwipeToPrev: boolean;
	month: string;
	mainEventList: WHEvent[] = [];
	dateList: Date[] = [];
	IPushMessage: any[];
	showPageView: boolean = false;
	selectedDay: any;
	selectedMonth: any;
	tmwDate: any;
	YearMonth: any;
	SelectDate: any;



	tripList: any[];
	tripsOption: any;
	isenabled: any;
	displayGuide: boolean;
	UserType: any;
	tripsOptionUser: any;
	tripsOptionGuide: any;
	eventFishingExchngList: FishingExchngEvent[] = [];
	isenabledAcceptDeny: boolean;
	isenabledAccept: boolean;
	isenabledDeny: boolean;
	mainEventFishingExchngList: FishingExchngEvent[] = [];
	dateFishingExchngList: Date[] = [];
	tripsOptionUserType1: any;
	tripsOptionGuideType1: any;
	img: any;
	constructor(public navCtrl: NavController, public navParams: NavParams, public authService: AuthService) {
		this.img = "assets/img/slides/fishing.jpg";
		var v = new Date();
		this.selectedDay = ("0" + (v.getDate())).slice(-2);
		this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
		this.YearMonth = v.getFullYear();
		this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
		//this.getFishNewsList();
		this.getEventDetails();
		console.log('this.SelectDate', this.SelectDate);
		this.tmwDate = "tmw";
		this.drawerOptions = {
			handleHeight: 50,
			thresholdFromBottom: 200,
			thresholdFromTop: 200,
			bounceBack: true
		};
	}
	ngOnInit() {
		this.UserType = localStorage.getItem("UserType");
		console.log("this.UserType", this.UserType);
		if (this.UserType == "guide") {
			this.displayGuide = true;
			this.tripsOptionGuide = "Wanted";
			this.getEventFishingExchgDetails('wanted');
			this.isenabledAcceptDeny = false;
		}
		else {
			this.displayGuide = false;
			this.tripsOptionUser = "Available";
			this.getEventFishingExchgDetails('available');

		}
	}
	tripAccept(value: FishingExchngEvent, i: any) {
		console.log(status);
		var data = {
			status: "accept"
		}
		this.authService.addTripStatus(data, value._id).subscribe(res => {
			this.getEventFishingExchgDetails('wanted');
			console.log(res);
			// this.isenabledAcceptDeny=true;
			// this.isenabledAccept=false;

		});
	}
	tripReject(value: FishingExchngEvent, i: any) {
		console.log(status);
		var data = {
			status: "deny"
		}
		this.authService.addTripStatus(data, value._id).subscribe(res => {
			this.getEventFishingExchgDetails('wanted');
			console.log(res);
			//    this.isenabledAcceptDeny=true;
			// 	this.isenabledDeny=false;

		});

	}

	getEventFishingExchgDetails(type) {
		console.log('type', type);
		if (type == "offered") {
			this.isenabledAcceptDeny = true;
		} else {
			this.isenabledAcceptDeny = false;
		}
		var data = {
			page: "string",
			list_type: type
		}
		//	}
		console.log('data', data);
		this.authService.getFishingExchngeDetail(data).subscribe(res => {
			let datefishexchg: any[] = [];
			this.mainEventFishingExchngList = [];
			this.dateFishingExchngList = [];
			for (let d of res) {
				try {
					//find out unique date
					if (datefishexchg.indexOf(new Date(d.date).getDate()) == -1) {
						if (this.SelectDate <= d.date) {
							datefishexchg.push(new Date(d.date).getDate());
							this.dateFishingExchngList.push(d.date);
						}
					}

				} catch (e) { }
			}
			console.log('dateFishingExchngList', this.dateFishingExchngList);
			this.dateFishingExchngList.sort(function (a, b) {
				// Turn your strings into dates, and then subtract them
				// to get a value that is either negative, positive, or zero.
				if (a > b) return 1;
				if (a < b) return -1;
				return 0;
			});
			this.mainEventFishingExchngList = res;
			console.log('mainEventFishingExchngList', this.mainEventFishingExchngList);
			if (this.mainEventFishingExchngList.length > 0) {
				//this.currentDay = date[0];
			}
			//  this.filterFishingExchgEvent();
			// this.NextDayfilterEvent();
			//this.eventList = res;
			// console.log('getEventByDate-->', res);
		});
		// }
	}

	openTripDetail(item) {
		this.navCtrl.push('FishingExchangDetailPage', { trip: item });
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad WhatHotPage');
	}

	ngAfterViewInit() {
		this.slider.freeMode = true;
	}

	saveFavorite(item) {
		item.isFavorite = !item.isFavorite;
	}
	getFishNewsList() {
		var token = localStorage.getItem("token");
		var data = {}
		this.authService.getFishNews(data).subscribe(res => {
			this.fishingNewslist = res;
			console.log('this.userDetails', this.fishingNewslist);
		});
	}
	getEventDetails() {
		this.dateList = [];
		var data = {
			sort: "desc"
		}
		this.authService.getWHEventByDate(data).subscribe(res => {
			let date: any[] = [];
			for (let d of res) {
				try {
					//find out unique date
					if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
						if (this.SelectDate <= d.event_date) {
							date.push(new Date(d.event_date).getDate());
							this.dateList.push(d.event_date);
						}
					}
				} catch (e) { }
			}
			console.log('dateList', this.dateList);
			this.dateList.sort(function (a, b) {
				// Turn your strings into dates, and then subtract them
				// to get a value that is either negative, positive, or zero.
				if (a > b) return 1;
				if (a < b) return -1;
				return 0;
			});
			this.mainEventList = res;
			console.log('mainEventList', this.mainEventList);
			if (this.mainEventList.length > 0) {
				//this.currentDay = date[0];
			}
			this.filterEvent();
		});
		// }
	}
	filterEvent() {
		try {
			if (this.mainEventList.length > 0) {

				// let dt: Date = new Date();
				var d = new Date();
				let today: Date = new Date(d);
				for (let mel of this.mainEventList) {
					if (today.toDateString() == new Date(mel.event_date).toDateString())
						this.eventList.push(mel);
				}
				console.log(this.eventList);
			}
		}
		catch (e) {
			console.log(e);
		}
	}

	addTofavourit(value: WHEvent) {
		var data = {
			event_id: value._id,
			status: !value.favouriteStatus
		};
		this.authService.addToFavouriteEvent(data).subscribe(res => {
			// added as a favourite.
			console.log('res', res);
			if (res.success) {
				value.favouriteStatus = !value.favouriteStatus;
				if (!value.favouriteStatus)
					value.likes -= 1;
				else value.likes += 1;
			}
			//let toast = this.toast.create({
			//    message: res.message,
			//    duration: 3000,
			//    position: 'bottom'
			//}).present();
			// console.log(res);
		});
	}
	AddWhatsHot() {
		this.navCtrl.push('AddWhatsHotPage');
	}
	openEventDetail(item) {
		this.navCtrl.push('EventDetailPage', { event: item });
	}

	Notification() {
        this.navCtrl.push('NotificationsPage');
    }

}
