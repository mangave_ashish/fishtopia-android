import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WhatHotPage } from './what-hot';

@NgModule({
  declarations: [
    WhatHotPage,
  ],
  imports: [
    IonicPageModule.forChild(WhatHotPage),
  ],
})
export class WhatHotPageModule {}
