import { Component, Input } from '@angular/core';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, Platform, PopoverController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
    selector: 'page-pdfView',
    templateUrl: 'pdfView.html'
})
export class pdfViewpage {
    @Input('progress') progress: number = 0;
    push: any;
    socket: any;
    chat_input: string;
    pdfUrl: any;
    pdfUrlSrc: string = '';
    storageDirectory: string = '';
    fishinglicen: boolean = false;
    progressBarEnabled: boolean = false;
    dynMargin: number = 0;
    constructor(
        public loadingCtrl: LoadingController,
        public navParams: NavParams,
        public platform: Platform,
        public navCtrl: NavController,
        public popoverCtrl: PopoverController,
        public authService: AuthService,
        public alertCtrl: AlertController,
        private transfer: Transfer, private file: File,
    ) {
        this.pdfUrl = this.navParams.get('data');
        this.platform.ready().then(() => {
            // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
            if (!this.platform.is('cordova')) {
                return false;
            }
            if (this.platform.is('ios')) {
                this.storageDirectory = this.file.documentsDirectory;
                this.checkFile();
            }
            else if (this.platform.is('android')) {
                this.storageDirectory = this.file.externalDataDirectory + 'Download/';
                //alert(JSON.stringify(this.storageDirectory))

                this.checkFile();
                // this.storageDirectory = this.file.dataDirectory;
            }
            else {
                // exit otherwise, but you could add further types here e.g. Windows
                return false;
            }
        });
    }

    checkFile() {
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3000
        });
        loading.present();
        this.file.checkFile(this.storageDirectory, this.pdfUrl.displayText + '.jpg')
            .then((result) => {
                if (result) {
                    //this.pdfUrlSrc = this.storageDirectory + this.pdfUrl.displayText + '.jpg';
                    this.file.readAsDataURL(this.storageDirectory, this.pdfUrl.displayText + '.jpg').then(result => {
                        loading.dismiss();
                       // alert('result main : ' + JSON.stringify(result))

                        this.pdfUrlSrc = result;
                    }, (error) => {
                        //alert('error3 : ' + JSON.stringify(error))

                        this.downloadPDF();
                        console.log(error);
                    }).catch(function (err: TypeError) {
//alert('catch : ' + err.message)

                        console.log(err.message);
                    });
                }
            }, (error) => {
              //  alert('error2 : ' + JSON.stringify(error))

                this.downloadPDF();

                console.log('error : ' + JSON.stringify(error));
            });
    }

    loadedPdf: any;
    totalDwn: any;
    downloadPDF() {
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 5000
        });
        loading.present();
        this.progressBarEnabled = true;
        this.platform.ready().then(() => {
            const fileTransfer: TransferObject = this.transfer.create();
            fileTransfer.download(this.pdfUrl.pdfURL, this.storageDirectory + this.pdfUrl.displayText + '.jpg').then((entry) => {
                this.file.readAsDataURL(this.storageDirectory, this.pdfUrl.displayText + '.jpg').then(result => {
                    loading.dismiss();
                  //  alert('result second : ' + JSON.stringify(result))

                    this.pdfUrlSrc = result;
                    // this.dbStoreregualtions(result);
                }, (error) => {
                   // alert('error4 : ' + JSON.stringify(error))

                    // alert('download' + JSON.stringify(error));
                    console.log(error);
                });
            }, (error) => {
                // console.log(error);
                //alert('error5 : ' + JSON.stringify(error))

                this.pdfUrlSrc = this.pdfUrl.pdfURL;
                // alert('download' + JSON.stringify(error));
            });
            let a = fileTransfer.onProgress((progressEvent: ProgressEvent) => {
                console.log(JSON.stringify(progressEvent) + JSON.stringify(a));
                if (progressEvent.lengthComputable) {
                    this.loadedPdf = progressEvent.loaded;
                    this.totalDwn = progressEvent.total;
                    this.progress = Math.floor(progressEvent.loaded / progressEvent.total * 100);
                    console.log(JSON.stringify(this.progress));
                    if (this.progress < 100) {
                        this.progress += 1;
                    } else {
                        clearInterval(this.progress);
                        this.progressBarEnabled = false;
                    }
                } else {
                    console.log(JSON.stringify(this.progress));

                }
            });
        });
    }
}
