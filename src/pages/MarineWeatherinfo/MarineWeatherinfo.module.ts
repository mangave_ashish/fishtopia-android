import { NgModule,CUSTOM_ELEMENTS_SCHEMA  } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MarineWeatherinfoPage } from './MarineWeatherinfo';
@NgModule({
  declarations: [
    MarineWeatherinfoPage,   
  ],
  imports: [
    IonicPageModule.forChild(MarineWeatherinfoPage)
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class MarineWeatherinfoPageModule {}
