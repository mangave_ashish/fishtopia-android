import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { AlertController, IonicPage, LoadingController, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
@IonicPage({
  priority: 'high'
})
@Component({
  selector: 'page-MarineWeatherinfo',
  templateUrl: 'MarineWeatherinfo.html',
})
export class MarineWeatherinfoPage {
  stars: any;
  reviews: '4.12 (78 reviews)';
  reviewForm: FormGroup;
  message: AbstractControl;
  rate: AbstractControl;
  messagepost: any = "";
  htmlDesp: any;
  MarineWeatherDetails: any;
  DataNA: Boolean = false;
  htmlUpdateDesp: any;
  LastUpdate: any;
  constructor(public loadingCtrl: LoadingController, public viewCtrl: ViewController, public alertCtrl: AlertController, public authService: AuthService, public navCtrl: NavController, public formBuilder: FormBuilder, public navParams: NavParams) {
    this.MarineWeatherDetails = navParams.get('MarineWeatherDetails');
    console.log('ionViewDidLoad GuideReviewAddPage', this.MarineWeatherDetails);
    this.getMarineinfo();
  }

  getMarineinfo() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 5500
    });
    loading.present();
    this.authService.getMarineDetails(this.MarineWeatherDetails.id).subscribe(res => {
      loading.dismiss();
      let parser = new DOMParser();

      if (res.data.html != undefined) {
        this.DataNA = false;
        this.htmlDesp = res.data.html;
        this.htmlUpdateDesp = res.data.updated_time_html;
        let parsedHtml = parser.parseFromString(this.htmlUpdateDesp, 'text/html');
        console.log('data-->', res.data.html);
        var t = parsedHtml.getElementsByTagName("tr")[0].children;
        var d = t[1].innerHTML;
        this.LastUpdate = d;
        console.log(d);

      } else {
        this.DataNA = true;
      }
    });
  }
  //}

  dismiss() {
    this.viewCtrl.dismiss();
  }
}


