import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService, FishNewsList } from '../../providers/auth-provider';
import moment from 'moment';

/**
 * Generated class for the FishingExchangDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-fishing-exchang-detail-angler',
	templateUrl: 'fishing-exchang-detail-angler.html',
})
export class FishingExchangDetailAnglerPage {
	tripItem: any;
	tripDay: any;
	tripDate: string;
	img: any;
	UserData: any;
	TripData: any;
	datereq: any;
	member: any;
	tripAmt: any;
	Triptime: any;
	TripPay: any[] = [];
	imgUrl: any;
	constructor(public authService: AuthService, public navCtrl: NavController, public navParams: NavParams) {
		this.tripItem = navParams.get('trip');
		if (this.tripItem.date != undefined) {
			var date = moment(this.tripItem.date);
		}
		this.tripDay = date.date();
		if (this.tripItem.imgUrl != undefined) {
			this.imgUrl = this.tripItem.imgUrl;
		} else {
			this.imgUrl = undefined;
		}
		//this.tripDay=(new Date(this.tripItem.date).getDate()).toString();
		if (this.tripItem.angler_pay.length !== 0) {
			this.TripPay = this.tripItem.angler_pay;
		} else if (this.tripItem.guide_accept.length !== 0) {
			this.TripPay = this.tripItem.guide_accept;
		}

		this.member = this.tripItem.member;

		//	 this.TripPay=this.tripItem.angler_pay;
		//	this.tripDay = this.tripItem.date.slice(0, 6);
		this.tripDate = this.tripItem.date.slice(3);
		this.Triptime = this.dattime(this.tripItem.time);
		console.log('tripItem', this.tripItem);
		this.img = "assets/img/icon/Alaska.jpg";
		this.getGuideDetails(this.tripItem.created_by);
		// this.getTripDetails();
	}

	dattime(time) {

		// Check correct time format and split into components
		time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

		if (time.length > 1) { // If time format correct
			time = time.slice(1);  // Remove full string match value
			time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
			time[0] = +time[0] % 12 || 12; // Adjust hours
		}
		let Dtime = time[0] + time[1] + time[2] + time[5];
		console.log('time', time[0] + time[1] + time[2] + time[5]);
		return Dtime; // return adjusted time or original string

	}



	getGuideDetails(data: any) {
		this.authService.getUserData(data).subscribe(res => {
			this.UserData = res.data;
			// for(let item of res.data.items.docs) {
			// 	this.fishingNewslist.push(item);
			// }



			//		infiniteScroll.enable(false);
			console.log('this.userDetails', this.UserData);
		});
	}

	getTripDetails() {
		this.TripData = [];
		this.authService.getTripDetails('5aa90cacae256822d2445972').subscribe(res => {
			this.TripData = res.data;
			this.datereq = this.TripData.date_time;
			this.member = this.TripData.member;;
			this.tripAmt = this.TripData.trip_amount
			console.log('TripDatauserDetails', this.TripData + this.member);
			console.log('TripDatauserDetails', this.TripData.request);

		});
	}

	ionViewDidLoad() {
		console.log('ionViewDidLoad FishingExchangDetailPage');
	}

	openGuideDetail() {
		this.navCtrl.push('GuideDetailPage');
	}

}
