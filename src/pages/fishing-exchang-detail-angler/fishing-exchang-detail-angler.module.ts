import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishingExchangDetailAnglerPage } from './fishing-exchang-detail-angler';
import { SharedModule } from '../../app/shared.module';
@NgModule({
  declarations: [
    FishingExchangDetailAnglerPage,
  ],
  imports: [
    IonicPageModule.forChild(FishingExchangDetailAnglerPage),
    SharedModule
  ],
})
export class  FishingExchangDetailAnglerPageModule {}
