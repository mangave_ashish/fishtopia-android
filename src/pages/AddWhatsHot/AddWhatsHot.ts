import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
  selector: 'page-AddWhatsHot',
  templateUrl: 'AddWhatsHot.html',
})
export class AddWhatsHotPage {

  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public authService:AuthService
  ) { }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
