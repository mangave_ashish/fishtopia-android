import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AddWhatsHotPage } from './AddWhatsHot';
@NgModule({
  declarations: [
    AddWhatsHotPage,
  ],
  imports: [
    IonicPageModule.forChild(AddWhatsHotPage),
  ],
  exports: [
    AddWhatsHotPage
  ]
})
export class PopupFabModalPageModule { }
