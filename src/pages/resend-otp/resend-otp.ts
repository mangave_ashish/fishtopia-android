import { Component } from '@angular/core';
import { NavParams, ViewController, IonicPage, AlertController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
  selector: 'page-resend-otp',
  templateUrl: 'resend-otp.html'
})
export class resendotpPage {
  myParam: string;
  phoneNo: any;
  
  constructor(
    public viewCtrl: ViewController,
    params: NavParams,
    public auth: AuthService,
    public alertCtrl: AlertController
  ) {
    this.myParam = params.get('Userdata');
    this.phoneNo = params.get('phone');
    console.log('Userdata', this.myParam + this.phoneNo);
    // this.phoneNo='844656854';
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  sendOTP() {
    var data = {
      user_id: this.myParam,
      phone_no: this.phoneNo
    }
    console.log('data', data);
    this.auth.otpResend(data).subscribe(res => {
      // this.phoneNo=data.phone_no;
      // this.myParam=data.user_id;
      this.viewCtrl.dismiss();
      this.alertMsg();
    })
  }
  alertMsg() {
    let alert = this.alertCtrl.create({
      // title: 'Confirm Trip',
      message: "Confirmation code has been sent to your phone.",
      buttons: [{
        text: 'Ok',
        handler: (data) => {
          alert = null;
        }
      }
      ]
    });
    alert.present();
  }

}
