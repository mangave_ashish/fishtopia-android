import { Component } from '@angular/core';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { IonicPage, LoadingController, MenuController, NavController, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AuthService } from '../../providers/auth-provider';
@IonicPage()
@Component({
    selector: 'subscribe-details',
    templateUrl: 'subscribeDetails.html'
})

export class subscribeDetailsPage {
    public product: any = {
        name: 'Alaska FishTopia',
        appleProductId: 'com.FishTopia.alaskaFishTopia.oneyearauto',
        googleProductId: 'akfishtopiayearlysubscription'
    };
    productId: any;
    productDetails: any;
    purchase_date: any;
    expiry_date: any;
    public productPrice: any;

    constructor(public platform: Platform,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public menuCtrl: MenuController,
        public authService: AuthService,
        private iap: InAppPurchase,
        private loadingCtrl: LoadingController
    ) {
        var dat = localStorage.getItem('SubsDetails');
        console.log(dat)
        this.productDetails = JSON.parse(dat);
        let purchaseDate = JSON.parse(this.productDetails.purchaseDateMs);
        let expiresDate = JSON.parse(this.productDetails.expiresDateMs);
        this.purchase_date = new Date(purchaseDate);
        this.expiry_date = new Date(expiresDate);
        if (!this.platform.is('cordova')) { return };
        if (this.platform.is('ios')) {
            this.productId = this.product.appleProductId;

        } else if (this.platform.is('android')) {
            this.productId = this.product.googleProductId;
        }
        this.iap
            .getProducts([this.productId]).then((products: any) => {
                this.productPrice = products[0].price;
            }).catch(err => {
                let data = {
                    "description": "getProducts() in subscribedeatils",
                    "response": JSON.stringify(err)
                }
                this.authService.postLog(data).subscribe(res => {

                });
            });

    }
    getReceipt() {
        let loading = this.loadingCtrl.create({
            spinner: 'crescent',
            content: 'Get receipt details. <br> Please wait...',
            duration: 1000
        });
        loading.present();
        this.iap
            .getReceipt()
            .then((data: any) => {
                // alert('validate receipt data' + data)
                if (data) {
                    var receipt_data = {
                        "receipt_no": data
                    }
                    this.authService.iosSubIAP(receipt_data).subscribe(res => {
                        if (res.success) {
                            loading.dismiss();
                            // alert("sucess true" + res.data);
                            if (res.data.status && res.data.expiry_date != null) {
                                localStorage.setItem('SubsDetails', JSON.stringify(res));
                                this.menuCtrl.swipeEnable(false);
                                this.navCtrl.setRoot('LoginListPage');
                            }
                        }
                    })
                }
            }).catch((err) => {
                // alert("getReceiptFailed " + JSON.stringify(err))
                loading.dismiss();
                console.log(JSON.stringify(err));
            });
    }
    statusCheck() {
        this.iap
            .restorePurchases()
            .then((data: any) => {
                console.log('consumeError' + JSON.stringify(data));
                if (data.length > 0) {
                    for (var i = 0; i < data.length; ++i) {
                        if (data[i].productId == this.productId) {
                            this.menuCtrl.swipeEnable(false);
                            this.navCtrl.setRoot('LoginListPage');
                        }
                    }
                }
            })
    }
}
