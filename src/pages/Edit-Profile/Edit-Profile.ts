import { Component } from '@angular/core';
import { Platform, ToastController, AlertController, Loading, App, LoadingController, NavController, Slides, IonicPage, ActionSheetController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../providers/auth-provider';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject, FileUploadOptions } from '@ionic-native/transfer';
import { FilePath } from '@ionic-native/file-path';
import { Camera } from '@ionic-native/camera';
declare var cordova: any;

@IonicPage()
@Component({
    selector: 'Edit-Profile',
    templateUrl: 'Edit-Profile.html',
})
export class EditProfilePage {
    public loginForm: any;
    User: any;
    type = "password";
    Countrycode: any;
    CountryName: any;
    countries: any[] = [];
    PhoneCode: any;
    show = false;
    userDetails: any;
    EditProfileForm: FormGroup;
    FirstName: AbstractControl;
    MiddleName: AbstractControl;
    LastName: AbstractControl;
    Email: AbstractControl;
    ConfirmEmail: AbstractControl;
    Password: AbstractControl;
    StrtAddr1: AbstractControl;
    StrtAddr2: AbstractControl;
    State: AbstractControl;
    City: AbstractControl;
    Countryoption: AbstractControl;
    ZipCode: AbstractControl;
    PhoneNumber: AbstractControl;
    userType: AbstractControl;
    UserPhonecode: any;
    loading: Loading;
    //PhoneCode: AbstractControl;
    firstName: string;
    MdleName: string;
    lastName: string;
    phoneNo: any;
    email: string;
    StreetAddr1: string;
    StreetAddr2: string;
    State_: string;
    City_: string;
    Country: string;
    Zip_Code: string;
    UserType: string;
    //  Phone_Number:string;
    Profileimg: any;
    UserProfileImg: any;
    image_url: any;
    img: any;
    lastImage: string = null;
    imgData: any;
    Country1: string;
    countryname: any;
    constructor(
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public http: Http,
        private formBuilder: FormBuilder,
        public authService: AuthService,
        private file: File,
        private filePath: FilePath,
        private camera: Camera,
        private transfer: Transfer,
        public actionSheetCtrl: ActionSheetController,
        public platform: Platform,
        public toastCtrl: ToastController
    ) {
        this.getProfileDetails();
        this.EditProfileForm = this.formBuilder.group({
            'FirstName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
            'MiddleName': ['', Validators.compose([Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
            'LastName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
            'Email': ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-z0-9.-]+[.]{1}[a-zA-Z]{2,}$')])],
            'StrtAddr1': [''],
            //  'StrtAddr1': ['',Validators.required],
            'StrtAddr2': [''],
            'State': [''],
            'City': [''],
            'Countryoption': [''],
            'ZipCode': [''],
            'PhoneNumber': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9][0-9 ]*')])],
        });
        this.FirstName = this.EditProfileForm.controls['FirstName'];
        this.MiddleName = this.EditProfileForm.controls['MiddleName'];
        this.LastName = this.EditProfileForm.controls['LastName'];
        this.Email = this.EditProfileForm.controls['Email'];
        this.StrtAddr1 = this.EditProfileForm.controls['StrtAddr1'];
        this.StrtAddr2 = this.EditProfileForm.controls['StrtAddr2'];
        this.State = this.EditProfileForm.controls['State'];
        this.City = this.EditProfileForm.controls['City'];
        this.Countryoption = this.EditProfileForm.controls['Countryoption'];
        this.ZipCode = this.EditProfileForm.controls['ZipCode'];
        this.PhoneNumber = this.EditProfileForm.controls['PhoneNumber'];
        this.User = "angler";
        this.http.get('assets/data/locations.json').subscribe(data => {
            var json = JSON.parse(data.text());
            this.countries = json.locations;
            // console.log(this.countries);
        });

    }

    matchingPasswords(EmailKey: string, ConfirmEmailKey: string) {
        return (group: FormGroup): { [key: string]: any } => {
            let Email = group.controls[EmailKey];
            let ConfirmEmail = group.controls[ConfirmEmailKey];
            if (Email.value !== ConfirmEmail.value) {
                return {
                    mismatchedPasswords: true
                };
            }
        }
    }

    getProfileDetails() {
        this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
            this.userDetails = res;
            // console.log('this.userDetails', res);
            this.firstName = this.userDetails.firstName;
            this.MdleName = this.userDetails.MiddleName;
            this.lastName = this.userDetails.lastName;
            this.phoneNo = this.userDetails.phoneNo;
            this.email = this.userDetails.email;
            this.StreetAddr1 = this.userDetails.StreetAddr1;
            this.StreetAddr2 = this.userDetails.StreetAddr2;
            this.State_ = this.userDetails.State;
            this.City_ = this.userDetails.City;
            this.Country = this.userDetails.Country;
            this.Zip_Code = this.userDetails.ZipCode;
            this.UserType = this.userDetails.userType;
            this.Profileimg = this.userDetails.image_url;
            //  this.UserPhonecode=this.Country;
            // console.log(this.UserPhonecode);
            //this.Phone_Number= this.userDetails.firstName;
            let i = 0;
            for (let item of this.countries) {
                if (item.name == this.Country) {
                    //this.countryname = item;
                    this.UserPhonecode = item;
                    // console.log('hi', this.countryname);
                    // console.log('hillo', this.UserPhonecode);
                    this.Countrycode = item.dial_code;
                    this.CountryName = item.name;
                }
                else {
                    // console.log("data not found");
                }

            }
            this.authService.getProfileDetails();
        });
    }

    onLocationChange() {
        var testname = this.EditProfileForm.controls['Countryoption'].value;
        // console.log(testname.dial_code);
        this.CountryName = testname.name;
        this.Countrycode = testname.dial_code;

    }

    public presentActionSheet() {
        let actionSheet = this.actionSheetCtrl.create({
            title: 'Select Image Source',
            buttons: [
                {
                    text: 'Load from Library',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
                    }
                },
                {
                    text: 'Use Camera',
                    handler: () => {
                        this.takePicture(this.camera.PictureSourceType.CAMERA);
                    }
                },
                {
                    text: 'Cancel',
                    role: 'cancel'
                }
            ]
        });
        actionSheet.present();
    }
    public takePicture(sourceType) {

        // Create options for the Camera Dialog
        var options = {
            // quality: 100,
            // targetWidth: 91,
            // targetHeight: 91,
            allowEdit: true,
            sourceType: sourceType,
            saveToPhotoAlbum: true,
            correctOrientation: true
        };

        // Get the data of an image
        this.camera.getPicture(options).then((imagePath) => {
            this.imgData = imagePath;
            // Special handling for Android library
            if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
                this.filePath.resolveNativePath(imagePath)
                    .then(filePath => {
                        console.log('filePath', filePath);
                        let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
                        let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
                        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                        this.uploadImage();
                    });
            } else {
                var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
                var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
                this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
                this.uploadImage();
            }
        }, (err) => {
            this.presentToast('Error while selecting image.');
        });
    }
    // Create a new name for the image
    private createFileName() {
        var d = new Date(),
            n = d.getTime(),
            newFileName = n + ".jpg";
        return newFileName;
    }

    // Copy the image to a local folder
    private copyFileToLocalDir(namePath, currentName, newFileName) {
        this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
            this.lastImage = newFileName;
        }, error => {
            this.presentToast('Error while storing file.');
        });
    }

    private presentToast(text) {
        let toast = this.toastCtrl.create({
            message: text,
            duration: 1500,
            position: 'bottom'
        });
        toast.present();
    }

    // Always get the accurate path to your apps folder
    public pathForImage(img) {
        if (img === null) {
            return '';
        } else {
            return cordova.file.dataDirectory + img;
        }
    }

    public uploadImage() {

        // Destination URL
        var url = "http://166.62.118.179:2152/api/users/upload-profile-image";
        var targetPath = this.pathForImage(this.lastImage);
        var filename = this.lastImage;
        let options1: FileUploadOptions = {
            fileKey: "image",
            fileName: filename,
            chunkedMode: false,
            mimeType: "multipart/form-data",
            headers: {
                "access_token": localStorage.getItem("token")
            }
        }
        const fileTransfer: TransferObject = this.transfer.create();
        this.loading = this.loadingCtrl.create({
            content: 'Uploading...',
        });
        this.loading.present();
        fileTransfer.upload(this.imgData, 'http://166.62.118.179:2152/api/users/upload-profile-image', options1)
            .then((data) => {
                this.presentToast('Image uploaded Successfully.');
                this.getProfileDetails();
                this.loading.dismissAll();
            }, err => {
                this.presentToast('Error while uploading file.');
                this.loading.dismissAll();
            });
    }

    presentLoading(message) {
        const loading = this.loadingCtrl.create({
            duration: 500
        });

        loading.onDidDismiss(() => {
            const alert = this.alertCtrl.create({
                title: 'Success',
                subTitle: message,
                buttons: ['Dismiss']
            });
            alert.present();
        });

        loading.present();
    }

    login() {
        this.presentLoading('Thanks for signing up!');
        // this.navCtrl.push(HomePage);
    }

    signup() {
        // this.presentLoading('Thanks for signing up!');
        this.navCtrl.push('SignUpSuccessPage');
        // console.log('countries', this.countries);
    }
    resetPassword() {
        this.presentLoading('An e-mail was sent with your new password.');
    }
    toggleShow() {
        this.show = !this.show;
        if (this.show) {
            this.type = "text";
        }
        else {
            this.type = "password";
        }
    }

    save() {
        // this.uploadImage();
        var mobNo = this.EditProfileForm.controls['PhoneNumber'].value;
        var PhCode = this.Countrycode;
        let data = {
            first_name: this.EditProfileForm.controls['FirstName'].value,
            middle_name: this.EditProfileForm.controls['MiddleName'].value,
            last_name: this.EditProfileForm.controls['LastName'].value,
            email: this.EditProfileForm.controls['Email'].value,
            street_address1: this.EditProfileForm.controls['StrtAddr1'].value,
            street_address2: this.EditProfileForm.controls['StrtAddr2'].value,
            state: this.EditProfileForm.controls['State'].value,
            city: this.EditProfileForm.controls['City'].value,
            country: this.CountryName,
            zip_code: this.EditProfileForm.controls['ZipCode'].value,
            phone_no: mobNo,
            UserType: localStorage.getItem("UserType"),
        }
        // console.log('data-->', data);

        //  if (this.RegisterForm.valid) {
        //    this.presentLoading();

        this.authService.updateUsersProfile(data).subscribe(res => {
            // console.log('data-->', res);
            if (res.success) {
                this.authService.getProfileDetails();
                let alert = this.alertCtrl.create({
                    //title: 'Exit?',
                    message: 'Profile successfully updated.',
                    buttons: [
                        {
                            text: 'OK',
                            role: 'OK',
                            handler: () => {
                                //  this.alert = null
                            }
                        }
                    ]
                });
                alert.present();
                // console.log('Output for signupdata-->');
            }

        });
    }

    getProfileEditDetails() {
        // localStorage.removeItem('UserType'); 
        localStorage.removeItem('Profileimg');
        localStorage.removeItem('UserName');
        this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
            this.userDetails = res;
            // console.log('this.userDetails', res);
            // localStorage.setItem("UserType", this.userDetails.UserType);
            localStorage.setItem("Profileimg", this.userDetails.image_url);
            localStorage.setItem("UserName", this.userDetails.firstName + "   " + this.userDetails.lastName);
            // console.log('this.userDetails', localStorage.getItem("UserType"));
        });
    }

}
