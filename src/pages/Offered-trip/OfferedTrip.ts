// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { ActionSheetController, AlertController, App, IonicPage, Loading, LoadingController, MenuController, NavController, Platform, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-provider';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'OfferedTrip',
  templateUrl: 'OfferedTrip.html',
})
export class OfferedTripPage {
  public loginForm: any;
  User: any;
  // public backgroundImage = 'assets/img/background/background-6.jpg';
  type = "password";
  Countrycode: any;
  CountryName: any;
  countries: any[] = [];
  PhoneCode: any;
  show = false;
  userDetails: any;
  RequestTripForm: FormGroup;
  fishingtypes: AbstractControl;
  Locationtypes: AbstractControl;
  WillingPrice: AbstractControl;
  Person: AbstractControl;
  Boatypes: AbstractControl;
  Date: AbstractControl;
  Time: AbstractControl;
  Buinessinfo: AbstractControl;
  UserPhonecode: any;
  loading: Loading;
  startDatetimeMin: any = new Date(Date.now()).toISOString();
  Profileimg: any;
  UserProfileImg: any;
  image_url: any;
  img: any;
  lastImage: string = null;
  imgData: any;
  Country1: string;
  TimeData: any[] = [];
  FishBoatTypeList: any;
  ErrorMsg: boolean = false;
  ErrormsgText: any;
  Business_Name: any;
  Business_Desp: any;
  cont_Number: any;
  Email_Id: any;
  FirstURL: any;
  SecondURL: any;
  ThirdURL: any;
  //BuinessChk:boolean;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public navCtrl: NavController,
    public http: Http,
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public menuCtrl: MenuController
  ) {
    this.TimeData = [
      {
        id: '1',
        title: '1/2 Day Morning'
      },
      {
        id: '2',
        title: '1/2 Day Afternoon'
      },
      {
        id: '3',
        title: 'Full Day'
      }];
    console.log('TimeData', this.TimeData);
    this.RequestTripForm = this.formBuilder.group({
      'fishingtypes': ['', Validators.required],
      'Locationtypes': ['', Validators.required],
      'WillingPrice': ['', Validators.required],
      'Person': ['', Validators.required],
      'Boatypes': ['', Validators.required],
      'Date': ['', Validators.required],
      'Time': ['', Validators.required],
      'Buinessinfo': ['false'],
    });
    this.User = "guide";

    this.getProfileDetails();
  }

  getProfileDetails() {
    this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
      this.userDetails = res.Details;
      this.Business_Name = this.userDetails.name;
      this.Business_Desp = this.userDetails.BusinessDesp;
      this.cont_Number = this.userDetails.Contact_No;
      this.Email_Id = this.userDetails.Email_Id;
      if (this.userDetails.URL1 === "http://" || this.userDetails.URL1 === "https://") {
        this.FirstURL = '';
      } else {
        this.FirstURL = this.userDetails.URL1;
      }
      if (this.userDetails.URL2 === "http://" || this.userDetails.URL2 === "https://") {
        this.SecondURL = '';
      } else {
        this.SecondURL = this.userDetails.URL2;
      }
      if (this.userDetails.URL3 === "http://" || this.userDetails.URL3 === "https://") {
        this.ThirdURL = '';
      } else {
        this.ThirdURL = this.userDetails.URL3;
      }

      // this.FirsThirdURL = this.userDetails.URL1;
      // this.SecondURL = this.userDetails.URL2;
      // this.ThirdURL = this.userDetails.URL3;
      console.log(this.userDetails);
    });
  }



  BusinesschkBox: boolean;
  save() {
    console.log('Buinessinfo', this.RequestTripForm.controls['Buinessinfo'].value)
    // this.uploadImage();
    // var mobNo= this.RequestTripForm.controls['fishingtypes'].value;
    // var PhCode= this.Countrycode;
    var stringValue = this.RequestTripForm.controls['Buinessinfo'].value;
    if (stringValue == "true" || stringValue == true) {
      this.BusinesschkBox = true;
    } else {
      this.BusinesschkBox = false;
    }
    // var boolValue = stringValue == 'true' ? true : false;
    let data = {
      title: this.User,
      fishing_type_id: this.RequestTripForm.controls['fishingtypes'].value,
      fishing_location_id: this.RequestTripForm.controls['Locationtypes'].value,
      boat_type_id: this.RequestTripForm.controls['Boatypes'].value,
      date: this.RequestTripForm.controls['Date'].value,
      time: this.RequestTripForm.controls['Time'].value,
      trip_amount: this.RequestTripForm.controls['WillingPrice'].value,
      trip_booking_type: "per_person",
      member: this.RequestTripForm.controls['Person'].value,
      show_membership_details: this.BusinesschkBox
    }
    console.log('data-->', data);

    //  if (this.RegisterForm.valid) {
    //    this.presentLoading();
    this.authService.AddRequestTrip(data).subscribe(res => {
      if (res.success) {
        this.navCtrl.setRoot('FishingExchangPage');
        //   this.presentToast('Request Trip successfully created.');
        let alert = this.alertCtrl.create({
          //title: 'Exit?',
          message: 'Request Trip successfully created.',
          buttons: [
            {
              text: 'OK',
              role: 'OK',
              handler: () => {
                //  this.alert = null
              }
            }
          ]
        });
        alert.present();
        console.log('Output for signupdata-->');
      } else if (res.error[0].param == "trip_amount") {
        this.ErrorMsg = true;
        this.ErrormsgText = res.error[0].msg;
      }
      // else{
      //   this.presentToast('Please add Payment card.');
      //   this.navCtrl.push('PaymentlistPage');
      // }

    });
  }
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }

  ionViewWillLeave() {
    this.menuCtrl.swipeEnable(true);
  }
  ionViewDidLeave() {
    this.menuCtrl.swipeEnable(true);
  }
}
