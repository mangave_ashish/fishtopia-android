import { Component, Output } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';
import * as _ from 'lodash';

/**
 * Generated class for the TidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-Fishing_Licen_Products',
	templateUrl: 'Fishing_Licen_Products.html',
})
@SideMenuDisplayText('Fish Counts')
export class FishingLicenProductsPage {
	FishCountLocList: any[] = [];
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	FavLocationList: any[] = [];
	FishCountOption: any;
	tabType: any;
	status: boolean;
	GeoLat: any;
	GeoLong: any;
	lat: number = 61.217381;
	lng: number = -149.863129;
	image_url = "assets/img/salamanfish.jpg";
	StartDate: any;
	EndDate: any;

	addcartData: any[] = [];
	Productlist: any[] = [
		{
			id: 1,
			title: "Sport Fishing License",
			Price: '$29.00',
			Checked: true
		}
		// {
		// 	id: 2,
		// 	title: "King Salmon Stamp",
		// 	Price: '$10.00',
		// 	Checked: true
		// }
	];
	// resident_sport_fishing_license: ,
	// resident_sport_fishing_license_amount: ,
	// resident_king_salmon_stamp: { type: Boolean, default: false, enum: [true, false] },
	// resident_king_salmon_stamp_amount: { type: Number },
	PersonInfo: any;
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public authService: AuthService,
		public loadingCtrl: LoadingController
	) {
		this.PersonInfo = navParams.get('Licendata');
		console.log('Licendata', this.PersonInfo);
		this.StartDate = new Date();
	}
	addcartcount: number = 0;
	addcart(data: any) {
		this.addcartData = [];
		this.addcartcount = 0;
		this.Productlist.forEach(element => {
			if (element.title == data.title) {
				element.Checked = !element.Checked
			}
			if (!element.Checked) {
				this.addcartData.push(data);
				this.addcartcount = this.addcartData.length;
			}
		});
		//data.Checked = !data.Checked;

	}

	Check() {
		this.navCtrl.push('fishingLicenCheckoutPage', {
			PersonInfo: this.PersonInfo,
			Productlist: this.addcartData
		});
	}

	ionViewWillLeave() {
		this.EndDate = new Date();
		this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		this.authService.audit_Page('Fish Counts');
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}
}
