import { HomePage } from './home';
import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module';


@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    SharedModule
  ],
  exports: [
    HomePage,

  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})

export class HomePageModule { }
