import { Component } from '@angular/core';
import { IonicPage, LoadingController, MenuController, NavController, NavParams, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AuthService, Event } from '../../providers/auth-provider';
// Side Menu Component
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';
@IonicPage()
@Component({
    selector: 'page-home',
    templateUrl: 'home.html'
})
@SideMenuDisplayText('Events/Entertainment')
export class HomePage {
    drawerOptions: any;
    eventList: Event[] = [];
    TmweventList: Event[] = [];
    eventSource;
    viewTitle;
    monthNames: any = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    isToday: boolean;
    lockSwipeToPrev: boolean;
    month: string;
    mainEventList: Event[] = [];
    mainEventFillterList: any[] = [];
    EventType: any[] = [];
    dateList: Date[] = [];
    IPushMessage: any[];
    showPageView: boolean = false;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
    filterobject: any[] = [];
    public filteredItems: any;
    public issearch = false;
    public i: any;
    public keywords: any[];
    inputName: any = '';
    page = 1;
    filterData: any;

    perPage = 0;
    totalData = 0;
    totalPage = 0;
    StartDate: any;
    EndDate: any;

    constructor(public platform: Platform,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public menuCtrl: MenuController,
        public authService: AuthService,
        public loadingCtrl: LoadingController,
        public navParams: NavParams,
    ) {
        this.filterData = this.navParams.get('filterData');

        this.InvalidToken();
        var v = new Date();
        this.selectedDay = ("0" + (v.getDate())).slice(-2);
        this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
        this.YearMonth = v.getFullYear();
        this.StartDate = v;
        this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
        this.menuCtrl.swipeEnable(true);
        if (this.filterData !== undefined && this.filterData !== null) {
        } else {
            // this.page = 1;
            // this.getNewEventList(this.page);
            // this.getEventItinerary();
            // this.authService.getNotificationsCount();
            // this.authService.getfishinglocationslist();
        }
        this.getEventItinerary();
    }

    ionViewWillLeave() {
        this.EndDate = new Date();
        this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
        this.authService.audit_Page('Events/Entertainment');
    }

    InvalidToken() {
        this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
            return res;
        }, (err) => {
            var json = JSON.parse(err.text());
            if (json.success == false && json.error.status === 401 || json.message == "Invalid access token") {
                localStorage.removeItem('token');
                localStorage.removeItem('UserType');
                localStorage.removeItem('UserName');
                localStorage.removeItem('Profileimg');
                localStorage.removeItem('userId');
                localStorage.removeItem('is_featured');
                this.navCtrl.setRoot('LoginListPage');
            }
            // this.InValidToken=true;
            //console.log('message',json.message);
            console.log('res in auth provider-->', json);
            //console.log(json._body);
            return err;
            //this.InValidToken=json;
        });

    }

    getEventItinerary() {
        this.authService.getEventItinerary().subscribe(res => {
            this.EventType = res.data;
        });
    }

    change() {
        this.issearch = true;
    }
    searchByLocation() {
        this.navCtrl.push('FilterLocationPage');
    }
    change1() {
        this.issearch = false;
    }

    cancelSearch() {
        this.inputName = '';
        this.issearch = false;
    }


    FilterByName(value: any) {

        console.log('value', value);
        this.filteredItems = [];
        if (value != '') {
            this.mainEventList.forEach((element: any) => {
                console.log("output:", element);
                let search = ""
                try {
                    if (element.venue_id.location)
                        search = element.venue_id.location
                    else
                        search = element.location
                } catch (err) {
                    search = element.location
                }
                if (search.toUpperCase().indexOf(value.toUpperCase()) >= 0) {
                    this.filteredItems.push(element);
                }
                else if (element.title.toUpperCase().indexOf(value.toUpperCase()) >= 0) {
                    this.filteredItems.push(element);
                }
                else {
                    this.mainEventFillterList = this.mainEventList;

                }
                this.mainEventFillterList = this.filteredItems;
                this.dateList = []
                for (let d of this.mainEventFillterList) {
                    try {
                        //find out unique date
                        //   if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
                        if (this.SelectDate <= d.event_date) {
                            //        date.push(new Date(d.event_date).getDate());
                            this.dateList.push(d.event_date);
                        }
                        //  }
                    } catch (e) { }
                }
                var seriesValues = this.dateList;
                seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
                console.log(seriesValues);
                this.dateList = seriesValues;
                console.log('dateList', this.dateList);
                this.dateList.sort(function (a, b) {
                    // Turn your strings into dates, and then subtract them
                    // to get a value that is either negative, positive, or zero.
                    if (a > b) return 1;
                    if (a < b) return -1;
                    return 0;
                });

            });
        }
        else {
            this.mainEventFillterList = this.mainEventList;
            //this.filteredItems = this.authService.filterdAppointmentlist;
        }
    }
    FilterByName1: any[] = [];
    showCheckbox() {

        this.keywords = this.EventType;

        let alert = this.alertCtrl.create({
            title: 'FILTER'
        });
        let inputArray = <Array<object>>alert.data.inputs;

        for (let i = 0; i < this.keywords.length; i++) {
            alert.addInput({
                type: 'checkbox',
                label: this.keywords[i].name.toString(),
                value: this.keywords[i].name.toString(),
                handler: () => {
                    let myValue = inputArray[i]['value'];
                }
            })
        }
        inputArray.map((input) => {
            for (let k = 0; k < this.filterobject.length; k++) {
                if (input['value'] == this.filterobject[k]) {
                    input['checked'] = true;
                }
            }
            console.log('abc:', input['value']);
        })

        alert.addButton({
            cssClass: 'button-cam',
            text: 'Cancel',
            handler: (data: any) => {
                console.log('Checkbox data:', data);
            }

        })
        alert.addButton({
            cssClass: 'button-cam',
            text: 'Clear',
            handler: (data: any) => {
                this.FilterByName1 = [];
                this.filterobject = [];
                //this.getEventFilterDetails();
                this.page = 1;
                this.getEventFilterDetailsNew(this.page)

                //  console.log('Checkbox data:', data);
            }

        })
        alert.addButton({
            cssClass: 'button-cam',
            text: 'Ok',
            handler: (data: any) => {
                this.FilterByName1 = data;
                this.filterobject = this.FilterByName1;
                //this.getEventFilterDetails();
                this.page = 1;
                this.getEventFilterDetailsNew(this.page)
                console.log('Checkbox data:', data);
            }

        })
        alert.present();
    }




    ionViewDidEnter() {
        console.log("IN NG" + JSON.stringify(this.filterData))
        if (this.filterData !== undefined && this.filterData !== null) {
            console.log("In if ngonint");
            this.page = 1;
            this.mainEventFillterList = [];
            this.dateList = [];
            this.getEventFilterDetailsNew(this.page)
        }
        else {
            console.log("In else ngonint");
            setTimeout(() => {
                this.page = 1;
                this.mainEventFillterList = [];
                this.dateList = [];
                this.getNewEventList(this.page);

            }, 1000);
        }

    }

    getNewEventList(page) {
        this.isinfiniteScroll = false

        var data = {
            page: page,
            sort: "asc",
            today_date: this.SelectDate
        }
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
          //  duration: 10000
        });
        loading.present()
        this.authService.getEventByDateNew(data).subscribe((res: any) => {
            console.log("NEW EVENT LIST", "IN")
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;

            for (let d of res) {
                try {
                    if (this.SelectDate <= d.event_date) {
                        this.dateList.push(d.event_date);
                    }

                } catch (e) {
                    // console.log("date error", e)
                }
            }
            var seriesValues = this.dateList;
            loading.dismiss();
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
            this.dateList = seriesValues;
            this.dateList.sort(function (a, b) {
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });

            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.isinfiniteScroll = true;
            this.mainEventList = this.mainEventFillterList;
            if (this.issearch) {
                this.FilterByName(this.inputName)
            }
        },err=>{
            console.log(err)
            loading.dismiss();

        this.isinfiniteScroll = true

        },()=>{
            console.log("finally")
            loading.dismiss();

            this.isinfiniteScroll = true

        });
    }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        setTimeout(() => {
            this.page = 1;
            this.mainEventFillterList = [];
            this.dateList = [];
            // this.getEventDetails(this.page);

            this.getNewEventList(this.page);
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }
    isinfiniteScroll: boolean = true;
    doInfinite(infiniteScroll) {
        this.page++;
        if (this.page <= this.totalPage) {
            console.log('Begin async operation');
            if (this.isinfiniteScroll) {
                if (this.filterData !== undefined && this.filterData !== null) {
                    console.log("With filter");
                    var abc = this.getEventFilterDetailsNew(this.page)

                } else {
                    console.log(this.page +" "+this.totalPage+" "+(this.page < this.totalPage));
                    
                    console.log("Without filter");
                    var abc = this.getNewEventList(this.page);
                }
                console.log('Async operation has ended', this.page);
                infiniteScroll.complete();
            }
            // setTimeout( () => {

            // }, 1000);
        }
        else {
            this.isinfiniteScroll = false
            //        infiniteScroll.enabled(false);
        }
    }
    SerachString1: any;
    SerachString(value: any) {
        this.SerachString1 = value;
        // this.getEventFilterDetails();
        this.page = 1;
        this.getEventFilterDetailsNew(this.page)
    }
    getEventFilterDetails() {
        //this.dateList=[];

        console.log("In filter " + JSON.stringify(this.filterData))
        var data;
        try {
            if (this.filterData.Location)
                data = {
                    search: this.SerachString1,
                    page: 1,
                    itinerary: this.FilterByName1,
                    sort: "asc",
                    today_date: this.SelectDate,
                    Location: this.filterData.Location
                }
            else
                data = {
                    search: this.SerachString1,
                    page: 1,
                    itinerary: this.FilterByName1,
                    sort: "asc",
                    today_date: this.SelectDate,
                }
        } catch (ew) {
            data = {
                search: this.SerachString1,
                page: 1,
                itinerary: this.FilterByName1,
                sort: "asc",
                today_date: this.SelectDate,
            }
        }
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3000
        });
        loading.present();
        this.authService.getEventByDate(data).subscribe(res => {
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;
            this.dateList = [];
            this.mainEventFillterList = [];
            for (let d of res) {
                try {
                    //find out unique date
                    if (this.SelectDate <= d.event_date) {
                        this.dateList.push(d.event_date);
                    }
                    // }
                } catch (e) { }
            }
            var seriesValues = this.dateList;
            loading.dismiss();
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
            console.log(seriesValues);
            this.dateList = seriesValues;
            console.log('dateList', this.dateList);
            this.dateList.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });
            //  this.mainEventList = res;
            console.log("hhh" + JSON.stringify(res));
            console.log("SIZE " + res.length)
            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.mainEventList = this.mainEventFillterList;
        });
    }
    getEventFilterDetailsNew(page) {
        //this.dateList=[];
        this.isinfiniteScroll = false
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            //duration: 3000
        });
        loading.present();
        console.log("In filter " + JSON.stringify(this.filterData))
        var data;
        try {
            if (this.filterData.Location)
                data = {
                    search: this.SerachString1,
                    page: page,
                    itinerary: this.FilterByName1,
                    sort: "asc",
                    today_date: this.SelectDate,
                    Location: this.filterData.Location
                }
            else
                data = {
                    search: this.SerachString1,
                    page: page,
                    itinerary: this.FilterByName1,
                    sort: "asc",
                    today_date: this.SelectDate,
                }
        } catch (ew) {
            data = {
                search: this.SerachString1,
                page: page,
                itinerary: this.FilterByName1,
                sort: "asc",
                today_date: this.SelectDate,
            }
        }

        this.authService.getEventByDateNew(data).subscribe(res => {
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;
            // this.dateList = [];
            // this.mainEventFillterList = [];

            for (let d of res) {
                try {

                    if (this.SelectDate <= d.event_date) {
                        this.dateList.push(d.event_date);
                    } else {

                    }
                } catch (e) {

                }
            }
            var seriesValues = this.dateList;
            loading.dismiss();
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
           // console.log(seriesValues);
            this.dateList = seriesValues;
            //console.log('dateList', this.dateList);
            this.dateList.sort(function (a, b) {

                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });

            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.isinfiniteScroll = true;
            this.mainEventList = this.mainEventFillterList;
        },err=>{
            console.log(err)
            loading.dismiss();
        this.isinfiniteScroll = true

        },()=>{
            console.log("finally")
            loading.dismiss();

            this.isinfiniteScroll = true

        });
    }

    getEventDetails(Page) {
        var data = {
            page: Page,
            sort: "asc",
            today_date: this.SelectDate
        }
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3000
        });
        loading.present();
        this.authService.getEventByDate(data).subscribe(res => {
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;

            for (let d of res) {
                try {
                    if (this.SelectDate <= d.event_date) {
                        this.dateList.push(d.event_date);
                    }

                } catch (e) { }
            }
            var seriesValues = this.dateList;
            loading.dismiss();
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
            console.log(seriesValues);
            this.dateList = seriesValues;
            console.log('dateList', this.dateList);
            this.dateList.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });
            //  this.mainEventList = res;
            console.log(this.mainEventList);
            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.mainEventList = this.mainEventFillterList;
            if (this.issearch) {
                this.FilterByName(this.inputName)
            }
        });
    }
    saveFavorite(item) {
        item.isFavorite = !item.isFavorite;

    }
    addTofavourit(value: Event) {
        var data = {
            event_id: value._id,
            status: !value.favouriteStatus
        };
        this.authService.addToFavouriteEvent(data).subscribe(res => {
            // added as a favourite.
            // console.log('res', res);
            if (res.success) {
                value.favouriteStatus = !value.favouriteStatus;
                if (!value.favouriteStatus)
                    value.likes -= 1;
                else value.likes += 1;
            }
        });
    }
    Notification() {
        this.navCtrl.push('NotificationsPage');
    }
    speechToSearch() {
        this.navCtrl.push('GeoLocationTrackPage');
    }

    openEventDetail(item) {
        // this.navCtrl.push('EventDetailPage', { event: item });
        this.navCtrl.push('EventDetailsPage', { event: item });

    }
}
