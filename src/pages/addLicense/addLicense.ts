import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { BarcodeScanner } from '@ionic-native/barcode-scanner';
import { Camera } from '@ionic-native/camera';
import { CardIO } from '@ionic-native/card-io';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { Stripe } from '@ionic-native/stripe';
import { FileUploadOptions, Transfer, TransferObject } from '@ionic-native/transfer';
import { ActionSheetController, IonicPage, Loading, LoadingController, NavController, NavParams, Platform, ToastController, ViewController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AuthService } from '../../providers/auth-provider';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'page-addLicensePage',
  templateUrl: 'addLicense.html',
})
export class addLicensePage {
  loading: Loading;
  tripItem: any;
  tripDay: string;
  tripDate: string;
  LicenseForm: FormGroup;
  LicenseName: AbstractControl;
  LicNo: AbstractControl;
  Residentdrp: AbstractControl;
  StartDate: AbstractControl;
  EndDate: AbstractControl;
  DOB: AbstractControl;
  DrvLicNo: AbstractControl;
  AnKingSalStamp: AbstractControl;
  Address: AbstractControl;
  Business_Name: any;
  Business_Desp: any;
  cont_Number: any;
  Email_Id: any;
  ErrorMsg: any;
  userDetails: any;
  imgPreview: any;
  ImageEncoded: string;
  FURL: any;
  SURL: any;
  TURL: any;
  FXdata: any;
  Lic_No: any;
  Lic_Name: any;
  Resident_drp: any;
  St_Date: any;
  En_Date: any;
  Residentoption: any = [{
    id: "1",
    name: "Resident"
  },
  {
    id: "2",
    name: "Non-Resident"
  }
  ];
  Enabled: boolean = false;
  DOB_Date: any;

  constructor(
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public platform: Platform,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public stripe: Stripe,
    public cardIO: CardIO,
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    private file: File,
    private filePath: FilePath,
    private camera: Camera,
    private transfer: Transfer,
    public actionSheetCtrl: ActionSheetController,
    private barcodeScanner: BarcodeScanner,
    public photoViewer: PhotoViewer
  ) {
    // this.FXdata = navParams.get('FishingExchnage');
    this.menuCtrl.swipeEnable(false);
    this.LicenseForm = this.formBuilder.group({
      'LicenseName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      'LicNo': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(30)])],
      'Residentdrp': ['', Validators.compose([Validators.required])],
      'StartDate': ['', Validators.compose([Validators.required])],
      'EndDate': ['', Validators.compose([Validators.required])],
      'DrvLicNo': ['', Validators.compose([Validators.required])],
      'DOB': ['', Validators.compose([Validators.required])],
      'AnKingSalStamp': ['', Validators.compose([Validators.required])],
      'Address': ['', Validators.compose([Validators.required])],

    });
    this.getLicenseDetails();
  }
  Uploadimg() {
    let actionSheet = this.actionSheetCtrl.create({
      title: 'Select Image Source',
      buttons: [
        {
          text: 'Load from Library',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
          }
        },
        {
          text: 'Use Camera',
          handler: () => {
            this.takePicture(this.camera.PictureSourceType.CAMERA);
          }
        },
        {
          text: 'Cancel',
          role: 'cancel'
        }
      ]
    });
    actionSheet.present();
  }

  imgData: any;
  lastImage: any;
  public takePicture(sourceType) {

    // Create options for the Camera Dialog
    var options = {
      // quality: 100,
      // targetWidth: 91,
      // targetHeight: 91,
      allowEdit: true,
      sourceType: sourceType,
      saveToPhotoAlbum: true,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      this.imgData = imagePath;
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {
        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {
            console.log('filePath', filePath);
            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            this.uploadImage();
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        this.uploadImage();
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }



  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 2500,
      position: 'bottom'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }




  public uploadImage() {

    // Destination URL
    var url = "http://166.62.118.179:2152/api/users/save-fishing-license";
    var targetPath = this.pathForImage(this.lastImage);
    var filename = this.lastImage;
    let options1: FileUploadOptions = {
      fileKey: "image",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      headers: {
        // "accept": "multipart/form-data",
        // "Content-Type": "multipart/form-data",
        "access_token": localStorage.getItem("token")
      }
    }
    const fileTransfer: TransferObject = this.transfer.create();
    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();
    console.log('options1', JSON.stringify(options1));
    fileTransfer.upload(this.imgData, 'http://166.62.118.179:2152/api/users/save-fishing-license', options1)
      .then((data) => {
        this.getLicenseDetails();
        this.presentToast('Image uploaded Successfully.');
        //this.getProfileDetails();
        this.loading.dismissAll();
      }, err => {
        this.presentToast('Error while uploading file.');
        this.loading.dismissAll();
      });
  }

  scannedCode: any;
  Lic_id: any;
  Lic_Year: any;
  scanCode() {
    this.barcodeScanner.scan().then(barcodeData => {
      this.scannedCode = JSON.parse(barcodeData.text);
      console.log('scannedCode', JSON.stringify(barcodeData.text));
      this.Lic_No = this.scannedCode.No;
      this.Lic_Name = this.scannedCode.Name;
      if (this.scannedCode.Res == "Nonresident") {
        this.Resident_drp = "Non-Resident";
      } else {
        this.Resident_drp = "Resident";
      }
      this.St_Date = this.scannedCode.Effect;
      this.En_Date = this.scannedCode.Expires;
      this.Lic_id = this.scannedCode.Id;
      this.Lic_Year = this.scannedCode.Year;
      console.log(this.Lic_No, this.Lic_Name, this.St_Date, this.En_Date);
    }, (err) => {
      console.log('Error: ', err);
    });
  }

  image_url: any;
  LicenName: any;
  DataNA: boolean=false;
  DrvLic_No: any;
  An_KingSalStamp: any;
  Address_bind: any;
  getLicenseDetails() {
    this.authService.getUserLicenseDetail(localStorage.getItem("token")).subscribe(res => {
      console.log('res get', JSON.stringify(res))
      if (res.success == true) {
        var todaysdate = new Date()
        var expdate = new Date(res.data.end_date)
        console.log("today's date", todaysdate);
        console.log("expiry date", expdate);
        console.log("DATA", todaysdate <= expdate);

        this.image_url = res.data.image_url;
        this.Lic_id = res.data.license_id;
        this.Lic_No = res.data.license_no;
        this.Lic_Name = res.data.name;
        this.Lic_Year = res.data.year;
        this.Resident_drp = res.data.resident;
        this.DOB_Date = res.data.dob;
        this.St_Date = res.data.start_date;
        this.En_Date = res.data.end_date;
        this.DrvLic_No = res.data.drivers_license;
        this.An_KingSalStamp = res.data.annual_king_salmon_stamp;
        this.Address_bind = res.data.address;


      } else {
        this.DataNA = true;
        this.image_url = '';
        this.Lic_id = '';
        this.Lic_No = '';
        this.Lic_Name = '';
        this.Lic_Year = '';
        this.Resident_drp = '';
        this.DOB_Date = '';
        this.St_Date = '';
        this.En_Date = '';
        this.DrvLic_No = '';
        this.An_KingSalStamp = '';
        this.Address_bind = '';

        //this.navCtrl.push('addLicensePage');
      }
    })
  }
  PhotoView() {
    this.photoViewer.show(this.image_url, this.LicenName, { share: false });
  }
  deleteLicense() {
    let self = this
    let alert = this.alertCtrl.create({
      title: 'Confirm delete license',
      message: 'Are you sure you want to permanently delete this license?',
      buttons: [
        {
          text: 'No',
          handler: () => {
            //console.log('Cancel clicked');
          }
        },
        {
          text: 'Yes',
          handler: () => {
            self.authService.deleteLicenseDetails().subscribe(res => {
              console.log('res->', res);
              if (res.success) {
                self.presentToast('License Details Deleted Successfully.');
                self.getLicenseDetails()
              }

            })
          }
        }
      ]
    })
    alert.present()

  }

  AddlicenseDetails() {
    var data = {
      license_id: this.Lic_id,
      license_no: this.Lic_No,
      name: this.Lic_Name,
      year: this.Lic_Year,
      resident: this.Resident_drp,
      dob: this.DOB_Date,
      start_date: this.St_Date,
      end_date: this.En_Date,
      drivers_license: this.DrvLic_No,
      annual_king_salmon_stamp: this.An_KingSalStamp,
      address: this.Address_bind
    }
    console.log('data', JSON.stringify(data));
    this.authService.addLicenseDetails(data).subscribe(res => {
      console.log('res->', res);
      if (res.success) {
        this.presentToast('License Details Uploaded Successfully.');
      }

    })
  }
}
