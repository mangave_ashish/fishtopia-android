// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';
import { AlertController, App, LoadingController, NavController, MenuController,Slides, IonicPage } from 'ionic-angular';
import { HomePage } from '../../pages/_home/home';
import { signup } from '../../pages/sign-up/signup';
import { LoginListPage } from '../../pages/login/login';
import { FabButton } from 'ionic-angular/components/fab/fab';
@IonicPage()
@Component({
  selector: 'page-resetPasswordSuccess',
  templateUrl: 'resetPasswordSuccess.html',
})
export class resetPasswordSuccessPage {
  public loginForm: any;
 // public backgroundImage = 'assets/img/background/background-6.jpg';
 isenabledEmail:any;
 isenabledPhone:any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public navCtrl: NavController,
    public menuCtrl: MenuController
  ) {
    this.isenabledEmail=true;
    this.isenabledPhone=false;


   }

  // Slider methods
  @ViewChild('slider') slider: Slides;
  @ViewChild('innerSlider') innerSlider: Slides;

  goToLogin() {
    this.slider.slideTo(1);
  }

  next(el) {
    el.setFocus();
  }
  backToLogin(){}

  Email(){
    this.isenabledEmail=true;
    this.isenabledPhone=false;

  }
Phone(){
  this.isenabledPhone=true;
  this.isenabledEmail=false;

}

  goToSignup() {
    //this.slider.slideTo(2);
    this.navCtrl.push('LoginListPage');
  }

  slideNext() {
    this.innerSlider.slideNext();
  }

  slidePrevious() {
    this.innerSlider.slidePrev();
  }

  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }

  login() {
    this.presentLoading('Thanks for signing up!');
     this.navCtrl.push('HomePage');
  }

  signup() {
    this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('signup');
  }
  
  resetPassword() {
    this.navCtrl.setRoot('HomePage');
    this.presentLoading('An e-mail was sent with your new password.');

  }
}
