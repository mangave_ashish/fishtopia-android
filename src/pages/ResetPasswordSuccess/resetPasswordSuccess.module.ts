import { resetPasswordSuccessPage } from './resetPasswordSuccess';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    resetPasswordSuccessPage,
  ],
  imports: [
    IonicPageModule.forChild(resetPasswordSuccessPage),
  ],
  exports: [
    resetPasswordSuccessPage
  ]
})

export class resetPasswordSuccessPageModule{ }
