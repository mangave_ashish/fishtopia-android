import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-notifications',
	templateUrl: 'notifications.html',
})
export class NotificationsPage {
	notificationList: any[] = [];
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	EndDate: any;
	StartDate: any;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public Auth: AuthService,
		public toastCtrl: ToastController,
		public loadingCtrl: LoadingController,
	) {
		this.Auth.NotifyCount = 0;
		this.getNotifications(1);
		this.ReadAllNotifications();
		this.StartDate = new Date();
	}

	ionViewWillLeave() {
		this.EndDate = new Date();
		this.Auth.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}



	ionViewDidLoad() {
		console.log('ionViewDidLoad NotificationsPage');
	}

	doRefresh(refresher) {
		console.log('Begin async operation', refresher);
		setTimeout(() => {
			this.page = 1;
			this.notificationList = [];
			this.getNotifications(this.page);
			console.log('Async operation has ended');
			refresher.complete();
		}, 2000);
	}

	doInfinite(infiniteScroll) {
		if (this.page < this.totalPage) {
			console.log('Begin async operation', infiniteScroll);
			setTimeout(() => {
				this.page++;
				this.getNotifications(this.page);
				console.log('Async operation has ended');
				infiniteScroll.complete();
			}, 1000);
		}
		else {
			infiniteScroll.enabled(false);
		}
	}
	getNotifications(Page: any) {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 3500
		});
		loading.present();
		var data = {
			page: Page,
			status: "all"
			//"status": "all / pending / seen / read"
			//is_read: false
		}
		this.Auth.getNotificationList(data).subscribe(res => {
			this.perPage = this.Auth.perPage;
			this.totalData = this.Auth.totalData;
			this.totalPage = this.Auth.totalPage;
			loading.dismiss();
			if (res.data.items.docs !== undefined) {
				for (let item of res.data.items.docs) {
					this.notificationList.push(item);
				}
				console.log('NotificationsPage', JSON.stringify(res));
			}
		})
	}
	ReadNotifications(Notifyid: any) {
		this.Auth.getNotificationReadList(Notifyid).subscribe(res => {
			//this.notificationList = res.data.items.docs;
			//	console.log('NotificationsPage', JSON.stringify(this.notificationList));
		})
	}
	ReadAllNotifications() {
		this.Auth.getNotificationReadAllList().subscribe(res => {
			// if(){}
			// this.notificationList = res.data.items.docs;
			// console.log('ionViewDidLoad NotificationsPage', this.notificationList);
		})
	}

	DeleteNotification(data: any) {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 5500
		});
		loading.present();
		this.Auth.getNotificationDelete(data._id).subscribe(res => {
			loading.dismiss();
			this.notificationList = [];
			this.getNotifications(1);
			this.presentToast('Notification Deleted Successfully.');
			console.log('ionViewDidLoad NotificationsPage', res);
		})
	}


	notificationDetails(data: any) {
		console.log('data', data)
		if (data.notification_slug === "news_and_fishing") {
			this.navCtrl.setRoot('FishingNewsPage');
		} else if (data.notification_slug === "whats_hot") {
			this.navCtrl.setRoot('WHEventPage');
		} else if (data.notification_slug === "fishing_exchange_global") {
			this.navCtrl.setRoot('FishingExchangPage');
		} else if (data.notification_slug === "fish_counts_global") {
			this.navCtrl.setRoot('FishCountsPage');
		} else if (data.notification_slug === "angler_booked_trip") {
			this.navCtrl.setRoot('FishingExchangPage');
		} else if (data.notification_slug === "trip_is_full") {
			this.navCtrl.setRoot('FishingExchangPage');
		} else if (data.notification_slug === "guide_accept_trip") {
			this.navCtrl.setRoot('FishingExchangPage');
		} else if (data.notification_slug === "guide_sent_trip_request") {
			this.navCtrl.setRoot('FishingExchangPage');
		}
		// else if (data.notification_slug === "to_all_users") {
		//   this.navCtrl.push('NotificationsPage');
		// } else {
		//   this.navCtrl.push('NotificationsPage');
		// }
		// let loading = this.loadingCtrl.create({
		// 	spinner: 'hide',
		// 	content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
		// 	duration: 5500
		// });
		// loading.present();
		// this.Auth.getNotificationDelete(data._id).subscribe(res => {
		// 	loading.dismiss();
		// 	this.notificationList = [];
		// 	this.getNotifications(1);
		// 	this.presentToast('Notification Deleted Successfully.');
		// 	console.log('ionViewDidLoad NotificationsPage', res);
		// })

	}

	private presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 1500,
			position: 'bottom'
		});
		toast.present();
	}
}
