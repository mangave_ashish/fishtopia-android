import { Component, NgZone, OnInit, ViewChild } from '@angular/core';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Geolocation, Geoposition } from '@ionic-native/geolocation';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { Keyboard } from '@ionic-native/keyboard';
import { NativeGeocoder, NativeGeocoderReverseResult } from '@ionic-native/native-geocoder';
import { Network } from '@ionic-native/network';
import { AlertController, IonicPage, LoadingController, MenuController, ModalController, NavController, NavParams, Platform, Slides } from 'ionic-angular';
import * as _ from 'lodash';
import moment from 'moment';
import { Subscription } from 'rxjs/Subscription';
import { AuthService, Event } from '../../providers/auth-provider';
import { UserData } from '../../providers/user-data-local';
import { CONFIG, DatabaseService, DataPoint, DEFAULT_METRICS, Forecast, ForecastService, Location, Metrics, UtilService } from '../../providers/weather';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';
declare let google: any;


/**
 * Generated class for the WhatHotPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage({
	priority: 'high'
})
@Component({
	selector: 'page-dashboard',
	templateUrl: 'dashboard.html',
})
@SideMenuDisplayText('Dashboard')
export class dashboardPage implements OnInit {
	@ViewChild('slider') slider: Slides;
	@ViewChild('slider_1') slider_1: Slides;
	topEventListing: any[];
	topTripListing: any[];
	fishingNewslist: any[];
	drawerOptions: any;
	eventList: Event[] = [];
	TmweventList: Event[] = [];
	eventSource;
	viewTitle;
	monthNames: any = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"];

	isToday: boolean;
	lockSwipeToPrev: boolean;
	month: string;
	mainEventList: Event[] = [];
	dateList: Date[] = [];
	IPushMessage: any[];
	showPageView: boolean = false;
	selectedDay: any;
	selectedMonth: any;
	tmwDate: any;
	YearMonth: any;
	SelectDate: any;
	img: any;
	//Whether page
	heading: string;
	autocompleteItems: Array<{ description: string, place_id: string }>;
	query: string;
	acService: any;
	locationObj: Location;
	showCancel: boolean;
	lat: number = 61.217381;
	lng: number = -149.863129;
	data: any = "Anchorage Alaska";
	location: Location;
	forecast: Forecast;
	metrics: Metrics;
	todayForecast: DataPoint;
	forecastSubscriber: Subscription;
	hourlyArray: Array<{
		time: number,
		icon: string,
		temperature: number
	}> = [];
	GeoLat: any;
	GeoLong: any;
	ShoutOutList: any[] = [];
	//End
	StartDate: any;
	EndDate: any;
	image_url = "assets/img/salamanfish.jpg";
	locationData: any;
	Sub_Location: any;
	waterLocList: any;
	public product: any = {
		name: 'Alaska FishTopia',
		appleProductId: 'AKFISHTOPIA01',
		googleProductId: 'akfishtopiayearlysubscription'
	};
	productId: any;
	AppVersion: any;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public authService: AuthService,
		public forecastService: ForecastService,
		public databaseService: DatabaseService,
		public utilService: UtilService,
		public modalCtrl: ModalController,
		public keyboard: Keyboard,
		public zone: NgZone,
		public loadingCtrl: LoadingController,
		public network: Network,
		public alertCtrl: AlertController,
		private iap: InAppPurchase,
		public platform: Platform,
		public menuCtrl: MenuController,
		public userData: UserData,
		public app: AppVersion,
		private device: Device,
		public geolocation: Geolocation,
		private nativeGeocoder: NativeGeocoder,
	) {
		if (localStorage.getItem("UserType") == "guest") {
			this.navCtrl.push('GuestUserErrorPage');
		}
		var data = {
			"lat": this.lat,
			"lng": this.lng,
			"name": this.data
		}

		this.locationAccuracyOn();

		var fishls = JSON.parse(localStorage.getItem("FishNewslist"))
		if (fishls) {
			this.localStorageSpace()

			this.FishNewslist = JSON.parse(localStorage.getItem("FishNewslist"))
			this.eventList = JSON.parse(localStorage.getItem("eventList"))
			this.ShoutOutList = JSON.parse(localStorage.getItem("ShoutOutList"))

			this.getFishNewsList(1);
			//this.getEventDetailsNew()

			//this.getActivityList(1);
		} else {
			this.getFishNewsList(1);
			//	this.getEventDetailsNew()

			//this.getActivityList(1);
		}


		this.authService.getNotificationsCount();
		// this.getfishingcountFavList(1);
		// this.getwaterFlowFavlist(1);
		// this.GeoLat = localStorage.getItem('GeoLat');
		// this.GeoLong = localStorage.getItem('GeoLong');
		// this.locationData = localStorage.getItem('GeoLocation');
		// this.Sub_Location = localStorage.getItem('SubGeoLocation');
		var v = new Date();
		this.StartDate = v;
		this.selectedDay = ("0" + (v.getDate())).slice(-2);
		this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
		this.YearMonth = v.getFullYear();
		this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
		this.tmwDate = "tmw";
		this.authService.getProfileDetails();
		app.getVersionNumber().then((res) => {
			this.AppVersion = res;
		});

		this.drawerOptions = {
			handleHeight: 50,
			thresholdFromBottom: 200,
			thresholdFromTop: 200,
			bounceBack: true
		};
		this.authService.getsinguserpoints(null).subscribe((result: any) => {
			//console.log("PONTS", result)
			this.authService.points = result.data[0].sum_of_points
		})
	}
	localStorageSpace = function () {
		var allStrings = '';
		for (var key in window.localStorage) {
			if (window.localStorage.hasOwnProperty(key)) {
				allStrings += window.localStorage[key];
			}
		}
		let localstoragesize = allStrings ? 3 + ((allStrings.length * 16) / (8 * 1024)) + ' KB' : 'Empty (0 KB)';
		console.log("localstoragesize", localstoragesize)
		// return allStrings ? 3 + ((allStrings.length*16)/(8*1024)) + ' KB' : 'Empty (0 KB)';
	};
	getEventDetailsNew() {
		this.dateList = [];
		var data = {
		}
		this.authService.getEventByDateNew(data).subscribe(res => {
			let date: any[] = [];
			for (let d of res) {
				try {
					//find out unique date
					if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
						if (this.SelectDate <= d.event_date) {
							date.push(new Date(d.event_date).getDate());
							this.dateList.push(d.event_date);
						}
					}
				} catch (e) { }
			}
			console.log('dateList', this.dateList);
			this.dateList.sort(function (a, b) {
				// Turn your strings into dates, and then subtract them
				// to get a value that is either negative, positive, or zero.
				if (a > b) return 1;
				if (a < b) return -1;
				return 0;
			});
			this.mainEventList = res;
			console.log('mainEventList', this.mainEventList);
			if (this.mainEventList.length > 0) {
				//this.currentDay = date[0];
			}
			this.filterEvent();
		});
	}
	locationAccuracyOn() {
		this.geolocation.getCurrentPosition().then((resp) => {
			console.log('location', resp);
			this.startTracking();
		}).catch((error) => {
			//this.locationAccuracyOn();
			console.log('Error getting location', error);
		});

	}
	public startTracking() {
		let options = {
			frequency: 1000,
			enableHighAccuracy: true
		};
		this.geolocation.getCurrentPosition(options).then((resp) => {
			this.GeoLocationNameTrack(resp.coords.latitude, resp.coords.longitude);
		}).catch((error) => {
			console.log('Error getting location', error);
		});
		this.geolocation.watchPosition(options).subscribe(position => {
			if ((position as Geoposition).coords != undefined) {
				var geoposition = (position as Geoposition);
				this.Trackuser(geoposition.coords.latitude, geoposition.coords.longitude, geoposition.coords.speed);
				// console.log('Latitude: ' + geoposition.coords.latitude + ' - Longitude: ' + geoposition.coords.longitude + 'Speed' + geoposition.coords.speed);
			}
		});
	}
	Trackuser(lat: any, lng: any, speed: any) {
		var data = {
			latitude: lat,
			longitude: lng,
			speed: speed
		}
		this.authService.GeoTracklat = lat;
		this.authService.GeoTracklng = lng;
		this.authService.getUserLocation(data).subscribe(res => {
			// console.log('track', res);
		})
	}


	getImage(data: any) {
		if (data.venue_id) {
			if (data.venue_id.logo == " ")
				return data.image_url
			else
				return data.venue_id.logo
		} else {
			return data.image_url
		}
	}
	GeoLocationNameTrack(lat: any, lng: any) {
		localStorage.removeItem('GeoLat');
		localStorage.removeItem('GeoLong');
		localStorage.removeItem('SubGeoLocation');
		localStorage.removeItem('GeoLocation');
		// /61.071544, -148.981580
		this.nativeGeocoder.reverseGeocode(lat, lng)
			.then((result: NativeGeocoderReverseResult[]) => {
				let nameLoc = result[0].subAdministrativeArea;
				let locationData = result[0].administrativeArea;
				localStorage.setItem('GeoLat', JSON.stringify(lat));
				localStorage.setItem('GeoLong', JSON.stringify(lng));
				localStorage.setItem('SubGeoLocation', nameLoc);
				localStorage.setItem('GeoLocation', locationData);
				this.getGeolocationPosition();

			})
			.catch((error: any) => console.log(error));
	}
	ngOnInit() {
		//this.subsCheck();
	}

	Subsdata: any;
	subsCheck() {
		var da = JSON.parse(localStorage.getItem('SubsDetails'));
		this.Subsdata = JSON.parse(da);
		if (this.Subsdata != null && this.Subsdata != undefined) {
			var v = new Date();
			var purchaseDate = new Date(this.Subsdata.purchaseTime);
			var Exp = new Date(purchaseDate.getUTCFullYear() + 1, purchaseDate.getUTCMonth(), purchaseDate.getUTCDate(), purchaseDate.getUTCHours(), purchaseDate.getUTCMinutes(), purchaseDate.getUTCSeconds());
			if (v >= Exp) {
				localStorage.removeItem('UserType');
				localStorage.removeItem('UserName');
				localStorage.removeItem('Profileimg');
				localStorage.removeItem('userId');
				localStorage.removeItem('is_featured');
				localStorage.removeItem('fcmToken');
				localStorage.setItem("old_token", localStorage.getItem("token"));
				localStorage.removeItem('token');
				this.userData.removeAuthToken();
				this.subsStatus();
			}
		} else {
			this.subsStatus();
		}
	}
	subsStatus() {
		this.platform.ready().then(() => {
			this.iap
				.restorePurchases()
				.then((data: any) => {
					if (data.length > 0) {
						for (var i = 0; i < data.length; ++i) {
							alert('hee' + JSON.stringify(data[i].receipt));
							if (data[i].productId == this.productId) {
								localStorage.setItem('SubsDetails', JSON.stringify(data[i].receipt));
							} else {
								this.menuCtrl.swipeEnable(false);
								this.navCtrl.setRoot('subscribeDialogPage');
							}
						}
					}
					else {
						this.menuCtrl.swipeEnable(false);
						this.navCtrl.setRoot('subscribeDialogPage');
					}
				}).catch((err) => {
					console.log(JSON.stringify(err));
				});
		});
	}


	ionViewWillLeave() {
		this.EndDate = new Date();
		this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		this.authService.audit_Page('Dashboard');
		this.updateAppversion();
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}



	updateAppversion() {
		var data = {
			app_version: this.AppVersion,
			device_type: this.device.platform
		}
		this.authService.updateAppVersion(data).subscribe(res => {
		})
	}

	Notification() {
		this.navCtrl.push('NotificationsPage');
	}

	//Fish Local wheather Content
	getGeolocationPosition() {
		this.GeoLat = localStorage.getItem('GeoLat');
		this.GeoLong = localStorage.getItem('GeoLong');
		this.locationData = localStorage.getItem('GeoLocation');
		this.Sub_Location = localStorage.getItem('SubGeoLocation');
		console.log("LOCSTION NAME: ", this.locationData);
		this.lat = JSON.parse(this.GeoLat);
		this.lng = JSON.parse(this.GeoLong);
		this.data = this.Sub_Location;
		// if (this.locationData == "Alaska") {
		// 	this.lat = JSON.parse(this.GeoLat);
		// 	this.lng = JSON.parse(this.GeoLong);
		// 	this.data = this.Sub_Location;	
		// } else {
		// 	// this.lat = 61.217381;
		// 	// this.lng = -149.863129;
		// 	// this.data = "Anchorage Alaska";
		// 	this.lat = JSON.parse(this.GeoLat);
		// 	this.lng = JSON.parse(this.GeoLong);
		// 	this.data = this.Sub_Location;
		// }
		var data = {
			"lat": this.lat,
			"lng": this.lng,
			"name": this.data
		}
		this.GetData(data);

	}
	GetData(dataloc) {
		let self = this;
		var data = dataloc;
		console.log('dataloc', dataloc);
		if (data === null) {
			let modal = self.modalCtrl.create('LocationPage', { heading: 'Enter Home City', showCancel: false });
			modal.onDidDismiss((data: Location) => {
				console.debug('page > modal dismissed > data > ', data);
				if (data) {
					self.databaseService.setJson(CONFIG.HOME_LOCATION, data);
					console.log(CONFIG.HOME_LOCATION, data);
					self.location = data;
					self.init(self.location);
					//self.emitInit();
				}
			});
			modal.present();
		} else {
			self.location = data;
			self.init(self.location);
		}

	}

	init(data) {
		let self = this;
		if (data) {
			self.getForecast(self.location);
		}
		this.databaseService.getJson(CONFIG.METRICS).then(data => {
			if (data === null) {
				self.databaseService.setJson(CONFIG.METRICS, DEFAULT_METRICS);
				self.metrics = DEFAULT_METRICS;
			} else {
				self.metrics = data;
			}
		});
	}



	getForecast(location: Location) {
		let self = this;
		self.forecastSubscriber = self.forecastService.getForecast(location)
			.subscribe((data: Forecast) => {
				self.forecast = data;
				if (self.forecast && self.forecast.daily && self.forecast.daily.data) {
					self.todayForecast = self.forecast.daily.data[0];
				}
				self.hourlyArray = [];
				let currentHour = self.utilService.getCurrentHour(self.forecast.timezone);
				let flag = false;
				_.forEach(self.forecast.hourly.data, (obj: DataPoint) => {
					if (!flag && self.utilService.epochToHour(obj.time, self.forecast.timezone) < currentHour) {
						return;
					}
					flag = true;
					self.hourlyArray.push({
						time: obj.time,
						icon: obj.icon,
						temperature: obj.temperature
					});
					if (self.hourlyArray.length > 10) {
						return false;
					}
				});
			}, err => {
				console.error(err);
			});
		//	this.getActivityList(1);
	}

	//Fish Today's Catch Photo Content

	getActivityList(page) {
		this.ShoutOutList = [];
		var token = localStorage.getItem("token");
		var data = {
			page: page,
			sort: 'desc'
		}
		console.log('data', data);
		this.authService.getShoutOutDetail(data).subscribe(res => {
			//this.ShoutOutList = res;
			this.perPage = this.authService.perPage;
			this.totalData = this.authService.totalData;
			this.totalPage = this.authService.totalPage;
			//console.log('this.userDetails', JSON.stringify(this.ShoutOutList));
			if (res.success) {
				this.ShoutOutList = []
				for (let item of res.data.items.docs) {
					try {
						if (item.user_id.image_url == null) {
							item.user_id.image_url = 'assets/img/icon/placeholder.png';
						}
					} catch (er) {
						item.user_id.image_url = 'assets/img/icon/placeholder.png';

					}
					item.shortDesp = this.truncate(item.description);
					try {
						if (item.images.length)
							item.image_url = item.images[0].image_url;
						else
							item.image_url = 'assets/img/icon/placeholder.png';
					} catch (ed) {
						item.image_url = 'assets/img/icon/placeholder.png';

					}
					//}
					var date = moment(item.created_at);
					item.eventDay = date.date();
					item.Day = date;
					//moment(date, "dddd");
					//console.log('item.shortDesp',item.shortDesp);
					this.ShoutOutList.push(item);
				}
				localStorage.setItem("ShoutOutList", JSON.stringify(this.ShoutOutList))

			}
			this.getfishingcountFavList(1);
			this.getwaterFlowFavlist(1);

		});

	}

	truncate(value: string, limit = 50, completeWords = true, ellipsis = '…') {
		let lastindex = limit;
		if (completeWords) {
			lastindex = value.substr(0, limit).lastIndexOf(' ');
		}
		return `${value.substr(0, limit)}${ellipsis}`;
	}



	ShoutOutDetails(card: any) {
		this.navCtrl.push('ShoutOutDetailsPage', { ShoutOutDetails: card });
	}




	//Fish NEws Content

	mainNewsFillterList: any[] = [];
	FishNewslist: any;
	getFishNewsList(Page) {
		//this.dateList=[];   
		var data = {
			page: Page,
			sort: "desc"
		}
		this.authService.getFishNews(data).subscribe(res => {

			for (let d of res) {
				try {
					this.dateList.push(d.date_time);
				} catch (e) { }
			}
			var seriesValues = this.dateList;
			seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
			console.log(seriesValues);
			this.dateList = seriesValues;
			for (let item of res) {
				this.mainNewsFillterList.push(item);
			}
			this.FishNewslist = []

			this.FishNewslist = this.mainNewsFillterList;
			console.log('this.FishNewslist', this.FishNewslist);
			localStorage.setItem("FishNewslist", JSON.stringify(this.FishNewslist))
			//this.locationAccuracyOn();
			this.getActivityList(1);

		});
	}
	openNewsDetail(news) {
		this.navCtrl.push('FishingNewsDetailPage', { news: news });
	}




	//Fish Event Content


	getEventDetails() {
		this.dateList = [];
		var data = {
		}
		this.authService.getEventByDateNew(data).subscribe(res => {
			let date: any[] = [];
			for (let d of res) {
				try {
					//find out unique date
					if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
						if (this.SelectDate <= d.event_date) {
							date.push(new Date(d.event_date).getDate());
							this.dateList.push(d.event_date);
						}
					}
				} catch (e) { }
			}
			console.log('dateList', this.dateList);
			this.dateList.sort(function (a, b) {
				// Turn your strings into dates, and then subtract them
				// to get a value that is either negative, positive, or zero.
				if (a > b) return 1;
				if (a < b) return -1;
				return 0;
			});
			this.mainEventList = res;
			console.log('mainEventList', this.mainEventList);
			if (this.mainEventList.length > 0) {

			}
			this.filterEvent();
		});
	}




	filterEvent() {
		try {
			if (this.mainEventList.length > 0) {
				var d = new Date();
				let today: Date = new Date(d);
				this.eventList = []
				for (let mel of this.mainEventList) {
					if (this.eventList.length < 10) {
						mel.title = this.truncateEvent(mel.title);
						this.eventList.push(mel);
					}
				}
				let tempeventList: any[] = []
				for (let index = 0; index < 4; index++) {
					const element = this.eventList[index];
					tempeventList.push(element)
				}
				localStorage.setItem("eventList", JSON.stringify(tempeventList))

			}
		}
		catch (e) {
			console.log(e);
		}
	}
	truncateEvent(value: string, limit = 25, completeWords = true, ellipsis = '…') {
		let lastindex = limit;
		if (completeWords) {
			lastindex = value.substr(0, limit).lastIndexOf(' ');
		}
		return `${value.substr(0, limit)}${ellipsis}`;
	}

	addTofavourit(value: Event) {

		var data = {
			event_id: value._id,
			status: !value.favouriteStatus
		};
		this.authService.addToFavouriteEvent(data).subscribe(res => {
			// added as a favourite.
			console.log('res', res);
			if (res.success) {
				value.favouriteStatus = !value.favouriteStatus;
				if (!value.favouriteStatus)
					value.likes -= 1;
				else value.likes += 1;
			}
		});
	}
	AddWhatsHot() {
		this.navCtrl.push('AddWhatsHotPage');
	}
	openEventDetail(item) {
		this.navCtrl.push('EventDetailsPage', { event: item });

		//this.navCtrl.push('EventDetailPage', { event: item });
	}


	itemClicked(item: any) {
		this.navCtrl.setRoot('HomeWeatherPage')
	}


	//Fish count location Content

	FavLocationList: any;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	FishCountLocList: any[] = [];
	arrayLength: number;
	Output: any[] = [];
	getfishingcountlocationlist(page_no: any) {
		let self = this;
		var token = localStorage.getItem("token");
		var data = {
			page: page_no,
			latitude: self.GeoLat,
			longitude: self.GeoLong
		}
		console.log('data', data);
		self.authService.getfishingcountlocation(data).subscribe(res => {
			console.log('res', res);
			let obj = res.data.items.docs;
			self.perPage = self.authService.perPage;
			self.totalData = self.authService.totalData;
			self.totalPage = self.authService.totalPage;
			var id = localStorage.getItem('userId');
			_.forEach(obj, (item: any) => {
				if (item.last_updated_count !== undefined && item.last_updated_count !== null) {
					item.Count_Date = item.last_updated_count.count_date;
					item.Fish_Count = item.last_updated_count.fish_count;
				} else {
					item.Count_Date = 'empty';
					item.Fish_Count = 'empty';
				}
				for (let fav of item.favourites) {

					if (fav.user_id._id == id) {
						item.favouriteStatus = true;
					} else {
						item.favouriteStatus = false;
					}
				}
				item.formDate = moment(item.Count_Date, "YYYYMMDD").fromNow();
				//console.log('formDate', formDate)
				self.FavLocationList = item.favourites
				//item.favouriteStatus = false;
				self.Output = [];
				self.arrayLength = 0;
				let json = item.title.split(' ');
				self.arrayLength = json.length;
				item.fromDate = json[self.arrayLength - 3] + " " + json[self.arrayLength - 2] + " " + json[self.arrayLength - 1];
				for (var i = 0; i < self.arrayLength - 3; i++) {
					self.Output.push(json[i]);
				}
				item.titleSplit = self.Output.join(" ");
				self.FishCountLocList.push(item);
			})
			console.log('getfishingcountlocation', self.FishCountLocList);
		});


	}

	openAreaDetail(area) {
		console.log('area', area);
		this.navCtrl.push('FishCountChartPage', { area: area });
	}


	getfishingcountFavList(page_no: any) {
		let self = this;
		self.FishCountLocList = [];
		var token = localStorage.getItem("token");
		var data = {
			page: page_no,
			latitude: self.GeoLat,
			longitude: self.GeoLong
		}
		self.authService.getFavFishCountlist(data).subscribe(res => {
			console.log('data', res);
			let obj = res.data.items.docs;
			self.perPage = self.authService.perPage;
			self.totalData = self.authService.totalData;
			self.totalPage = self.authService.totalPage;
			var id = localStorage.getItem('userId');
			_.forEach(obj, (item: any) => {
				if (item.last_updated_count !== undefined && item.last_updated_count !== null) {
					item.Count_Date = item.last_updated_count.count_date;
					item.Fish_Count = item.last_updated_count.fish_count;
				} else {
					item.Count_Date = 'empty';
					item.Fish_Count = 'empty';
				}
				item.favouriteStatus = true;
				//item.formDate = moment(item.Count_Date, "YYYYMMDD").fromNow();
				self.Output = [];
				let split_string = item.title.split(/(\d+)/);
				item.titleSplit = split_string[0];
				self.Output = split_string.slice(1, split_string.length);
				item.fromDate = self.Output.join(" ");
				self.FishCountLocList.push(item);
			});
			//self.ngAfterViewInit();

			this.getEventDetailsNew()
		});


	}


	//River Flow Data

	getwaterFlowFavlist(page_no: any) {
		let self = this;
		self.waterLocList = [];
		var token = localStorage.getItem("token");
		var data = {
			page: page_no,
			latitude: self.GeoLat,
			longitude: self.GeoLong,
			sort: "asc"
		}
		self.authService.getwaterFlowFavlist(data).subscribe(res => {
			console.log('data', res);
			let obj = res.result;
			self.perPage = self.authService.perPage;
			self.totalData = self.authService.totalData;
			self.totalPage = self.authService.totalPage;
			obj.forEach(item => {
				item.favouriteStatus = true;
				item.water_temp = Math.round(JSON.parse(item.water_temp) * 100) / 100;
				//JSON.parse(item.water_temp);
				item.air_temp = Math.round(JSON.parse(item.air_temp) * 100) / 100;
				//JSON.parse(item.air_temp);
				item.gage_height = Math.round(JSON.parse(item.gage_height) * 100) / 100;
				// JSON.parse(item.gage_height);
				item.discharge = Math.round(JSON.parse(item.discharge) * 100) / 100;
				// JSON.parse(item.discharge);

				if (item.water_temp < 1 || item.water_temp == null) {
					item.water_temp = 0;
				}

				if (item.air_temp < 1 || item.air_temp == null) {
					item.air_temp = 0;
				}

				if (item.discharge < 1 || item.discharge == null) {
					item.discharge = 0;

				}

				if (item.gage_height < 1 || item.gage_height == null) {
					item.gage_height = 0;

				}

				self.waterLocList.push(item);
			})
			// loading.dismiss();
			console.log('getfishingcountlocation', self.waterLocList);
		});
	}


	openWaterAreaDetail(area) {
		console.log('area', area);
		this.navCtrl.push('USGS_DetailsPage', { USGS_Details: area });
	}





}


