import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { dashboardPage } from './dashboard';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    dashboardPage,
  ],
  imports: [
    IonicPageModule.forChild(dashboardPage),
    SharedModule
  ],
})
export class dashboardPageModule { }
