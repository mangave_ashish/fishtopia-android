import { Component } from '@angular/core';
import { NavController, Platform, NavParams, IonicPage, LoadingController, normalizeURL } from 'ionic-angular';
import { PopoverController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { File } from '@ionic-native/file';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { DatabaseService } from '../../providers/weather';
const ZOOM_STEP: number = 0.25;
const DEFAULT_ZOOM: number = 1;
declare var cordova: any;

@IonicPage()
@Component({
    selector: 'page-LicenpdfView',
    templateUrl: 'LicenpdfView.html'
})
export class LicenpdfViewpage {
    IPushMessage: any[];
    push: any;
    socket: any;
    chat_input: string;
    pdfUrl: any;
    pdfUrlSrc: string;
    public pdfZoom: number = DEFAULT_ZOOM;
    storageDirectory: string = '';
    fishinglicen: boolean = false;
    constructor(public loadCtrl: LoadingController,
        public navParams: NavParams,
        public platform: Platform,
        public navCtrl: NavController,
        public popoverCtrl: PopoverController,
        public authService: AuthService,
        public alertCtrl: AlertController,
        public databaseService: DatabaseService,
        private transfer: Transfer, private file: File,
    ) {
        this.pdfUrl = this.navParams.get('data');
        this.fishinglicen = this.navParams.get('fishingLicen');
        let loading = this.loadCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3500
        });
        loading.present();
        // this.platform.ready().then(() => {
        //     // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
        //     if (!this.platform.is('cordova')) {
        //         return false;
        //     }

        //     if (this.platform.is('ios')) {
        //         this.storageDirectory = this.file.documentsDirectory;
        //         this.downloadPDF();
        //     }
        //     else if (this.platform.is('android')) {
        //         this.storageDirectory = this.file.dataDirectory;
        //         this.downloadPDF();
        //     }
        //     else {
        //         // exit otherwise, but you could add further types here e.g. Windows
        //         return false;
        //     }
        // });
    }


    ionViewWillLeave() {
        if (this.fishinglicen) {
            this.navCtrl.setRoot('FishingLicenformPage');
        }
    }

    retrieveImage() {
        let self = this;
        let load = self.loadCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 15000
        });
        load.present();
        self.databaseService.getRegualtions(this.pdfUrl)
            .then(data => {
                if (data != undefined) {
                    let Data = JSON.parse(data.data);
                    self.pdfUrlSrc = Data;
                    load.dismiss();
                } else {
                    self.downloadPDF();
                    load.dismiss();
                }
            })
    }

    downloadPDF() {
        var load = this.loadCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 5000
        });
        load.present();
        console.log('Order Number: ', JSON.stringify(this.pdfUrl));
        this.platform.ready().then(() => {
            const fileTransfer: TransferObject = this.transfer.create();
            console.log('Order pdf: ', this.pdfUrl.pdfURL, this.storageDirectory + this.pdfUrl.displayText + '.pdf');
            fileTransfer.download(this.pdfUrl.pdfURL, this.storageDirectory + this.pdfUrl.displayText + '.pdf').then((entry) => {
                console.log('Order entry: ', JSON.stringify(this.pdfUrl.displayText));
                console.log('Order URL: ', this.storageDirectory, this.pdfUrl.displayText + '.pdf');
                this.file.readAsDataURL(this.storageDirectory, this.pdfUrl.displayText + '.pdf').then(result => {
                    console.log('Order result: ', JSON.stringify(result));
                    this.pdfUrlSrc = result;
                    load.dismiss();
                }, (error) => {
                    console.log('Order error: ', JSON.stringify(error));
                    load.dismiss();
                });
            }, (error) => {
                load.dismiss();
                console.log('Order error: ', JSON.stringify(error));
                this.pdfUrlSrc = this.pdfUrl.pdfURL;
            });
        });
    }

    dbStoreregualtions(result: any) {
        this.databaseService.addRegulations_Data(result, this.pdfUrl);
    }

    NotificationSettings() {
        //this.modalCtrl.create('PaymentPage').present();;
        this.navCtrl.push('notificationsettingsPage');
    }


    public zoomIn() {
        this.pdfZoom += ZOOM_STEP;
    }

    public zoomOut() {
        if (this.pdfZoom > DEFAULT_ZOOM) {
            this.pdfZoom -= ZOOM_STEP;
        }
    }

    public resetZoom() {
        this.pdfZoom = DEFAULT_ZOOM;
    }

}
