import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController,NavParams, Content } from 'ionic-angular';
import { AuthService,FishNewsList } from '../../providers/auth-provider';
import { FormBuilder,FormControl, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AlertService } from '../../providers/util/alert.service';

@IonicPage()
@Component({
  selector: 'page-ShoutOutDetailsCmts',
  templateUrl: 'ShoutOutDetailsCmts.html'
})
export class ShoutOutDetailsCmtsPage {
  toUser = {
    _id: '534b8e5aaa5e7afc1b23e69b',
    pic: 'assets/img/avatar/ian-avatar.png',
    username: 'Venkman',
  };

  user = {
    _id: '534b8fb2aa5e7afc1b23e69c',
    pic: 'assets/img/avatar/marty-avatar.png',
    username: 'Marty',
  };

  doneLoading = false;

  @ViewChild(Content) content: Content;

  
  chatBox: any;
  post : any;

  chats = [{
    imageUrl: 'assets/img/avatar/marty-avatar.png',
    title: 'McFly',
    lastMessage: 'Hey, what happened yesterday?',
    timestamp: new Date()
  },
  {
    imageUrl: 'assets/img/avatar/ian-avatar.png',
    title: 'Venkman',
    lastMessage: 'Sup, dude',
    timestamp: new Date()
  }
  ,
  {
    imageUrl: 'assets/img/avatar/sarah-avatar.jpg',
    title: 'Sarah Mcconnor',
    lastMessage: 'You still ow me that pizza.',
    timestamp: new Date()
  }];

  messages:any[]=[];

  messageForm : FormGroup;
  message: AbstractControl;
  constructor( public alertService: AlertService,public navParams: NavParams, public navCtrl: NavController, public formBuilder: FormBuilder,public authService:AuthService) {
    this.messageForm = formBuilder.group({
      'message': [''],
    });
    this.chatBox = '';
    this.message = this.messageForm.controls['message'];
    this.post = navParams.get('post');
    console.log('this.post',this.post);
    this.getCmmts();

  }

  send1(message) {
    this.messages.push(message);
   

    if (message && message !== '') {
      // this.messageService.sendMessage(chatId, message);

      const messageData =
        {
          toId: this.toUser._id,
          _id: 6,
          date: new Date(),
          userId: this.user._id,
          username: this.toUser.username,
          pic: this.toUser.pic,
          text: message
        };

      this.messages.push(messageData);
      this.scrollToBottom();

      setTimeout(() => {
        const replyData =
          {
            toId: this.toUser._id,
            _id: 6,
            date: new Date(),
            userId: this.toUser._id,
            username: this.toUser.username,
            pic: this.toUser.pic,
            text: 'Just a quick reply'
          };
        this.messages.push(replyData);
        this.scrollToBottom();
      }, 1000);
    }
    this.chatBox = '';
  }

  scrollToBottom() {
    setTimeout(() => {
      this.content.scrollToBottom();
    }, 100);
  }

  getCmmts() {
    this.chatBox = '';
    this.message = this.messageForm.controls['message'].value;
    var data = {
      activity_id: this.post._id
    };
    console.log('res',data);
    this.authService.getToCmmtShoutOut(this.post._id).subscribe(res => {
        // added as a favourite.
     console.log('res',res);
        if (res.success) {     
              
          this.messages=res.data.items.docs;
          console.log('this.messages',this.messages);
          for (let item of res.data) {
          //  this.messages
          }
        } 
        //let toast = this.toast.create({
        //    message: res.message,
        //    duration: 3000,
        //    position: 'bottom'
        //}).present();
       // console.log(res);
    });
}




  sendCmmt() {
    this.messages=[];
    this.message = this.messageForm.controls['message'].value;
    var data = {
        activity_id: this.post._id,        
        comment:this.message
    };
    this.authService.addToCmmtShoutOut(data).subscribe(res => {
        // added as a favourite.
     console.log('res',res);
        if (res.success) {
          this.getCmmts();      
        }   
    });
}



  
}
