
import { ShoutOutDetailsCmtsPage } from './ShoutOutDetailsCmts';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module';


@NgModule({
  declarations: [
    ShoutOutDetailsCmtsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoutOutDetailsCmtsPage),  
    SharedModule  
  ],
  exports: [
    ShoutOutDetailsCmtsPage
  ]
})

export class MessagesPageModule { }
