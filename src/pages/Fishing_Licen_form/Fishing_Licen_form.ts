// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';
import { Platform, ToastController, AlertController, Loading, App, LoadingController, NavController, Slides, IonicPage, ActionSheetController } from 'ionic-angular';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Rx';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../providers/auth-provider';
import { MatRadioChange } from '@angular/material';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

declare var cordova: any;

@IonicPage()
@Component({
  selector: 'Fishing_Licen_form',
  templateUrl: 'Fishing_Licen_form.html',
})

@SideMenuDisplayText('Fishing License')
export class FishingLicenformPage {
  // public loginForm: any;
  User: any;
  show = false;
  userDetails: any;
  RequestLicenForm: FormGroup;
  FirstName: AbstractControl;
  MiddleName: AbstractControl;
  LastName: AbstractControl;
  Suffix: AbstractControl;
  DOB: AbstractControl;
  Gender: AbstractControl;
  LicenState: AbstractControl;
  LicenseNo: AbstractControl;
  Residency: AbstractControl;
  Month: AbstractControl;
  Year: AbstractControl;
  UserPhonecode: any;
  loading: Loading;
  startDatetimeMin: any = new Date(Date.now()).toISOString();
  Profileimg: any;
  UserProfileImg: any;
  image_url: any;
  img: any;
  lastImage: string = null;
  imgData: any;
  Country1: string;
  mnth: any[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
  SuffixData: any[] = [
    {
      id: 1,
      Suffix_name: 'None'
    },
    {
      id: 2,
      Suffix_name: 'Jr'
    },
    {
      id: 3,
      Suffix_name: 'II'
    },
    {
      id: 4,
      Suffix_name: 'Sr.'
    },
    {
      id: 5,
      Suffix_name: 'III'
    },
    {
      id: 6,
      Suffix_name: 'IV'
    },
    {
      id: 7,
      Suffix_name: 'V'
    }
  ];

  statelist: any[] = [
    {
      id: 1,
      title: 'None'
    },
    {
      id: 2,
      title: 'Alaska'
    },
    {
      id: 3,
      title: 'Arkansas'
    },
    {
      id: 4,
      title: 'Arizona'
    },
    {
      id: 5,
      title: 'California'
    },
    {
      id: 6,
      title: 'Colorado'
    },
    {
      id: 7,
      title: 'Connecticut'
    },
    {
      id: 8,
      title: 'District of Columbia'
    },
    {
      id: 9,
      title: 'Delaware'
    },
    {
      id: 10,
      title: 'Florida'
    },
    {
      id: 11,
      title: 'Georgia'
    },
    {
      id: 12,
      title: 'Hawaii'
    },
    {
      id: 13,
      title: 'Iowa'
    },
    {
      id: 14,
      title: 'Idaho'
    },
    {
      id: 15,
      title: 'Illinois'
    },
    {
      id: 16,
      title: 'Indiana'
    },
    {
      id: 17,
      title: 'Kansas'
    },
    {
      id: 18,
      title: 'Kentucky'
    },
    {
      id: 19,
      title: 'Louisiana'
    },
    {
      id: 20,
      title: 'Massachusetts'
    },
    {
      id: 21,
      title: 'Maryland'
    },
    {
      id: 22,
      title: 'Maine'
    },
    {
      id: 23,
      title: 'Michigan'
    },
    {
      id: 24,
      title: 'Minnesota'
    },
    {
      id: 25,
      title: 'Missouri'
    },
    {
      id: 26,
      title: 'Mississippi'
    },
    {
      id: 27,
      title: 'Montana'
    },
    {
      id: 28,
      title: 'North Carolina'
    },
    {
      id: 29,
      title: 'North Dakota'
    },
    {
      id: 30,
      title: 'Nebraska'
    },
    {
      id: 31,
      title: 'New Hampshire'
    },
    {
      id: 32,
      title: 'New Jersey'
    },
    {
      id: 33,
      title: 'New York'
    },
    {
      id: 34,
      title: 'Ohio'
    },
    {
      id: 35,
      title: 'Oklahoma'
    },
    {
      id: 36,
      title: 'Oregon'
    },
    {
      id: 37,
      title: 'Pennsylvania'
    },
    {
      id: 38,
      title: 'Rhode Island'
    },
    {
      id: 39,
      title: 'South Carolina'
    },
    {
      id: 40,
      title: 'South Dakota'
    },
    {
      id: 41,
      title: 'Tennessee'
    },
    {
      id: 42,
      title: 'Texas'
    },
    {
      id: 43,
      title: 'Utah'
    },
    {
      id: 44,
      title: 'Virginia'
    },
    {
      id: 45,
      title: 'Vermont'
    },
    {
      id: 46,
      title: 'Washington'
    },
    {
      id: 47,
      title: 'Wisconsin'
    },
    {
      id: 48,
      title: 'West Virginia'
    },
    {
      id: 49,
      title: 'Wyoming'
    },
    {
      id: 50,
      title: 'Armed Forces Americas'
    },
    {
      id: 51,
      title: 'Armed Forces Europe'
    },
    {
      id: 52,
      title: 'Armed Forces Pacific'
    },
    {
      id: 53,
      title: 'Alberta'
    },
    {
      id: 54,
      title: 'British Columbia'
    },
    {
      id: 55,
      title: 'Manitoba'
    },
    {
      id: 56,
      title: 'Newfoundland and Labrador'
    },
    {
      id: 57,
      title: 'Northwest Territories'
    },
    {
      id: 58,
      title: 'Nova Scotia'
    },
    {
      id: 59,
      title: 'Nunavut'
    },
    {
      id: 60,
      title: 'Ontario'
    },
    {
      id: 61,
      title: 'Prince Edward Island'
    },
    {
      id: 62,
      title: 'Quebec'
    },
    {
      id: 63,
      title: 'Saskatchewan'
    },
    {
      id: 64,
      title: 'Yukon'
    }
  ];
  seasons: any[] = [
    {
      id: 1,
      title: 'Resident',
      Checked: false
    },
    {
      id: 2,
      title: 'Nonresident',
      Checked: true
    }, {
      id: 3,
      title: 'Nonresident Military',
      Checked: true
    },
    {
      id: 4,
      title: 'Nonresident Alien',
      Checked: true
    }
  ];
  favoriteSeason: string;
  chosenItem: any;
  ResidencyMnthEnabled: boolean = false;
  chosenSuffix: any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public navCtrl: NavController,
    public http: Http,
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,

  ) {
    this.RequestLicenForm = this.formBuilder.group({
      'FirstName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
      'MiddleName': ['', Validators.compose([Validators.minLength(1), Validators.maxLength(1), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
      'LastName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
      'DOB': [''],
      'Residency': [''],
      'Month': [''],
      'Year': [''],
      'Suffix': [''],
      'Gender': [''],
      'LicenState': ['', Validators.compose([Validators.required])],
      'LicenseNo': ['']
    });
    this.FirstName = this.RequestLicenForm.controls['FirstName'];
    this.MiddleName = this.RequestLicenForm.controls['MiddleName'];
    this.LastName = this.RequestLicenForm.controls['LastName'];
    this.Suffix = this.RequestLicenForm.controls['Suffix'];
    this.DOB = this.RequestLicenForm.controls['DOB'];
    this.Gender = this.RequestLicenForm.controls['Gender'];
    this.LicenState = this.RequestLicenForm.controls['LicenState'];
    this.LicenseNo = this.RequestLicenForm.controls['LicenseNo'];
    this.Residency = this.RequestLicenForm.controls['Residency'];
    this.Month = this.RequestLicenForm.controls['Month'];
    this.Year = this.RequestLicenForm.controls['Year'];
    this.User = "angler";
    this.chosenItem = this.seasons[0].title;
    this.chosenSuffix = this.SuffixData[0].Suffix_name;
    this.ResidencyMnthEnabled = true;
  }

  ErrorMsg: boolean = false;
  ErrormsgText: any;
  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  radioChange(event: MatRadioChange) {
    //this.ResidencyMnthEnabled = !this.ResidencyMnthEnabled;
    if (event.value == "Resident") {
      this.ResidencyMnthEnabled = true;
    } else {
      this.ErrorMsg = false;
      this.ResidencyMnthEnabled = false;
    }
    console.log('event', event);
  }

  DOBError: boolean = false;
  licenError: Boolean = false;
  LicenStateError: Boolean = false;
  StartShopping() {
    var mnth;
    var year;
    var DOBdata = this.RequestLicenForm.controls['DOB'].value;
    if (DOBdata == '') {
      this.DOBError = true;
    } else {
      this.DOBError = false;
    }
    if (this.RequestLicenForm.controls['Residency'].value == "Resident") {
      mnth = this.RequestLicenForm.controls['Month'].value;
      year = this.RequestLicenForm.controls['Year'].value;
    } else {
      mnth = '';
      year = '';
    }
    var data = {
      FirstName: this.RequestLicenForm.controls['FirstName'].value,
      MiddleName: this.RequestLicenForm.controls['MiddleName'].value,
      LastName: this.RequestLicenForm.controls['LastName'].value,
      Suffix: this.RequestLicenForm.controls['Suffix'].value,
      DOB: this.RequestLicenForm.controls['DOB'].value,
      Gender: this.RequestLicenForm.controls['Gender'].value,
      LicenState: this.RequestLicenForm.controls['LicenState'].value,
      LicenseNo: this.RequestLicenForm.controls['LicenseNo'].value,
      Residency: this.RequestLicenForm.controls['Residency'].value,
      Month: mnth,
      Year: year,
      FishingType: 'SportLicense'
    }
    if (data.LicenState == '') {
      this.LicenStateError = true;
    } else {
      this.LicenStateError = false;
    }

    if (this.RequestLicenForm.valid) {
      console.log('LicenData', data);
      if (data.LicenState != "None" && data.LicenseNo == "") {
        this.licenError = true;
      } else {
        this.licenError = false;
      }
      if (data.Residency == "Resident") {
        if (mnth == "") {
          this.ErrorMsg = true;
          this.ErrormsgText = "Residency start month is required";
        } else if (year == "") {
          this.ErrorMsg = true;
          this.ErrormsgText = "Residency start year is required";
        } else {
          this.navCtrl.push('FishingLicenProductsPage', { Licendata: data });
        }
      } else {
        this.ErrorMsg = false;
        this.navCtrl.push('FishingLicenProductsPage', { Licendata: data });
      }
    }
  }



  Notification() {
    this.navCtrl.push('FishingLicenCartPage');
  }
}
