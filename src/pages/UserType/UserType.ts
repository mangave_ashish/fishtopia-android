import { Component } from '@angular/core';
import { Device } from '@ionic-native/device';
import { Market } from '@ionic-native/market';
import { AlertController, IonicPage, LoadingController, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { UserData } from '../../providers/user-data-local';

@IonicPage()
@Component({
  selector: 'UserType',
  templateUrl: 'UserType.html',
})
export class UserTypePage {

  social_db: any;
  Social_NetEnabled: any;
  dataDB: any;
  userType: any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public authService: AuthService,
    public navParams: NavParams,
    public userData: UserData,
    private device: Device,
    private market: Market,

  ) {
    this.social_db = navParams.get('SocialNetdata');
    if (this.social_db !== null && this.social_db !== undefined) {
      this.Social_NetEnabled = this.social_db.Social_NetEnabled;
    }
  }

  UserSelection(data: any) {
    this.userType = data;
    if (this.social_db !== null && this.social_db !== undefined) {
      // if (this.social_db.Social_NetEnabled == 'Google+' || this.social_db.Social_NetEnabled == 'Facebook') 
      this.doLoginSocialNetwork();
    } else {
      this.navCtrl.push('signup', { UserType: data });
    }
  }

  doLoginSocialNetwork() {
    var data = {
      email: this.social_db.email,
      device_type: this.social_db.device_type,
      unique_device_id: this.social_db.unique_device_id,
      device_id: this.social_db.device_id,
      user_type: this.userType,
      app_version: this.social_db.app_version
    }
    //alert(JSON.stringify(data));
    this.authService.googleSignUp(data).subscribe(
      data => this.loginSucessfully1(data),
      error => console.log(JSON.stringify(error))
    );
  }


  loginSucessfully1(data) {
    let output = data;
        console.log("OUTPUT DATA ",JSON.stringify(output));

        this.device.uuid = output.data.uuid
        output.data.access_token = output.data.user_token.access_token
        output.data.user_type = output.data.user_type
        output.data.original_otp=output.data.original_otp
        this.loginSucessfully(output)
  }
  output: any;
    loginSucessfully(data) {
        let otpuser = JSON.parse(localStorage.getItem('OTPL'))
        console.log('otpuser click login', JSON.stringify(otpuser))
        this.output = data;
        let self = this;

        console.log('Login data', this.output);
        if (this.output.success === false) {
            // this.errorMsg = this.output.message;
            // this.submitAttempt = true;

        }
        else {
            if (this.output.data.is_outdated_app_version) {
                let alert = this.alertCtrl.create({
                    title: 'Get our latest app',
                    message: "Update to our latest app to get the most out of Fishtopia",
                    enableBackdropDismiss: false,
                    buttons: [
                        {
                            text: 'Not now',
                            handler: () => {
                                alert.dismiss()
                                let auth = self.output.data.access_token;
                                self.userData.setAuthToken(auth);
                                console.log('output.data.', self.output.data);
                                localStorage.setItem("token", auth);
                                localStorage.setItem("unique_device_id", self.device.uuid);
                                localStorage.setItem("userId", self.output.data._id);
                                localStorage.setItem("UserType", self.output.data.user_type);
                                localStorage.setItem("original_otp", self.output.data.original_otp);

                                if (self.output.data.user_type == "guest") {
                                    //this.getDataOffline();
                                    self.authService.getMarinelistinfo();
                                    self.authService.getProfileDetails();
                                    self.navCtrl.setRoot('marineWeatherPage');

                                    // if (self.pageName === 'regulations') {
                                    //     self.navCtrl.setRoot('regulationsList');
                                    // } else {
                                    //     self.navCtrl.setRoot('marineWeatherPage');
                                    // }
                                } else {
                                    let original_otp = JSON.parse(localStorage.getItem("original_otp"));
                                    console.log("original_otp", original_otp)
                                    if (original_otp == true) {
                                        let abc: any = false
                                        let otpuser = JSON.parse(localStorage.getItem("OTPL"))
                                        if (otpuser) {
                                            console.log("local storage otp");
                                            self.navCtrl.setRoot('dashboardPage');
                                            self.getDataOffline();
                                        } else {
                                            let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))

                                            if (subscriptionDate) {
                                                var todaysdate = new Date()
                                                var expdate = new Date(subscriptionDate.expirationDate)
                                                console.log("today's date", todaysdate);
                                                console.log("expiry date", expdate);
                                                if (todaysdate <= expdate) {
                                                    console.log("valid subcription");
                                                    self.navCtrl.setRoot('dashboardPage');
                                                    self.getDataOffline();
                                                } else {
                                                    console.log("IN login original_otp true")
                                                    let alert = this.alertCtrl.create({
                                                        message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                                        enableBackdropDismiss: false,
                                                        buttons: [
                                                            {
                                                                text: 'Okay',
                                                                handler: () => {
                                                                    if (abc) {
                                                                        console.log("Abc true one")
                                                                        // localStorage.setItem("OTP_logged", 'true')
                                                                        this.setOtp()
                                                                        // self.navCtrl.setRoot('dashboardPage');
                                                                        // self.getDataOffline();
                                                                    }
                                                                    else {
                                                                        console.log("Abc false")
                                                                        return false

                                                                    }
                                                                }
                                                            },
                                                            {
                                                                text: 'Subscribe',
                                                                handler: () => {
                                                                    console.log("SUbscription")
                                                                    self.navCtrl.setRoot('SubscriptionLoaderPage');
                                                                }
                                                            }
                                                        ]

                                                    });

                                                    alert.present()
                                                    setTimeout(function () {
                                                        abc = true

                                                    }, 5000);
                                                }
                                            } else {
                                                console.log("IN login original_otp true")
                                                let alert = this.alertCtrl.create({
                                                    message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                                    enableBackdropDismiss: false,
                                                    buttons: [
                                                        {
                                                            text: 'Okay',
                                                            handler: () => {
                                                                if (abc) {
                                                                    console.log("Abc true two")
                                                                    // localStorage.setItem("OTP_logged", 'true')
                                                                    this.setOtp()
                                                                    // self.navCtrl.setRoot('dashboardPage');
                                                                    // self.getDataOffline();
                                                                }
                                                                else {
                                                                    console.log("Abc false")
                                                                    return false

                                                                }
                                                            }
                                                        },
                                                        {
                                                            text: 'Subscribe',
                                                            handler: () => {
                                                                console.log("SUbscription")
                                                                self.navCtrl.setRoot('SubscriptionLoaderPage');
                                                            }
                                                        }
                                                    ]

                                                });

                                                alert.present()
                                                setTimeout(function () {
                                                    abc = true

                                                }, 5000);
                                            }
                                        }
                                    } else {
                                        console.log("IN login original_otp false")
                                        let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))
                                        if (subscriptionDate) {
                                            var todaysdate = new Date()
                                            var expdate = new Date(subscriptionDate.expirationDate)
                                            console.log("today's date", todaysdate);
                                            console.log("expiry date", expdate);
                                            if (todaysdate <= expdate) {


                                                console.log("valid subcription");
                                                self.navCtrl.setRoot('dashboardPage');
                                                self.getDataOffline();
                                            } else {
                                                self.navCtrl.setRoot('SubscriptionLoaderPage');
                                            }
                                        } else {
                                            self.navCtrl.setRoot('SubscriptionLoaderPage');
                                        }
                                    }
                                }
                            }
                        },
                        {
                            text: 'Update',
                            handler: () => {
                                /// console.log('Buy clicked');
                                self.market.open('id1384360605');
                            }
                        }
                    ]
                });
                alert.present()
            } else {
                let auth = this.output.data.access_token;
                this.userData.setAuthToken(auth);
                console.log('output.data.', JSON.stringify(this.output.data));
                localStorage.setItem("token", auth);
                localStorage.setItem("unique_device_id", this.device.uuid);
                localStorage.setItem("userId", this.output.data._id);
                localStorage.setItem("UserType", this.output.data.user_type);
                localStorage.setItem("original_otp", this.output.data.original_otp);

                if (this.output.data.user_type == "guest") {
                    //this.getDataOffline();
                    this.authService.getMarinelistinfo();
                    this.authService.getProfileDetails();
                    this.navCtrl.setRoot('marineWeatherPage');

                    // if (this.pageName === 'regulations') {
                    //     this.navCtrl.setRoot('regulationsList');
                    // } else {
                    //     this.navCtrl.setRoot('marineWeatherPage');
                    // }
                } else {
                    let original_otp = JSON.parse(localStorage.getItem("original_otp"));

                    console.log("original_otp", original_otp)
                    if (original_otp == true) {
                        let self = this;
                        let abc: any = false
                        console.log("IN login original_otp true")
                        let otpuser = JSON.parse(localStorage.getItem('OTPL'))
                        console.log('otpuser', JSON.stringify(otpuser))
                        if (otpuser) {
                            console.log("local storage otp");
                            self.navCtrl.setRoot('dashboardPage');
                            self.getDataOffline();
                        } else {
                            let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))

                            if (subscriptionDate) {
                                var todaysdate = new Date()
                                var expdate = new Date(subscriptionDate.expirationDate)
                                console.log("today's date", todaysdate);
                                console.log("expiry date", expdate);
                                if (todaysdate <= expdate) {
                                    console.log("valid subcription");
                                    this.navCtrl.setRoot('dashboardPage');
                                    this.getDataOffline();
                                } else {

                                    let alert = this.alertCtrl.create({
                                        message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                        enableBackdropDismiss: false,
                                        buttons: [
                                            {
                                                text: 'Okay',
                                                handler: () => {
                                                    if (abc) {

                                                        console.log("Abc true three")
                                                        this.setOtp()
                                                    }
                                                    else {
                                                        console.log("Abc false")
                                                        return false

                                                    }
                                                }
                                            },
                                            {
                                                text: 'Subscribe',
                                                handler: () => {
                                                    console.log("SUbscription")
                                                    self.navCtrl.setRoot('SubscriptionLoaderPage');
                                                }
                                            }
                                        ]

                                    });
                                    alert.present()
                                    setTimeout(function () {
                                        abc = true

                                    }, 5000);
                                }
                            } else {
                                let alert = this.alertCtrl.create({
                                    message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: 'Okay',
                                            handler: () => {
                                                if (abc) {
                                                    console.log("Abc true four")
                                                    this.setOtp()
                                                    // self.navCtrl.setRoot('dashboardPage');
                                                    // self.getDataOffline();
                                                }
                                                else {
                                                    console.log("Abc false")
                                                    return false

                                                }
                                            }
                                        },
                                        {
                                            text: 'Subscribe',
                                            handler: () => {
                                                console.log("SUbscription")
                                                self.navCtrl.setRoot('SubscriptionLoaderPage');
                                            }
                                        }
                                    ]

                                });
                                alert.present()
                                setTimeout(function () {
                                    abc = true

                                }, 5000);
                            }
                        }

                    } else {
                        console.log("IN login original_otp false")

                        let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))
                        if (subscriptionDate) {
                            var todaysdate = new Date()
                            var expdate = new Date(subscriptionDate.expirationDate)
                            console.log("today's date", todaysdate);
                            console.log("expiry date", expdate);
                            if (todaysdate <= expdate) {


                                console.log("valid subcription");
                                this.navCtrl.setRoot('dashboardPage');
                                this.getDataOffline();
                            } else {
                                this.navCtrl.setRoot('SubscriptionLoaderPage');
                            }
                        } else {
                            this.navCtrl.setRoot('SubscriptionLoaderPage');
                        }
                    }
                }
            }
        }
    }
    public product: any = {
        name: 'Alaska FishTopia',
        appleProductId: 'AKFISHTOPIA001',
        googleProductId: 'akfishtopiayearlysubscription'
    };
    setOtp() {
        console.log('otp data', this.product)

        localStorage.setItem('OTPL', JSON.stringify(this.product))
        this.navCtrl.setRoot('dashboardPage');
        this.getDataOffline();
    }
  getDataOffline() {
    this.dataDB = JSON.parse(localStorage.getItem("tbl_TideData"));
    if (this.dataDB === null) {
      this.authService.getProfileDetails();
      this.authService.getTideDetails_New();
      this.authService.getCurrentsDetails();
      this.authService.getBuoyDetails();
      this.authService.getMarinelistinfo();
    }
  }
}
