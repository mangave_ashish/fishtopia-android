import { Component } from '@angular/core';
import { IonicPage, NavController,ModalController, NavParams } from 'ionic-angular';
import { GuideReviewPage } from '../guide-review/guide-review';
import { GuideReviewAddPage } from '../guide-review-add/guide-review-add';
import { AuthService } from '../../providers/auth-provider';
/**
 * Generated class for the GuideDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

 @IonicPage()
 @Component({
 	selector: 'page-guide-detail',
 	templateUrl: 'guide-detail.html',
 })
 export class GuideDetailPage {
 	guideReviewList: any;
	// firstGuideReviewItem: any;
	 UserDetails:any;
	 Review:any;
	 userReviewDetails:any;
	 firstGuideReviewItem:any;
	 image_url:any;
	 GuideDetailsData:any;
	 userDetails:any;
 	constructor(public modalCtrl: ModalController,public navCtrl: NavController, public navParams: NavParams,public authService:AuthService) {
	//	this.authService.userDetailsData = this.authService.userDetailsData;	
	//	this.UserDetails=aut
		//this.image_url =this.UserDetails.image_url;
		this.userDetails = navParams.get('userDetails');
		this.userReviewDetails =this.authService.ReviewAll;
		this.firstGuideReviewItem = this.authService.ReviewAll[0];
		console.log('this.UserDetails',this.UserDetails+this.firstGuideReviewItem);	
 	}

	

 	openReviewList(guideReviewList){
 		this.navCtrl.push('GuideReviewPage', {guideReviewList:this.userReviewDetails,userDetailsData:this.userDetails});
 	}

 	openReviewForm(){
		this.modalCtrl.create('GuideReviewAddPage', {userDetailsData: this.userDetails})
		.present();
 	//	this.navCtrl.push('GuideReviewAddPage',{guideReviewList:this.authService.userDetailsData});
 	}
 }
