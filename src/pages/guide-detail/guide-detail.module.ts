import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GuideDetailPage } from './guide-detail';
import { SharedModule } from '../../app/shared.module';
// import { Ionic2RatingModule } from 'ionic2-rating';
@NgModule({
  declarations: [
    GuideDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(GuideDetailPage),
    SharedModule
    // Ionic2RatingModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class GuideDetailPageModule {}
