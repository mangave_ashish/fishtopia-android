import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import Highcharts from 'highcharts/highstock';
import moment from 'moment';


@IonicPage({
    priority: 'high'
})
@Component({
    selector: 'page-CurrentsPageDetails',
    templateUrl: 'CurrentsPageDetails.html',

})
export class CurrentsPageDetailsPage {
    @ViewChild('leftCats') leftItems;
    dateArray: any = [];
    monthArray: any[] = ["January", "February ", "March ", "April ", "May ", "June ", "July", "August", "September", "October", "November", "December"];
    doneLoading = false;
    messages: any[] = [];
    Currents: any;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    yearArray: any[];
    SelectDate: any;
    mainEventList: any;
    dateList: Date[] = [];
    eventList: any[] = [];
    ChartdateList: Date[] = [];
    dataft: any;
    title: any;
    CurrentStationlist: any[] = [];
    CurrentStationlistfilter: any[] = [];
    currStationminlist: any[] = [];
    hgchart: any;
    dataDB: any;
    WsChartData: any[];
    WsRecords: any[];
    CurrStatus: any[];
    CurrOption: any;
    // chartData: any[];
    range: any;
    randomColor = ['#000000', '#0000FF', '#FF00FF', '#D2691E', '#008000', '#8d44ad'];
    errormsgEnabled: boolean = false;
    graphEnabled: boolean = false;
    constructor(
        public navParams: NavParams,
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public authService: AuthService
    ) {
        this.CurrOption = 'Calendar';
        this.Currents = navParams.get('CurrentDetails');
        this.dataDB = navParams.get('CurrentData');
        this.title = this.Currents.location;
        console.log('Currents', this.Currents);
        var v = new Date();
        this.yearArray = [v.getFullYear(), (v.getFullYear() + 1)];
        this.selectedDay = v.getDate();
        this.selectedMonth = v.getMonth();
        this.YearMonth = v.getFullYear();
        this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
        console.log('this.selectedMonth', this.selectedMonth);
        this.allDates();
    }

    getYear(YearMonth: any) {
        console.log('this.SelectDate', YearMonth);
        if (YearMonth == new Date().getFullYear()) {
            this.YearMonth = YearMonth;
            this.selectedMonth = new Date().getMonth();
            this.selectedDay = new Date().getDate();
            this.updatePlotGraph();
        } else {
            this.YearMonth = YearMonth;
            this.selectedMonth = 0;
            this.selectedDay = 1;
            this.updatePlotGraph();
        }

    }



    onLinkClick(event: any) {
        this.selectedMonth = new Date().getMonth();
        if (event == "Graph") {
            this.range = 0;
            this.graphEnabled = true;
            this.allDates();
            setTimeout(() => {
                this.plotChart();
            }, 150);
        } else {
            this.graphEnabled = false;
            this.allDates();
        }
    }

    onDateClick(date: any, i: any) {
        this.selectedDay = date;
        if (!this.graphEnabled) {
            this.filterCurrent(this.dateString);
        } else {
            this.filterMinDataCurr(this.dateString);
        }
    }
    monthChange() {
        if (!this.graphEnabled) {
            this.filterCurrent(this.dateString);
        } else {
            this.filterMinDataCurr(this.dateString);
        }
    }

    getCurrentStationDetails(station: any) {
        this.dateList = [];
        var dd = ("0" + (this.selectedDay)).slice(-2);
        var mn = ("0" + (this.selectedMonth + 1)).slice(-2);
        var yy = this.YearMonth;
        var dateformat = [yy, mn, dd].join("-");
        if (this.dataDB != null) {
            this.CurrentStationlist = this.dataDB;
            this.CurrentStationlistfilter = this.dataDB;
            this.mainEventList = this.dataDB;
            this.filterCurrent(this.dateString);
            console.log('Curr_Cal_station', this.dataDB);
        } else {
            this.authService.getCurrentsStationData(station).subscribe(res => {
                if (res.data.length > 1) {
                    this.errormsgEnabled = true;
                }
                this.CurrentStationlist = res.data;
                localStorage.setItem('Curr_Cal_' + station, JSON.stringify(this.CurrentStationlist));
                this.CurrentStationlistfilter = res.data;
                let date: any[] = [];
                for (let d of res.data) {
                    try {
                        if (dateformat == d.date) {
                            date.push(new Date(d.date).getDate());
                            this.dateList.push(d.date);
                        }
                    } catch (e) { }
                }
                //   loading.dismiss();
                console.log('dateList', this.dateList);
                this.dateList.sort(function (a, b) {
                    // Turn your strings into dates, and then subtract them
                    // to get a value that is either negative, positive, or zero.
                    if (a > b) return 1;
                    if (a < b) return -1;
                    return 0;
                });
                this.mainEventList = res.data;
                console.log('mainEventList', this.mainEventList);
                this.filterCurrent(this.dateString);
            });
        }
    }

    filterCurrent(date: any) {
        var dd = ("0" + (this.selectedDay)).slice(-2);
        var mn = ("0" + (this.selectedMonth + 1)).slice(-2);
        var yy = this.YearMonth;
        var dateformat = [yy, mn, dd].join("-");
        try {
            if (this.CurrentStationlist.length > 0) {
                this.eventList = [];
                for (let mel of this.CurrentStationlist) {
                    if (dateformat == mel.date) {
                        this.eventList.push(mel);
                    }
                }
            }
        }
        catch (e) {
            console.log(e);
        }
    }

    getCurrentsStationMinDetailsnew(stationId: any, fromstr: string, tostr: string, list, index) {
        let self = this;
        if (list.length - 1 !== index) {
            var data = {
                stationId: stationId,
                fromDate: moment(fromstr).format("YYYY-MM-DD"),
                toDate: moment(tostr).format("YYYY-MM-DD")
            }
            self.authService.getCurrentsStationMinDetailsNew(data).subscribe(res => {
                if (res.success) {
                    if (res.data.length < 1) {
                        self.errormsgEnabled = true;
                    }
                    console.log("Index", index)
                    if (res.data.length > 0) {

                        self.currStationminlist.push.apply(self.currStationminlist, res.data);

                        if (index == 0) {
                            self.filterMinDataCurr(1, res.data);
                        } else {
                            self.filterMinDataCurr(0, res.data);
                        }

                        self.getCurrentsStationMinDetailsnew(this.Currents.stationId, list[index + 1].fromDate, list[index + 1].endDate, list, index + 1);
                    } else {
                        if (self.currStationminlist.length < 1) {
                            self.filterMinDataCurr(1, self.CurrentStationlist);
                        }
                    }
                }
            });
        } else {
            console.log('terminated ')
        }
    }

    Errortext: string = '';
    chartData: any;
    filterMinDataCurr(date: any, stationList?: any[]) {
        try {
            if (stationList)
                if (stationList.length > 0) {
                    for (let mel of stationList) {
                        if (mel.event_val == "-") {
                            mel.event_val = "0";
                        }
                        if (mel.event_val) {
                            this.chartData.push([(moment(mel.date_time).unix()) * 1000, JSON.parse(mel.event_val)]);
                        } else {
                            this.chartData.push([(moment(mel.date_time).unix()) * 1000, JSON.parse(mel.speed)]);
                        }
                    }
                    if (this.chartData.length > 1)
                        this.errormsgEnabled = true;

                    this.WsChartData.push({ name: 'Speed (knots)', color: "#000000", data: this.chartData, tooltip: { valueDecimals: 2 } });
                }
            if (date === 1 && this.WsChartData.length > 0)
                this.plotChart()
            else if (this.hgchart && date === 0)
                this.hgchart.update({
                    series: this.WsChartData
                })
            else if (date) {
                this.updatePlotGraph();
            }
        }
        catch (e) {
            console.log(e.messages);
        }
    }

    getfuturemonth() {
        //   debugger
        let dateList = [];
        const today = new Date()
        // today.setDate(1)
        // today.setMonth(today.getMonth() - 3)
        for (let index = 1; index <= 12; index++) {
            const startDate = new Date(today.getFullYear(), today.getDate(), today.getMonth());

            const endDate = new Date(today.getFullYear(), 0, today.getMonth() + 1);
            today.setDate(1)

            dateList.push({
                fromDate: startDate.toLocaleDateString().split('/').reverse().join('-'),
                endDate: endDate.toLocaleDateString().split('/').reverse().join('-'),
            })
            today.setMonth(today.getMonth() + 1)

        }
        return dateList;
    }

    allDates() {
        // get all days in selected month
        // debugger
        let date: Date = new Date(new Date(this.YearMonth, this.selectedMonth, 1));
        console.log('dataDate', date);
        var names = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        this.authService.dateArray = [];
        while (date.getMonth() == this.selectedMonth) {
            this.authService.dateArray.push({ day: date.getDate(), name: names[date.getDay()] });
            date.setDate(date.getDate() + 1);
        }
        if (this.selectedDay > this.authService.dateArray.length)
            this.selectedDay = 1;

        this.getCurrentStationDetails(this.Currents.stationId);
        let futureDateList: any[] = this.getfuturemonth()
        this.currStationminlist = [];
        this.WsChartData = [];
        this.chartData = [];
        if (this.graphEnabled) {
            this.getCurrentsStationMinDetailsnew(this.Currents.stationId, futureDateList[0].fromDate, futureDateList[0].endDate, futureDateList, 0);
        }
        setTimeout(() => {
            try {
                this.leftItems._scrollContent.nativeElement.scrollLeft = (50 * this.selectedDay) - 200;//(temp.clientWidth*(scroll))-50;
            } catch (ex) {

            }
        }, 1000);
    }
    dattime(time) {
        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        let Dtime = time[0] + time[1] + time[2] + time[5];
        console.log('time', time[0] + time[1] + time[2] + time[5]);
        return Dtime; // return adjusted time or original string
    }
    get dateString() {// concat date,month and year to create date string
        return new Date().getFullYear() + "-" + this.selectedMonth + 1 + "-" + this.selectedDay;
    }

    plotChart() {
        const self = this;
        var chart = Highcharts.stockChart('container', {
            chart: {
                height: 550,
                type: 'spline',
                zoomType: 'x',
                polar: true,
                // plotBackgroundImage: 'assets/img/bgchart.png',
                backgroundColor: '#4f92c1',
                style: {
                    color: "#FFFFFF"
                }
            },
            tooltip: {
                xDateFormat: '%A, %b %e, %Y, %l:%M %p',
            },
            title: {
                text: 'Todays Currents (LST/LDT)',
                style: {
                    color: "#FFFFFF"
                }
            },
            xAxis: {
                alternateGridColor: '#FFFFFF',
                type: 'datetime',
                dateTimeLabelFormats: {
                    // day: '%e of %b',
                    // minute:  '%I:%M',
                    hour: '%I:%M %p'
                },
                labels: {
                    overflow: 'justify',
                    style: {
                        color: "#FFFFFF"
                    }
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: "#000000"
                    }
                },
                title: {
                    text: 'Speed (knots)',
                    style: {
                        color: "#FFFFFF"
                    }

                },
                plotLines: [
                    {
                        value: 1,
                        color: 'red',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: 'Flood'
                        }
                    }, {
                        value: 0,
                        color: 'green',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: 'Slack'
                        }
                    }, {
                        value: -1,
                        color: 'yellow',
                        dashStyle: 'shortdash',
                        width: 2,
                        label: {
                            text: 'Ebb'
                        }
                    }]
            },
            plotOptions: {
                series: {
                    fillColor: {
                        linearGradient: [0, 0, 0, 300],
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    }
                }
            },
            rangeSelector: {
                inputEnabled: false,
                buttonTheme: { // styles for the buttons             
                    style: {
                        color: '#039',
                        fontWeight: 'bold',
                        Width: '220px',
                        height: '50px'
                    },
                    states: {
                        hover: {
                        },
                        select: {
                            fill: '#039',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                },
                labelStyle: {
                    color: '#FFF',
                    fontWeight: 'bold'
                },
                selected: this.range,
                buttons: [{
                    type: 'day',
                    count: 3,
                    text: '1d',
                    days: 1
                }, {
                    type: 'week',
                    count: 3,
                    text: '1w',
                    days: 7
                }, {
                    type: 'month',
                    count: 2,
                    text: '1m',
                    days: 30
                }, {
                    type: 'all',
                    text: 'All'
                }],
            },
            series: this.WsChartData
        });
        var submit = document.getElementById('btnBack');
        submit.addEventListener('click', () => {
            var theChart = chart,
                extremes = theChart.xAxis[0].getExtremes(),
                minTime = extremes.min,
                maxTime = extremes.max,
                currRangeIndex = theChart.rangeSelector.selected,
                currRange = theChart.rangeSelector.buttonOptions[currRangeIndex],
                newMaxTime = minTime;

            // new start/end times
            var startTime,
                endTime;

            if (currRange.type == 'month') {
                var endDate = new Date(newMaxTime);
                var startDate = moment(endDate);
                startDate.subtract(currRange.count, 'months');

                startTime = startDate.valueOf();

                endTime = newMaxTime;
               // this.setGraphChangedate(endTime)

               console.log("selectedMonth", this.selectedMonth)
               this.selectedMonth = this.selectedMonth - 1
               console.log("selectedMonth", this.selectedMonth)
             

               if ((this.selectedMonth) >= 0)
                   this.monthChange()
               else {
                   this.YearMonth = this.YearMonth - 1
                   console.log("YearMonth", this.YearMonth)
                   this.getYear(this.YearMonth)


               }

            } else if (currRange.type == 'week') {
                var endDate = new Date(newMaxTime);
                var startDate = moment(endDate);
                startDate.subtract(currRange.count, 'week');

                startTime = startDate.valueOf();
                endTime = newMaxTime;
                //this.setGraphChangedate(endTime)
                if ((this.selectedDay - currRange.days) > 0) {
                    if (this.selectedMonth == 2 && (this.selectedDay - currRange.days) > 27) {
                        this.onDateClick((27), 0)
                    } else {
                        this.onDateClick((this.selectedDay - currRange.days), 0)
                    }
                }
                else {
                    this.selectedMonth = this.selectedMonth - 1
                    this.selectedDay = (this.selectedDay - currRange.days) + 30
                    if (this.selectedMonth == 2 && (this.selectedDay) > 27) {
                        this.selectedDay = 27
                        this.monthChange()
                    }
                    else {
                        this.monthChange()
                    }
                }

            } else if (currRange.type == 'day') {
                var endDate = new Date(newMaxTime);
                var startDate = moment(endDate);
                startDate.subtract(currRange.count, 'day');

                startTime = startDate.valueOf();
                endTime = newMaxTime;
               // this.setGraphChangedate(endTime)

               if ((this.selectedDay - currRange.days) > 0) {
                if (this.selectedMonth == 2 && (this.selectedDay - currRange.days) > 27) {
                    this.onDateClick((27), 0)
                } else {
                    this.onDateClick((this.selectedDay - currRange.days), 0)

                }
            } else {
                this.selectedMonth = this.selectedMonth - 1
                this.selectedDay = 1
                if (this.selectedMonth == 2 && (this.selectedDay) > 27) {
                    this.selectedDay = 27
                    this.monthChange()
                } else {
                    this.monthChange()

                }
            }

            }
            //theChart.xAxis[0].setExtremes(startTime, endTime);
        });
        var submit = document.getElementById('btnFwd');
        submit.addEventListener('click', () => {
            var theChart = chart,
                extremes = theChart.xAxis[0].getExtremes(),
                minTime = extremes.min,
                maxTime = extremes.max,
                currRangeIndex = theChart.rangeSelector.selected,
                currRange = theChart.rangeSelector.buttonOptions[currRangeIndex],
                newMaxTime = maxTime;

            // new start/end times
            var startTime,
                endTime;

            if (currRange.type == 'month') {
                var endDate = new Date(newMaxTime);
                var startDate = moment(endDate);
                startDate.add(currRange.count, 'months');
                startTime = startDate.valueOf();
                endTime = newMaxTime;
                //this.setGraphChangedate(endTime)
                console.log("selectedMonth", this.selectedMonth)
                this.selectedMonth = this.selectedMonth + 1
                console.log("selectedMonth", this.selectedMonth)
               
                if ((this.selectedMonth) < 12) {
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.monthChange()

                    } else {
                        this.monthChange()
                    }
                }
                else {
                    this.YearMonth = this.YearMonth + 1
                    console.log("YearMonth", this.YearMonth)
                    this.getYear(this.YearMonth)

                }

            } else if (currRange.type == 'week') {
                var endDate = new Date(newMaxTime);
                var startDate = moment(endDate);
                startDate.add(currRange.count, 'week');

                startTime = startDate.valueOf();
                endTime = newMaxTime;
                // this.setGraphChangedate(endTime)

                if ((this.selectedDay + currRange.days) < 30) {
                    this.onDateClick((this.selectedDay + currRange.days), 0)
                }
                else {
                    this.selectedMonth = this.selectedMonth + 1
                    this.selectedDay = (this.selectedDay + currRange.days) - 30
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.monthChange()
                    } else {
                        this.monthChange()
                    }
                }

            } else if (currRange.type == 'day') {
                var endDate = new Date(newMaxTime);
                var startDate = moment(endDate);
                startDate.add(currRange.count, 'day');

                startTime = startDate.valueOf();
                endTime = newMaxTime;
                // this.setGraphChangedate(endTime)

                if ((this.selectedDay + currRange.days) < 31) {
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.onDateClick((this.selectedDay), 0)
                    } else {
                        this.onDateClick((this.selectedDay + currRange.days), 0)

                    }
                } else {
                    this.selectedMonth = this.selectedMonth + 1
                    this.selectedDay = 1
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.monthChange()

                    } else {
                        this.monthChange()
                    }
                }

            }
            //theChart.xAxis[0].setExtremes(endTime, startTime);
        });
        this.hgchart = chart;
        this.updatePlotGraph();
    }
    setGraphChangedate(setdate: any) {

        this.YearMonth = new Date(setdate).getFullYear()
        //alert(this.YearMonth)
        this.selectedMonth = new Date(setdate).getMonth()
        this.selectedDay = new Date(setdate).getDate()
    }

    updatePlotGraph() {
        var v = new Date();
        setTimeout(() => {
            try {
                this.leftItems._scrollContent.nativeElement.scrollLeft = (50 * this.selectedDay) - 200;//(temp.clientWidth*(scroll))-50;
            } catch (ex) {

            }
        }, 1000);
        this.hgchart.xAxis[0].setExtremes(
            Date.UTC(v.getFullYear(), this.selectedMonth, this.selectedDay - 2),
            Date.UTC(v.getFullYear(), this.selectedMonth, this.selectedDay + 1)
        );
        if (this.hgchart.series.length < 1) {
            if (this.errormsgEnabled) {
                this.hgchart.hideLoading();
                this.hgchart.renderer.text('Data is not available for this station', 10, 120)
                    .css({
                        color: '#FFFFFF',
                        fontSize: '20px'
                    })
                    .add();
            }
        } else if (this.hgchart.series.length >= 1) {
            this.hgchart.hideLoading();
        }
        let currRangeIndex = this.hgchart.rangeSelector.selected;
        let currRange = this.hgchart.rangeSelector.buttonOptions[currRangeIndex];
        var endDate_1 = (Date.UTC(this.YearMonth, this.selectedMonth, this.selectedDay)) - 1 * 24 * 60 * 60 * 1000;
        var startDate_1 = moment(endDate_1);
        //startDate_1.add(currRange.count, 'day');
        switch (currRangeIndex) {
            case 0:
                startDate_1.add(currRange.count, 'day');
                break;
            case 1:
                startDate_1.add(currRange.count, 'week');
                break;
            case 2:
                startDate_1.add(currRange.count, 'month');
                break;
            default:
                startDate_1.add(currRange.count, 'day');


        }
        var startTime_1 = startDate_1.valueOf();
        var endTime_1 = endDate_1;
        console.log(endTime_1, startTime_1);
        this.hgchart.xAxis[0].setExtremes(endTime_1, startTime_1);
        // chart.showLoading();
        // this.hgchart.showLoading();
        // let currRangeIndex = this.hgchart.rangeSelector.selected;
        // let currRange = this.hgchart.rangeSelector.buttonOptions[currRangeIndex];
        // var endDate_1 = (Date.UTC(this.YearMonth, this.selectedMonth + 1, this.selectedDay)) - 1 * 24 * 60 * 60 * 1000;
        // var startDate_1 = moment(endDate_1);
        // startDate_1.add(currRange.count, 'day');
        // var startTime_1 = startDate_1.valueOf();
        // var endTime_1 = endDate_1;
        // console.log(endTime_1, startTime_1);
        // this.hgchart.xAxis[0].setExtremes(endTime_1, startTime_1);
        // if (this.hgchart.series.length < 1) {
        //     if (this.errormsgEnabled) {
        //         this.hgchart.hideLoading();
        //         this.hgchart.renderer.text('6 minutes prediction data is not available for this station', 10, 120)
        //             .css({
        //                 color: '#FFFFFF',
        //                 fontSize: '16px'
        //             })
        //             .add();
        //     }
        // } else if (this.hgchart.series.length >= 1) {
        //     this.hgchart.hideLoading();
        // }
    }

}