// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { ActionSheetController, AlertController, App, IonicPage, Loading, LoadingController, NavController, Platform, Slides, ToastController } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-provider';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'RequestTrip',
  templateUrl: 'RequestTrip.html',
})
export class RequestTripPage {
  public loginForm: any;
  User: any;
  // public backgroundImage = 'assets/img/background/background-6.jpg';
  type = "password";
  Countrycode: any;
  CountryName: any;
  countries: any[] = [];
  PhoneCode: any;
  show = false;
  userDetails: any;
  RequestTripForm: FormGroup;
  fishingtypes: AbstractControl;
  Locationtypes: AbstractControl;
  WillingPrice: AbstractControl;
  Person: AbstractControl;
  Boatypes: AbstractControl;
  Date: AbstractControl;
  Time: AbstractControl;
  UserPhonecode: any;
  loading: Loading;
  startDatetimeMin: any = new Date(Date.now()).toISOString();
  Profileimg: any;
  UserProfileImg: any;
  image_url: any;
  img: any;
  lastImage: string = null;
  imgData: any;
  Country1: string;
  TimeData: any[] = [
    {
      id: 1,
      title: "1/2 Day Morning"
    },
    {
      id: 2,
      title: "1/2 Day Afternoon"
    },
    {
      id: 3,
      title: "Full Day"
    }];
  FishBoatTypeList: any;
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public navCtrl: NavController,
    public http: Http,
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,

  ) {
    this.countries = [
      {
        id: '1',
        Person: 'abc',
        title: 'Rod Busting Seriest',
      },
      {
        id: '1',
        Person: 'abc',
        title: 'Rod Busting Seriest',
      },
      {
        id: '1',
        Person: 'abc',
        title: 'Rod Busting Seriest',
      }
    ];

    this.RequestTripForm = this.formBuilder.group({
      'fishingtypes': ['', Validators.required],
      'Locationtypes': ['', Validators.required],
      'WillingPrice': ['', Validators.required],
      'Person': ['', Validators.required],
      'Boatypes': ['', Validators.required],
      'Date': ['', Validators.required],
      'Time': ['', Validators.required],
    });
    //     this.FirstName = this.RequestTripForm.controls['FirstName'];
    //     this.MiddleName = this.RequestTripForm.controls['MiddleName'];
    //     this.LastName = this.RequestTripForm.controls['LastName'];
    this.User = "angler";
  }

  ErrorMsg: boolean = false;
  ErrormsgText:any;
  save() {
    // this.uploadImage();
    // var mobNo= this.RequestTripForm.controls['fishingtypes'].value;
    // var PhCode= this.Countrycode;
    let data = {
      title: this.User,
      fishing_type_id: this.RequestTripForm.controls['fishingtypes'].value,
      fishing_location_id: this.RequestTripForm.controls['Locationtypes'].value,
      boat_type_id: this.RequestTripForm.controls['Boatypes'].value,
      date: this.RequestTripForm.controls['Date'].value,
      time: this.RequestTripForm.controls['Time'].value,
      trip_amount: this.RequestTripForm.controls['WillingPrice'].value,
      trip_booking_type: "per_person",
      member: this.RequestTripForm.controls['Person'].value
    }
    console.log('data-->', data);

    //  if (this.RegisterForm.valid) {
    //    this.presentLoading();
    this.authService.AddRequestTrip(data).subscribe(res => {
      if (res.success) {
      //  this.presentToast('Request Trip successfully created.');
        this.navCtrl.setRoot('FishingExchangPage');
        let alert = this.alertCtrl.create({
          //title: 'Exit?',
          message: 'Request Trip successfully created.',
          buttons: [
            {
              text: 'OK',
              role: 'OK',
              handler: () => {
                //  this.alert = null
              }
            }
          ]
        });
        alert.present();
        console.log('Output for signupdata-->');
      } else if (res.error[0].param == "trip_amount") {
        this.ErrorMsg = true;
        this.ErrormsgText=res.error[0].msg;
      }
      // else {
      //   this.presentToast('Please add Payment card.');
      //   this.navCtrl.push('PaymentlistPage');
      // }

    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  //}





  onLocationChange() {
    // var testname= this.RequestTripForm.controls['Countryoption'].value;
    //  console.log(testname.dial_code);
    // this.CountryName=testname.name;
    // this.Countrycode=testname.dial_code;

  }
  // Slider methods
  @ViewChild('slider') slider: Slides;
  @ViewChild('innerSlider') innerSlider: Slides;

  // backToPage() {
  //  // alert("Hello");
  //   this.navCtrl.push('LoginListPage');
  // }


  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }


}
