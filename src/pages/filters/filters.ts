import { Component, Input } from '@angular/core';
import { IonicPage, NavController, MenuController, NavParams } from 'ionic-angular';
import { AuthService, Event } from '../../providers/auth-provider';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import * as _ from 'lodash';
import { ViewController } from 'ionic-angular/navigation/view-controller';

@IonicPage()
@Component({
    selector: 'page-filters',
    templateUrl: 'filters.html'
})
export class filtersPage {
    @Input('data') data: any;
    @Input('events') events: any;

    drawerOptions: any;
    eventList: Event[] = [];
    TmweventList: Event[] = [];
    eventSource;
    viewTitle;
    monthNames: any = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    isToday: boolean;
    lockSwipeToPrev: boolean;
    month: string;
    mainEventList: Event[] = [];
    mainEventFillterList: any[] = [];
    EventType: any[] = [];
    dateList: Date[] = [];
    IPushMessage: any[];
    showPageView: boolean = false;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
    filterobject: any[] = [];
    public filteredItems: any;
    public issearch = false;
    public i: any;
    public keywords: any[];
    inputName: any = '';
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;
    FXdata: any;
    EndDate: any;
    StartDate: any;
    filterOption: any[] = [];
    filterlist: any[] = [
        {
            Id: 5,
            Label: 'Date'
        },
        {
            Id: 1,
            Label: 'Location'
        },
        // {
        //     Id: 2,
        //     Label: 'Fish Type'
        // },
        // {
        //     Id: 3,
        //     Label: 'Boat Type'
        // },
        // {
        //     Id: 4,
        //     Label: 'No Of Seats'
        // },
        // {
        //     Id: 6,
        //     Label: 'Duration Of Trip'
        // },

    ];
    TimeData: any[] = [
        {
            id: 1,
            title: "1/2 Trip - Day"
        },
        {
            id: 2,
            title: "1/2 Trip - Afternoon"
        },
        {
            id: 3,
            title: "Full Day Trip"
        }];
    startDatetimeMin: any = new Date(Date.now()).toISOString();
    fromDate: any;
    toDate: any;
    noSeats: number = 1;
    minRange: number;
    maxRange: number;
    mat_selectedIndex: number = 0;
    constructor(
        public navParams: NavParams, public viewCtrl: ViewController,
        public navCtrl: NavController, public alertCtrl: AlertController,
        public menuCtrl: MenuController, public authService: AuthService
    ) {
        console.log('filterlist', this.filterlist);
        this.DateEnabled = true;
        this.authService.getboattypeslist();
        this.authService.getfishinglocationslist();
        this.authService.getfishingtypeslist();
        this.filterOption = this.authService.LocationtypesList;
        this.data = {
            max: "1000",
            min: "1",
            step: "10",
            textLeft: "1",
            textRight: "1000",
            title: "TWO SLIDERS",
            value: {
                lower: 1,
                upper: 1000
            }
        }
    }

    formatLabel(value: number | null) {
        if (!value) {
            return 0;
        }

        if (value >= 1000) {
            return Math.round(value / 1000) + 'k';
        }

        return value;
    }

    loc_data: any[] = [];
    saveData() {
        this.authService.LocationtypesList.forEach(ele => {
            if (ele.selected)
                this.loc_data.push(ele._id);
        });
        this.authService.filterData = {
            fromDate: this.fromDate,
            toDate: this.toDate,
            price: {
                minRange: this.minRange,
                maxRange: this.maxRange
            },
            Location: this.loc_data,
            //  filterDataEnabled: true,
        }
        // this.authService.filterData = {
        //     fromDate: this.fromDate,
        //     toDate: this.toDate,
        //     minAmount: this.minRange,
        //     maxAmount: this.maxRange,
        //     fishing_location_id: this.loc_data,
        //     //  filterDataEnabled: true,
        // }
        this.navCtrl.setRoot('FishingExchangPage', { filterData: this.authService.filterData });
    }
    dataFilter: any;

    clearData() {
        this.fromDate = '';
        this.toDate = '';
        // this.filterlist = [];
        // this.DateEnabled = false;
        this.Allselected = true;
        this.authService.getboattypeslist();
        this.authService.getfishinglocationslist();
        this.authService.getfishingtypeslist();
        this.filterOption = this.authService.LocationtypesList;
        this.data = {
            max: "1000",
            min: "1",
            step: "10",
            textLeft: "1",
            textRight: "1000",
            title: "TWO SLIDERS",
            value: {
                lower: 1,
                upper: 1000
            }
        };
    }

    myrange(newRangeValue) {
        this.minRange = newRangeValue._valA;
        this.maxRange = newRangeValue._valB;
        console.log("Range :" + newRangeValue._valA + newRangeValue._valB);
    }

    SeatsEnabled: boolean = false;
    DateEnabled: boolean = false;
    onLinkClick(event: any) {
        console.log('even', event.tab.textLabel);
        console.log('even', event);
        this.SeatsEnabled = false;
        this.DateEnabled = false;
        this.mat_selectedIndex = event.index;
        if (event.tab.textLabel === "Fish Type") {
            this.filterOption = this.authService.fishingtypesList;
        } else if (event.tab.textLabel === "Boat Type") {
            this.filterOption = this.authService.BoatypesList;
        } else if (event.tab.textLabel === "Location") {
            this.filterOption = this.authService.LocationtypesList;
        } else if (event.tab.textLabel === "Duration Of Trip") {
            this.filterOption = this.TimeData;
        } else if (event.tab.textLabel === "No Of Seats") {
            this.SeatsEnabled = true;
        } else if (event.tab.textLabel === "Date") {
            this.DateEnabled = true;
        }
        console.log('event', this.filterOption);
    }
    minusAdult() {
        this.noSeats--;
    }
    plusAdult() {
        this.noSeats++;
    }

    ngOnChanges(changes: { [propKey: string]: any }) {
        this.data = changes['data'].currentValue;
    }
    singleValue4: any = 10;
    onEvent = (event: string, item: any): void => {
        // if (this.events[event]) {
        //     this.events[event](item);
        // }
    }
    Allselected: boolean = true;
    selectedOptions: string[];
    onNgModelChange(list: any) {
        console.log('event', list);
        // this.Allselected = false;
        if (list === 'All') {
            this.Allselected = !this.Allselected;
            if (this.Allselected) {
                this.authService.LocationtypesList.forEach(element => {
                    element.selected = true;
                });
                this.filterOption = this.authService.LocationtypesList;
            } else {
                this.authService.LocationtypesList.forEach(element => {
                    element.selected = false;
                });
                this.filterOption = this.authService.LocationtypesList;
            }
        } else {
            this.authService.LocationtypesList.forEach(element => {
                if (element._id == list._id) {
                    if (element.selected)
                        element.selected = false;
                    else
                        element.selected = true;

                }
            });
            this.filterOption = this.authService.LocationtypesList;
        }
    }
}
