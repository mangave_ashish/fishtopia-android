import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Facebook } from '@ionic-native/facebook';
import { FCM } from '@ionic-native/fcm';
import { GooglePlus } from '@ionic-native/google-plus';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { Market } from '@ionic-native/market';
import { Network } from '@ionic-native/network';
import { AlertController, IonicPage, LoadingController, MenuController, NavController, Platform } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { UserData } from '../../providers/user-data-local';

@IonicPage()
@Component({
    selector: 'page-login',
    templateUrl: 'login.html',
})
export class LoginListPage implements OnInit {
    type = "password";
    show = false;
    loginForm: FormGroup;
    Email: AbstractControl;
    Password: AbstractControl;
    submitAttempt: boolean = false;
    //loading: any;
    loading: any;
    public showSpinner: boolean;
    AppVersion: any;
    device_idd: any;
    isLoggedIn: boolean = false;
    users: any;
    pageName: string;
    Social_NetEnabled: any;
    Fb_UserData: any;
    userType: any;
    public product: any = {
        name: 'Alaska FishTopia',
        appleProductId: 'AKFISHTOPIA01',
        googleProductId: 'akfishtopiayearlysubscription'
    };
    productId: any;
    constructor(
        public loadingCtrl: LoadingController,
        public alertCtrl: AlertController,
        public navCtrl: NavController,
        public menuCtrl: MenuController,
        private formBuilder: FormBuilder,
        public userData: UserData,
        public authService: AuthService,
        private device: Device,
        public fcm: FCM,
        public app: AppVersion,
        private fb: Facebook,
        private Gplus: GooglePlus,
        public network: Network,
        private iap: InAppPurchase,
        public platform: Platform,
        private market: Market,
    ) {
        this.menuCtrl.swipeEnable(false);
        this.loginForm = this.formBuilder.group({
            'Email': ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-z0-9.-]+[.]{1}[a-zA-Z]{2,}$')])],
            'Password': ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
        });
        this.Email = this.loginForm.controls['Email'];
        this.Password = this.loginForm.controls['Password'];
        this.fb.logout().then(res_ => {
            console.log(JSON.stringify(res_));
        });

        this.Gplus.logout().then(res_ => {
            console.log(JSON.stringify(res_));
        })
        fb.getLoginStatus()
            .then(res => {
                if (res.status === "connect") {
                    this.isLoggedIn = true;
                } else {
                    this.isLoggedIn = false;
                }
            })
            .catch(e => alert(e));
        app.getVersionNumber().then((res) => {
            this.AppVersion = res;
        });
    }

    ngOnInit() {
        // if (this.platform.is('ios')) {
        //     this.productId = this.product.appleProductId;
        // } else if (this.platform.is('android')) {
        //     this.productId = this.product.googleProductId;
        // }
        // this.platform.ready().then(() => {
        //     this.iapCheck();
        // });
    }

    iapCheck() {
        this.iap
            .restorePurchases()
            .then((data: any) => {
                if (data.length > 0) {
                    for (var i = 0; i < data.length; ++i) {
                        if (data[i].productId == this.productId) {
                            localStorage.setItem('SubsDetails', JSON.stringify(data[i].receipt));
                        } else {
                            this.menuCtrl.swipeEnable(false);
                            this.navCtrl.setRoot('subscribeDialogPage');
                        }
                    }
                } else {
                    this.menuCtrl.swipeEnable(false);
                    this.navCtrl.setRoot('subscribeDialogPage');
                }
            }).catch((err) => {
                console.log(JSON.stringify(err));
            });
    }

    alert: any;
    alertcheck: boolean = false;
    NetworkCheck() {
        this.network.onDisconnect().subscribe(() => {
            if (this.alert) {
                this.alert.dismiss();
                this.alert = null;
            } else {
                this.authService.INTERNETCHECK = true;
                if (this.alertcheck == false) {
                    var title = 'NO INTERNET';
                    var msg = 'Please check your network connection.';
                    this.showAlertNetwork(title, msg);
                }
            }
        });
        this.network.onConnect().subscribe(() => {
            this.authService.INTERNETCHECK = false;
            if (this.alert) {
                this.alert.dismiss();
                this.alert = null;
            }
            //  else {
            //   if (this.alertcheck == false) {
            //     var title = 'NO INTERNET';
            //     var msg = 'Please check your network connection.';
            //     this.showAlertNetwork(title, msg);
            //   }
            //}
        });
    }
    showAlertNetwork(title: any, msg: any) {
        this.alertcheck = true;
        //	let alert = this.alertCtrl.create({
        // 	title: 'Information',
        // 	message: message,
        // 	buttons: ['Ok']
        // });
        // alert.present();
        let alert = this.alertCtrl.create({
            title: title,
            message: msg,
            buttons: [
                {
                    text: 'Ok',
                    role: 'Ok',
                    handler: () => {
                        this.alertcheck = false;
                        this.alert = null
                    }
                }
            ]
        });
        alert.present();
        return true;
    }
    GplusDB: any;
    googlePlus_login() {
        this.Gplus.login({})
            .then((res: any) => {
                console.log('JSON.stringify(res)', JSON.stringify(res));
                this.GplusDB = {
                    tem_id: res.accessToken,
                    email: res.email,
                    displayName: res.displayName,
                    familyName: res.familyName,
                    givenName: res.givenName,
                    userId: res.userId,
                    expires: res.expires,
                    expires_in: res.expires_i
                }
                this.Social_NetEnabled = 'Google+';
                this.login();
            })
            .catch(err => console.log(JSON.stringify(err)));
    }
    fb_login() {
        this.fb.login(['public_profile', 'email'])
            .then(res => {
                if (res.status === "connected") {
                    this.isLoggedIn = true;
                    this.GplusDB = res.authResponse;
                    this.getUserDetail(res.authResponse.userID);
                } else {
                    this.isLoggedIn = false;
                }
            })
            .catch(e => console.log('Error logging into Facebook', e));
    }
    getUserDetail(userid) {
        this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
            .then(res => {
                this.Social_NetEnabled = 'Facebook';
                this.Fb_UserData = res;
                this.login();
                // alert(JSON.stringify(res));
                //  console.log(JSON.stringify(res));
                //this.facebookLogin(res);

            })
            .catch(e => {
                console.log(e);
            });
    }
    facebookLogin() {
        var data = {
            tem_id: this.GplusDB.accessToken,
            email: this.Fb_UserData.email,
            displayName: this.Fb_UserData.name,
            familyName: this.Fb_UserData.name,
            givenName: this.Fb_UserData.name,
            userId: this.GplusDB.userID,
            expires: this.GplusDB.expiresIn,
            expires_in: this.GplusDB.expiresIn,
            signUpType: 'Facebook'
        }
        this.authService.googleAccsess(data).subscribe(
            res => this.SocialNetworkSignup(res),
            error => alert('push' + JSON.stringify(error))
        )
    }


    googleLogin() {
        var data = {
            tem_id: this.GplusDB.accessToken,
            email: this.GplusDB.email,
            displayName: this.GplusDB.displayName,
            familyName: this.GplusDB.familyName,
            givenName: this.GplusDB.givenName,
            userId: this.GplusDB.userId,
            expires: this.GplusDB.expires,
            expires_in: this.GplusDB.expires_i,
            signUpType: 'Google+'
        }
        this.authService.googleAccsess(data).subscribe(
            res => this.SocialNetworkSignup(res),
            error => alert('push' + JSON.stringify(error))
        )
    }

    // SocialNetwork_Data: any;
    SocialNetworkSignup(res: any) {
        var data = {
            email: res.data.email,
            device_type: this.device.platform,
            unique_device_id: this.device.uuid,
            device_id: this.Notifytoken,
            app_version: this.AppVersion,
            Social_NetEnabled: this.Social_NetEnabled
        }
        if (res.data._id == undefined && res.data._id == null) {
            this.navCtrl.push('UserTypePage', { SocialNetdata: data });
        } else {
            this.doLoginSocialNetwork(data, res.data.user_type);
        }
    }
    doLoginSocialNetwork(res: any, user_type: any) {
        var data = {
            email: res.email,
            device_type: res.device_type,
            unique_device_id: res.unique_device_id,
            device_id: res.device_id,
            user_type: user_type,
            app_version: this.AppVersion,
            Social_NetEnabled: this.Social_NetEnabled
        }
        // alert('Arjun' + JSON.stringify(data));
        this.authService.googleSignUp(data).subscribe(
            data => this.SN_loginSucessfully(data),
            error => console.log(JSON.stringify(error))
        );
    }

    SN_loginSucessfully(data) {
        // let output = data;
        // // console.log('Login data', JSON.stringify(output));
        // if (output.success === false) {
        //     this.errorMsg = output.message;
        //     this.submitAttempt = true;
        // }
        // else {
        //     let auth = output.data.user_token.access_token;
        //     this.userData.setAuthToken(auth);
        //     console.log('output.data.', output.data);
        //     localStorage.setItem("token", auth);
        //     localStorage.setItem("unique_device_id", output.data.uuid);
        //     localStorage.setItem("userId", output.data._id);
        //     localStorage.setItem("UserType", output.data.user_type);
        //     this.getDataOffline();
        //     this.navCtrl.setRoot('dashboardPage');
        // }
        let output = data;
        console.log("OUTPUT DATA ", JSON.stringify(output));

        this.device.uuid = output.data.uuid
        output.data.access_token = output.data.user_token.access_token
        output.data.user_type = output.data.user_type
        output.data.original_otp = output.data.original_otp

        this.loginSucessfully(output)
    }

    logout() {
        this.fb.logout()
            .then(res => this.isLoggedIn = false)
            .catch(e => console.log('Error logout from Facebook', e));
    }


    goToSignup() {
        //this.slider.slideTo(2);
        this.navCtrl.push('UserTypePage');
    }
    signup() {
        //this.presentLoading('Thanks for signing up!');
        this.navCtrl.push('UserTypePage');
    }
    resetPassword() {
        this.navCtrl.push('forgotPasswordPage');
    }

    regulationRedirect() {
        this.authService.UserType = "guest";
        this.pageName = "regulations";
        this.loginregulation();
    }
    loginregulation() {
        let self = this;

        self.fcm.subscribeToTopic('Hello');
        self.fcm.getToken().then(token => {
            self.Notifytoken = token;
            console.log('token', token);
            // self.dologin(self.Notifytoken);
            if (self.Social_NetEnabled == 'Google+') {
                self.googleLogin();
            } else if (self.Social_NetEnabled == 'Facebook') {
                self.facebookLogin();
            } else {
                self.Social_NetEnabled = '';
                self.dologinregulation(self.Notifytoken);
            }
        })
        self.fcm.onNotification().subscribe(data => {
            console.log('data', JSON.stringify(data))
            if (data.wasTapped) {
                console.log("Received in background");
                //if user NOT using app and push notification comes
                //TODO: Your logic on click of push notification directly
                console.log('Push notification clicked');
                self.redirectPage(data);
            }
            else {
                let confirmAlert = self.alertCtrl.create({
                    title: 'New Notification',
                    message: data.body,
                    buttons: [{
                        text: 'Ignore',
                        role: 'cancel'
                    }, {
                        text: 'View',
                        handler: () => {
                            //TODO: Your logic here
                            self.redirectPage(data);
                        }
                    }]
                });
                confirmAlert.present();
            }
            //console.log("Received in background", JSON.stringify(data));
            self.authService.getNotificationsCount();
        })
        self.fcm.onTokenRefresh().subscribe(token => {
            console.log(token);
            self.refreshToken(token);
        });
    }
    dologinregulation(Notifytoken: any) {
        var Notify_token = Notifytoken;
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3500
        });
        loading.present();
        if (this.authService.UserType == "guest") {
            var email = 'guest@gmail.com';
            var password = 'admin123';
            Notify_token = '';
        } else {
            email = this.loginForm.value['Email'];
            password = this.loginForm.value['Password'];
        }
        var data = {
            email: email,
            password: password,
            device_type: this.device.platform,
            unique_device_id: this.device.uuid,
            device_id: Notify_token,
            app_version: this.AppVersion


            // email: email,
            // password: password,
            // device_type: "ios",
            // unique_device_id: "",
            // device_id: Notify_token,
            // app_version: "6.0.47"
        };
        if (localStorage.getItem("old_token") !== undefined && localStorage.getItem("old_token") !== null) {
            this.authService.logout(localStorage.getItem("token")).subscribe(
                res => res,
                error => error
            )
            this.authService.doLogin(data).subscribe(
                data => {
                    loading.dismiss();
                    this.loginSucessfullyregulation(data)
                },
                error => console.log(error)
            );
        } else if (this.loginForm.valid) {
            this.authService.doLogin(data).subscribe(
                data => {
                    loading.dismiss();
                    this.loginSucessfully(data)
                },
                error => console.log(JSON.stringify(error))
            );
        } else if (this.authService.UserType == "guest") {
            this.authService.doLogin(data).subscribe(
                data => {
                    loading.dismiss();
                    this.loginSucessfully(data)
                },
                error => console.log(JSON.stringify(error))
            );
        }
    }
    loginSucessfullyregulation(data) {
        let output = data;
        console.log('Login data', output);
        if (output.success === false) {
            this.errorMsg = output.message;
            this.submitAttempt = true;
        }
        else {
            let auth = output.data.access_token;
            this.userData.setAuthToken(auth);
            console.log('output.data.', output.data);
            localStorage.setItem("token", auth);
            localStorage.setItem("unique_device_id", this.device.uuid);
            localStorage.setItem("userId", output.data._id);
            localStorage.setItem("UserType", output.data.user_type);
            localStorage.setItem("original_otp", output.data.original_otp);

            if (output.data.user_type == "guest") {
                //this.getDataOffline();
                this.authService.getMarinelistinfo();
                this.authService.getProfileDetails();
                if (this.pageName === 'regulations') {
                    this.navCtrl.setRoot('regulationsList');
                } else {
                    this.navCtrl.setRoot('marineWeatherPage');
                }
            } else {
                // localStorage.removeItem('original_otp');
                let self = this;
                let original_otp = localStorage.getItem("original_otp");
                console.log("original_otp", original_otp)
                if (original_otp != null) {
                    if (original_otp) {
                        let abc: any = false
                        let alert = this.alertCtrl.create({
                            message: "Please upgrade an app",
                            buttons: [
                                {
                                    text: 'Okay',
                                    handler: () => {
                                        if (abc) {
                                            console.log("Abc true")
                                            localStorage.setItem("OTP_logged", 'true')
                                            self.navCtrl.setRoot('dashboardPage');
                                            self.getDataOffline();
                                        }
                                        else {
                                            console.log("Abc false")
                                            return false

                                        }
                                    }
                                },
                                {
                                    text: 'Subscribe',
                                    handler: () => {
                                        console.log("SUbscription")

                                    }
                                }
                            ]

                        });
                        alert.present()
                        setTimeout(function () {
                            abc = true
                            // alert.dismiss()
                            // self.navCtrl.setRoot('dashboardPage');
                            // self.getDataOffline();
                        }, 5000);
                        // this.navCtrl.setRoot('dashboardPage');
                        // this.getDataOffline();
                    } else {
                        console.log("NEW USER")
                    }
                } else {
                    console.log("data emptty")

                }
            }
        }
    }
    GuestLogin() {
        this.pageName = "";
        this.authService.UserType = "guest";
        this.login();

    }
    eyesicon: boolean = false;
    toggleShow() {
        this.show = !this.show;
        if (this.show) {
            this.eyesicon = true;
            this.type = "text";
        }
        else {
            this.eyesicon = false;
            this.type = "password";
        }
    }
    getLoader(): void {
        this.loading = this.loadingCtrl.create({
            duration: 3000,
        });
        this.loading.present();
    }
    loaderDismiss() {
        this.loading.dismiss();
    }

    presentLoading() {
        this.loading = this.loadingCtrl.create({
            duration: 3000,
        });
        this.loading.present();
    }
    dologin(Notifytoken: any) {
        var Notify_token = Notifytoken;
        let loading = this.loadingCtrl.create({
            spinner: 'hide',
            content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
            duration: 3500
        });
        loading.present();
        if (this.authService.UserType == "guest") {
            var email = 'guest@gmail.com';
            var password = 'admin123';
            Notify_token = '';
        } else {
            email = this.loginForm.value['Email'];
            password = this.loginForm.value['Password'];
        }
        var data = {
            email: email,
            password: password,
            device_type: this.device.platform,
            unique_device_id: this.device.uuid,
            device_id: Notify_token,
            app_version: this.AppVersion

            // email: email,
            // password: password,
            // device_type:"android",
            // unique_device_id: "",
            // device_id: Notify_token,
            // app_version:"6.0.2"
        };
        if (localStorage.getItem("old_token") !== undefined && localStorage.getItem("old_token") !== null) {
            this.authService.logout(localStorage.getItem("token")).subscribe(
                res => res,
                error => error
            )
            this.authService.doLogin(data).subscribe(
                data => {
                    loading.dismiss();
                    this.loginSucessfully(data)
                },
                error => console.log(error)
            );
        } else if (this.loginForm.valid) {
            this.authService.doLogin(data).subscribe(
                data => {
                    loading.dismiss();
                    this.loginSucessfully(data)
                },
                error => console.log(JSON.stringify(error))
            );
        } else if (this.authService.UserType == "guest") {
            this.authService.doLogin(data).subscribe(
                data => {
                    loading.dismiss();
                    this.loginSucessfully(data)
                },
                error => console.log(JSON.stringify(error))
            );
        }
    }
    errorMsg: any;
    output: any;

    loginSucessfully(data) {
        let otpuser = JSON.parse(localStorage.getItem('OTPL'))
        console.log('otpuser click login', JSON.stringify(otpuser))
        this.output = data;
        let self = this;

        console.log('Login data', this.output);
        if (this.output.success === false) {
            this.errorMsg = this.output.message;
            this.submitAttempt = true;

        }
        else {
            if (this.output.data.is_outdated_app_version) {
                let alert = this.alertCtrl.create({
                    title: 'Get our latest app',
                    message: "Update to our latest app to get the most out of Fishtopia",
                    enableBackdropDismiss: false,
                    buttons: [
                        {
                            text: 'Not now',
                            handler: () => {
                                alert.dismiss()
                                let auth = self.output.data.access_token;
                                self.userData.setAuthToken(auth);
                                console.log('output.data.', self.output.data);
                                localStorage.setItem("token", auth);
                                localStorage.setItem("unique_device_id", self.device.uuid);
                                localStorage.setItem("userId", self.output.data._id);
                                localStorage.setItem("UserType", self.output.data.user_type);
                                localStorage.setItem("original_otp", self.output.data.original_otp);

                                if (self.output.data.user_type == "guest") {
                                    self.authService.getMarinelistinfo();
                                    self.authService.getProfileDetails();
                                    if (self.pageName === 'regulations') {
                                        self.navCtrl.setRoot('regulationsList');
                                    } else {
                                        self.navCtrl.setRoot('marineWeatherPage');
                                    }
                                } else {
                                    let original_otp = JSON.parse(localStorage.getItem("original_otp"));
                                    console.log("original_otp", original_otp)
                                    if (original_otp == true) {
                                        let abc: any = false
                                        let otpuser = JSON.parse(localStorage.getItem("OTPL"))
                                        if (otpuser) {
                                            console.log("local storage otp");
                                            self.navCtrl.setRoot('dashboardPage');
                                            self.getDataOffline();
                                        } else {
                                            let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))
                                            //let otpuser = JSON.parse(localStorage.getItem("OTP_logged"))

                                            if (subscriptionDate) {
                                                var todaysdate = new Date()
                                                var expdate = new Date(subscriptionDate.expirationDate)
                                                console.log("today's date", todaysdate);
                                                console.log("expiry date", expdate);
                                                if (todaysdate <= expdate) {
                                                    console.log("valid subcription");
                                                    self.navCtrl.setRoot('dashboardPage');
                                                    self.getDataOffline();
                                                } else {
                                                    console.log("IN login original_otp true")
                                                    let alert = this.alertCtrl.create({
                                                        message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                                        enableBackdropDismiss: false,
                                                        buttons: [
                                                            {
                                                                text: 'Okay',
                                                                handler: () => {
                                                                    if (abc) {
                                                                        console.log("Abc true")
                                                                        this.setOtp()

                                                                    }
                                                                    else {
                                                                        console.log("Abc false")
                                                                        return false

                                                                    }
                                                                }
                                                            },
                                                            {
                                                                text: 'Subscribe',
                                                                handler: () => {
                                                                    console.log("SUbscription")
                                                                    self.navCtrl.setRoot('SubscriptionLoaderPage');
                                                                }
                                                            }
                                                        ]

                                                    });

                                                    alert.present()
                                                    setTimeout(function () {
                                                        abc = true

                                                    }, 5000);
                                                }
                                            } else {
                                                console.log("IN login original_otp true")
                                                let alert = this.alertCtrl.create({
                                                    message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                                    enableBackdropDismiss: false,
                                                    buttons: [
                                                        {
                                                            text: 'Okay',
                                                            handler: () => {
                                                                if (abc) {
                                                                    console.log("Abc true")
                                                                    this.setOtp()

                                                                }
                                                                else {
                                                                    console.log("Abc false")
                                                                    return false

                                                                }
                                                            }
                                                        },
                                                        {
                                                            text: 'Subscribe',
                                                            handler: () => {
                                                                console.log("SUbscription")
                                                                self.navCtrl.setRoot('SubscriptionLoaderPage');
                                                            }
                                                        }
                                                    ]

                                                });

                                                alert.present()
                                                setTimeout(function () {
                                                    abc = true

                                                }, 5000);
                                            }
                                        }
                                    } else {
                                        console.log("IN login original_otp false")
                                        let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))
                                        if (subscriptionDate) {
                                            var todaysdate = new Date()
                                            var expdate = new Date(subscriptionDate.expirationDate)
                                            console.log("today's date", todaysdate);
                                            console.log("expiry date", expdate);
                                            if (todaysdate <= expdate) {


                                                console.log("valid subcription");
                                                self.navCtrl.setRoot('dashboardPage');
                                                self.getDataOffline();
                                            } else {
                                                self.navCtrl.setRoot('SubscriptionLoaderPage');
                                            }
                                        } else {
                                            self.navCtrl.setRoot('SubscriptionLoaderPage');
                                        }
                                    }
                                }
                            }
                        },
                        {
                            text: 'Update',
                            handler: () => {
                                /// console.log('Buy clicked');
                                self.market.open('com.FishTopia.alaskaFishTopia');
                            }
                        }
                    ]
                });
                alert.present()
            } else {
                let auth = this.output.data.access_token;
                this.userData.setAuthToken(auth);
                console.log('output.data.', this.output.data);
                localStorage.setItem("token", auth);
                localStorage.setItem("unique_device_id", this.device.uuid);
                localStorage.setItem("userId", this.output.data._id);
                localStorage.setItem("UserType", this.output.data.user_type);
                localStorage.setItem("original_otp", this.output.data.original_otp);

                if (this.output.data.user_type == "guest") {
                    //this.getDataOffline();
                    this.authService.getMarinelistinfo();
                    this.authService.getProfileDetails();
                    if (this.pageName === 'regulations') {
                        this.navCtrl.setRoot('regulationsList');
                    } else {
                        this.navCtrl.setRoot('marineWeatherPage');
                    }
                } else {
                    let original_otp = JSON.parse(localStorage.getItem("original_otp"));
                    console.log("original_otp", original_otp)

                    if (original_otp == true) {
                        let self = this;
                        let abc: any = false
                        console.log("IN login original_otp true")

                        let otpuser = JSON.parse(localStorage.getItem('OTPL'))
                        console.log('otpuser', JSON.stringify(otpuser))
                        if (otpuser) {
                            console.log("local storage otp");
                            self.navCtrl.setRoot('dashboardPage');
                            self.getDataOffline();
                        } else {
                            let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))
                            if (subscriptionDate) {
                                if (subscriptionDate != null) {
                                    var todaysdate = new Date()
                                    var expdate = new Date(subscriptionDate.expirationDate)
                                    console.log("today's date", todaysdate);
                                    console.log("expiry date", expdate);
                                    if (todaysdate <= expdate) {
                                        console.log("valid subcription");
                                        this.navCtrl.setRoot('dashboardPage');
                                        this.getDataOffline();
                                    } else {
                                        let alert = this.alertCtrl.create({
                                            message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                            enableBackdropDismiss: false,
                                            buttons: [
                                                {
                                                    text: 'Okay',
                                                    handler: () => {
                                                        if (abc) {
                                                            console.log("Abc true")
                                                            this.setOtp()
                                                        }
                                                        else {
                                                            console.log("Abc false")
                                                            return false

                                                        }
                                                    }
                                                },
                                                {
                                                    text: 'Subscribe',
                                                    handler: () => {
                                                        console.log("SUbscription")
                                                        self.navCtrl.setRoot('SubscriptionLoaderPage');
                                                    }
                                                }
                                            ]

                                        });
                                        alert.present()
                                        setTimeout(function () {
                                            abc = true

                                        }, 5000);
                                    }
                                } else {

                                    this.navCtrl.setRoot('SubscriptionLoaderPage');

                                }
                            } else {
                                let alert = this.alertCtrl.create({
                                    message: "Alaska FishTopia is a subscription service for $1.99 per year. Customers who purchased the original one-time only payment are welcome to continue to use the product, but we kindly ask that you purchase the $1.99 annual subscription to help support the continued development of the product. We have a lot of new features that we would love to continue to develop on the product and for less than a cup of coffee at Starbucks your subscription would mean an awful lot!  ",
                                    enableBackdropDismiss: false,
                                    buttons: [
                                        {
                                            text: 'Okay',
                                            handler: () => {
                                                if (abc) {
                                                    console.log("Abc true")
                                                    this.setOtp()
                                                }
                                                else {
                                                    console.log("Abc false")
                                                    return false

                                                }
                                            }
                                        },
                                        {
                                            text: 'Subscribe',
                                            handler: () => {
                                                console.log("SUbscription")
                                                self.navCtrl.setRoot('SubscriptionLoaderPage');
                                            }
                                        }
                                    ]

                                });
                                alert.present()
                                setTimeout(function () {
                                    abc = true

                                }, 5000);
                            }
                        }


                    } else {
                        console.log("IN login original_otp false")
                        // this.navCtrl.setRoot('dashboardPage');
                        //         this.getDataOffline();

                        try {
                            let subscriptionDate = JSON.parse(localStorage.getItem("SubsDetails"))
                            if (subscriptionDate) {
                                if (subscriptionDate != null) {
                                    var todaysdate = new Date()
                                    var expdate = new Date(subscriptionDate.expirationDate)
                                    console.log("today's date", todaysdate);
                                    console.log("expiry date", expdate);
                                    if (todaysdate <= expdate) {


                                        console.log("valid subcription");
                                        this.navCtrl.setRoot('dashboardPage');
                                        this.getDataOffline();
                                    } else {
                                        this.navCtrl.setRoot('SubscriptionLoaderPage');
                                    }
                                } else {
                                    this.navCtrl.setRoot('SubscriptionLoaderPage');

                                }
                            } else {
                                this.navCtrl.setRoot('SubscriptionLoaderPage');
                            }

                        } catch (er) {
                            console.log(er)
                            this.navCtrl.setRoot('SubscriptionLoaderPage');

                        }
                    }

                }
            }
        }
    }
    setOtp() {
        console.log('otp data', this.product)

        localStorage.setItem('OTPL', JSON.stringify(this.product))
        this.navCtrl.setRoot('dashboardPage');
        this.getDataOffline();
    }
    dataDB: any[] = [];
    getDataOffline() {
        this.dataDB = JSON.parse(localStorage.getItem("tbl_BuoyData"));
        if (this.dataDB === null) {
            this.authService.getProfileDetails();
            this.authService.getTideDetails_New();
            this.authService.getCurrentsDetails();
            this.authService.getBuoyDetails();
            this.authService.getMarinelistinfo();
        }
    }
    verfyOTP() {
        this.navCtrl.push('SignUpSuccessPage');
    }
    Subsdata: any;
    Notifytoken: any;
    login() {
        let self = this;
        //self.dologin(self.Notifytoken);
        // this.Notifytoken=localStorage.getItem("FcmToken");                       
        // this.dologin(this.Notifytoken);

        // var da = JSON.parse(localStorage.getItem('SubsDetails'));
        // self.Subsdata = JSON.parse(da);
        // if (self.Subsdata != null && self.Subsdata != undefined) {
        //     self.iapCheck();
        // }
        self.fcm.subscribeToTopic('Hello');
        self.fcm.getToken().then(token => {
            self.Notifytoken = token;
            console.log('token', token);
            // self.dologin(self.Notifytoken);
            if (self.Social_NetEnabled == 'Google+') {
                self.googleLogin();
            } else if (self.Social_NetEnabled == 'Facebook') {
                self.facebookLogin();
            } else {
                self.Social_NetEnabled = '';
                self.dologin(self.Notifytoken);
            }
        })
        self.fcm.onNotification().subscribe(data => {
            console.log('data', JSON.stringify(data))
            if (data.wasTapped) {
                console.log("Received in background");
                //if user NOT using app and push notification comes
                //TODO: Your logic on click of push notification directly
                console.log('Push notification clicked');
                self.redirectPage(data);
            }
            else {
                let confirmAlert = self.alertCtrl.create({
                    title: 'New Notification',
                    message: data.body,
                    buttons: [{
                        text: 'Ignore',
                        role: 'cancel'
                    }, {
                        text: 'View',
                        handler: () => {
                            //TODO: Your logic here
                            self.redirectPage(data);
                        }
                    }]
                });
                confirmAlert.present();
            }
            //console.log("Received in background", JSON.stringify(data));
            self.authService.getNotificationsCount();
        })
        self.fcm.onTokenRefresh().subscribe(token => {
            console.log(token);
            self.refreshToken(token);
        });
    }
    refreshToken(token: any) {
        var unique_device_id = localStorage.getItem("unique_device_id");
        var data = {
            unique_device_id: unique_device_id,
            new_device_id: token
        }
        this.authService.UpdateNotificationToken(data).subscribe(res => {
            console.log(res);
        })
    }

    redirectPage(data: any) {
        console.log('data', data);
        this.authService.getNotificationsCount();
        if (data.notification_slug === "news_and_fishing") {
            this.navCtrl.setRoot('FishingNewsPage');
        } else if (data.notification_slug === "whats_hot") {
            this.navCtrl.setRoot('WHEventPage');
        } else if (data.notification_slug === "fishing_exchange_global") {
            this.navCtrl.setRoot('FishingExchangPage');
        } else if (data.notification_slug === "fish_counts_global") {
            this.navCtrl.setRoot('FishCountsPage');
        } else if (data.notification_slug === "payment_success") {
            this.navCtrl.push('NotificationsPage');
        } else if (data.notification_slug === "angler_booked_trip") {
            this.navCtrl.setRoot('FishingExchangPage');
        } else if (data.notification_slug === "trip_is_full") {
            this.navCtrl.setRoot('FishingExchangPage');
        } else if (data.notification_slug === "guide_accept_trip") {
            this.navCtrl.setRoot('FishingExchangPage');
        } else if (data.notification_slug === "guide_sent_trip_request") {
            this.navCtrl.setRoot('FishingExchangPage');
        } else if (data.notification_slug === "to_all_users") {
            this.navCtrl.push('NotificationsPage');
        } else {
            this.navCtrl.push('NotificationsPage');
        }
    }
}
