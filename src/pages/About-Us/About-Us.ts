import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
@IonicPage()
@Component({
  selector: 'page-About-Us',
  templateUrl: 'About-Us.html',
})
export class AboutUsPage {
  AboutData: any;
  aboutInfo: any;
  title: any;
  constructor(
    public navCtrl: NavController,
    public viewCtrl: ViewController,
    public navParams: NavParams,
    public authService: AuthService
  ) {
    this.AboutData = this.navParams.get('AboutData');
    if (this.AboutData === "Fishtopia") {
      this.title = "About Alaska FishTopia";
      this.aboutInfo = "<p>The word utopia comes from Greek origin and means an imaginary place in which everything is perfect. Well we can’t think of a more perfect place in the world for outdoor activities, and fishing in particular, and the name Alaska Fishtopia was chosen for our app.</p>" +
        "<p>The epic salmon migration that occurs every summer across the entire state provides economic benefit in the form of commercial fishing, sustenance for our residents, and an amazing sport fishery bringing anglers from all over the world. That salmon migration supports bears, eagles," +
        "resident fish, brings halibut in pursuit, and more. To call it inspiring is an under-statement. There is really nothing that compares to catching a world-famous Kenai King, a Homer & Seward halibut, fishing when up to 20,000 King salmon a day migrating up the Nushagak River, and watching a massive glacier calve.</p>" +
        "<p>Alaska FishTopia is a continual effort to be the most informational planning source available for fishing and other outdoor activities in Alaska – and to put it in all in a single app right at your finger tips.</p>" +
        "<h2>Our current areas include:</h2>" +
        "<ul>" +
        "<li>	<strong>Fish counts </strong> – to help plan the “when to fish” part of your trip. Updated daily counts of the most popular rivers in the various species of salmon migrating up the rivers. " +
        "<li>	<strong>Local weather</strong> – to plan your day & week weather." +
        "<li>	<strong>Marine Weather</strong> – to plan your halibut, rock, lingod and coastal fishing weather. " +
        "<li> <strong>Live Buoys </strong>– to see current wind and sea conditions in the area your fishing." +
        "<li> <strong>Tides</strong> – to plan your fishing times on the river as the salmon primarily enter our streams on the incoming tides as well as other activities such as clam digging." +
        "<li>	<strong>Currents</strong> – To help better plan that slack time when bottom fishing for halibut, rock fish, and ling cod will be the best." +
        "<li>	<strong>News & Fishing Reports</strong> – To be informed about regulations which change throughout the season, emergency orders, and other fishing related news." +
        "<li>	<strong>Today’s Catch </strong>– a place to celebrate your catch! Show it off, post your pictures!." +
        "<li> <strong>The Fishing Exchange</strong> – a place for anglers to post wanted seats in a guide boat and a place for guides to post open and available seats open for anglers. Booking is accomplished directly through the app and guide/angler information is exchanged immediately after booking." +
        "<li>	<strong>Events & Entertainment</strong> – After fishing & exploring its time to relax and enjoy.  The events & entertainment section provides all kinds of music, shows, festivals, specials and more going on throughout the state." +
        "</ul>" +
        "<p>Alaska Fishtopia was started on the beautiful Kenai Peninsula and was developed in partnership with The Algorithm LLC, a high-tech globally distributed software development company headquartered in Colorado.</p>";
    } else {
      this.title = "About The Algorithm";
      this.aboutInfo = "<p>With years of experience in countless Software and Web development projects, we’re ready to take your business to the next level. At The Algorithm, we combine our insights on how to transform your projects, processes, strategies," +
        "and in turn your company. And our staff has the capabilities and experience to actually do it. We’re proud to help shape how leading companies structure and manage their business.</p>" +
        "<h2>Our Philosophy</h2>" +
        "<p>The realization of any holistic, simple, and refined product is founded on the successful integration of three main pillars: People, Process, and Program.  It is The Algorithm’s philosophy that a solution can only be truly and completely achieved through the application of creative vision, discussion, and problem solving from each pillar through the lenses of Quality and Agility.</p>" +
        "<p>Every aspect of our projects is expertly designed to achieve a seamless and effortless user experience.  Ease of use, logical layouts, and consolidated themes are constantly evaluated to ensure a thoroughly designed and expertly implemented solution." +
        "<p>As feedback, priorities, and unplanned considerations shape the development of the product, The Algorithm remains flexible and adaptable to the ever changing requirements.</p>" +
        "<h2>Our Specialty</h2>" +
        "<p>The Algorithm specializes in the quality, precision, design, development and customization of software and web development, as well as simplified and streamlined workflow optimization.  Through communication, comprehension, and creativity, The Algorithm will apply the right people, knowledge and tools towards any problem that would be otherwise too big to solve.</p>" +
        "<p>The Algorithm is proud to be the trusted development team that took the “What if…” of Alaska FishTopia to “Done!”</p>" +
        "<p>More information about us is available at <a href='http://www.the-algo.com/'>www.the-algo.com.</a></p>";
    }
    // console.log('AboutData', this.AboutData);
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}
