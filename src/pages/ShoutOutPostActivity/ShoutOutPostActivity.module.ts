import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoutOutPostActivityPage } from './ShoutOutPostActivity';

@NgModule({
  declarations: [
    ShoutOutPostActivityPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoutOutPostActivityPage),
  ],
  exports: [
    ShoutOutPostActivityPage
  ]
})
export class PopupFabModalPageModule {}
