import { Component, ViewChild } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { Camera } from '@ionic-native/camera';
import { File } from '@ionic-native/file';
import { FilePath } from '@ionic-native/file-path';
import { FileUploadOptions, Transfer, TransferObject } from '@ionic-native/transfer';
import { ActionSheetController, AlertController, IonicPage, Loading, LoadingController, ModalController, NavController, NavParams, Platform, Slides, ToastController, ViewController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { AlertService } from '../../providers/util/alert.service';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'ShoutOutPostActivity',
  templateUrl: 'ShoutOutPostActivity.html',
})
export class ShoutOutPostActivityPage {
  userDetails: any;
  firstName: any;
  MdleName: any;
  lastName: any;
  Profileimg: any;

  @ViewChild('slider') slider: Slides;
  loading: Loading;
  users = new Array(10);
  option: any;
  slideIndex = 0;
  ShoutOutPost: FormGroup;
  message: AbstractControl;
  chatBox: any;
  isenabledlikeBox: any;
  isenabledCmtBox: any;
  color: any;
  messagepost: any = "";
  colors = ['#e43737', '#e0e437', '#37e446', '#375be4', '#972cb1'];
  ShoutOutList: any[];
  imglist: any[];
  image_url: any;
  img: any;
  lastImage: string = null;
  imgData: any;
  postImageUrl: any;
  ShoutOutPageList: any = [];
  ImageUrllist: any = [];
  imgid: any;
  page = 1;
  perPage = 0;
  totalData = 0;
  totalPage = 0;
  expanded: any;
  contracted: any;
  showIcon = true;
  preload = true;
  selectedDay: any;
  selectedMonth: any;
  YearMonth: any;
  SelectDate: any;
  ErrorEnabled: boolean = false;
  BtnShoutOutPost: boolean = false;
  constructor(
    public viewCtrl: ViewController,
    public alertService: AlertService,
    public navCtrl: NavController,
    public formBuilder: FormBuilder,
    public authService: AuthService,
    private file: File,
    private filePath: FilePath,
    private camera: Camera,
    private transfer: Transfer,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public actionSheetCtrl: ActionSheetController,
    public platform: Platform,
    public toastCtrl: ToastController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController
  ) {
    this.ShoutOutPost = formBuilder.group({
      'message': [''],
    });
    this.chatBox = '';
    this.message = this.ShoutOutPost.controls['message'];
    this.getProfileDetails();
    //  this.getActivityImgList();
    var v = new Date();
    //this.selectedDay = v.getDate();
    this.selectedDay = ("0" + (v.getDate())).slice(-2);
    this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
    this.YearMonth = v.getFullYear();
    this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
    console.log('this.SelectDate', this.SelectDate);
  }


  ionViewDidEnter() {
    this.getProfileDetails();
  }
  ionViewWillEnter() {
    this.getProfileDetails();
  }

  onSlideChanged(i: any, slide: any) {
    this.ImageUrllist.forEach(element => {
      if (element._id == slide._id) {
        element.status = true;
      } else {
        element.status = false;
      }
    });
    this.postImageUrl = i.image_url;
    this.imgid = slide._id;
    this.color = "2px solid";
    // this.slideIndex = i.getActiveIndex();
    console.log('Slide changed! Current index is', this.postImageUrl + this.imgid);
    //this.color = this.colors[this.slideIndex];
    console.log('Slide changed! Current index is');
  }

  getProfileDetails() {
    this.authService.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
      this.userDetails = res;
      console.log('this.userDetails', res);
      this.firstName = this.userDetails.firstName;
      this.MdleName = this.userDetails.MiddleName;
      this.lastName = this.userDetails.lastName;
      this.Profileimg = this.userDetails.image_url;
      //  this.UserPhonecode=this.Country;
      //this.Phone_Number= this.userDetails.firstName;
    });
  }


  dismiss() {
    this.viewCtrl.dismiss();
  }

  CameraPhoto() {
    this.takePicture(this.camera.PictureSourceType.CAMERA);
  }
  AddPhoto() {
    this.takePicture(this.camera.PictureSourceType.PHOTOLIBRARY);
  }

  public takePicture(sourceType) {
    // Create options for the Camera Dialog
    var options = {
      // quality: 100,
      // targetWidth: 91,
      // targetHeight: 91,
      allowEdit: true,
      sourceType: sourceType,
      saveToPhotoAlbum: false,
      correctOrientation: true
    };

    // Get the data of an image
    this.camera.getPicture(options).then((imagePath) => {
      this.imgData = imagePath;
      // Special handling for Android library
      if (this.platform.is('android') && sourceType === this.camera.PictureSourceType.PHOTOLIBRARY) {

        this.filePath.resolveNativePath(imagePath)
          .then(filePath => {

            let correctPath = filePath.substr(0, filePath.lastIndexOf('/') + 1);
            let currentName = imagePath.substring(imagePath.lastIndexOf('/') + 1, imagePath.lastIndexOf('?'));
            this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
            this.uploadImage();
          });
      } else {
        var currentName = imagePath.substr(imagePath.lastIndexOf('/') + 1);
        var correctPath = imagePath.substr(0, imagePath.lastIndexOf('/') + 1);
        this.copyFileToLocalDir(correctPath, currentName, this.createFileName());
        this.uploadImage();
      }
    }, (err) => {
      this.presentToast('Error while selecting image.');
    });
  }
  // Create a new name for the image
  private createFileName() {
    var d = new Date(),
      n = d.getTime(),
      newFileName = n + ".jpg";
    return newFileName;
  }

  // Copy the image to a local folder
  private copyFileToLocalDir(namePath, currentName, newFileName) {
    //alert(newFileName);
    this.file.copyFile(namePath, currentName, cordova.file.dataDirectory, newFileName).then(success => {
      this.lastImage = newFileName;
    }, error => {
      this.presentToast('Error while storing file.');
    });
  }

  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }

  // Always get the accurate path to your apps folder
  public pathForImage(img) {
    if (img === null) {
      return '';
    } else {
      return cordova.file.dataDirectory + img;
    }
  }
  imgURL: any;
  public uploadImage() {
    // alert("Hello");
    // Destination URL
    var url = "http://166.62.118.179:2152/api/activity/upload-activity-image";
    var targetPath = this.pathForImage(this.lastImage);
    var filename = this.lastImage;
    let options1: FileUploadOptions = {
      fileKey: "image",
      fileName: filename,
      chunkedMode: false,
      mimeType: "multipart/form-data",
      headers: {
        "access_token": localStorage.getItem("token")
      }
    }
    const fileTransfer: TransferObject = this.transfer.create();
    this.loading = this.loadingCtrl.create({
      content: 'Uploading...',
    });
    this.loading.present();
    // alert(this.imgData);
    //alert(options1);
    fileTransfer.upload(this.imgData, 'http://166.62.118.179:2152/api/activity/upload-activity-image', options1)
      .then((data) => {
        let res = JSON.parse(data.response);
        this.imgURL = res.data.image_url;
        this.imgid = res.data._id;
        this.BtnShoutOutPost = true;
        this.presentToast('Image uploaded Successfully.');
        // this.getActivityImgList();
        this.loading.dismissAll();
        this.authService.getsinguserpoints(null).subscribe((result: any) => {
          console.log("PONTS", result)
          this.authService.points = result.data[0].sum_of_points
        })

      }, err => {
        //  alert(err);
        this.presentToast('Error while uploading file.');
        this.loading.dismissAll();

      });
  }


  getActivityImgList() {
    this.ImageUrllist = [];
    this.authService.getToImgShoutOut().subscribe(res => {
      console.log('shoutout', res);
      const newDate = new Date();
      console.log('shoutout', newDate);
      if (res.success) {
        for (let item of res.data) {
          console.log('dat', this.imgid, item._id);
          if (this.imgid == item._id) {
            this.imgURL = item.image_url;
            console.log(this.imgURL);
          }
          // if(this.SelectDate <=item.created_at){
          //   item.status = false;
          //   this.ImageUrllist.push(item);
          // }

        }
        // var lng = this.ImageUrllist.length - 1;
        // this.ImageUrllist[lng].status = true;
        // console.log('item', this.ImageUrllist);
        // if(this.ImageUrllist[lng]){

        // }
      }

    });

  }


  PostActivity() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    loading.present();
    //this.messagepost = "";
    let msg = this.ShoutOutPost.controls['message'].value;
    console.log('msg', msg, this.imgid);
    if (this.imgid !== undefined) {
      let data = {
        description: msg,
        image_ids: [this.imgid],
        imgURL: this.imgURL
      }
      console.log('data-->', data, this.imgid);
      //  if (this.RegisterForm.valid) {
      //    this.presentLoading();
      //if(this.imgid != null){
      this.authService.addShoutOutPost(data).subscribe(res => {
        console.log('data-->', res);
        if (res.success) {
          loading.dismiss();
          this.ErrorEnabled = false;
          this.messagepost = "";
          this.getActivityList(1);
          this.showToast("Activity successfully posted.");
          this.viewCtrl.dismiss();
        }
        // else{
        //   this.showToast("Please upload image.");
        // }
      });
    } else {
      this.ErrorEnabled = true;
      loading.dismiss();
      this.showToast("Please upload image.");
    }

  }
  getActivityList(page) {
    this.authService.ShoutOutPageList = [];
    var token = localStorage.getItem("token");
    var data = {
      page: page,
      sort: "desc"
    }
    this.authService.getShoutOutDetail(data).subscribe(res => {
      this.ShoutOutList = res;
      this.perPage = this.authService.perPage;
      this.totalData = this.authService.totalData;
      this.totalPage = this.authService.totalPage;
      console.log('this.userDetails', res.data.items.docs);
      if (res.success) {
        for (let item of res.data.items.docs) {
          if (item.user_id.image_url == null) {
            item.user_id.image_url = 'assets/img/icon/placeholder.png';
          }
          item.shortDesp = this.truncate(item.description);
          console.log('item.shortDesp', item.shortDesp);
          this.authService.ShoutOutPageList.push(item);
        }

        console.log('ShoutOutPageList', this.ShoutOutPageList);
        //this.getActivityImgList();
      }

    });

  }

  truncate(value: string, limit = 75, completeWords = true, ellipsis = '…') {
    let lastindex = limit;
    if (completeWords) {
      lastindex = value.substr(0, limit).lastIndexOf(' ');
    }
    return `${value.substr(0, limit)}${ellipsis}`;
  }

  showToast(msg: any) {
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000
    });

    toast.onDidDismiss(() => {
      ////console.log'Dismissed toast');
    });

    toast.present();
  }


}
