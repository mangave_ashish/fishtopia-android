
import { FishNewsDetailsCmtsPage } from './FishNewsDetailsCmts';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    FishNewsDetailsCmtsPage,
  ],
  imports: [
    IonicPageModule.forChild(FishNewsDetailsCmtsPage),
    SharedModule
  ],
  exports: [
    FishNewsDetailsCmtsPage
  ]
})

export class MessagesPageModule { }
