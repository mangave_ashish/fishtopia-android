import { Component, Input } from '@angular/core';
import { IonicPage, MenuController, NavController, NavParams } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { ViewController } from 'ionic-angular/navigation/view-controller';
import { AuthService, Event } from '../../providers/auth-provider';

/**
 * Generated class for the FilterLocationPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-filter-location',
    templateUrl: 'filter-location.html',
})
export class FilterLocationPage {
    @Input('data') data: any;
    @Input('events') events: any;

    drawerOptions: any;
    eventList: Event[] = [];
    TmweventList: Event[] = [];
    eventSource;
    viewTitle;
    monthNames: any = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];
    isToday: boolean;
    lockSwipeToPrev: boolean;
    month: string;
    mainEventList: Event[] = [];
    mainEventFillterList: any[] = [];
    EventType: any[] = [];
    dateList: Date[] = [];
    IPushMessage: any[];
    showPageView: boolean = false;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
    filterobject: any[] = [];
    public filteredItems: any;
    public issearch = false;
    public i: any;
    public keywords: any[];
    inputName: any = '';
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;
    FXdata: any;
    EndDate: any;
    StartDate: any;
    filterOption: any[] = [];
    filterlist: any[] = [

        {
            Id: 5,
            Label: 'Location'
        }
    ];
    TimeData: any[] = [
        {
            id: 1,
            title: "1/2 Trip - Day"
        },
        {
            id: 2,
            title: "1/2 Trip - Afternoon"
        },
        {
            id: 3,
            title: "Full Day Trip"
        }];
    startDatetimeMin: any = new Date(Date.now()).toISOString();
    fromDate: any;
    toDate: any;
    noSeats: number = 1;
    minRange: number;
    maxRange: number;
    mat_selectedIndex: number = 0;
    constructor(
        public navParams: NavParams, public viewCtrl: ViewController,
        public navCtrl: NavController, public alertCtrl: AlertController,
        public menuCtrl: MenuController, public authService: AuthService
    ) {
        console.log('filterlist', this.filterlist);
        this.DateEnabled = true;
        //  this.authService.getboattypeslist();
        this.authService.getallfishinglocationslist();
        //  this.authService.getfishingtypeslist();
        this.filterOption = this.authService.AllLocationtypesList;

        this.Allselected = this.authService.isalllocationselected
//console.log("Data Allselected",this.Allselected)

        this.data = {
            max: "1000",
            min: "1",
            step: "10",
            textLeft: "1",
            textRight: "1000",
            title: "TWO SLIDERS",
            value: {
                lower: 1,
                upper: 1000
            }
        }
        console.log(JSON.stringify(this.authService.AllLocationtypesList))

    }

    formatLabel(value: number | null) {
        if (!value) {
            return 0;
        }

        if (value >= 1000) {
            return Math.round(value / 1000) + 'k';
        }

        return value;
    }

    loc_data: any[] = [];
    saveData() {
        this.authService.AllLocationtypesList.forEach(ele => {
            if (ele.selected)
                this.loc_data.push(ele._id);
        });
        this.authService.filterData = {
            Location: this.loc_data,
        }
        console.log("loc_data " + this.loc_data.length)
        if (this.loc_data.length > 0) {
            console.log(JSON.stringify(this.authService.filterData))
            localStorage.setItem("filterData", JSON.stringify(this.loc_data))
            this.navCtrl.setRoot('HomePage', { filterData: this.authService.filterData });
        } else {
            let alert = this.alertCtrl.create({
                title: 'Location',
                subTitle: 'Select at least one location',
                buttons: ['Cancel']
            });
            alert.present();
        }
    }
    dataFilter: any;

    clearData() {
        this.fromDate = '';
        this.toDate = '';
        // this.filterlist = [];
        // this.DateEnabled = false;
        localStorage.removeItem("filterData")
        //this.Allselected = true;
        this.authService.isalllocationselected= true;
        this.authService.getallfishinglocationslist();
        this.filterOption = this.authService.AllLocationtypesList;

    }

    myrange(newRangeValue) {
        this.minRange = newRangeValue._valA;
        this.maxRange = newRangeValue._valB;
        console.log("Range :" + newRangeValue._valA + newRangeValue._valB);
    }

    SeatsEnabled: boolean = false;
    DateEnabled: boolean = false;
    onLinkClick(event: any) {
        console.log('even', event.tab.textLabel);
        console.log('even', event);
        this.SeatsEnabled = false;
        this.DateEnabled = false;
        this.mat_selectedIndex = event.index;
        if (event.tab.textLabel === "Fish Type") {
            this.filterOption = this.authService.fishingtypesList;
        } else if (event.tab.textLabel === "Boat Type") {
            this.filterOption = this.authService.BoatypesList;
        } else if (event.tab.textLabel === "Location") {
            this.filterOption = this.authService.AllLocationtypesList;
        } else if (event.tab.textLabel === "Duration Of Trip") {
            this.filterOption = this.TimeData;
        } else if (event.tab.textLabel === "No Of Seats") {
            this.SeatsEnabled = true;
        } else if (event.tab.textLabel === "Date") {
            this.DateEnabled = true;
        }
        console.log('event', this.filterOption);
    }
    minusAdult() {
        this.noSeats--;
    }
    plusAdult() {
        this.noSeats++;
    }

    ngOnChanges(changes: { [propKey: string]: any }) {
        this.data = changes['data'].currentValue;
    }
    singleValue4: any = 10;
    onEvent = (event: string, item: any): void => {
        // if (this.events[event]) {
        //     this.events[event](item);
        // }
    }
    Allselected: boolean = false;
    selectedOptions: string[];
    onNgModelChange(list: any) {
        console.log('event', list);
        // this.Allselected = false;
        if (list === 'All') {
           // this.Allselected = !this.Allselected;
        this.authService.isalllocationselected= !this.authService.isalllocationselected;

            if (this.authService.isalllocationselected) {
                console.log("IN");
                this.authService.AllLocationtypesList.forEach(element => {
                    element.selected = true;
                });
                this.filterOption = this.authService.AllLocationtypesList;
            } else {
                console.log("OFF");

                this.authService.AllLocationtypesList.forEach(element => {
                    element.selected = false;
                });
                this.filterOption = this.authService.AllLocationtypesList;
            }
        } else {
            this.authService.isalllocationselected= false
            console.log("IN witout all");
            this.authService.AllLocationtypesList.forEach(element => {
                if (element._id == list._id) {
                    console.log("IN witout all if");
                    if (element.selected)
                        element.selected = false;
                    else
                        element.selected = true;

                } else {
                    console.log("IN witout all else");

                }
            });
            this.filterOption = this.authService.AllLocationtypesList;
        }
    }

}
