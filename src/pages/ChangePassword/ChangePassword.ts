import { Component } from '@angular/core';
import { AlertController, LoadingController, NavController, MenuController, IonicPage } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
  selector: 'page-ChangePassword',
  templateUrl: 'ChangePassword.html',
})
export class ChangePasswordPage {
  public loginForm: any;
  isenabledEmail: any;
  isenabledPhone: any;
  ChangePasswordForm: FormGroup;
  currentPassword: AbstractControl;
  password: AbstractControl;
  confirmPassword: AbstractControl;
  submitAttempt: boolean = false;
  User: any;
  show = false;
  NewType = "password";
  CurrType = "password";
  ReType = "password";
  type = "password";
  CurrPassword = "";
  Newpassword = "";
  ConfirmPassword = "";
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    public authService: AuthService
  ) {
    this.isenabledEmail = true;
    this.isenabledPhone = false;
    this.ChangePasswordForm = this.formBuilder.group({
      'currentPassword': ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      'confirmPassword': ['', Validators.required]
    }, { validator: this.matchingPasswords('password', 'confirmPassword') });
    this.currentPassword = this.ChangePasswordForm.controls['currentPassword'];
    this.password = this.ChangePasswordForm.controls['password'];
    this.confirmPassword = this.ChangePasswordForm.controls['confirmPassword'];
  }


  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        this.submitAttempt = true;
        return {
          mismatchedPasswords: true
        };
      }
      else {
        this.submitAttempt = false;
      }
    }
  }

  eyesCurricon: boolean = false;
  toggleCurrShow() {
    // if(text=="Current"){
    this.show = !this.show;
    if (this.show) {
      this.CurrType = "text";
      this.eyesCurricon = true;
    }
    else {
      this.CurrType = "password";
      this.eyesCurricon = false;
    }
    // }
    // else if(text=="New"){
    //   this.show = !this.show;
    //   if (this.show){
    //       this.type = "text";
    //   }
    //   else {
    //       this.type = "password";
    //   }

    // }
    // else if(text="retype"){
    //   this.show = !this.show;
    //   if (this.show){
    //       this.type = "text";
    //   }
    //   else {
    //       this.type = "password";
    //   }

    // }
  }
  eyesNewicon: boolean = false;
  toggleNewShow() {
    // if(text=="Current"){
    this.show = !this.show;
    if (this.show) {
      this.eyesNewicon = true;
      this.NewType = "text";
    }
    else {
      this.eyesNewicon = false;
      this.NewType = "password";
    }
  }
  eyesReicon: boolean = false;
  toggleRetypeShow() {
    // if(text=="Current"){
    this.show = !this.show;
    if (this.show) {
      this.eyesReicon = true;
      this.ReType = "text";

    }
    else {
      this.eyesReicon = false;
      this.ReType = "password";
    }
  }
  Email() {
    this.isenabledEmail = true;
    this.isenabledPhone = false;

  }
  Phone() {
    this.isenabledPhone = true;
    this.isenabledEmail = false;

  }

  goToSignup() {
    //this.slider.slideTo(2);
    this.navCtrl.push('signup');
  }


  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }

  login() {
    //   this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('dashboardPage');
  }

  signup() {
    //    this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('signup');
  }


  ChngePassword() {
    var CurrentPassword = this.ChangePasswordForm.controls['currentPassword'].value;
    var newPassword = this.ChangePasswordForm.controls['password'].value;
    var data = {
      current_password: CurrentPassword,
      new_password: this.ChangePasswordForm.controls['password'].value,
    }
    console.log('data', data);
    this.authService.changePwd(data).subscribe(res => {
      console.log('res', res);
      if (res.success) {
        // this.resetPassword();
        // this.navCtrl.setRoot('HomePage');
        this.alertinfo(res.message);
        this.clear();
      }
      else {
        this.alertinfo(res.message);
      }
    })


    //   this.presentLoading('An e-mail was sent with your new password.');

  }

  alertinfo(msg: any) {
    console.log(msg.message);
    let alert = this.alertCtrl.create({
      //title: 'Exit?',
      message: msg,
      buttons: [
        {
          text: 'OK',
          role: 'OK',
          handler: () => {
            this.navCtrl.setRoot('dashboardPage');
          }
        }
      ]
    });
    alert.present();
  }
  clear() {
    this.CurrPassword = "";
    this.Newpassword = "";
    this.ConfirmPassword = "";
  }

}
