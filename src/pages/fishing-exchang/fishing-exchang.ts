import { Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup } from '@angular/forms';
import { DomSanitizer } from '@angular/platform-browser';
import { ActionSheetController, AlertController, IonicPage, LoadingController, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
//import { SocialSharing } from '@ionic-native/social-sharing';
import { AuthService, FishingExchngEvent } from '../../providers/auth-provider';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';



/**
 * Generated class for the FishingExchangPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-fishing-exchang',
	templateUrl: 'fishing-exchang.html',
})
@SideMenuDisplayText('The Fishing Exchange')
export class FishingExchangPage implements OnInit {
	tripList: any[];
	tripsOption: any;
	isenabled: any;
	displayGuide: boolean;
	UserType: any;
	FishExchngForm: FormGroup;
	tripsOptionUserType: AbstractControl;
	tripsOptionGuideType: AbstractControl;
	tripsOptionUser: any;
	tripsOptionGuide: any;
	drawerOptions: any;
	eventList: FishingExchngEvent[] = [];
	TmweventList: FishingExchngEvent[] = [];
	eventSource;
	viewTitle;
	monthNames: any = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];
	isenabledAcceptDeny: boolean;
	isenabledAccept: boolean;
	isenabledDeny: boolean;
	isToday: boolean;
	lockSwipeToPrev: boolean;
	month: string;
	mainEventList: any[] = [];
	dateList: Date[] = [];
	IPushMessage: any[];
	showPageView: boolean = false;
	selectedDay: any;
	selectedMonth: any;
	tmwDate: any;
	YearMonth: any;
	SelectDate: any;
	tripsOptionUserType1: any;
	tripsOptionGuideType1: any;
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	TripType: any;
	is_featured: any;
	EndDate: any;
	StartDate: any;
	public issearch = false;
	filterData: any;
	constructor(
		public modalCtrl: ModalController,
		private sanitizer: DomSanitizer,
		public toastCtrl: ToastController,
		public alertCtrl: AlertController,
		public loadingCtrl: LoadingController,
		private formBuilder: FormBuilder,
		public authService: AuthService,
		public navCtrl: NavController,
		public navParams: NavParams,
		public actionSheetCtrl: ActionSheetController,

	) {
		// this.FishExchngForm = this.formBuilder.group({
		// 	'tripsOptionUserType': [''],
		// 	'tripsOptionGuideType': [''],
		// });
		// this.tripsOptionUserType1 = this.FishExchngForm.controls['tripsOptionUserType'];
		// this.tripsOptionGuideType1 = this.FishExchngForm.controls['tripsOptionGuideType'];	
		this.filterData = navParams.get('filterData');
		var v = new Date();
		this.StartDate = v;
		this.selectedDay = ("0" + (v.getDate())).slice(-2);
		this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
		this.YearMonth = v.getFullYear();
		//this.SelectDate=this.selectedMonth+'-'+this.selectedDay+'-'+this.YearMonth;
		this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
		console.log('this.SelectDate', this.SelectDate);
		this.tmwDate = "tmw";
		this.drawerOptions = {
			handleHeight: 50,
			thresholdFromBottom: 200,
			thresholdFromTop: 200,
			bounceBack: true
		};

		//	this.tripsOption="availabletrips";
		this.isenabled = false;
		this.is_featured = localStorage.getItem("is_featured");
	}

	openDialog(message: any, isupgradebutton?: any) {
		if (isupgradebutton == 'yes') {
			let alert = this.alertCtrl.create({
				// title: 'Confirm Trip',
				message: message,
				buttons: [{
					text: 'Ok',

				}, {
					text: 'upgrade to guide direct',
					handler: () => {
						this.navCtrl.push('Guide_DirectPage');
					}

				}
				]
			});
			alert.present();
		} else {
			let alert = this.alertCtrl.create({
				// title: 'Confirm Trip',
				message: message,
				buttons: [{
					text: 'Ok',

				}
				]
			});
			alert.present();
		}
	}
	ngOnInit() {
		this.mainEventList = [];
		this.dateList = [];
		this.UserType = localStorage.getItem("UserType");
		let arr = ['available', 'wanted'];
		//let arr = ['available'];
		//let arr = ['wanted'];

		if (this.filterData !== undefined && this.filterData !== null) {
			if (this.UserType == "guide") {
				this.displayGuide = true;
				this.tripsOptionGuide = "Wanted";
				this.getFilterDetails();
			} else {
				this.displayGuide = false;
				this.tripsOptionUser = "Available";
				this.getFilterDetails();
			}
		} else {
			console.log("this.UserType", this.UserType);
			if (this.UserType == "guide") {
				this.displayGuide = true;
				this.tripsOptionGuide = "Wanted";
				this.isenabledAcceptDeny = false;
				arr.forEach(element => {
					if (element == 'available')
						this.getEventDetails(element, this.page, false);
					else
						this.getEventDetails(element, this.page, true);


				});
				// this.getEventDetails('wanted', this.page);
			}
			else {
				this.displayGuide = false;
				this.tripsOptionUser = "Available";
				arr.forEach(element => {
					if (element == 'available')
						this.getEventDetails(element, this.page, false);
					else
						this.getEventDetails(element, this.page, true);
					//this.getEventDetails(element, this.page);
				});
			}
		}
	}





	ionViewWillLeave() {
		this.EndDate = new Date();
		this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		this.authService.audit_Page('The Fishing Exchange');
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}

	change(Event: any) {
		if (Event == 'search') {
			this.issearch = true;
		} else if (Event == 'filter') {
			this.navCtrl.push('filtersPage');
		}
	}
	change1() {
		this.issearch = false;
	}
	inputName: any = '';
	cancelSearch() {
		this.inputName = '';
		this.issearch = false;
	}

	presentActionSheet() {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Sort By',
			buttons: [
				{
					// cssClass:  'button-cam',
					text: 'Recently Posted',
					handler: () => {
						this.orderbyPipe('recentPost');
					}
				},
				// {
				// 	// cssClass:  'button-cam',
				// 	text: 'Nearest',
				// 	handler: () => {
				// 		//this.getFishNewsListfilter(1, 'asc');
				// 	}
				// },
				{
					// cssClass:  'button-cam',
					text: 'Lowest Price',
					handler: () => {
						this.orderbyPipe('lowPrice');
					}
				}, {
					text: 'Highest Price',
					handler: () => {
						this.orderbyPipe('highPrice');
					}
				}, {
					text: 'Cancel',
					role: 'cancel'
				}
			]
		});
		actionSheet.present();
	}

	orderbyPipe(sortType: any) {
		this.dateList = [];
		if (sortType == 'lowPrice') {
			this.mainEventList.sort((a: any, b: any) => {
				if (a.trip_amount < b.trip_amount) {
					return -1;
				} else if (a.trip_amount > b.trip_amount) {
					return 1;
				} else {
					return 0;
				}
			});
			this.mainEventList.forEach(d => {
				this.dateList.push(d.date);
			});
		} else if (sortType == 'highPrice') {
			this.mainEventList.sort((a: any, b: any) => {
				if (a.trip_amount < b.trip_amount) {
					return 1;
				} else if (a.trip_amount > b.trip_amount) {
					return -1;
				} else {
					return 0;
				}
			});
			this.mainEventList.forEach(d => {
				this.dateList.push(d.date);
			});
		} else if (sortType == 'recentPost') {
			this.mainEventList.sort((a: any, b: any) => {
				if (a.created_at < b.created_at) {
					return 1;
				} else if (a.created_at > b.created_at) {
					return -1;
				} else {
					return 0;
				}
			});
			this.mainEventList.forEach(d => {
				this.dateList.push(d.date);
			});
		}
		console.log(this.mainEventList, this.dateList);
	};


	mainEventFillterList: any[] = [];
	filteredItems: any[] = [];
	FilterByName(value: any) {
		console.log('value', this.mainEventFillterList);
		console.log('value', this.mainEventList);
		console.log('value', value);
		this.filteredItems = [];
		if (value != '') {
			this.mainEventList.forEach(element => {
				console.log("output:" + element);
				if (element.boat_type_id.title.toUpperCase().indexOf(value.toUpperCase()) >= 0) { this.filteredItems.push(element); }
				else if (element.fishing_type_id.title.toUpperCase().indexOf(value.toUpperCase()) >= 0) { this.filteredItems.push(element); }
				else if (element.fishing_location_id.title.toUpperCase().indexOf(value.toUpperCase()) >= 0) { this.filteredItems.push(element); }
				else { this.mainEventList = this.mainEventFillterList; }
				this.mainEventList = this.filteredItems;
				console.log('filter', this.filteredItems);
			});
		}
		else {
			this.mainEventList = this.mainEventFillterList;
			//this.filteredItems = this.authService.filterdAppointmentlist;
		}
	}


	btnSegmentCall(type, Page) {
		this.mainEventList = [];
		this.dateList = [];
		console.log('type', type);
		if (Page == undefined) {
			Page = 1;
		}
		if (type == 'available' || type == 'wanted') {
			this.isenabledAcceptDeny = false;
			let arr = ['available', 'wanted'];
			arr.forEach(element => {
				if (element == 'available')
					this.getEventDetails(element, this.page, false);
				else
					this.getEventDetails(element, this.page, true);
			});
		} else if (type === "offered" || type === "my_trip") {
			this.isenabledAcceptDeny = true;
			this.getEventDetails(type, Page, true);
		}
		// else {
		// 	this.isenabledAcceptDeny = false;
		// 	this.getEventDetails(type, Page);
		// }
	}



	doRefresh(refresher) {
		console.log('Begin async operation', refresher);
		this.page = 1;
		setTimeout(() => {
			this.mainEventList = [];
			this.dateList = [];
			if (this.TripType == 'available' || this.TripType == 'wanted') {
				let arr = ['available', 'wanted'];
				arr.forEach(element => {
					if (element == 'available')
						this.getEventDetails(element, this.page, false);
					else
						this.getEventDetails(element, this.page, true);
				});
			} else {
				this.getEventDetails(this.TripType, this.page, true);
			}
			console.log('Async operation has ended');
			refresher.complete();
		}, 2000);
	}

	doInfinite(infiniteScroll) {
		if (this.page < this.totalPage) {
			console.log('Begin async operation', infiniteScroll);
			setTimeout(() => {
				this.page++;
				this.getEventDetails(this.TripType, this.page, true);
				console.log('Async operation has ended');
				infiniteScroll.complete();
			}, 1000);
		}
		else {
			infiniteScroll.enabled(false);
		}
	}






	NotPrime() {
		this.navCtrl.push('Guide_DirectPage');
	}

	presentPrompt(item: any) {
		let alert = this.alertCtrl.create({
			title: 'Number of seats',
			inputs: [
				{
					name: 'Seat',
					placeholder: 'Enter Number of seats',
					type: 'number'
				},

			],
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: data => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Submit',
					handler: (data: any) => {
						var data1 = data;
						console.log(data);
						this.presentConfirm(item, data1.Seat);
						//this.BookTrip(item, data1.Seat);
					}
				}
			]
		});
		alert.present();
	}
	alertmsg: any;
	presentConfirm(item: any, seatNo: any) {
		let total_Amt = seatNo * item.trip_amount;
		if (seatNo <= item.member) {
			this.alertmsg = 'Are you sure you want to book this trip? The trip value is <b style="color: #18cbef;font-size: 16px;"> $ ' + total_Amt + '</b> for <b style="color: #18cbef;font-size: 16px;">' + seatNo + '</b> seats.';
			//	'Number of seats is <b style="color: #18cbef;font-size: 16px;">' + seatNo + '</b><br> Total Amount paid is <b style="color: #18cbef;font-size: 16px;"> $ ' + total_Amt + '</b>';
			console.log('total_Amt', total_Amt);
			let alert = this.alertCtrl.create({
				// title: 'Confirm Trip',
				message: this.alertmsg,
				buttons: [
					{
						text: 'Cancel',
						role: 'cancel',
						handler: () => {
							console.log('Cancel clicked');
						}
					},
					{
						text: 'Ok',
						handler: (data) => {
							var data1 = data;
							console.log('Ok clicked', item, seatNo);
							this.BookTrip(item, seatNo);
						}
					}
				]
			});
			alert.present();
			//this.presentConfirm(item, data1.Seat);
		} else {
			this.alertmsg = 'The requested number of seats cannot be more than the number of seats available.';
			let alert = this.alertCtrl.create({
				// title: 'Confirm Trip',
				message: this.alertmsg,
				buttons: [{
					text: 'Ok',
					handler: (data) => {
						console.log('Ok clicked', item, seatNo);
					}
				}
				]
			});
			alert.present();
		}

	}

	BookTrip(item: any, seatNo: any) {
		console.log(JSON.stringify(item));
		console.log('item', item._id)
		var data = {
			member: seatNo,
		}
		this.authService.StripeAnglerPost(data, item._id).subscribe(res => {
			if (res.success) {
				this.mainEventList = [];
				this.dateList = [];
				this.alertmsg = 'Let\'s go fishing! Your trip is confirmed and you can find the details of your guide and more under the My Trips tab. ';
				let alert = this.alertCtrl.create({
					// title: 'Confirm Trip',
					message: this.alertmsg,
					buttons: [{
						text: 'Ok',
						handler: (data) => {
							console.log('Ok clicked', item, seatNo);
						}
					}
					]
				});
				alert.present();
				//this.presentToast('Trip successfully booked.');
				this.getEventDetails('available', 1, true);
				console.log(res);
			} else {
				this.presentToast('Please add Payment card.');
				this.navCtrl.push('PaymentlistPage');
			}
		});
	}
	private presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 1500,
			position: 'bottom'
		});
		toast.present();
	}



	ionViewDidLoad() {
		console.log('ionViewDidLoad FishingExchangPage');
	}
	tripAccept(value: FishingExchngEvent, i: any) {
		console.log(status);
		var data = {
			status: "accept"
		}
		this.authService.addTripStatus(data, value._id).subscribe(res => {
			if (res.success) {
				this.mainEventList = [];
				this.dateList = [];
				this.alertmsg = 'Let\'s go fishing!  You can find your angler\'s details and more under the My Trips Offered tab.';
				let alert = this.alertCtrl.create({
					// title: 'Confirm Trip',
					message: this.alertmsg,
					buttons: [{
						text: 'Ok',
						handler: (data) => {

						}
					}
					]
				});
				alert.present();
				this.getEventDetails(this.TripType, this.page, true);
				console.log(res);
			}
			// this.isenabledAcceptDeny=true;
			// this.isenabledAccept=false;

		});
	}

	AcceptConfirm(item: any, i: any) {
		let total_Amt = item.member * item.trip_amount;
		//	console.log('total_Amt',total_Amt);
		let alert = this.alertCtrl.create({
			// title: 'Confirm Trip',
			message: 'Are you sure you want to accept this trip? The trip is valued at $<b style="color: #18cbef;font-size: 16px;">' + total_Amt + '</b> at $<b style="color: #18cbef;font-size: 16px;">' + item.trip_amount + '</b>/ person.',
			//		  Are you sure.You want to accept this trip.<br>You have to pay $ at <b style="color: #18cbef;font-size: 16px;">'+item.member+'</b> $ per person.',
			//	  Number of seats is <br> Total Amount paid is <b style="color: #18cbef;font-size: 16px;"> $ '++'</b>',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Ok',
					handler: (data) => {
						var data1 = data;
						console.log('Ok clicked', item, i);
						this.tripAccept(item, i);
					}
				}
			]
		});
		alert.present();
	}

	CancelTrip(item: any, i: any) {
		let total_Amt = item.member * item.trip_amount;
		//	console.log('total_Amt',total_Amt);
		let alert = this.alertCtrl.create({
			// title: 'Confirm Trip',
			message: 'Are you sure you want to cancel this trip?',
			//		  Are you sure.You want to accept this trip.<br>You have to pay $ at <b style="color: #18cbef;font-size: 16px;">'+item.member+'</b> $ per person.',
			//	  Number of seats is <br> Total Amount paid is <b style="color: #18cbef;font-size: 16px;"> $ '++'</b>',
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Ok',
					handler: (data) => {
						var data1 = data;
						console.log('Ok clicked', item, i);
						this.tripCancel(item, i);
						//this.tripAccept(item, i);
					}
				}
			]
		});
		alert.present();
	}

	tripCancel(value: any, i: any) {
		console.log(value);
		this.authService.addTripCancel(value._id).subscribe(res => {
			this.mainEventList = [];
			this.dateList = [];
			this.getEventDetails(this.TripType, this.page, true);
			this.presentToast('Your trip has been cancelled.');
			console.log(res);
			//    this.isenabledAcceptDeny=true;
			// 	this.isenabledDeny=false;

		});

	}




	tripReject(value: FishingExchngEvent, i: any) {
		console.log(status);
		var data = {
			status: "deny"
		}
		this.authService.addTripStatus(data, value._id).subscribe(res => {
			this.getEventDetails('wanted', this.page, true);
			console.log(res);
			//    this.isenabledAcceptDeny=true;
			// 	this.isenabledDeny=false;

		});

	}


	getEventDetails(type, Page, isfilter) {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 30000
		});
		loading.present();
		var data = {
			page: Page,
			list_type: type,
			sort: "asc",
			today_date: this.SelectDate
		}
		console.log("FISHING EXCHNAGE DaTA", data);

		this.authService.getFishingExchngeDetail(data).subscribe(res => {
			console.log("RESULT", res)
			this.TripType = type;
			console.log('TripType', this.TripType);
			console.log('TripType', this.perPage + this.totalData + this.totalPage);
			this.perPage = this.authService.perPage;
			this.totalData = this.authService.totalData;
			this.totalPage = this.authService.totalPage;

			// for (let d of res) {
			// 	try {
			// 		//find out unique date
			// 		//	if (date.indexOf(new Date(d.date).getDate()) == -1) {
			// 		if (this.SelectDate <= d.date) {
			// 			//	date.push(new Date(d.date).getDate());
			// 			this.dateList.push(d.date);
			// 		}
			// 		//	}

			// 	} catch (e) { }
			// }
			// loading.dismiss();
			// var seriesValues = this.dateList;
			// seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
			// console.log(seriesValues);
			// this.dateList = seriesValues;

			for (let item of res) {
				let split_string = item.imgUrl.split(',');
				if (split_string[0] == 'data:image/*;charset=utf-8;base64') {
					// if (item.imgUrl != '' && item.imgUrl != undefined) {
					// 	item.imgUrl = 'data:image/jpeg;base64,' + item.imgUrl;
					item.imgUrl = this.sanitizer.bypassSecurityTrustResourceUrl(item.imgUrl);
				}
				// else {
				// 	item.imgUrl = undefined;
				// }

				this.mainEventList.push(item);

				this.mainEventFillterList = this.mainEventList;
				//	console.log('mainEventList', item.imgUrl);
			}
			loading.dismiss();


			this.dateList = []
			for (let d of this.mainEventList) {
				try {

					if (this.SelectDate <= d.date) {
						
						let ispresent = this.dateList.indexOf(d.date)
						if (ispresent != 0) {
							this.dateList.push(d.date);
						}
					}
					//	}

				} catch (e) { }
			}
			this.dateList.sort(function (a, b) {

				if (a < b) { return -1; }
				if (a > b) { return 1; }
				return 0;
			})
			//var seriesValues = this.dateList;
			console.log("DST", this.dateList);
			//this.dateList = seriesValues;

		});
	}


	getFilterDetails() {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 3000
		});
		loading.present();
		// var data = {
		// 	page: Page,
		// 	list_type: type,
		// 	sort: "asc",
		// 	today_date: this.SelectDate
		// }
		this.authService.AddfilterTrip(this.filterData).subscribe(res => {
			this.TripType = 'available';
			console.log('TripType', res);
			console.log('TripType', this.perPage + this.totalData + this.totalPage);
			this.perPage = this.authService.perPage;
			this.totalData = this.authService.totalData;
			this.totalPage = this.authService.totalPage;

			for (let item of res) {
				let split_string = item.imgUrl.split(',');
				if (split_string[0] == 'data:image/*;charset=utf-8;base64') {

					item.imgUrl = this.sanitizer.bypassSecurityTrustResourceUrl(item.imgUrl);
				}


				this.mainEventList.push(item);

				this.mainEventFillterList = this.mainEventList;
				//	console.log('mainEventList', item.imgUrl);
			}
			loading.dismiss();


			this.dateList = []
			for (let d of this.mainEventList) {
				try {

					if (this.SelectDate <= d.date) {
						//let da=this.dateList.splice(this.dateList.indexOf(d.date),1)
						//console.log(this.dateList.indexOf(d.date),d.date);
						let ispresent = this.dateList.indexOf(d.date)
						if (ispresent != 0) {
							this.dateList.push(d.date);
						}
					}
					//	}

				} catch (e) { }
			}
			this.dateList.sort(function (a, b) {

				if (a < b) { return -1; }
				if (a > b) { return 1; }
				return 0;
			})
			console.log("DST", this.dateList);

			// for (let d of res) {
			// 	try {
			// 		//find out unique date
			// 		//	if (date.indexOf(new Date(d.date).getDate()) == -1) {
			// 		if (this.SelectDate <= d.date) {
			// 			//	date.push(new Date(d.date).getDate());
			// 			this.dateList.push(d.date);
			// 		}
			// 		//	}

			// 	} catch (e) { }
			// }
			// loading.dismiss();
			// var seriesValues = this.dateList;
			// seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
			// console.log(seriesValues);
			// this.dateList = seriesValues;
			// console.log('dateList', this.dateList);
			// for (let item of res) {
			// 	let split_string = item.imgUrl.split(',');
			// 	if (split_string[0] == 'data:image/*;charset=utf-8;base64') {
			// 		// if (item.imgUrl != '' && item.imgUrl != undefined) {
			// 		// 	item.imgUrl = 'data:image/jpeg;base64,' + item.imgUrl;
			// 		item.imgUrl = this.sanitizer.bypassSecurityTrustResourceUrl(item.imgUrl);
			// 	}
			// 	// else {
			// 	// 	item.imgUrl = undefined;
			// 	// }
			// 	this.mainEventList.push(item);
			// 	this.mainEventFillterList = this.mainEventList;
			// 	//	console.log('mainEventList', item.imgUrl);
			// }
		});
	}




	WantedTrips(refresher) {
		console.log('Begin async operation', refresher);
		console.log('Begin async operation', this.tripsOptionGuide.value);
		console.log('Begin async operation', refresher.value);
		console.log('Begin async operation', this.tripsOptionGuide);
	}


	saveFavorite(value: FishingExchngEvent) {
		//item.isFavorite= !item.isFavorite;

		//  var data = {
		// 	event_id: value._id,        
		// 	status:!value.favouriteStatus
		// };
		// this.authService.addToFavouriteEvent(data).subscribe(res => {
		// 	// added as a favourite.
		// 	console.log('res',res);
		// 	if (res.success) {
		// 		value.favouriteStatus = !value.favouriteStatus;
		// 		if (!value.favouriteStatus)
		// 			value.likes -= 1;
		// 		else value.likes += 1;
		// 	} 
		//let toast = this.toast.create({
		//    message: res.message,
		//    duration: 3000,
		//    position: 'bottom'
		//}).present();
		// console.log(res);
		//	});
	}

	openTripDetail(item) {
		console.log('Trip_Details', item);
		console.log('this.UserType', this.UserType);
		console.log('this.is_featured', this.is_featured);
		console.log('this.TripType', this.TripType);
		console.log('item.created_by.is_featured', item.created_by.is_featured);
		console.log('item.show_membership_details', item.show_membership_details);
		//item.created_by.is_featured = 'yes'
		if (this.UserType == "guide") {
			if (this.is_featured == 'yes') {
				if (this.UserType == "guide" && this.TripType == "offered") {
					console.log('FishingExchangDetailAnglerPage', "1");

					this.navCtrl.push('FishingExchangDetailAnglerPage', { trip: item });
					// } else if (item.created_by.is_featured == 'yes') {
					// 	console.log('FishingExchangDetailPage', "2");

					// 	this.navCtrl.push('FishingExchangDetailPage', { trip: item });
					// } else if (this.UserType == "angler" && this.TripType == "available") {
					// 	console.log('FishingExchangDetailPage', "3");

					// 	this.navCtrl.push('FishingExchangDetailPage', { trip: item });
					// 
				} else if (this.UserType == "angler" && item.show_membership_details) {
					console.log('FishingExchangDetailPage', "4");

					this.navCtrl.push('FishingExchangDetailPage', { trip: item, is_feature: item.created_by.is_featured });
				}
				else {
					this.navCtrl.push('FishingExchangDetailPage', { trip: item });
				}
			} else {
				if (item.user_type == 'guide') {
					if (this.UserType == "guide" && this.TripType == "offered") {
						console.log('FishingExchangDetailAnglerPage', "1");
						this.navCtrl.push('FishingExchangDetailAnglerPage', { trip: item });
						// } else if (item.created_by.is_featured == 'yes') {
						// 	console.log('FishingExchangDetailPage', "2");

						// 	this.navCtrl.push('FishingExchangDetailPage', { trip: item });
						// } else if (this.UserType == "angler" && this.TripType == "available") {
						// 	console.log('FishingExchangDetailPage', "3");

						// 	this.navCtrl.push('FishingExchangDetailPage', { trip: item });
					} else if (this.UserType == "angler" && item.show_membership_details) {
						console.log('FishingExchangDetailPage', "4");

						this.navCtrl.push('FishingExchangDetailPage', { trip: item, is_feature: item.created_by.is_featured });
					} else {
						this.navCtrl.push('FishingExchangDetailPage', { trip: item });
					}
				} else {
					this.openDialog('Please enroll in our Guide Direct subscription to view and contact anglers as well as post available trips and seats you may have. Please click on Upgrade To Guide Direct at the bottom of main menu page.', 'yes')
				}
			}
		} else {
			if (this.UserType == "guide" && this.TripType == "offered") {
				console.log('FishingExchangDetailAnglerPage', "1");

				this.navCtrl.push('FishingExchangDetailAnglerPage', { trip: item });
				// } else if (item.created_by.is_featured == 'yes') {
				// 	console.log('FishingExchangDetailPage', "2");

				// 	this.navCtrl.push('FishingExchangDetailPage', { trip: item });
				// } else if (this.UserType == "angler" && this.TripType == "available") {
				// 	console.log('FishingExchangDetailPage', "3");

				// 	this.navCtrl.push('FishingExchangDetailPage', { trip: item });
			} else if (this.UserType == "angler" && item.show_membership_details) {
				console.log('FishingExchangDetailPage', "4");

				this.navCtrl.push('FishingExchangDetailPage', { trip: item, is_feature: item.created_by.is_featured });
			} else {
				this.navCtrl.push('FishingExchangDetailPage', { trip: item });
			}
		}
	}

	MyTrips() {
		this.isenabled = true;
	}
	AvailableTrips() {
		this.isenabled = false;
	}

	// tripShare(item){
	// 	this.socialSharing.share(item.description, item.title, item.img, "").then(() => {
	// 		alert("Success");
	// 	}).catch(() => {
	// 		alert("Error");
	// 	});
	// }
	RequestTrip() {

		this.authService.getboattypeslist();
		this.authService.getfishinglocationslist();
		this.authService.getfishingtypeslist();
		this.UserType = localStorage.getItem("UserType");
		console.log("this.UserType", this.UserType);
		if (this.UserType == "guide") {
			if (this.is_featured == "yes") {
				this.navCtrl.push('OfferedTripPage');
			} else {
				let message = "Please enroll in our guide direct program to post your open guided seats and to view the contact info for anglers looking for guides"
				this.openDialog(message)
			}
		}
		else {
			this.navCtrl.push('RequestTripPage');
		}

	}
	Notification() {
		this.navCtrl.push('NotificationsPage');
	}

	BusinessForm() {
		if (this.UserType === "guide") {
			//	this.modalCtrl.create('BusinessformPage').present();
			this.navCtrl.push('BusinessformPage', { FishingExchnage: 'push' });
		} else {
			//	this.modalCtrl.create('PrimeOfferPage').present();
			this.navCtrl.push('PrimeOfferPage', { FishingExchnage: 'push' });
		}
	}

}
