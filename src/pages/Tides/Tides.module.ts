import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TidesPage } from './Tides';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    TidesPage,
  ],
  imports: [
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en   
      apiKey: 'AIzaSyCHvwvPHFQSagXqGQXz4bqHbOZr03_rRxo'
    }),
    IonicPageModule.forChild(TidesPage),
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TidesPageModule { }
