import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, Alert } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { LoadingController, Platform } from 'ionic-angular';
import * as _ from 'lodash';
import { DatabaseService } from '../../providers/weather';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

@IonicPage({
  priority: 'high'
})

@Component({
  selector: 'page-Tides',
  templateUrl: 'Tides.html',
})
@SideMenuDisplayText('Tides')

export class TidesPage {
  title: string;
  iconUrl: any;
  lat: number = 61.217381;;
  lng: number = -149.863129;
  latlong: any[] = [];
  markers: any[] = [];
  infoWindowOpened = null;
  map: any;
  regionals: any = [];
  currentregional: any;
  selectedDay: any;
  selectedMonth: any;
  tmwDate: any;
  YearMonth: any;
  SelectDate: any;
  mainEventList: any;
  dateList: Date[] = [];
  eventListData: any[] = [];
  ChartdateList: Date[] = [];
  GeoLat: any;
  GeoLong: any;
  pinsDataList: any;
  currentDateData: any[] = [];
  MapEnabled: boolean = false;
  data_DB: any[] = [];
  dataDB: any[] = [];
  TideStationlist: any[] = [];
  stationId: any;
  DataSet: any[] = [];
  Currents_Pin: any = '';
  EndDate: any;
  StartDate: any;
  PrevPage: any;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public authService: AuthService,
    public platform: Platform,
    public databaseService: DatabaseService,
    public zone: NgZone
  ) {
    var v = new Date();
    this.StartDate = v;
    this.selectedDay = v.getDate();
    this.selectedMonth = v.getMonth();
    this.YearMonth = v.getFullYear();
    this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    loading.present();
    this.getGeolocationPosition();
  }

  ionViewWillEnter() {
    this.PrevPage = this.authService.title;
  }

  ionViewWillLeave() {
    this.EndDate = new Date();
    this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
    this.authService.audit_Page(this.PrevPage);
  }

  getGeolocationPosition() {
    let self = this;
    self.GeoLat = localStorage.getItem('GeoLat');
    self.GeoLong = localStorage.getItem('GeoLong');
    let locationData = localStorage.getItem('GeoLocation');
    let Sub_Location = localStorage.getItem('SubGeoLocation');
    if (locationData === "Alaska") {
      self.lat = JSON.parse(self.GeoLat);
      self.lng = JSON.parse(self.GeoLong);
      self.getTide();
    } else {
      self.lat = 61.217381;
      self.lng = -149.863129;
      self.getTide();
    }
  }

  getTide() {
    //debugger
    let self = this;
    self.databaseService.getTide_Data('Tide_DB')
      .then(data => {
        console.log('fetch data', data);
        if (data != undefined) {
          let TideData = JSON.parse(data.data);
          if (TideData.length < 0 || _.isEmpty(data.data)) {
            self.authService.getTideDetails_New();
          } else {
            self.pinsDataList = TideData;
          }
        } else {
          self.authService.getTideDetails_New();
        }
      })
  }

  clickedViewMarker(data: any, infoWindow, index: number) {
    let self = this;

    console.log('clicked the marker:', data)
    if (self.infoWindowOpened === infoWindow)
      return;

    if (self.infoWindowOpened !== null) {
      self.infoWindowOpened.close();
    }
    self.infoWindowOpened = infoWindow;
    self.stationId = data;
    self.dateList = [];
    self.currentDateData = [];
    self.getTidePins('Tide_Cal_' + data.stationId, data);
  }

  getTidePins(station: any, stationDetails: any) {
   // debugger
    let self = this;
    self.databaseService.getTidepins(station)
      .then(data => {
        if (data != undefined) {
          let TideData = JSON.parse(data.data);
          console.log('Tide', TideData);
          //if (_.isEmpty(TideData)) {
          if (TideData.length < 0 || _.isEmpty(data.data)) {
            self.getTideStationDetails(stationDetails);
            //throw new Error('Invalid database forecast, fallback to server > ');
          } else {
            data.iconUrl = "assets/img/plain/yellow.png";
            self.Cal_Tide_Data(TideData);
            self.TideStationlist = TideData;
          }
        } else {
          self.getTideStationDetails(stationDetails);
        }
      })
    // .catch(err => {
    //   if (err && err.message) {
    //     console.error(err.message);
    //     self.getTideStationDetails(stationDetails);
    //   }
    //   // self.getTideDetails();
    // });
  }


  markerChge(station: any, ) {
    //debugger
    this.DataSet = [];
    _.forEach(this.pinsDataList, obj => {
      if (station == obj.stationId) {
        obj.iconUrl = "assets/img/plain/yellow.png";
      }
      this.DataSet.push(obj);
    });
    this.databaseService.updateTide_Data(JSON.stringify(this.DataSet));
  }

  getTideStationDetails(station: any) {
    console.log(station);
    let self = this;
    self.dateList = [];
    self.currentDateData = [];
    self.dataDB = [];
    self.authService.PinsMarked = [];
    self.authService.getTideStationData(station.stationId).subscribe(res => {
      station.iconUrl = "assets/img/plain/yellow.png";
      self.TideStationlist = res.data;
      self.Cal_Tide_Data(res.data);
      self.databaseService.addTidepins(this.TideStationlist, 'Tide_Cal_' + station.stationId).then(res => {
        if (res) {
          self.markerChge(station._id);
        }
      })
    });
  }


  // Cal Tide Data

  Cal_Tide_Data(data: any) {
    let self = this;
    let date: any[] = [];
    var dd = ("0" + (this.selectedDay)).slice(-2);
    var mn = ("0" + (this.selectedMonth + 1)).slice(-2);
    var yy = self.YearMonth;
    var dateformat = [yy, mn, dd].join("-");
    console.log('dateformat', dateformat);
    _.forEach(data, (mel: any) => {
      try {
        //find out unique date
        if (dateformat == mel.date) {
          var t = mel.time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [mel.time];
          if (t.length > 1) {
            let d = self.dattime(mel.time);
            mel.time = d;
          }
          self.currentDateData.push(mel);
          self.dateList.push(mel.date);
        }
      } catch (e) { }
    });
  }

  dattime(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    let Dtime = time[0] + time[1] + time[2] + time[5];
    console.log('time', time[0] + time[1] + time[2] + time[5]);
    return Dtime; // return adjusted time or original string
  }


  MarkerViewDetails() {
    this.navCtrl.push('TidePageDetailsPage', { TideDetails: this.stationId, TideData: this.TideStationlist });
  }


}



