import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishingExchangDetailPage } from './fishing-exchang-detail';
import { SharedModule } from '../../app/shared.module';
@NgModule({
  declarations: [
    FishingExchangDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FishingExchangDetailPage),
    SharedModule
  ],
})
export class FishingExchangDetailPageModule {}
