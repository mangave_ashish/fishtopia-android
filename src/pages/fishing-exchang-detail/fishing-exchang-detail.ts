import { Component } from '@angular/core';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import moment from 'moment';
import { AuthService } from '../../providers/auth-provider';

/**
 * Generated class for the FishingExchangDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-fishing-exchang-detail',
	templateUrl: 'fishing-exchang-detail.html',
})
export class FishingExchangDetailPage {
	tripItem: any;
	tripDay: any;
	tripDate: string;
	image_url: any;
	userDetails: any;
	img: any;
	firstName: string;
	MdleName: string;
	lastName: string;
	phoneNo: any;
	email: string;
	StreetAddr1: string;
	StreetAddr2: string;
	State_: string;
	City_: string;
	Country: string;
	Zip_Code: string;
	UserType: string;
	Profileimg: any;
	ReviewAll: any[] = [];
	firstGuideReviewItem: any[] = [];
	trip_is_feature: string = 'no';
	imgUrl: any;
	options: InAppBrowserOptions = {
		location: 'yes',//Or 'no' 
		hidden: 'no', //Or  'yes'
		clearcache: 'yes',
		clearsessioncache: 'yes',
		zoom: 'yes',//Android only ,shows browser zoom controls 
		hardwareback: 'yes',
		mediaPlaybackRequiresUserAction: 'no',
		shouldPauseOnSuspend: 'no', //Android only 
		closebuttoncaption: 'Close', //iOS only
		disallowoverscroll: 'no', //iOS only 
		toolbar: 'yes', //iOS only 
		enableViewportScale: 'no', //iOS only 
		allowInlineMediaPlayback: 'no',//iOS only 
		presentationstyle: 'pagesheet',//iOS only 
		fullscreen: 'yes',//Windows only    
	};
	constructor(public authService: AuthService, private theInAppBrowser: InAppBrowser, public navCtrl: NavController, public navParams: NavParams) {
		this.tripItem = navParams.get('trip');
		this.trip_is_feature = navParams.get('is_feature');
		if (this.tripItem.imgUrl != undefined) {
			this.imgUrl = this.tripItem.imgUrl;
		} else {
			this.imgUrl = undefined;
		}
		if (this.tripItem.date !== undefined) {
			var date = moment(this.tripItem.date);
		}
		this.tripDay = date.date();
		this.tripDate = this.tripItem.date.slice(3);
		console.log('tripItem', this.tripItem);
		this.img = "assets/img/default-event.png";
		this.image_url = "assets/img/slides/fishing2.png";
		this.getGuideDetails(this.tripItem.created_by);
	}
	public openWithCordovaBrowser(url: string) {
		let target = "_self";
		window.open(url,'_blank')

		//this.theInAppBrowser.create(url, target, this.options);
	}
	getGuideDetails(data: any) {
		this.authService.getUserData(data).subscribe(res => {
			this.userDetails = res.data;
			this.firstName = this.userDetails.username;
			this.MdleName = this.userDetails.MiddleName;
			this.lastName = this.userDetails.lastName;
			this.phoneNo = this.userDetails.phoneNo;
			this.email = this.userDetails.email;
			this.StreetAddr1 = this.userDetails.StreetAddr1;
			this.StreetAddr2 = this.userDetails.StreetAddr2;
			this.State_ = this.userDetails.State;
			this.City_ = this.userDetails.City;
			this.Country = this.userDetails.Country;
			this.Zip_Code = this.userDetails.ZipCode;
			this.UserType = this.userDetails.userType;
			this.Profileimg = this.userDetails.image_url;
			//	this.authService.getReviewAll(this.userDetails._id, 1);
			this.authService.userDetailsData = this.userDetails
			console.log('this.userDetails', this.userDetails);
		});



	}

	//    getReviewAll(id:any) {		
	// 	var data={
	// 	   page: "1",
	// 	}	
	//    this.authService.addReviewaAllPost(data).subscribe(res => {
	// 	   // added as a favourite.
	// 	console.log('res',res);
	// 	this.ReviewAll=[];
	// 	   if (res.success) {     
	// 			 for(let item of res.data.items.docs){
	// 			   if(item.reviewed_to._id==id){						
	// 				   this.ReviewAll.push(item);
	// 			   }					
	// 			 }

	// 		 //this.ReviewAll=res.data.items.docs;

	// 	   } 
	// 	   console.log(' this.Review', this.ReviewAll);
	// 	   //let toast = this.toast.create({
	// 	   //    message: res.message,
	// 	   //    duration: 3000,
	// 	   //    position: 'bottom'
	// 	   //}).present();
	// 	  // console.log(res);
	//    });
	// }

	BookTrip() {
		this.navCtrl.push('PaymentPage', { Payment: this.tripItem });
	}

	openGuideDetail() {
		//	 this.navCtrl.push('');
		this.navCtrl.push('GuideDetailPage', { userDetails: this.userDetails, userReviewDetails: this.ReviewAll });

	}




}
