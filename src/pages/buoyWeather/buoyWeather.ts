import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, Alert } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { LoadingController, Platform } from 'ionic-angular';
import * as _ from 'lodash';
import { DatabaseService } from '../../providers/weather';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

@SideMenuDisplayText('Live Buoys')

@IonicPage({
  priority: 'high'
})

@Component({
  selector: 'page-buoy-Weather',
  templateUrl: 'buoyWeather.html',
})

export class buoyWeatherPage {
  title: string;
  iconUrl: any;
  lat: number;
  lng: number;
  latlong: any[] = [];
  markers: any[] = [];
  infoWindowOpened = null;
  map: any;
  regionals: any = [];
  currentregional: any;
  selectedDay: any;
  selectedMonth: any;
  tmwDate: any;
  YearMonth: any;
  SelectDate: any;
  mainEventList: any;
  dateList: Date[] = [];
  eventListData: any[] = [];
  ChartdateList: Date[] = [];
  GeoLat: any;
  GeoLong: any;
  pinsDataList: any;
  currentDateData: any[] = [];
  MapEnabled: boolean = false;
  data_DB: any[] = [];
  dataDB: any[] = [];
  TideStationlist: any[] = [];
  stationId: any;
  DataSet: any[] = [];
  Currents_Pin: any = '';
  EndDate: any;
  StartDate: any;
  PrevPage: any;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public authService: AuthService,
    public platform: Platform,
    public databaseService: DatabaseService,
    public zone: NgZone
  ) {
    var v = new Date();
    this.StartDate = v;
    this.selectedDay = v.getDate();
    this.selectedMonth = v.getMonth();
    this.YearMonth = v.getFullYear();
    this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `Fishing for data. Please wait...`,
      duration: 3500
    });
    loading.present();
    this.getGeolocationPosition();

  }

  ionViewWillEnter() {
    this.PrevPage = this.authService.title;
  }

  ionViewWillLeave() {
    this.EndDate = new Date();
    this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
    this.authService.audit_Page(this.PrevPage);
  }

  getGeolocationPosition() {
    let self = this;
    self.pinsDataList = [];
    self.GeoLat = localStorage.getItem('GeoLat');
    self.GeoLong = localStorage.getItem('GeoLong');
    let locationData = localStorage.getItem('GeoLocation');
    let Sub_Location = localStorage.getItem('SubGeoLocation');
    let datacheck = localStorage.getItem('tbl_BuoyData');
    if (locationData === "Alaska") {
      self.lat = JSON.parse(self.GeoLat);
      self.lng = JSON.parse(self.GeoLong);
      self.pinsDataList = JSON.parse(localStorage.getItem('tbl_BuoyData'));
    } else {
      self.lat = 61.217381;
      self.lng = -149.863129;
      self.pinsDataList = JSON.parse(localStorage.getItem('tbl_BuoyData'));
    }
  }


  MarkerDetails(marker: any, infoWindow, index: number) {
    this.navCtrl.push('BuoyinfoDetailsPage', { BuoyinfoDetails: marker });
  }
}



