import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { buoyWeatherPage } from './buoyWeather';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    buoyWeatherPage,
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCHvwvPHFQSagXqGQXz4bqHbOZr03_rRxo'
    }),
    IonicPageModule.forChild(buoyWeatherPage),
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class buoyWeatherPageModule { }
