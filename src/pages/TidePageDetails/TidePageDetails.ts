import { Component, ViewChild } from '@angular/core';
import Highcharts from 'highcharts/highstock';
import { Content, DateTime, IonicPage, LoadingController, NavController, NavParams, Platform, Slides } from 'ionic-angular';
import * as _ from 'lodash';
import moment from 'moment';
import { AuthService } from '../../providers/auth-provider';
import { DatabaseService } from '../../providers/weather';

@IonicPage()
@Component({
    selector: 'page-TidePageDetails',
    templateUrl: 'TidePageDetails.html'
})
export class TidePageDetailsPage {
    @ViewChild('leftCats') leftItems;
    @ViewChild(Content) content: Content;
    @ViewChild('lineCanvas') lineCanvas;
    @ViewChild('slider') slider: Slides;
    dateArray: any = [];
    monthArray: any[] = ["January", "February ", "March ", "April ", "May ", "June ", "July", "August", "September", "October", "November", "December"];
    yearArray: any[];
    doneLoading = false;
    messages: any[] = [];
    Tides: any;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
    mainEventList: any;
    dateList: Date[] = [];
    eventList: Event[] = [];
    ChartdateList: Date[] = [];
    ChartdateTimeList: DateTime[] = [];
    dataft: any;
    lineChart: any;
    title: any;
    TideStationlist: any[] = [];
    TideStationminlist: any[] = [];
    TideStationlistfilter: any[] = [];
    dataDB: any;
    WsChartData: any[] = [];
    WsRecords: any[];
    tabCheck: any;
    graphEnabled: boolean = false;
    timeSpan: boolean = false;
    GraphOn: boolean = false;
    imageUrl: any = 'assets/img/ocean.png';
    TideOption: any;
    range: number = 0;
    img: any;
    fromDate: any;
    toDate: any;
    errormsgEnabled: boolean = false;
    fromDatearray: any[] = [];
    toDatearray: any[] = [];

    constructor(public navParams: NavParams,
        public navCtrl: NavController,
        public loadingCtrl: LoadingController,
        public platform: Platform,
        public authService: AuthService,
        public databaseService: DatabaseService,
    ) {
        this.TideOption = 'Calendar';
        this.Tides = navParams.get('TideDetails');
        this.dataDB = navParams.get('TideData');
        this.title = this.Tides.geoGroupName;
        var v = new Date();
        this.yearArray = [v.getFullYear(), (v.getFullYear() + 1)];
        this.selectedDay = v.getDate();
        this.selectedMonth = v.getMonth();
        this.YearMonth = v.getFullYear();
        this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
        this.allDates();
    }

    ngAfterViewInit() {
        setTimeout(() => {
            this.plotChart();
        }, 150);
    }

    getYear(YearMonth: any) {
        console.log('this.SelectDate', YearMonth);
        this.range = 0;
        if (YearMonth == new Date().getFullYear()) {
            this.YearMonth = YearMonth;
            this.selectedMonth = new Date().getMonth();
            this.selectedDay = new Date().getDate();
            this.updatePlotGraph();
            this.filterTide("")
        } else {
            this.YearMonth = YearMonth;
            this.selectedMonth = 0;
            this.selectedDay = 1;
            this.updatePlotGraph();
            this.filterTide("")

        }

    }

    onLinkClick(event: any) {
        if (event == "Graph") {
            this.range = 2;
            this.graphEnabled = true;
            this.toggleButton('d');
            //  this.GraphOn = true;
        } else {
            this.range = 0;
            this.allDates();
            this.graphEnabled = false;
        }
    }
    graphlblEnabled: any;
    toggleButton(data: any) {
        console.log('data', data);
        this.GraphOn = !this.GraphOn;
        this.graphEnabled = true;
        if (!this.GraphOn) {
            this.range = 2;
            this.allDates();
        }
    }

    onDateClick(date: any, i: any) {
        //alert(date)
        this.selectedDay = date;
        this.graphEnabled = false;
        this.filterTide(this.dateString);
        this.filterMinDataTide(this.dateString);
    }

    monthselect() {
        this.updatePlotGraph();
        this.filterTide("")
    }

    getTideStationDetails(station: any) {
        this.dateList = [];
        ////GET ALL DATA////
        if (this.dataDB != null) {
            this.TideStationlist = this.dataDB;
            this.mainEventList = this.dataDB;
            console.log("with data getTideStationDetails", this.mainEventList);

            this.filterTide(this.dateString);
            this.filterMinDataTide(this.dateString);
        } else {
            this.authService.getTideStationData(station).subscribe(res => {
                let date: any[] = [];
                var dd = ("0" + (this.selectedDay)).slice(-2);
                var mn = ("0" + (this.selectedMonth + 1)).slice(-2);
                var yy = this.YearMonth;
                var dateformat = [yy, mn, dd].join("-");
                console.log(dateformat);
                console.log("getTideStationDetails",res);


                for (let d of res.data) {
                    try {
                        if (dateformat == d.date) {
                            date.push(new Date(d.date).getDate());
                            this.dateList.push(d.date);
                        }
                    } catch (e) { }
                }
                //   loading.dismiss();
                console.log('dateList', this.dateList);
                this.dateList.sort(function (a, b) {
                    if (a > b) return 1;
                    if (a < b) return -1;
                    return 0;
                });


                this.mainEventList = res.data;

                console.log('mainEventList', this.mainEventList);
                if (this.mainEventList.length > 0) {
                    //this.currentDay = date[0];
                }
                this.filterTide(this.dateString);
                this.filterMinDataTide(this.dateString);
            });
        }
    }

    //13-12-2018
    pinsDataList: any;
    getTide() {
        // debugger
        let self = this;
        self.databaseService.getTideMinpins('Tide_Min_DB')
            .then(data => {
                console.log('fetch data', data);
                if (data != undefined) {
                    let TideData = JSON.parse(data.data);
                    if (TideData.length < 0 || _.isEmpty(data.data)) {
                        //  self.getTideStationMinDetails(this.Tides.stationId);
                    } else {
                        self.pinsDataList = TideData;
                    }
                } else {
                    //  self.getTideStationMinDetails(this.Tides.stationId);
                }
            })
    }

    getTideStationMinDetailsnew(stationId: any, fromstr: string, tostr: string, list, index) {
        let self = this;
        if (list.length - 1 !== index) {
            var data = {
                stationId: stationId,
                fromDate: moment(fromstr).format("YYYY-MM-DD"),
                toDate: moment(tostr).format("YYYY-MM-DD")
            }
            self.authService.getTidesStationMinDetailsNew(data).subscribe(res => {
                if (res.success) {
                    if (res.data.length < 1) {
                        self.errormsgEnabled = true;
                    }
                    console.log("Index", res)
                    if (res.data.length > 0) {
                        self.TideStationminlist.push.apply(self.TideStationminlist, res.data);
                        if (index == 0) {
                            self.filterMinDataTide(1, res.data);
                        }
                        else {
                            self.filterMinDataTide(0, res.data);
                        }
                        self.getTideStationMinDetailsnew(self.Tides.stationId, list[index + 1].fromDate, list[index + 1].endDate, list, index + 1);
                    } else {
                        if (self.TideStationminlist.length < 1) {
                            self.filterMinDataTide(1, self.TideStationlist);
                        }
                        //self.TideStationminlist.push.apply(self.TideStationminlist, self.TideStationlist);
                    }
                }
            });
        } else {
            console.log('terminated ')
        }
    }

    DataDay: any[];
    filterTide(date: any) {
        try {
            if (this.TideStationlist.length > 0) {
                this.eventList = [];
                this.dataft = [];
                this.ChartdateTimeList = [];
                this.ChartdateList = [];
                // let chartData = [];
                // this.WsChartData = [];
                this.DataDay = [];
                var dd = ("0" + (this.selectedDay)).slice(-2);
                var mn = ("0" + (this.selectedMonth + 1)).slice(-2);
                var yy = this.YearMonth;
                var dateformat = [yy, mn, dd].join("-");
                console.log("Convertdate", dateformat);
                for (let mel of this.TideStationlist) {
                    let mnth = ("0" + moment(mel.date).format("M")).slice(-2);
                    let Year = moment(mel.date).format("Y");
                    var t = mel.time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [mel.time];
                    if (t.length > 1) {
                        let dtime = this.dattime(mel.time);
                        mel.time = dtime;
                    }
                    if (dateformat == mel.date) {
                        this.eventList.push(mel);
                    }
                    if (mn == mnth && yy == Year) {
                        if (this.DataDay.length === 0) {
                            this.DataDay.push(mel);
                        } else if (this.DataDay[0].date == mel.date) {
                            this.DataDay.push(mel);
                        } else {
                            this.DataDay = [];
                            this.DataDay.push(mel);
                        }
                        mel.DataDay = this.DataDay;
                        this.dataft.push(mel);
                    }
                }
                this.dataft = _.uniqBy(this.dataft, 'date');
                // console.log('dataft', this.dataft, chartData, this.eventList);
                // this.WsChartData.push({ name: 'Pred_in_ft', color: "#000000", data: chartData, tooltip: { valueDecimals: 2 } });
            }
            // setTimeout(() => {
            //     this.plotChart();
            // }, 250);
        }
        catch (e) {
            console.log(e.messages);
        }
    }

    Errortext: string = '';
    chartData = []
    filterMinDataTide(date: any, stationList?: any[]) {
        try {
            if (stationList)
                if (stationList.length > 0) {
                    this.DataDay = [];
                    var dd = ("0" + (this.selectedDay)).slice(-2);
                    var mn = ("0" + (this.selectedMonth + 1)).slice(-2);
                    var yy = this.YearMonth;
                    var dateformat = [yy, mn, dd].join("-");
                    for (let mel of stationList) {
                        this.chartData.push([(moment(mel.date_time).unix()) * 1000, JSON.parse(mel.pred_in_ft)]);
                    }
                    if (this.chartData.length > 1)
                        this.errormsgEnabled = true;

                    this.WsChartData.push({ name: 'Pred_in_ft', color: "#000000", data: this.chartData, tooltip: { valueDecimals: 2 } });
                }
            if (date === 1 && this.WsChartData.length > 0)
                this.plotChart()
            else if (this.hgchart && date === 0)
                this.hgchart.update({
                    series: this.WsChartData
                })
            else if (date) {
                this.updatePlotGraph();
            }

        }
        catch (e) {
            console.log(e.messages);
        }
    }
    hgchart: any;
    dattime(time) {

        // Check correct time format and split into components
        time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        let Dtime = time[0] + time[1] + time[2] + time[5];
        //   console.log('time', time[0] + time[1] + time[2] + time[5]);
        return Dtime; // return adjusted time or original string
    }
    getfuturemonth() {
        //   debugger
        let dateList = [];
        const today = new Date()
        // today.setDate(1)
        // today.setMonth(today.getMonth() - 3)
        for (let index = 1; index <= 12; index++) {
            const startDate = new Date(today.getFullYear(), today.getDate(), today.getMonth());

            const endDate = new Date(today.getFullYear(), 0, today.getMonth() + 1);
            today.setDate(1)

            dateList.push({
                fromDate: startDate.toLocaleDateString().split('/').reverse().join('-'),
                endDate: endDate.toLocaleDateString().split('/').reverse().join('-'),
            })
            today.setMonth(today.getMonth() + 1)

        }
        return dateList;
    }
    allDates() {
        // get all days in selected month
        // debugger
        let date: Date = new Date(new Date(this.YearMonth, this.selectedMonth, 1));
        console.log('dataDate', date);
        var names = ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'];
        this.authService.dateArray = [];
        while (date.getMonth() == this.selectedMonth) {
            this.authService.dateArray.push({ day: date.getDate(), name: names[date.getDay()] });
            date.setDate(date.getDate() + 1);
        }
        if (this.selectedDay > this.authService.dateArray.length)
            this.selectedDay = 1;
        this.getTideStationDetails(this.Tides.stationId);
        let futureDateList: any[] = this.getfuturemonth()
        this.TideStationminlist = [];
        this.WsChartData = [];
        this.chartData = [];
        this.getTideStationMinDetailsnew(this.Tides.stationId, futureDateList[0].fromDate, futureDateList[0].endDate, futureDateList, 0);
        setTimeout(() => {
            try {
                this.leftItems._scrollContent.nativeElement.scrollLeft = (50 * this.selectedDay) - 200;//(temp.clientWidth*(scroll))-50;
            } catch (ex) {

            }
        }, 1000);
    }
    get dateString() {// concat date,month and year to create date string
        return new Date().getFullYear() + "-" + this.selectedMonth + "-" + this.selectedDay;
    }

    plotChart() {
        // const self = this;
        var v = new Date();
        var chart = Highcharts.stockChart('container', {
            chart: {
                height: 500,
                type: 'spline',
                zoomType: 'x',
                polar: true,
                // plotBackgroundImage: this.img,
                backgroundColor: '#4f92c1',
                style: {
                    color: "#FFFFFF"
                }
            },
            tooltip: {
                xDateFormat: '%A, %b %e, %Y, %l:%M %p',
            },
            xAxis: {
                //tickInterval: 24 * 3600 * 1000, // one day
                alternateGridColor: '#FFFFFF',
                type: 'datetime',
                labels: {
                    overflow: 'justify',
                    style: {
                        color: "#FFFFFF"
                    }
                },
                dateTimeLabelFormats: {
                    // day: '%e of %b',
                    // minute:  '%I:%M',
                    hour: '%I:%M %p'
                }
            },
            yAxis: {
                labels: {
                    style: {
                        color: "#000000"
                    }
                },
                title: {
                    text: 'Height in feet (MLLW)',
                    style: {
                        color: "#FFFFFF"
                    }
                }
            },
            plotOptions: {
                series: {
                    fillColor: {
                        linearGradient: [0, 0, 0, 300],
                        stops: [
                            [0, Highcharts.getOptions().colors[0]],
                            [1, Highcharts.Color(Highcharts.getOptions().colors[0]).setOpacity(0).get('rgba')]
                        ]
                    }
                }
            },
            rangeSelector: {
                inputEnabled: false,
                buttonTheme: { // styles for the buttons
                    style: {
                        color: '#039',
                        fontWeight: 'bold',
                        Width: '220px',
                        height: '50px'
                    },
                    states: {
                        hover: {
                        },
                        select: {
                            fill: '#039',
                            style: {
                                color: 'white'
                            }
                        }
                    }
                },
                labelStyle: {
                    color: '#FFF',
                    fontWeight: 'bold'
                },
                selected: this.range,
                buttons: [{
                    type: 'day',
                    count: 3,//3
                    text: '1d',
                    days: 1
                }, {
                    type: 'week',
                    count: 3,//3
                    text: '1w',
                    days: 7
                }, {
                    type: 'month',
                    count: 3,//2
                    text: '1m',
                    days: 30

                }, {
                    type: 'all',
                    text: 'All'
                }],

            },
            series: this.WsChartData
        });
        var submit = document.getElementById('btnBack');
        submit.addEventListener('click', () => {
            var theChart = chart,
                extremes = theChart.xAxis[0].getExtremes(),
                minTime = extremes.min,
                maxTime = extremes.max,
                currRangeIndex = theChart.rangeSelector.selected,
                currRange = theChart.rangeSelector.buttonOptions[currRangeIndex],
                newMaxTime = minTime;
            // new start/end times
            var startTime,
                endTime;

            if (currRange.type == 'month') {
                var endDate = new Date(newMaxTime);
                var startDate = moment(endDate);
                startDate.subtract(currRange.count, 'months');

                startTime = startDate.valueOf();
                endTime = newMaxTime;
                console.log("selectedMonth", this.selectedMonth)
                this.selectedMonth = this.selectedMonth - 1
                console.log("selectedMonth", this.selectedMonth)
                //console.log(this.range)
                // this.range = 2

                if ((this.selectedMonth) >= 0)
                    this.monthselect()
                else {
                    this.YearMonth = this.YearMonth - 1
                    console.log("YearMonth", this.YearMonth)
                    this.getYear(this.YearMonth)


                }
                //this.setGraphChangedate(endTime)

            } else if (currRange.type == 'week') {
                // var endDate = new Date(newMaxTime);
                // var startDate = moment(endDate);
                // startDate.subtract(currRange.count, 'week');
                // startTime = startDate.valueOf();
                // endTime = newMaxTime;
                console.log("selectedDay week", this.selectedDay - currRange.count)
                console.log("selectedDay week", this.selectedMonth)

                if ((this.selectedDay - currRange.days) > 0) {
                    if (this.selectedMonth == 2 && (this.selectedDay - currRange.days) > 27) {
                        this.onDateClick((27), 0)
                    } else {
                        this.onDateClick((this.selectedDay - currRange.days), 0)
                    }
                }
                else {
                    this.selectedMonth = this.selectedMonth - 1
                    this.selectedDay = (this.selectedDay - currRange.days) + 30
                    if (this.selectedMonth == 2 && (this.selectedDay) > 27) {
                        this.selectedDay = 27
                        this.monthselect()
                    }
                    else {
                        this.monthselect()
                    }
                }
                // this.setGraphChangedate(endTime)

            } else if (currRange.type == 'day') {
                // var endDate = new Date(newMaxTime);
                // var startDate = moment(endDate);

                // startDate.subtract(currRange.count, 'day');
                // console.log("START DATE", startDate)
                // console.log("END DATE", newMaxTime)

                // startTime = startDate.valueOf();
                // endTime = newMaxTime;
                if ((this.selectedDay - currRange.days) > 0) {
                    if (this.selectedMonth == 2 && (this.selectedDay - currRange.days) > 27) {
                        this.onDateClick((27), 0)
                    } else {
                        this.onDateClick((this.selectedDay - currRange.days), 0)

                    }
                } else {
                    this.selectedMonth = this.selectedMonth - 1
                    this.selectedDay = 1
                    if (this.selectedMonth == 2 && (this.selectedDay) > 27) {
                        this.selectedDay = 27
                        this.monthselect()
                    } else {
                        this.monthselect()

                    }
                }
                //this.setGraphChangedate(endTime)
            }
            // console.log(endTime, startTime);
            //  theChart.xAxis[0].setExtremes(startTime, endTime);
        });
        var dd = null;
        var submit = document.getElementById('btnFwd');
        submit.addEventListener('click', () => {
            //alert(chart)
            var theChart = chart,
                extremes = theChart.xAxis[0].getExtremes(),
                minTime = extremes.min,
                maxTime = extremes.max,
                currRangeIndex = theChart.rangeSelector.selected,
                currRange = theChart.rangeSelector.buttonOptions[currRangeIndex],
                newMaxTime = maxTime;
            // new start/end times
            var startTime,
                endTime;
            if (currRange.type == 'month') {
                // var endDate = new Date(newMaxTime);
                // var startDate = moment(endDate);
                // startDate.add(currRange.count, 'months');
                // startTime = startDate.valueOf();
                // endTime = newMaxTime;
                // console.log("START DATE", startTime)
                // console.log("END DATE", newMaxTime)
                // console.log("count", currRange.count)
                // console.log("currRangeIndex", currRangeIndex)
                // console.log("currRange", currRange)

                console.log("selectedMonth", this.selectedMonth)
                this.selectedMonth = this.selectedMonth + 1
                console.log("selectedMonth", this.selectedMonth)
               

                if ((this.selectedMonth) < 12) {
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.monthselect()

                    } else {
                        this.monthselect()
                    }
                }
                else {
                    this.YearMonth = this.YearMonth + 1
                    console.log("YearMonth", this.YearMonth)
                    this.getYear(this.YearMonth)

                }

                // this.setGraphChangedate(endTime)

            } else if (currRange.type == 'week') {
                // var endDate = new Date(newMaxTime);
                // var startDate = moment(endDate);
                // startDate.add(currRange.count, 'week');
                // startTime = startDate.valueOf();
                // endTime = newMaxTime;
                // console.log("START DATE", startTime)
                // console.log("END DATE", newMaxTime)
                // console.log("count", currRange.count)
                // console.log("currRangeIndex", currRangeIndex)
                // console.log("currRange", currRange)
                // console.log("selectedDay week", this.selectedDay + currRange.count)

                if ((this.selectedDay + currRange.days) < 30) {
                    this.onDateClick((this.selectedDay + currRange.days), 0)
                }
                else {
                    this.selectedMonth = this.selectedMonth + 1
                    this.selectedDay = (this.selectedDay + currRange.days) - 30
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.monthselect()
                    } else {
                        this.monthselect()
                    }
                }
                //this.setGraphChangedate(endTime)

            } else if (currRange.type == 'day') {
                // var endDate = new Date(newMaxTime);
                // var startDate = moment(endDate);
                // startDate.add(currRange.count, 'day');
                // startTime = startDate.valueOf();
                // endTime = newMaxTime;
                // console.log("START DATE", startTime)
                // console.log("END DATE", newMaxTime)
                // console.log("count", currRange.count)
                // console.log("currRangeIndex", currRangeIndex)
                // console.log("currRange", currRange)
                // console.log("selectedDay day", this.selectedDay)

                // this.hgchart.rangeSelector.selected = 0
                // console.log(this.range)

                if ((this.selectedDay + currRange.days) < 31) {
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.onDateClick((this.selectedDay), 0)
                    } else {
                        this.onDateClick((this.selectedDay + currRange.days), 0)

                    }
                } else {
                    this.selectedMonth = this.selectedMonth + 1
                    this.selectedDay = 1
                    if (this.selectedMonth == 2 && this.selectedDay > 28) {
                        this.selectedDay = 27
                        this.monthselect()

                    } else {
                        this.monthselect()
                    }
                }
                // this.setGraphChangedate(endTime)

            }

            // alert(dd.getDate())
            // console.log("TIme " + endTime, startTime);
            //theChart.xAxis[0].setExtremes(endTime, startTime);
        });
        //this.selectedDay=dd.getDate()

        this.hgchart = chart
        this.updatePlotGraph()
    }
    setGraphChangedate(setdate: any) {

        this.YearMonth = new Date(setdate).getFullYear()
        //alert(this.YearMonth)
        this.selectedMonth = new Date(setdate).getMonth()
        this.selectedDay = new Date(setdate).getDate()
    }
    updatePlotGraph() {
        // chart.showLoading();
        setTimeout(() => {
            try {
                this.leftItems._scrollContent.nativeElement.scrollLeft = (50 * this.selectedDay) - 200;//(temp.clientWidth*(scroll))-50;
            } catch (ex) {

            }
        }, 1000);
        if (this.graphEnabled) {
            this.hgchart.xAxis[0].setExtremes(
                (Date.UTC(this.YearMonth, this.selectedMonth, 1)) - 15 * 24 * 60 * 60 * 1000,
                (Date.UTC(this.YearMonth, this.selectedMonth, 31)) + 15 * 24 * 60 * 60 * 1000,
            );
        } else {
            this.hgchart.showLoading();
            let currRangeIndex = this.hgchart.rangeSelector.selected;
            console.log("updatePlotGraph currRangeIndex", currRangeIndex)
            let currRange = this.hgchart.rangeSelector.buttonOptions[currRangeIndex];
            //this.hgchart.rangeSelector.buttons[currRangeIndex].element.onclick();

            var endDate_1 = (Date.UTC(this.YearMonth, this.selectedMonth, this.selectedDay)) - 1 * 24 * 60 * 60 * 1000;
            var startDate_1 = moment(endDate_1);
            console.log("currRange in main", currRange)
            switch (currRangeIndex) {
                case 0:
                    startDate_1.add(currRange.count, 'day');
                    break;
                case 1:
                    startDate_1.add(currRange.count, 'week');
                    break;
                case 2:
                    startDate_1.add(currRange.count, 'month');
                    break;
                default:
                    startDate_1.add(currRange.count, 'day');


            }
            // startDate_1.add(currRange.count, 'day');

            var startTime_1 = startDate_1.valueOf();
            var endTime_1 = endDate_1;
            console.log(endTime_1, startTime_1);
            this.hgchart.xAxis[0].setExtremes(endTime_1, startTime_1);

        }
        if (this.hgchart.series.length < 1) {
            if (this.errormsgEnabled) {
                this.hgchart.hideLoading();
                this.hgchart.renderer.text('6 minutes prediction data is not available for this station', 10, 120)
                    .css({
                        color: '#FFFFFF',
                        fontSize: '20px'
                    })
                    .add();
            }
        } else if (this.hgchart.series.length >= 1) {
            this.hgchart.hideLoading();
        }
    }
}
