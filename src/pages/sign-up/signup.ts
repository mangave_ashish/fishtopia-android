import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Http } from '@angular/http';
import { AppVersion } from '@ionic-native/app-version';
import { Device } from '@ionic-native/device';
import { Facebook } from '@ionic-native/facebook';
import { FCM } from '@ionic-native/fcm';
import { GooglePlus } from '@ionic-native/google-plus';
import { AlertController, IonicPage, LoadingController, MenuController, NavController, NavParams, Platform } from 'ionic-angular';
import 'rxjs/add/operator/map';
import { AuthService } from '../../providers/auth-provider';
import { UserData } from '../../providers/user-data-local';


@IonicPage()
@Component({
  selector: 'sign-up',
  templateUrl: 'signup.html',
})
export class signup {
  public loginForm: any;
  User: any;
  type = "password";
  Countrycode: any;
  CountryName: any;
  countries: any[] = [];
  PhoneCode: any;
  show = false;
  RegisterForm: FormGroup;
  FirstName: AbstractControl;
  MiddleName: AbstractControl;
  LastName: AbstractControl;
  Email: AbstractControl;
  ConfirmEmail: AbstractControl;
  Password: AbstractControl;
  StrtAddr1: AbstractControl;
  StrtAddr2: AbstractControl;
  State: AbstractControl;
  City: AbstractControl;
  Countryoption: AbstractControl;
  ZipCode: AbstractControl;
  PhoneNumber: AbstractControl;
  Refercode: AbstractControl;
  userType: any;
  submitAttempt: boolean = false;
  mismatchEmail: boolean = false;
  private phoneMask = ['(', /[1-9]/, /\d/, /\d/, ')', ' ', /\d/, /\d/, /\d/, '-', /\d/, /\d/, /\d/, /\d/];
  isLoggedIn: boolean = false;
  Social_NetEnabled: any;
  Fb_UserData: any;
  AppVersion: any;
  device_idd: any;
  Referralcodetxt: string = 'Apply code';
  public product: any = {
    name: 'Alaska FishTopia',
    appleProductId: 'AKFISHTOPIA001',
    googleProductId: 'akfishtopiayearlysubscription'
  };
  productId: any;
  //PhoneCode: AbstractControl;
  constructor(
    private loadingCtrl: LoadingController,
    private alertCtrl: AlertController,
    private navCtrl: NavController,
    private http: Http,
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private navParams: NavParams,
    private fb: Facebook,
    private Gplus: GooglePlus,
    private device: Device,
    private fcm: FCM,
    private app: AppVersion,
    public userData: UserData,
    // private iap: InAppPurchase,
    public platform: Platform,
    public menuCtrl: MenuController
  ) {
    this.userType = navParams.get('UserType');
    app.getVersionNumber().then((res) => {
      this.AppVersion = res;
    });
    this.RegisterForm = this.formBuilder.group({
      'FirstName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
      'MiddleName': ['', Validators.compose([Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
      'LastName': ['', Validators.compose([Validators.required, Validators.minLength(3), Validators.maxLength(25), Validators.pattern('[a-zA-Z][a-zA-Z ]*')])],
      'Email': ['', Validators.compose([Validators.required, Validators.pattern('[a-zA-Z0-9._%+-]+@[a-z0-9.-]+[.]{1}[a-zA-Z]{2,}$')])],
      'ConfirmEmail': ['', Validators.required],
      'Password': ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      'StrtAddr1': [''],
      'StrtAddr2': [''],
      'State': [''],
      'City': [''],
      'Countryoption': ['', Validators.required],
      'ZipCode': [''],
      'Refercode': [''],
      'PhoneNumber': ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10), Validators.pattern('[0-9][0-9 ]*')])],
      //'userType': [''],
    }, { validator: this.matchingPasswords('Email', 'ConfirmEmail') });
    this.FirstName = this.RegisterForm.controls['FirstName'];
    this.MiddleName = this.RegisterForm.controls['MiddleName'];
    this.LastName = this.RegisterForm.controls['LastName'];
    this.Email = this.RegisterForm.controls['Email'];
    this.ConfirmEmail = this.RegisterForm.controls['ConfirmEmail'];
    this.Password = this.RegisterForm.controls['Password'];
    this.StrtAddr1 = this.RegisterForm.controls['StrtAddr1'];
    this.StrtAddr2 = this.RegisterForm.controls['StrtAddr2'];
    this.State = this.RegisterForm.controls['State'];
    this.City = this.RegisterForm.controls['City'];
    this.Countryoption = this.RegisterForm.controls['Countryoption'];
    this.ZipCode = this.RegisterForm.controls['ZipCode'];
    this.PhoneNumber = this.RegisterForm.controls['PhoneNumber'];
    this.Refercode = this.RegisterForm.controls['Refercode'];

    //   this.userType = this.RegisterForm.controls['userType'];
    //  this.PhoneCode = this.RegisterForm.controls['PhoneCode'];
    this.User = "angler";
    //console.log(this.http.get('assets/data/locations.json'));
    this.http.get('assets/data/locations.json').subscribe(data => {
      var json = JSON.parse(data.text());
      this.countries = json.locations;
      console.log(this.countries);
      //JSON.stringify(data)
    });
    fb.getLoginStatus()
      .then(res => {
        console.log(res.status);
        if (res.status === "connect") {
          this.isLoggedIn = true;
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log(e));
  }

  matchingPasswords(EmailKey: string, ConfirmEmailKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let Email = group.controls[EmailKey];
      let ConfirmEmail = group.controls[ConfirmEmailKey];
      if (Email.value !== ConfirmEmail.value) {
        this.mismatchEmail = true;
        return {
          mismatchedPasswords: true
        };
      }
      else {
        this.mismatchEmail = false;
      }
    }
  }

  onLocationChange() {
    var testname = this.RegisterForm.controls['Countryoption'].value;
    console.log(testname.dial_code);
    this.CountryName = testname.name;
    this.Countrycode = testname.dial_code;

  }

  SignUp() {
    console.log();

  }
  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });
    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }


  signup() {
    // this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('SignUpSuccessPage');
    //console.log('countries',this.countries);
  }
  resetPassword() {
    this.presentLoading('An e-mail was sent with your new password.');
  }
  eyesicon: boolean = false;
  toggleShow() {
    this.show = !this.show;
    if (this.show) {
      this.eyesicon = true;
      this.type = "text";
    }
    else {
      this.eyesicon = false;
      this.type = "password";
    }
  }

  register() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    loading.present();
    var mobNo = this.RegisterForm.controls['PhoneNumber'].value;
    var PhCode = this.Countrycode;
    //var   ConfirmEmail: this.RegisterForm.controls['ConfirmEmail'].value;
    let data = {
      first_name: this.RegisterForm.controls['FirstName'].value,
      middle_name: this.RegisterForm.controls['MiddleName'].value,
      last_name: this.RegisterForm.controls['LastName'].value,
      email: this.RegisterForm.controls['Email'].value,
      password: this.RegisterForm.controls['Password'].value,
      street_address1: this.RegisterForm.controls['StrtAddr1'].value,
      street_address2: this.RegisterForm.controls['StrtAddr2'].value,
      state: this.RegisterForm.controls['State'].value,
      city: this.RegisterForm.controls['City'].value,
      country: this.CountryName,
      zip_code: this.RegisterForm.controls['ZipCode'].value,
      phone_no: PhCode+mobNo,
      user_type: this.userType,
      inviters_referral_code: this.RegisterForm.controls['Refercode'].value,
      inviters_id: this.inviters_id
    }
    console.log(data)
    //  if (this.RegisterForm.valid) {
    //    this.presentLoading();



    this.authService.signup(data).subscribe(res => {
      if (res.success) {
        loading.dismiss();
        let email = res.data.email;
        localStorage.setItem("userEmail", email);
        let alert = this.alertCtrl.create({
          //title: 'Exit?',
          message: 'Your information has been received and a one-time use code has been sent to the mobile number provided. Please enter this number on the next screen to complete registration.',
          buttons: [
            {
              text: 'OK',
              role: 'OK',
              handler: () => {
                //  this.alert = null
              }
            }
          ]
        });
        alert.present();
        console.log('Output for signupdata-->');
        this.RegisterForm.reset();
        this.navCtrl.push('SignUpSuccessPage', { phone: mobNo, phoneCode: PhCode, UserId: res.data.user_id });
        //   this.navCtrl.setRoot(LoginPage);

      } else {
        this.submitAttempt = true;
        // let alert = this.alertCtrl.create({
        //     //title: 'Exit?',
        //     message: 'The Email you entered is already registered. Try to login using this email.',
        //     buttons: [
        //         {
        //             text: 'OK',
        //             role: 'OK',
        //             handler: () => {
        //                 //  this.alert = null
        //             }
        //         }
        //     ]
        // });
        // alert.present();
      }
    });
  }

  goToSignup() {
    //this.slider.slideTo(2);
    this.navCtrl.push('LoginListPage');
  }
  //}


  GplusDB: any;
  googlePlus_login() {
    this.Gplus.login({})
      .then((res: any) => {
        console.log('JSON.stringify(res)', JSON.stringify(res));
        this.GplusDB = {
          tem_id: res.accessToken,
          email: res.email,
          displayName: res.displayName,
          familyName: res.familyName,
          givenName: res.givenName,
          userId: res.userId,
          expires: res.expires,
          expires_in: res.expires_i
        }
        this.Social_NetEnabled = 'Google+';
        this.login();
      })
      .catch(err => console.log(JSON.stringify(err)));
  }
  fb_login() {
    this.fb.login(['public_profile', 'email'])
      .then(res => {
        //   console.log(JSON.stringify(res));
        //  alert(JSON.stringify(res));
        if (res.status === "connected") {
          this.isLoggedIn = true;
          this.GplusDB = res.authResponse;
          this.getUserDetail(res.authResponse.userID);
        } else {
          this.isLoggedIn = false;
        }
      })
      .catch(e => console.log('Error logging into Facebook', e));
  }
  getUserDetail(userid) {
    this.fb.api("/" + userid + "/?fields=id,email,name,picture,gender", ["public_profile"])
      .then(res => {
        this.Social_NetEnabled = 'Facebook';
        this.Fb_UserData = res;
        this.login();
      })
      .catch(e => {
        console.log(e);
      });
  }
  facebookLogin() {
    var data = {
      tem_id: this.GplusDB.accessToken,
      email: this.Fb_UserData.email,
      displayName: this.Fb_UserData.name,
      familyName: this.Fb_UserData.name,
      givenName: this.Fb_UserData.name,
      userId: this.GplusDB.userID,
      expires: this.GplusDB.expiresIn,
      expires_in: this.GplusDB.expiresIn,
      signUpType: 'Facebook',
      user_type: this.userType,
      inviters_referral_code: this.RegisterForm.controls['Refercode'].value,
      inviters_id: this.inviters_id
    }
    // alert('push' + JSON.stringify(data));
    this.authService.googleAccsess(data).subscribe(
      res => this.SocialNetworkSignup(res),
      error => alert(JSON.stringify(error))
    )
  }


  googleLogin() {
    var data = {
      tem_id: this.GplusDB.accessToken,
      email: this.GplusDB.email,
      displayName: this.GplusDB.displayName,
      familyName: this.GplusDB.familyName,
      givenName: this.GplusDB.givenName,
      userId: this.GplusDB.userId,
      expires: this.GplusDB.expires,
      expires_in: this.GplusDB.expires_i,
      signUpType: 'Google+',
      inviters_referral_code: this.RegisterForm.controls['Refercode'].value,
      inviters_id: this.inviters_id
    }
    //   alert('push' + JSON.stringify(data));
    this.authService.googleAccsess(data).subscribe(
      res => this.SocialNetworkSignup(res),
      error => alert(JSON.stringify(error))
    )
  }

  // SocialNetwork_Data: any;
  SocialNetworkSignup(res: any) {
    var data = {
      email: res.data.email,
      device_type: this.device.platform,
      unique_device_id: this.device.uuid,
      device_id: this.Notifytoken,
      app_version: this.AppVersion,
      Social_NetEnabled: this.Social_NetEnabled,

    }
    // alert(JSON.stringify(res));
    // alert('res ' + res.data._id + res._id);
    if (res.data._id == undefined && res.data._id == null) {
      this.doLoginSocialNetwork(data, this.userType);
      // this.navCtrl.push('UserTypePage', { SocialNetdata: data });
    } else {
      this.doLoginSocialNetwork(data, res.data.user_type);
    }
  }

  doLoginSocialNetwork(res: any, user_type) {
    var data = {
      email: res.email,
      device_type: this.device.platform,
      unique_device_id: this.device.uuid,
      device_id: this.Notifytoken,
      user_type: user_type,
      app_version: this.AppVersion
    }
    //alert(JSON.stringify(data));
    this.authService.googleSignUp(data).subscribe(
      data => this.SN_loginSucessfully(data),
      error => console.log(JSON.stringify(error))
    );
  }

  Referralcode: any;
  inviters_id: any;
  ReferralcodeApply() {
    if (this.Referralcode != null) {
      var data = {
        referral_code: this.Referralcode
      }
      //   alert('push' + JSON.stringify(data));
      this.authService.referralVerifed(data).subscribe(
        res => {
          // console.log(res);
          if (res.success) {
            this.Referralcodetxt = "Applied";
            this.inviters_id = res.inviters_id;
          }
        })
    }
    //  error => alert('push' + JSON.stringify(error))
    //)
  }
  Subsdata: any;
  Notifytoken: any;
  login() {
    let self = this;
    // let dat = localStorage.getItem('SubsDetails');
    // if (dat == null) {
    //   self.iapCheck();
    // }
    self.fcm.subscribeToTopic('Hello');
    self.fcm.getToken().then(token => {
      self.Notifytoken = token;
      console.log('token', token);
      if (self.Social_NetEnabled == 'Google+') {
        self.googleLogin();
      } else if (self.Social_NetEnabled == 'Facebook') {
        self.facebookLogin();
      }
    })
    self.fcm.onNotification().subscribe(data => {
      console.log('data', JSON.stringify(data))
      if (data.wasTapped) {
        console.log("Received in background");
        //if user NOT using app and push notification comes
        //TODO: Your logic on click of push notification directly
        console.log('Push notification clicked');
        self.redirectPage(data);
      }
      else {
        let confirmAlert = self.alertCtrl.create({
          title: 'New Notification',
          message: data.body,
          buttons: [{
            text: 'Ignore',
            role: 'cancel'
          }, {
            text: 'View',
            handler: () => {
              //TODO: Your logic here
              self.redirectPage(data);
            }
          }]
        });
        confirmAlert.present();
      }
      //console.log("Received in background", JSON.stringify(data));
      self.authService.getNotificationsCount();
    })
    self.fcm.onTokenRefresh().subscribe(token => {
      console.log(token);
      self.refreshToken(token);
    });
  }
  refreshToken(token: any) {
    var unique_device_id = localStorage.getItem("unique_device_id");
    var data = {
      unique_device_id: unique_device_id,
      new_device_id: token
    }
    this.authService.UpdateNotificationToken(data).subscribe(res => {
      console.log(res);
    })
  }

  redirectPage(data: any) {
    console.log('data', data);
    this.authService.getNotificationsCount();
    if (data.notification_slug === "news_and_fishing") {
      this.navCtrl.setRoot('FishingNewsPage');
    } else if (data.notification_slug === "whats_hot") {
      this.navCtrl.setRoot('WHEventPage');
    } else if (data.notification_slug === "fishing_exchange_global") {
      this.navCtrl.setRoot('FishingExchangPage');
    } else if (data.notification_slug === "fish_counts_global") {
      this.navCtrl.setRoot('FishCountsPage');
    } else if (data.notification_slug === "payment_success") {
      this.navCtrl.push('NotificationsPage');
    } else if (data.notification_slug === "angler_booked_trip") {
      this.navCtrl.setRoot('FishingExchangPage');
    } else if (data.notification_slug === "trip_is_full") {
      this.navCtrl.setRoot('FishingExchangPage');
    } else if (data.notification_slug === "guide_accept_trip") {
      this.navCtrl.setRoot('FishingExchangPage');
    } else if (data.notification_slug === "guide_sent_trip_request") {
      this.navCtrl.setRoot('FishingExchangPage');
    } else if (data.notification_slug === "to_all_users") {
      this.navCtrl.push('NotificationsPage');
    } else {
      this.navCtrl.push('NotificationsPage');
    }
  }



  SN_loginSucessfully(data) {
    let output = data;
    // console.log('Login data', JSON.stringify(output));
    if (output.success === false) {
      //this.errorMsg = output.message;
      this.submitAttempt = true;
    }
    else {
      let auth = output.data.user_token.access_token;
      this.userData.setAuthToken(auth);
      console.log('output.data.', output.data);
      localStorage.setItem("token", auth);
      localStorage.setItem("unique_device_id", output.data.user_token.unique_device_id);
      localStorage.setItem("userId", output.data._id);
      localStorage.setItem("UserType", output.data.user_type);
      this.getDataOffline();
      this.navCtrl.setRoot('dashboardPage');
    }
  }

  dataDB: any[] = [];
  getDataOffline() {
    this.dataDB = JSON.parse(localStorage.getItem("tbl_BuoyData"));
    if (this.dataDB === null) {
      this.authService.getProfileDetails();
      this.authService.getTideDetails_New();
      this.authService.getCurrentsDetails();
      this.authService.getBuoyDetails();
      this.authService.getMarinelistinfo();
    }
  }

  // iapCheck() {
  //   var dat = localStorage.getItem('SubsDetails');
  //   var dr = JSON.parse(dat);
  //   let CurrDate = new Date();
  //   let expiry_date = new Date(dr.data.expiry_date);
  //   if (CurrDate >= expiry_date) {
  //     localStorage.removeItem('UserType');
  //     localStorage.removeItem('UserName');
  //     localStorage.removeItem('Profileimg');
  //     localStorage.removeItem('userId');
  //     localStorage.removeItem('is_featured');
  //     localStorage.removeItem('fcmToken');
  //     localStorage.setItem("old_token", localStorage.getItem("token"));
  //     localStorage.removeItem('token');
  //     this.userData.removeAuthToken();
  //     this.getReceipt();
  //   } else {
  //     this.getReceipt()
  //   }
  // }
  // getReceipt() {
  //   this.iap
  //     .getReceipt()
  //     .then((data: any) => {
  //       console.log('validate receipt data' + data)
  //       if (data) {
  //         var receipt_data = {
  //           "receipt_no": data
  //         }
  //         this.authService.iosSubIAP(receipt_data).subscribe(res => {
  //           if (res.success) {
  //             if (res.data.status && res.data.expiry_date != null) {
  //               localStorage.setItem('SubsDetails', JSON.stringify(res));
  //             } else {
  //               this.menuCtrl.swipeEnable(false);
  //               this.navCtrl.setRoot('subscribeDialogPage');
  //             }
  //           } else {
  //             this.menuCtrl.swipeEnable(false);
  //             this.navCtrl.setRoot('subscribeDialogPage');
  //           }
  //         })
  //       } else {
  //         this.menuCtrl.swipeEnable(false);
  //         this.navCtrl.setRoot('subscribeDialogPage');
  //       }
  //     }).catch((err) => {
  //       console.log(JSON.stringify(err));
  //     });
  // }

}
