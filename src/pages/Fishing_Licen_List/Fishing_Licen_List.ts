import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

/**
 * Generated class for the NotificationsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-Fishing_Licen_List',
	templateUrl: 'Fishing_Licen_List.html',
})
export class FishingLicenListPage {
	fishingLicenList: any[] = [];
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	EndDate: any;
	StartDate: any;
	constructor(
		public navCtrl: NavController,
		public navParams: NavParams,
		public Auth: AuthService,
		public toastCtrl: ToastController,
		public loadingCtrl: LoadingController,
	) {
		this.fishingLicenlist(1);
		this.StartDate = new Date();
	}

	ionViewWillLeave() {
		this.EndDate = new Date();
		this.Auth.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}



	ionViewDidLoad() {
		console.log('ionViewDidLoad NotificationsPage');
	}

	licenDetails(item: any) {
		var license_number = item.license_number.split(/(\d+)/);
		var pdfUrl = {
			title: 'Fishing License Details',
			displayText: license_number[1],
			parent_Name: 'fishingLicense',
			pdfURL: item.license_url
		}
		console.log('pdfUrl', pdfUrl);
		this.navCtrl.push('LicenpdfViewpage', { data: pdfUrl });
	}
	fishingLicenlist(Page: any) {
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 3500
		});
		loading.present();
		var data = {
			user_id: localStorage.getItem('userId')
		}
		this.Auth.fishingLicenlist(data).subscribe(res => {
			// console.log('NotificationsPage' + JSON.stringify(res));
			this.fishingLicenList = res.result;
		})
	}


	DeleteNotification(data: any) {
		// let loading = this.loadingCtrl.create({
		// 	spinner: 'hide',
		// 	content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
		// 	duration: 5500
		// });
		// loading.present();
		// this.Auth.getNotificationDelete(data._id).subscribe(res => {
		// 	loading.dismiss();
		// 	this.notificationList = [];
		// 	this.getNotifications(1);
		// 	this.presentToast('Notification Deleted Successfully.');
		// 	console.log('ionViewDidLoad NotificationsPage', res);
		// })
	}
	private presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 1500,
			position: 'bottom'
		});
		toast.present();
	}
}
