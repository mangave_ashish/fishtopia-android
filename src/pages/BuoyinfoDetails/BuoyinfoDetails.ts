import { Component } from '@angular/core';
import { IonicPage, NavController, ViewController, NavParams, AlertController, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
  selector: 'page-BuoyinfoDetails',
  templateUrl: 'BuoyinfoDetails.html',
})
export class BuoyinfoDetailsPage {
  stars: any;
  messagepost: any = "";
  BuoyinfoDetails: any;
  htmlDesp: any;
  DataNA: boolean = false;
  textstr: any;
  constructor(
    public loadingCtrl: LoadingController,
    public viewCtrl: ViewController,
    public alertCtrl: AlertController,
    public authService: AuthService,
    public navCtrl: NavController,
    public navParams: NavParams
  ) {
    this.BuoyinfoDetails = navParams.get('BuoyinfoDetails');
    this.getBUOYinfo();
  }
  getBUOYinfo() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      //duration: 7500
    });
    loading.present();
    this.authService.getBUOYStationDetails(this.BuoyinfoDetails).subscribe(res => {
      loading.dismiss();
      this.textstr = res.data.html;
      if (this.textstr != undefined) {
        this.DataNA = false;
        this.htmlDesp = this.textstr.replace('Feedback:', '');
        this.htmlDesp = this.htmlDesp.replace('Enter a station ID:', '');
      } else {
        this.DataNA = true;
      }
    });

  }

  dismiss() {
    this.viewCtrl.dismiss();
  }
}


