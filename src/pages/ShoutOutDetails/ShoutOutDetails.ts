import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
import { AuthService } from '../../providers/auth-provider';
@IonicPage()
@Component({
  selector: 'page-ShoutOutDetails',
  templateUrl: 'ShoutOutDetails.html',
})
export class ShoutOutDetailsPage {
  users = new Array(10);
  option: any;
  chatBox: any;
  isenabledlikeBox: any;
  isenabledCmtBox: any;
  ShoutOut: any;
  constructor(
    public navCtrl: NavController, public photoViewer: PhotoViewer, public authService: AuthService,
    public navParams: NavParams) {
    this.ShoutOut = navParams.get('ShoutOutDetails');
    console.log('ShoutOutDetails', this.ShoutOut);
  }
  sendCmmt(news) {
    this.navCtrl.push('ShoutOutDetailsCmtsPage', { card: this.ShoutOut });
    //return this.openMenu = !this.openMenu;
  }
  PhotoView() {
    this.photoViewer.show(this.ShoutOut.images[0].image_url, this.ShoutOut.description, { share: false });
  }
}
