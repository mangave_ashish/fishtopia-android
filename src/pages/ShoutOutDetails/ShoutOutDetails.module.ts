import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoutOutDetailsPage } from './ShoutOutDetails';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    ShoutOutDetailsPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoutOutDetailsPage),
    SharedModule
  ],
})
export class ShoutOutDetailsPageModule {}
