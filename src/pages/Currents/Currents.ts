import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, Alert } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { LoadingController, Platform } from 'ionic-angular';
import * as _ from 'lodash';
import { DatabaseService } from '../../providers/weather';

@IonicPage({
  priority: 'high'
})

@Component({
  selector: 'page-Currents',
  templateUrl: 'Currents.html',
})

export class currentsPage {
  title: string;
  iconUrl: any;
  lat: number = 61.217381;;
  lng: number = -149.863129;
  latlong: any[] = [];
  markers: any[] = [];
  infoWindowOpened = null;
  map: any;
  regionals: any = [];
  currentregional: any;
  selectedDay: any;
  selectedMonth: any;
  tmwDate: any;
  YearMonth: any;
  SelectDate: any;
  mainEventList: any;
  dateList: Date[] = [];
  eventListData: any[] = [];
  ChartdateList: Date[] = [];
  GeoLat: any;
  GeoLong: any;
  pinsDataList: any;
  currentDateData: any[] = [];
  MapEnabled: boolean = false;
  data_DB: any[] = [];
  dataDB: any[] = [];
  TideStationlist: any[] = [];
  stationId: any;
  DataSet: any[] = [];
  Currents_Pin: any = '';
  EndDate: any;
  StartDate: any;
  PrevPage: any;
  constructor(
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public authService: AuthService,
    public platform: Platform,
    public databaseService: DatabaseService,
    public zone: NgZone
  ) {
    var v = new Date();
    this.StartDate = v;
    this.selectedDay = v.getDate();
    this.selectedMonth = v.getMonth();
    this.YearMonth = v.getFullYear();
    this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
    //this.load_Data();
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    loading.present();
    this.getGeolocationPosition();
  }

  ionViewWillEnter() {
    this.PrevPage = this.authService.title;
  }

  ionViewWillLeave() {
    this.EndDate = new Date();
    this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
    this.authService.audit_Page(this.PrevPage);
  }

  getGeolocationPosition() {
    let self = this;
    self.GeoLat = localStorage.getItem('GeoLat');
    self.GeoLong = localStorage.getItem('GeoLong');
    var locationData = localStorage.getItem('GeoLocation');
    var Sub_Location = localStorage.getItem('SubGeoLocation');
    if (locationData === "Alaska") {
      self.lat = JSON.parse(self.GeoLat);
      self.lng = JSON.parse(self.GeoLong);
      self.getCurrent();
    } else {
      self.lat = 61.217381;
      self.lng = -149.863129;
      self.getCurrent();
    }
  }


  getCurrent() {
    let self = this;
    self.pinsDataList = [];
    self.databaseService.getCurrentsPins('Curr_DB')
      .then(data => {
        // console.log('fetch data', data);
        if (data != undefined) {
          let CurrData = JSON.parse(data.data);
          console.log('fetch data', CurrData);
          if (CurrData.length < 0 || _.isEmpty(data.data)) {
            self.authService.getCurrentsDetails();
          } else {
            self.pinsDataList = CurrData;
          }
        } else {
          self.authService.getCurrentsDetails();
        }
      })
  }



  clickedViewMarker(data: any, infoWindow, index: number) {
    let self = this;

    console.log('clicked the marker:', data)
    if (self.infoWindowOpened === infoWindow)
      return;

    if (self.infoWindowOpened !== null) {
      self.infoWindowOpened.close();
    }
    self.infoWindowOpened = infoWindow;
    self.stationId = data;
    self.dateList = [];
    self.currentDateData = [];
    self.getCurrentPins('Curr_Cal_' + data._id, data);
  }

  getCurrentPins(station: any, stationDetails: any) {
    let self = this;
    self.databaseService.getCurrentsPins(station)
      .then(data => {
        if (data != undefined) {
          let CurrData = JSON.parse(data.data);
          console.log('getTide', JSON.parse(data.data));
          if (CurrData.length < 0 || _.isEmpty(data.data)) {
            self.getCurrentsStationDetails(stationDetails);
            // throw new Error('Invalid database forecast, fallback to server > ');
          } else {
            stationDetails.iconUrl = "assets/img/plain/yellow.png";
            self.Cal_Curr_Data(CurrData);
            self.CurrentStationlist = CurrData;
          }
        } else {
          self.getCurrentsStationDetails(stationDetails);
        }
      })
    // .catch(err => {
    //   if (err && err.message) {
    //     console.error(err.message);
    //     self.getCurrentsStationDetails(stationDetails);
    //   }
    //   // self.getTideDetails();
    // });
  }

  markerChge(station: any, ) {
    //debugger
    this.DataSet = [];
    _.forEach(this.pinsDataList, obj => {
      if (station == obj.stationId) {
        obj.iconUrl = "assets/img/plain/yellow.png";
      }
      this.DataSet.push(obj);
    });
    this.databaseService.updateCurr_Data(JSON.stringify(this.DataSet));
  }

  // Current Data
  CurrentStationlist: any[] = [];
  getCurrentsStationDetails(station: any) {
    console.log(station);
    let self = this;
    this.dateList = [];
    self.currentDateData = [];
    self.authService.getCurrentsStationData(station._id).subscribe(res => {
      station.iconUrl = "assets/img/plain/yellow.png";
      self.CurrentStationlist = res.data;
      self.Cal_Curr_Data(res.data);
      self.databaseService.addCurrentsPins(self.CurrentStationlist, 'Curr_Cal_' + station._id).then(res => {
        if (res) {
          self.markerChge(station._id);
        }
      })
    });
  }

  // Cal Current Data
  Cal_Curr_Data(data: any) {
    let self = this;
    var dd = ("0" + (this.selectedDay)).slice(-2);
    var mn = ("0" + (this.selectedMonth + 1)).slice(-2);
    var yy = self.YearMonth;
    var dateformat = [yy, mn, dd].join("-");
    console.log('dateformat', dateformat);
    _.forEach(data, (mel: any) => {
      try {
        //find out unique date                               
        if (dateformat == mel.date) {
          if (mel.event_val == "-") {
            mel.event_val = "0";
          }
          if (self.currentDateData.length < 5) {
            self.currentDateData.push(mel);
          }
          self.dateList.push(mel.date);
        }
      } catch (e) { }
    })
  }




  dattime(time) {
    // Check correct time format and split into components
    time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

    if (time.length > 1) { // If time format correct
      time = time.slice(1);  // Remove full string match value
      time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
      time[0] = +time[0] % 12 || 12; // Adjust hours
    }
    let Dtime = time[0] + time[1] + time[2] + time[5];
    console.log('time', time[0] + time[1] + time[2] + time[5]);
    return Dtime; // return adjusted time or original string
  }


  MarkerViewDetails() {
    this.navCtrl.push('CurrentsPageDetailsPage', { CurrentDetails: this.stationId, CurrentData: this.CurrentStationlist });
  }


}



