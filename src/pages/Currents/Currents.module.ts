import { NgModule, CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { currentsPage } from './Currents';
import { AgmCoreModule } from '@agm/core';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    currentsPage,
  ],
  imports: [
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCHvwvPHFQSagXqGQXz4bqHbOZr03_rRxo'
    }),
    IonicPageModule.forChild(currentsPage),
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class currentsPageModule { }
