import { Component, OnInit } from '@angular/core';
import { IonicPage, NavController, MenuController } from 'ionic-angular';
import { AuthService, WHEvent } from '../../providers/auth-provider';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { InAppBrowser, InAppBrowserOptions } from '@ionic-native/in-app-browser';

@IonicPage()
@Component({
    selector: 'page-WHEvent',
    templateUrl: 'Whats-hot-page.html'
})
export class WHEventPage implements OnInit {
    drawerOptions: any;
    eventList: WHEvent[] = [];
    TmweventList: WHEvent[] = [];
    eventSource;
    viewTitle;
    monthNames: any = ["January", "February", "March", "April", "May", "June",
        "July", "August", "September", "October", "November", "December"
    ];

    isToday: boolean;
    lockSwipeToPrev: boolean;
    month: string;
    mainEventList: WHEvent[] = [];
    mainEventFillterList: any[] = [];
    EventType: any[] = [];
    dateList: Date[] = [];
    IPushMessage: any[];
    showPageView: boolean = false;
    selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
    filterobject: any[] = [];
    public filteredItems: any;
    public issearch = false;
    public i: any;
    public keywords: any[];
    inputName: any = '';
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;
    EndDate: any;
    StartDate: any;
    options: InAppBrowserOptions = {
        location: 'yes',//Or 'no' 
        hidden: 'no', //Or  'yes'
        clearcache: 'yes',
        clearsessioncache: 'yes',
        zoom: 'yes',//Android only ,shows browser zoom controls 
        hardwareback: 'yes',
        mediaPlaybackRequiresUserAction: 'no',
        shouldPauseOnSuspend: 'no', //Android only 
        closebuttoncaption: 'Close', //iOS only
        disallowoverscroll: 'no', //iOS only 
        toolbar: 'yes', //iOS only 
        enableViewportScale: 'no', //iOS only 
        allowInlineMediaPlayback: 'no',//iOS only 
        presentationstyle: 'pagesheet',//iOS only 
        fullscreen: 'yes',//Windows only    
    };
    constructor(private theInAppBrowser: InAppBrowser, public navCtrl: NavController, public alertCtrl: AlertController, public menuCtrl: MenuController, public authService: AuthService) {
        this.menuCtrl.swipeEnable(true);
        this.page = 1;
        this.getEventDetails(this.page);
        //  this.getEventItinerary();
        //  this.authService.getTideDetails();
        //  this.authService.getCurrentsDetails();
        //  this.authService.getBuoyDetails();
        var v = new Date();
        this.StartDate = v;
        //this.selectedDay = v.getDate();
        this.selectedDay = ("0" + (v.getDate())).slice(-2);
        this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
        this.YearMonth = v.getFullYear();
        this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
        console.log('this.SelectDate', this.SelectDate);
        this.tmwDate = "tmw";
        this.drawerOptions = {
            handleHeight: 50,
            thresholdFromBottom: 200,
            thresholdFromTop: 200,
            bounceBack: true
        };
    }


    ionViewWillLeave() {
        this.EndDate = new Date();
        this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
        this.authService.audit_Page('WHEvent');
        //console.log('this.authService.TimeSpan', this.authService.TimeSpan);
    }


    public openWithSystemBrowser(url: string) {
        console.log('url', url);
        let target = "_blank";
        this.theInAppBrowser.create(url, target, this.options);
    }
    public openWithInAppBrowser(url: string) {
        let target = "_blank";
        this.theInAppBrowser.create(url, target, this.options);
    }
    public openWithCordovaBrowser(url: string) {
        let target = "_self";
        this.theInAppBrowser.create(url, target, this.options);
    }

    getEventItinerary() {
        this.authService.getEventItinerary().subscribe(res => {
            this.EventType = res.data;
        });
    }


    change() {
        this.issearch = true;
    }
    change1() {
        this.issearch = false;
    }

    cancelSearch() {
        this.issearch = false;
    }


    FilterByName(value: any) {

        console.log('value', value);
        this.filteredItems = [];
        if (value != '') {
            this.mainEventList.forEach(element => {
                console.log("output:" + element);
                if (element.location.toUpperCase().indexOf(value.toUpperCase()) >= 0) { this.filteredItems.push(element); }
                else if (element.title.toUpperCase().indexOf(value.toUpperCase()) >= 0) { this.filteredItems.push(element); }
                else {
                    this.mainEventFillterList = this.mainEventList;
                    // console.log('filter', this.DocAppointmentlist);
                } this.mainEventFillterList = this.filteredItems;
                // console.log('filter', this.authService.filterdAppointmentlist); 
            });
        }
        else {
            this.mainEventFillterList = this.mainEventList;
            //this.filteredItems = this.authService.filterdAppointmentlist;
        }
    }
    FilterByName1: any[] = [];
    showCheckbox() {

        this.keywords = this.EventType;

        let alert = this.alertCtrl.create({
            title: 'FILTER'
        });
        let inputArray = <Array<object>>alert.data.inputs;

        for (let i = 0; i < this.keywords.length; i++) {
            alert.addInput({
                type: 'checkbox',
                label: this.keywords[i].name.toString(),
                value: this.keywords[i].name.toString(),

                handler: () => {

                    // inputArray = <Array<object>>alert.data.inputs;
                    //console.log('Checkbox dataff:', inputArray[i]['value'])
                    let myValue = inputArray[i]['value'];


                }
            })

            //      if(this.keywords[i].name.toString()==this.filterobject){
            //          console.log('data:', this.keywords[i].name.toString());
            //          //input['value']
            //  }

        }
        inputArray.map((input) => {
            for (let k = 0; k < this.filterobject.length; k++) {
                if (input['value'] == this.filterobject[k]) {
                    input['checked'] = true;
                }
            }
            console.log('abc:', input['value']);
        })

        alert.addButton({

            text: 'Cancel',
            handler: (data: any) => {
                console.log('Checkbox data:', data);
            }

        })
        alert.addButton({

            text: 'Clear',
            handler: (data: any) => {
                this.FilterByName1 = [];
                this.filterobject = [];
                this.getEventFilterDetails();
                //  console.log('Checkbox data:', data);
            }

        })
        alert.addButton({

            text: 'Ok',
            handler: (data: any) => {
                this.FilterByName1 = data;
                this.filterobject = this.FilterByName1;
                this.getEventFilterDetails();
                console.log('Checkbox data:', data);
            }

        })


        alert.present();
    }




    ngOnInit() {
        //called after the constructor and called  after the first ngOnChanges() 
        // this.menuCtrl.enable(true, 'menu-right');    
    }



    //  getEventDetails(){

    //   var token=localStorage.getItem("token");
    //     var data={}
    //     this.authService.getEventByDate(data).subscribe(res=>{
    //       this.eventList = res;
    //       console.log('this.userDetails', this.eventList);     
    //     } ); 

    //   }

    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        setTimeout(() => {
            this.page = 1;
            this.mainEventFillterList = [];
            this.dateList = [];
            this.getEventDetails(this.page);
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }

    doInfinite(infiniteScroll) {
        if (this.page < this.totalPage) {
            console.log('Begin async operation', infiniteScroll);
            setTimeout(() => {
                this.page++;
                this.getEventDetails(this.page);
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 1000);
        }
        else {
            infiniteScroll.enabled(false);
        }
    }
    SerachString1: any;
    SerachString(value: any) {
        this.SerachString1 = value;
        this.getEventFilterDetails();
    }
    getEventFilterDetails() {
        console.log(this.SerachString1, this.FilterByName1);
        //this.dateList=[];   
        var data = {
            search: this.SerachString1,
            page: 1,
            itinerary: this.FilterByName1,
            sort: "asc",
            today_date: this.SelectDate
        }
        this.authService.getWHEventByDate(data).subscribe(res => {
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;
            this.dateList = [];
            this.mainEventFillterList = [];
            for (let d of res) {
                try {
                    //find out unique date
                    if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
                        if (this.SelectDate <= d.event_date) {
                            date.push(new Date(d.event_date).getDate());
                            this.dateList.push(d.event_date);
                        }
                    }

                    //   if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
                    //     date.push(new Date(d.event_date).getDate());
                    //     this.dateList.push(new Date(d.event_date));
                    // }
                    // this.dateList[0].getDay()
                } catch (e) { }
            }
            var seriesValues = this.dateList;
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
            console.log(seriesValues);
            this.dateList = seriesValues;
            console.log('dateList', this.dateList);
            this.dateList.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });
            //  this.mainEventList = res;
            console.log(this.mainEventList);
            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.mainEventList = this.mainEventFillterList;
            //this.EventType=res.itinerarylist;
            // this.mainEventFillterList=this.mainEventList
            console.log('mainlist', this.mainEventFillterList)
            console.log('mainlist', this.EventType);

            console.log('mainEventList', this.mainEventList);
            if (this.mainEventList.length > 0) {
                //this.currentDay = date[0];
            }
            this.filterEvent();
            // this.NextDayfilterEvent();
            //this.eventList = res;
            // console.log('getEventByDate-->', res);

        });


        // }
    }


    getEventDetails(Page) {

        //this.dateList=[];   
        var data = {
            page: Page,
            sort: "asc",
            today_date: this.SelectDate
        }
        this.authService.getWHEventByDate(data).subscribe(res => {
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;

            for (let d of res) {
                try {
                    //find out unique date
                    if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
                        if (this.SelectDate <= d.event_date) {
                            date.push(new Date(d.event_date).getDate());
                            this.dateList.push(d.event_date);
                        }
                    }

                    //   if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
                    //     date.push(new Date(d.event_date).getDate());
                    //     this.dateList.push(new Date(d.event_date));
                    // }
                    // this.dateList[0].getDay()
                } catch (e) { }
            }
            var seriesValues = this.dateList;
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
            console.log(seriesValues);
            this.dateList = seriesValues;
            console.log('dateList', this.dateList);
            this.dateList.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });
            //  this.mainEventList = res;
            console.log(this.mainEventList);
            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.mainEventList = this.mainEventFillterList;
            //this.EventType=res.itinerarylist;
            // this.mainEventFillterList=this.mainEventList
            console.log('mainlist', this.mainEventFillterList)
            console.log('mainlist', this.EventType);

            console.log('mainEventList', this.mainEventList);
            if (this.mainEventList.length > 0) {
                //this.currentDay = date[0];
            }
            this.filterEvent();
            // this.NextDayfilterEvent();
            //this.eventList = res;
            // console.log('getEventByDate-->', res);
        });
    }

    filterEvent() {
        try {
            if (this.mainEventList.length > 0) {

                // let dt: Date = new Date();
                var d = new Date();
                let today: Date = new Date(d);
                for (let mel of this.mainEventList) {
                    if (today.toDateString() == new Date(mel.event_date).toDateString())
                        this.eventList.push(mel);
                }
                console.log(this.eventList);

            }
        }
        catch (e) {
            console.log(e);
        }
    }

    NextDayfilterEvent() {
        try {
            if (this.mainEventList.length > 0) {
                //  this.eventList = [];
                // let dt: Date = new Date();
                var d = new Date();
                let tomorrow = d.getDate() + 1;
                // console.log('tomorrow',tomorrow);
                for (let mel of this.mainEventList) {
                    if (tomorrow.toString() == new Date(mel.event_date).toDateString())
                        this.TmweventList.push(mel);
                    //   console.log('tomorrow',this.TmweventList);
                }

            }
        }
        catch (e) {
            console.log(e);
        }
    }

    saveFavorite(item) {
        item.isFavorite = !item.isFavorite;

    }

    addTofavourit(value: WHEvent) {

        var data = {
            event_id: value._id,
            status: !value.favouriteStatus
        };
        this.authService.addToFavouriteWHEvent(data).subscribe(res => {
            // added as a favourite.
            console.log('res', res);
            if (res.success) {
                value.favouriteStatus = !value.favouriteStatus;
                if (!value.favouriteStatus)
                    value.likes -= 1;
                else value.likes += 1;
            }
        });
    }
    Notification() {
        this.navCtrl.push('NotificationsPage');
    }

    AddWhatsHot() {
        this.navCtrl.push('AddWhatsHotPage');
    }
    openEventDetail(item) {
        this.navCtrl.push('EventDetailPage', { event: item });
    }
}
