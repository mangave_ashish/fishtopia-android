import { WHEventPage } from './Whats-hot-page';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SharedModule } from '../../app/shared.module';


@NgModule({
  declarations: [
    WHEventPage,
  ],
  imports: [
    IonicPageModule.forChild(WHEventPage),
    SharedModule
  ],
  exports: [
    WHEventPage,
    
  ]
})

export class WHEventPageModule { }
