import { AgmCoreModule } from '@agm/core';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CommercialFishingInfoPage } from './commercial-fishing-info';

@NgModule({
  declarations: [
    CommercialFishingInfoPage,
  ],
  imports: [
    IonicPageModule.forChild(CommercialFishingInfoPage),
    AgmCoreModule.forRoot({
      // please get your own API key here:
      // https://developers.google.com/maps/documentation/javascript/get-api-key?hl=en   
      apiKey: 'AIzaSyCHvwvPHFQSagXqGQXz4bqHbOZr03_rRxo'
    }),
  ],
})
export class CommercialFishingInfoPageModule {}
