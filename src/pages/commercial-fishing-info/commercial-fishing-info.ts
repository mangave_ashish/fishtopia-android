import { Component } from '@angular/core';
import { Http } from '@angular/http';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';

/**
 * Generated class for the CommercialFishingInfoPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-commercial-fishing-info',
  templateUrl: 'commercial-fishing-info.html',
})
export class CommercialFishingInfoPage {
  lat: number = 60.361964082000028;
  lng: number = -151.429778274999999;
  ploypath: any[] = [];
  marinePins: any[] = [];
  pinCnvrt: any[] = [];
  Currents_Pin: any[] = [];
  comm_loc: any[] = [];

  ploypins: any[] = [];
  ployareascolor: any[] = ['#f44336', '#ff9800', '#e1f5fe'];
  ployareas: any[] = ['Cohoe', 'South K-beach', 'North K-beach', 'salamantof', 'East forelands', 'Ninilchik'];
  infoWindowOpened = null;

  constructor(public navCtrl: NavController, public authService: AuthService, public navParams: NavParams, public http: Http) {
    this.ploypath = [];
    //this.authService.commercial_fishing_location
    //'assets/data/ADFG_Salmon_CookInlet_Select_SetNet_Fishery_StatAreas.shp.json'
    this.http.get(this.authService.commercial_fishing_location).subscribe(data => {
      var json = JSON.parse(data.text());
      this.marinePins = json.features;
      this.marinePins.forEach(element => {
        this.Currents_Pin = [];
        element.geometry.coordinates.forEach(item => {
          this.pinCnvrt = [];
          item.forEach(pin => {
            this.pinCnvrt.push({ lng: pin[0], lat: pin[1] });
          });
          this.Currents_Pin.push(this.pinCnvrt);
        });

        this.ploypath.push({ point: this.Currents_Pin, name: element.properties.Name })
      });

      this.authService.getcommercialFishingLOcation().subscribe((result: any) => {
        if (result.success) {
          this.comm_loc = result.data
          console.log(this.comm_loc)
          this.ploypath.forEach((element, i) => {
            var daata = element.point[0]

            if (this.comm_loc.length > 0) {
              this.comm_loc.forEach(element1 => {
                // console.log(element1.location_name + " " + element.name)

                if (element1.location_code == element.name) {
                  element.color = element1.color
                  if (element1.eo_data.length > 0) {
                    element1.iseo = true
                    this.ploypins.push({ name: element1.location_name, iseo: element1.iseo, lat: daata[0].lat, lng: daata[0].lng, eodescription: element1.eo_data, iconUrl: 'assets/img/plain/green.png' })
                  } else {
                    element1.iseo = false

                    this.ploypins.push({ name: element1.location_name, iseo: element1.iseo, lat: daata[0].lat, eodescription: [], lng: daata[0].lng, iconUrl: 'null' })

                  }
                }

              });
            }

          });
          console.log(this.ploypins);

          console.log("ploypath", this.ploypath);
        }
      })


    });
  }
  MarkerDetails(marker: any, infoWindow) {
    if (this.infoWindowOpened === infoWindow)
      return;

    if (this.infoWindowOpened !== null) {
      this.infoWindowOpened.close();

    }
    this.infoWindowOpened = infoWindow;

  }
  onClick(data: any) {
    this.comm_loc.forEach(element => {
      console.log(data.name + " " + element.location_code)

      if (data.name === element.location_code) {
        this.navCtrl.push('CommercialInfoPage', {
          data: element
        })
        return false;
      }
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CommercialFishingInfoPage');
  }

}
