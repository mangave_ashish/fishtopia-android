import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShoutOutMainPage } from './ShoutOutMainPage';
import { SharedModule } from '../../app/shared.module';
// //import { File } from '@ionic-native/file';
// import { Transfer } from '@ionic-native/transfer';
// import { FilePath } from '@ionic-native/file-path';
// import { Camera } from '@ionic-native/camera';
@NgModule({
  declarations: [
    ShoutOutMainPage,
  ],
  imports: [
    IonicPageModule.forChild(ShoutOutMainPage),
    SharedModule,
    // File,
    // Transfer,
    // Camera,
    // FilePath
  ],
})
export class ShoutOutMainPageModule {}
