import { Component, ViewChild, Input } from '@angular/core';
import { Platform, ToastController, NavParams, AlertController, Loading, LoadingController, NavController, Slides, IonicPage, ActionSheetController, ModalController } from 'ionic-angular';
import { AuthService, ShoutOutActivityList } from '../../providers/auth-provider';
import { AlertService } from '../../providers/util/alert.service';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

declare var cordova: any;
@IonicPage()
@Component({
    selector: 'page-ShoutOutMainPage',
    templateUrl: 'ShoutOutMainPage.html',
})
@SideMenuDisplayText('Today\'s Catch - Photos')
export class ShoutOutMainPage {
    loading: Loading;
    users = new Array(10);
    option: any;
    slideIndex = 0;
    chatBox: any;
    isenabledlikeBox: any;
    isenabledCmtBox: any;
    color: any;
    messagepost: any = "";
    ShoutOutList: any[];
    imglist: any[];
    image_url: any;
    img: any;
    lastImage: string = null;
    imgData: any;
    postImageUrl: any;
    ShoutOutPageList: any = [];
    ImageUrllist: any = [];
    imgid: any;
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;
    expanded: any;
    contracted: any;
    showIcon = true;
    preload = true;
    EndDate: any;
    StartDate: any;
    constructor(
        public alertService: AlertService,
        public navCtrl: NavController,
        public authService: AuthService,
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public actionSheetCtrl: ActionSheetController,
        public platform: Platform,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController
    ) {

        this.StartDate = new Date();
        this.option = "Likes";
        this.chatBox = '';
        this.option = "Likes";
        // this.isenabledlikeBox=true;
        this.isenabledCmtBox = false;
        this.getActivityList(1);
        this.getActivityImgList();
    }

    ionViewWillLeave() {
        this.EndDate = new Date();
        this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
        this.authService.audit_Page('Today\'s Catch - Photos');
    }


    doRefresh(refresher) {
        //console.log('Begin async operation', refresher);

        setTimeout(() => {
            this.authService.ShoutOutPageList = [];
            this.getActivityList(1);

            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }



    doInfinite(infiniteScroll) {
        if (this.page < this.totalPage) {
            //console.log('Begin async operation', infiniteScroll);
            setTimeout(() => {
                this.page++;
                this.getActivityList(this.page);
                //console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 1000);
        }
        else {
            infiniteScroll.enabled(false);
        }
    }

    getActivityList(page) {
        //   this.ShoutOutPageList=[];   
        var token = localStorage.getItem("token");
        var data = {
            page: page,
            sort: 'desc'
        }
        console.log('data', data);
        this.authService.getShoutOutDetail(data).subscribe(res => {
            this.ShoutOutList = res;
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;
            console.log('this.userDetails', JSON.stringify(this.ShoutOutList));
            if (res.success) {
                for (let item of res.data.items.docs) {
                    if (item.user_id.image_url == null) {
                        item.user_id.image_url = 'assets/img/icon/placeholder.png';
                    }
                    item.shortDesp = this.truncate(item.description);
                    //console.log('item.shortDesp',item.shortDesp);
                    this.authService.ShoutOutPageList.push(item);
                }
                //console.log('ShoutOutPageList', this.ShoutOutPageList);
                this.getActivityImgList();
            }

        });

    }
    truncate(value: string, limit = 75, completeWords = true, ellipsis = '…') {
        let lastindex = limit;
        if (completeWords) {
            lastindex = value.substr(0, limit).lastIndexOf(' ');
        }
        return `${value.substr(0, limit)}${ellipsis}`;
    }

    getActivityImgList() {
        this.ImageUrllist = [];
        this.authService.getToImgShoutOut().subscribe(res => {
            //console.log('shoutout',res);
            if (res.success) {
                for (let item of res.data) {
                    this.ImageUrllist.push(item);
                    //console.log('item', this.ImageUrllist);
                }
            }

        });

    }




    comment(post: any) {
        this.navCtrl.push('ShoutOutDetailsCmtsPage', { post: post });
    }


    Abuse(value: ShoutOutActivityList) {
        this.alertService.presentAlertWithCallback('Report Abuse', '').then((yes) => {
            if (yes) {
                var data = {
                    activity_id: value._id
                };
                this.authService.addToShoutOutAbuse(data).subscribe(res => {
                    // added as a favourite.
                    if (res.success) {
                        this.showToast("Abuse successfully submitted.");
                    }
                    //console.log('res',res);         
                });
            }
        });
    }



    showToast(msg: any) {
        let toast = this.toastCtrl.create({
            message: msg,
            duration: 3000
        });

        toast.onDidDismiss(() => {
            //////console.log'Dismissed toast');
        });

        toast.present();
    }

    commentPage() {
        this.isenabledCmtBox = true;
        //this.isenabledlikeBox=false;

    }
    LikePage() {
        // this.isenabledlikeBox=true;
        this.isenabledCmtBox = false;

    }
    cardTapped(card) {
        // alert(card.title + ' was tapped.');
    }

    share(card) {
        // alert(card.title + ' was shared.');
    }

    listen(card) {
        // alert('Listening to ' + card.title);
    }

    favorite(card) {
        // alert(card.title + ' was favorited.');
    }



    addTofavourit(value: ShoutOutActivityList) {

        var data = {
            activity_id: value._id,
            status: !value.favouriteStatus
        };
        this.authService.addToFavouriteShoutOut(data).subscribe(res => {
            // added as a favourite.
            //console.log('res',res);
            if (res.success) {
                value.favouriteStatus = !value.favouriteStatus;
                if (!value.favouriteStatus)
                    value.likes -= 1;
                else value.likes += 1;
            }
            //let toast = this.toast.create({
            //    message: res.message,
            //    duration: 3000,
            //    position: 'bottom'
            //}).present();
            // //console.log(res);
        });
    }

    ShoutOutDetails(card: any) {
        this.navCtrl.push('ShoutOutDetailsPage', { ShoutOutDetails: card });
    }


    expand() {
        this.navCtrl.push('ShoutOutPostActivityPage');
    }
    Notification() {
        this.navCtrl.push('NotificationsPage');
    }

    sendCmmt(card: any) {
        this.navCtrl.push('ShoutOutDetailsCmtsPage', { post: card });
        //return this.openMenu = !this.openMenu;
    }

}
