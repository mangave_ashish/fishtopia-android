import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { SocialSharing } from '@ionic-native/social-sharing';
import { GuideDetailPage } from '../guide-detail/guide-detail';
import { AuthService, FishNewsList } from '../../providers/auth-provider';
import { FishNewsDetailsCmtsPage } from '../fish-news-DetailsCmts/FishNewsDetailsCmts';

/**
 * Generated class for the FishingNewsDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
    selector: 'page-fishing-news-detail',
    templateUrl: 'fishing-news-detail.html',
})
export class FishingNewsDetailPage {
    news: any;
    isenabledCmtBox: any;
    openMenu = false;

    constructor(public navCtrl: NavController, public navParams: NavParams, private socialSharing: SocialSharing, public authService: AuthService) {
        this.news = navParams.get('news');
        console.log('ionViewDidLoad FishingNewsDetailPage', this.news);
    }

    sendCmmt(news) {
        this.navCtrl.push('FishNewsDetailsCmtsPage', { news: news });
        //return this.openMenu = !this.openMenu;
    }


    ionViewDidLoad() {
        console.log('ionViewDidLoad FishingNewsDetailPage');
    }

    newsShare(news) {

        let plan_text = news.description.replace(/<\/?[^>]+>/gi, "");
        console.log('Plan', plan_text);
        this.socialSharing.share(plan_text, news.title, news.img, "").then(() => {
            // alert("Success");
        }).catch(() => {
            // alert("Error");
        });
    }

    openGuideDetail() {
        this.navCtrl.push('GuideDetailPage');
    }

    status: boolean;
    addTofavourit(value: FishNewsList) {

        if (!value.favouriteStatus) {
            this.status = true;
        } else {
            this.status = false;
        }
        var data = {
            news_id: value._id,
            status: this.status
        };
        this.authService.addToFavouriteNews(data).subscribe(res => {
            // added as a favourite.
            console.log('res', res);
            // if (!res.success) {
            value.favouriteStatus = !value.favouriteStatus;
            // if (!value.favouriteStatus)
            //     value.likes -= 1;
            // else value.likes += 1;
            // } 
            //let toast = this.toast.create({
            //    message: res.message,
            //    duration: 3000,
            //    position: 'bottom'
            //}).present();
            // console.log(res);
        });
    }

    addToLike(value: FishNewsList) {

        var data = {
            news_id: value._id,
            status: !value.LikeStatus
        };
        this.authService.addTolikeNews(data).subscribe(res => {
            // added as a favourite.
            console.log('res', res);
            // if (!res.success) {
            value.LikeStatus = !value.LikeStatus;
            if (!value.LikeStatus)
                value.likes -= 1;
            else value.likes += 1;
            //   } 
            //let toast = this.toast.create({
            //    message: res.message,
            //    duration: 3000,
            //    position: 'bottom'
            //}).present();
            // console.log(res);
        });
    }


    send(chatBox: any) {
        this.navCtrl.push('fishing-exchang-detail');
        // alert('Shop clicked.');
        // this.togglePopupMenu();
    }


}
