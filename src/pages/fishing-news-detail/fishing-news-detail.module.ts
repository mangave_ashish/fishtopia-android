import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishingNewsDetailPage } from './fishing-news-detail';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    FishingNewsDetailPage,
  ],
  imports: [
    IonicPageModule.forChild(FishingNewsDetailPage),
    SharedModule
  ],
})
export class FishingNewsDetailPageModule {}
