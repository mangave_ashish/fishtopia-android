// import { FormBuilder, FormControl, Validator } from '@angular/forms';
import { Component, ViewChild } from '@angular/core';
import { AlertController, App, LoadingController, NavController, MenuController, Slides, IonicPage } from 'ionic-angular';
import { HomePage } from '../../pages/_home/home';
import { signup } from '../../pages/sign-up/signup';
import { FabButton } from 'ionic-angular/components/fab/fab';
import { resetPasswordSuccessPage } from '../../pages/ResetPasswordSuccess/resetPasswordSuccess';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
  selector: 'page-resetPassword',
  templateUrl: 'resetPassword.html',
})
export class resetPasswordPage {
  public loginForm: any;
  // public backgroundImage = 'assets/img/background/background-6.jpg';
  isenabledEmail: any;
  isenabledPhone: any;
  ResetForm: FormGroup;
  FirstNo: AbstractControl;
  SecondNo: AbstractControl;
  ThirdNo: AbstractControl;
  ForthNo: AbstractControl;
  password: AbstractControl;
  confirmPassword: AbstractControl;
  submitAttempt: boolean = false;
  NewType = "password";
  CurrType = "password";
  ReType = "password";
  type = "password";
  show = false;
  InvalidOtp: boolean = false;
  public otp1 = [/\d/];
  public otp2 = [/\d/];
  public otp3 = [/\d/];
  public otp4 = [/\d/];
  constructor(
    public loadingCtrl: LoadingController,
    public alertCtrl: AlertController,
    public app: App,
    public navCtrl: NavController,
    public menuCtrl: MenuController,
    private formBuilder: FormBuilder,
    public authService: AuthService
  ) {
    this.isenabledEmail = true;
    this.isenabledPhone = false;
    this.ResetForm = this.formBuilder.group({
      'FirstNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
      'SecondNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
      'ThirdNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
      'ForthNo': ['', Validators.compose([Validators.required, Validators.pattern('[0-9][0-9 ]*')])],
      'password': ['', Validators.compose([Validators.required, Validators.minLength(8), Validators.maxLength(20)])],
      'confirmPassword': ['', Validators.required]
    }, { validator: this.matchingPasswords('password', 'confirmPassword') });
    this.FirstNo = this.ResetForm.controls['FirstNo'];
    this.SecondNo = this.ResetForm.controls['SecondNo'];
    this.ThirdNo = this.ResetForm.controls['ThirdNo'];
    this.ForthNo = this.ResetForm.controls['ForthNo'];
    this.password = this.ResetForm.controls['password'];
    this.confirmPassword = this.ResetForm.controls['confirmPassword'];
  }


  matchingPasswords(passwordKey: string, confirmPasswordKey: string) {
    return (group: FormGroup): { [key: string]: any } => {
      let password = group.controls[passwordKey];
      let confirmPassword = group.controls[confirmPasswordKey];
      if (password.value !== confirmPassword.value) {
        this.submitAttempt = true;
        return {
          mismatchedPasswords: true
        };
      }
      else {
        this.submitAttempt = false;
      }
    }
  }




  Email() {
    this.isenabledEmail = true;
    this.isenabledPhone = false;

  }
  Phone() {
    this.isenabledPhone = true;
    this.isenabledEmail = false;

  }

  goToSignup() {
    //this.slider.slideTo(2);
    this.navCtrl.push('signup');
  }



  presentLoading(message) {
    const loading = this.loadingCtrl.create({
      duration: 500
    });

    loading.onDidDismiss(() => {
      const alert = this.alertCtrl.create({
        title: 'Success',
        subTitle: message,
        buttons: ['Dismiss']
      });
      alert.present();
    });

    loading.present();
  }

  login() {
    //   this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('HomePage');
  }

  signup() {
    //    this.presentLoading('Thanks for signing up!');
    this.navCtrl.push('signup');
  }

  // otpverify() {
  //   var FirstNo = this.RegisterForm.controls['FirstNo'].value;
  //   var SecondNo = this.RegisterForm.controls['SecondNo'].value;
  //   var ThirdNo = this.RegisterForm.controls['ThirdNo'].value;
  //   var ForthNo = this.RegisterForm.controls['ForthNo'].value;
  //   let otpno = FirstNo + SecondNo + ThirdNo + ForthNo;
  //   var data = {
  //     otp_code: otpno
  //   }
  //   this.authService.otpVerifed(data).subscribe(res => {
  //     if (res.success) {
  //       this.resetPassword();
  //     //  this.navCtrl.push('HomePage');
  //       //   this.navCtrl.setRoot(LoginPage);

  //     }
  //   })

  // }
  eyesiconConfirm: boolean = false;
  eyesicon: boolean = false;
  toggleNewShow() {

    // if(text=="Current"){
    this.show = !this.show;
    if (this.show) {
      this.eyesicon = true;
      this.NewType = "text";
    }
    else {
      this.eyesicon = false;
      this.NewType = "password";
    }
  }
  toggleRetypeShow() {
    // if(text=="Current"){
    this.show = !this.show;
    if (this.show) {
      this.eyesiconConfirm=true;
      this.ReType = "text";
    }
    else {
      this.eyesiconConfirm=false;
      this.ReType = "password";
    }
  }

  next(el) {
    el.setFocus();
  }

  resetPassword() {
    var FirstNo = this.ResetForm.controls['FirstNo'].value;
    var SecondNo = this.ResetForm.controls['SecondNo'].value;
    var ThirdNo = this.ResetForm.controls['ThirdNo'].value;
    var ForthNo = this.ResetForm.controls['ForthNo'].value;
    let otpno = FirstNo + SecondNo + ThirdNo + ForthNo;
    var data = {
      otp_code: otpno,
      password: this.ResetForm.controls['password'].value,
    }
    this.authService.resetPassword(data).subscribe(res => {
      console.log('res', res);
      if (res.success) {
        // this.resetPassword();
        this.navCtrl.setRoot('resetPasswordSuccessPage');
      }
      else {
        this.InvalidOtp = true;
      }
    })


    //   this.presentLoading('An e-mail was sent with your new password.');

  }



}
