import { Component } from '@angular/core';
import { InAppPurchase } from '@ionic-native/in-app-purchase';
import { IonicPage, MenuController, NavController, Platform } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { AuthService } from '../../providers/auth-provider';

@IonicPage()
@Component({
    selector: 'subscribe-dialog',
    templateUrl: 'subscribe-dialog.html'
})

export class subscribeDialogPage {
    public product: any = {
        name: 'Alaska FishTopia',
        appleProductId: 'AKFISHTOPIA01',
        googleProductId: 'akfishtopiayearlysubscription'
    };
    productId: any;
    constructor(public platform: Platform,
        public navCtrl: NavController,
        public alertCtrl: AlertController,
        public menuCtrl: MenuController,
        public authService: AuthService,
        private iap: InAppPurchase
    ) {
        if (!this.platform.is('cordova')) { return };
        if (this.platform.is('ios')) {
            this.productId = this.product.appleProductId;
        } else if (this.platform.is('android')) {
            this.productId = this.product.googleProductId;
        }
    }

    getProdutIAP() {
        this.iap
            .getProducts([this.productId]).then((products: any) => {
                this.buy(products);
            })
            .catch((err) => {
                console.log('getProdutIAP Purchase' + JSON.stringify(err));
            });
    }

    buy(products: any) {
        this.iap.subscribe(products[0].productId)
            .then(data => {
                if (data.receipt != '' && data.receipt != null && data.receipt != undefined) {
                    console.log(data.receipt)
                    let subscriptiondata: any =JSON.parse(data.receipt)
                    console.log(subscriptiondata)
                    try {
                        // let tempdate = moment.unix(subscriptiondata.purchaseTime).format('dddd, MMMM, YYYY h:mm:ss A')
                        let expirationDate = new Date(subscriptiondata.purchaseTime)
                        console.log("before date add" + expirationDate)
                        expirationDate.setFullYear(expirationDate.getFullYear() + 1)
                        console.log("after date add" + expirationDate)
                        subscriptiondata.expirationDate = expirationDate
                        console.log(subscriptiondata)
                        localStorage.setItem('SubsDetails', JSON.stringify(subscriptiondata));
                        
                    } catch (er) {
                        console.log(er)
                       

                    }
                    let userid = localStorage.getItem('userId')
                    let data4 = {
                        "user": userid,
                        "subscription": true,
                        "angler_prime": false

                    }
                    console.log(data4)
                    this.authService.addpoint(data4).subscribe(res => {
                        console.log(res)
                    });
                    this.navCtrl.setRoot('dashboardPage');
                    this.authService.getProfileDetails();
                }
            }).catch((err) => {
                this.statusCheck();
                console.log('consumeError' + JSON.stringify(err));
            });
    }

    statusCheck() {
        this.iap
            .restorePurchases()
            .then((data: any) => {
                console.log('consumeError' + JSON.stringify(data));
                if (data.length > 0) {
                    for (var i = 0; i < data.length; ++i) {
                        if (data[i].productId == this.productId) {
                            this.navCtrl.setRoot('dashboardPage');
                            this.authService.getProfileDetails();
                        }
                    }
                }
            })
    }
}
