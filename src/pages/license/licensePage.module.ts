import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { licensePage } from './licensePage';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    licensePage,
  ],
  imports: [
    IonicPageModule.forChild(licensePage),
    SharedModule,
  ],
})
export class licensePageModule { }
