import { Component, ViewChild, Input } from '@angular/core';
import { Platform, ToastController, NavParams, AlertController, Loading, LoadingController, NavController, Slides, IonicPage, ActionSheetController, ModalController } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators, AbstractControl } from '@angular/forms';
import { AuthService } from '../../providers/auth-provider';
import { AlertService } from '../../providers/util/alert.service';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';
declare var cordova: any;

@IonicPage()
@Component({
    selector: 'page-licensePage',
    templateUrl: 'licensePage.html',
})

export class licensePage {
    loading: Loading;
    users = new Array(10);
    option: any;
    slideIndex = 0;
    ShoutOutPost: FormGroup;
    message: AbstractControl;
    chatBox: any;
    isenabledlikeBox: any;
    isenabledCmtBox: any;
    color: any;
    messagepost: any = "";
    imglist: any[];
    image_url: any;
    img: any;
    lastImage: string = null;
    imgData: any;
    postImageUrl: any;
    ShoutOutPageList: any = [];
    ImageUrllist: any = [];
    imgid: any;
    page = 1;
    perPage = 0;
    totalData = 0;
    totalPage = 0;
    expanded: any;
    contracted: any;
    showIcon = true;
    preload = true;

    constructor(
        public alertService: AlertService,
        public navCtrl: NavController,
        public formBuilder: FormBuilder,
        public authService: AuthService,
        public navParams: NavParams,
        public loadingCtrl: LoadingController,
        public actionSheetCtrl: ActionSheetController,
        public platform: Platform,
        public toastCtrl: ToastController,
        public alertCtrl: AlertController,
        public modalCtrl: ModalController,

        public photoViewer: PhotoViewer
    ) {
        // this.isenabledlikeBox=true;
        this.isenabledCmtBox = false;
        this.getLicenseDetails();

    }

    LicenName: any;
    DataNA: boolean;
    getLicenseDetails() {
        this.authService.getUserLicenseDetail(localStorage.getItem("token")).subscribe(res => {
            console.log('res', res)
            if (res.success == true) {
                this.image_url = res.data.image_url;
                this.LicenName = res.data.name;
            } else {
                this.DataNA = true;
                this.navCtrl.push('addLicensePage');
            }
        })
    }

    PhotoView() {
        this.photoViewer.show(this.image_url, this.LicenName, { share: false });
    }
    doRefresh(refresher) {
        //console.log('Begin async operation', refresher);

        setTimeout(() => {
            this.authService.ShoutOutPageList = [];
            //console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }
    AddLience() {
        this.navCtrl.push('addLicensePage');
    }
    Notification() {
        this.navCtrl.push('NotificationsPage');
    }
}
