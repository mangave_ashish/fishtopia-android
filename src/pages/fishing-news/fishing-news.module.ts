import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FishingNewsPage } from './fishing-news';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    FishingNewsPage,
  ],
  imports: [
    IonicPageModule.forChild(FishingNewsPage),
    SharedModule
  ],
})
export class FishingNewsPageModule { }
