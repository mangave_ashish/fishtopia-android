import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController, ActionSheetController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { SocialSharing } from '@ionic-native/social-sharing';
// Side Menu Component
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';


/**
 * Generated class for the FishingNewsPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-fishing-news',
	templateUrl: 'fishing-news.html',
})
@SideMenuDisplayText('News & Fishing Reports')
export class FishingNewsPage {
	fishingNewslist: any[] = [];
	eventList: Event[] = [];
	TmweventList: Event[] = [];
	eventSource;
	viewTitle;
	monthNames: any = ["January", "February", "March", "April", "May", "June",
		"July", "August", "September", "October", "November", "December"
	];
	isToday: boolean;
	lockSwipeToPrev: boolean;
	month: string;
	mainEventList: any[] = [];
	mainEventFillterList: any[] = [];
	LocationFilter: any;
	dateList: Date[] = [];
	IPushMessage: any[];
	showPageView: boolean = false;
	selectedDay: any;
	selectedMonth: any;
	tmwDate: any;
	YearMonth: any;
	SelectDate: any;
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	public filteredItems: any;
	public issearch = false;
	public i: any;
	public keywords: any[];
	inputName: any = '';
	EndDate: any;
	StartDate: any;
	constructor(
		public actionSheetCtrl: ActionSheetController,
		private socialSharing: SocialSharing,
		public alertCtrl: AlertController,
		public navCtrl: NavController,
		public navParams: NavParams,
		public authService: AuthService
	) {
		this.page = 1;
		this.getFishNewsList(this.page);
		var v = new Date();
		this.StartDate = v;
		//this.selectedDay = v.getDate();
		this.selectedDay = ("0" + (v.getDate())).slice(-2);
		this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
		this.YearMonth = v.getFullYear();
		this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
		// this.SelectDate=new Date(SelectDate1).toUTCString();
		console.log('this.SelectDate', this.SelectDate);
		this.tmwDate = "tmw";

	}


	ionViewWillLeave() {
		this.EndDate = new Date();
		this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		this.authService.audit_Page('News & Fishing Reports');
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}


	change() {
		this.issearch = true;
	}
	change1() {
		this.issearch = false;
	}

	cancelSearch() {
		this.inputName = '';
		this.issearch = false;
	}

	doRefresh(refresher) {
		console.log('Begin async operation', refresher);
		setTimeout(() => {
			this.page = 1;
			this.dateList = [];
			this.mainEventFillterList = [];
			this.getFishNewsList(this.page);
			console.log('Async operation has ended');
			refresher.complete();
		}, 2000);
	}

	doInfinite(infiniteScroll) {
		if (this.page < this.totalPage) {
			console.log('Begin async operation', infiniteScroll);
			setTimeout(() => {
				this.page++;
				this.getFishNewsList(this.page);
				console.log('Async operation has ended');
				infiniteScroll.complete();
			}, 1000);
		}
		else {
			infiniteScroll.enabled(false);
		}
	}





	FilterByName(value: any) {
		console.log('value', value);
		this.filteredItems = [];
		if (value != '') {
			this.mainEventList.forEach(element => {
				console.log("output:" + element);
				if (element.short_description.toUpperCase().indexOf(value.toUpperCase()) >= 0) { this.filteredItems.push(element); }
				else if (element.title.toUpperCase().indexOf(value.toUpperCase()) >= 0) { this.filteredItems.push(element); }
				else {
					this.mainEventFillterList = this.mainEventList;
					// console.log('filter', this.DocAppointmentlist);
				} this.mainEventFillterList = this.filteredItems;
				// console.log('filter', this.authService.filterdAppointmentlist); 
			});
		}
		else {
			this.mainEventFillterList = this.mainEventList;
			//this.filteredItems = this.authService.filterdAppointmentlist;
		}
	}

	showCheckbox() {

		this.keywords = this.LocationFilter;

		let alert = this.alertCtrl.create({
			title: 'FILTER'
		});
		for (let i = 0; i < this.keywords.length; i++) {

			alert.addInput({
				type: 'checkbox',
				label: this.keywords[i].toString(),
				value: i.toString(),
				handler: () => {

					let inputArray = <Array<object>>alert.data.inputs;
					let myValue = inputArray[i]['value'];

					inputArray.map((input) => {
						if (input['value'] !== myValue) {
							input['checked'] = false;
						}
					})
				}
			})
		}

		alert.addButton({

			text: 'cancel',
			handler: (data: any) => {
				console.log('Checkbox data:', data);
			}

		})
		alert.addButton({

			text: 'Ok',
			handler: (data: any) => {
				let FilterByName1 = this.keywords[data];
				this.FilterByName(FilterByName1);
				console.log('Checkbox data:', data);
			}

		})


		alert.present();
	}



	newsShare(news) {
		let plan_text = news.description.replace(/<\/?[^>]+>/gi, "");
		console.log('Plan', plan_text);
		this.socialSharing.share(plan_text, news.title, news.img, "").then(() => {
			// alert("Success");
		}).catch(() => {
			// alert("Error");
		});
	}


	getFishNewsList(Page) {

		//this.dateList=[];   
		var data = {
			page: Page,
			sort: "desc"
		}
		this.authService.getFishNews(data).subscribe(res => {
			//   let date: any[] = [];
			this.perPage = this.authService.perPage;
			this.totalData = this.authService.totalData;
			this.totalPage = this.authService.totalPage;
			console.log('dateList', res);
			for (let d of res) {
				try {
					//find out unique date
					//   if (date.indexOf(new Date(d.event_date).getDate()) == -1) {                      
					//   if(this.SelectDate <= d.date_time){
					//     date.push(new Date(d.date_time).getDate());
					this.dateList.push(d.date_time);
					//    }
					//    }

					//   if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
					//     date.push(new Date(d.event_date).getDate());
					//     this.dateList.push(new Date(d.event_date));
					// }
					// this.dateList[0].getDay()
				} catch (e) { }
			}
			var seriesValues = this.dateList;
			seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
			console.log(seriesValues);
			this.dateList = seriesValues;
			console.log('dateList', this.dateList);
			// this.dateList.sort(function (a, b) {
			// 	// Turn your strings into dates, and then subtract them
			// 	// to get a value that is either negative, positive, or zero.
			// 	if (a > b) return 1;
			// 	if (a < b) return -1;
			// 	return 0;
			// });
			//  this.mainEventList = res;
			console.log(this.mainEventList);
			for (let item of res) {
				this.mainEventFillterList.push(item);
			}
			this.mainEventList = this.mainEventFillterList;
			//this.EventType=res.itinerarylist;
			// this.mainEventFillterList=this.mainEventList
			console.log('mainlist', this.mainEventFillterList);
			console.log('mainEventList', this.mainEventList);
			if (this.mainEventList.length > 0) {
				//this.currentDay = date[0];
			}
			//	this.filterEvent();
			// this.NextDayfilterEvent();
			//this.eventList = res;
			// console.log('getEventByDate-->', res);

		});


		// }
	}
	presentActionSheet() {
		let actionSheet = this.actionSheetCtrl.create({
			title: 'Sort by',
			buttons: [
				{
					text: 'Newest',
					handler: () => {
						this.getFishNewsListfilter(1, 'desc');
					}
				},
				{
					text: 'Oldest',
					handler: () => {
						this.getFishNewsListfilter(1, 'asc');
					}
				},
				{
					text: 'Cancel',
					role: 'cancel'
				}
			]
		});
		actionSheet.present();
	}

	getFishNewsListfilter(Page: any, Order: any) {
		this.dateList = [];
		this.mainEventFillterList = [];
		var data = {
			page: Page,
			sort: 'desc'
		}
		console.log('data', data);
		this.authService.getFishNews(data).subscribe(res => {
			//   let date: any[] = [];
			this.perPage = this.authService.perPage;
			this.totalData = this.authService.totalData;
			this.totalPage = this.authService.totalPage;
			console.log('dateList', res);
			for (let d of res) {
				try {
					//find out unique date
					//   if (date.indexOf(new Date(d.event_date).getDate()) == -1) {                      
					//   if(this.SelectDate <= d.date_time){
					//     date.push(new Date(d.date_time).getDate());
					this.dateList.push(d.date_time);
					//    }
					//    }

					//   if (date.indexOf(new Date(d.event_date).getDate()) == -1) {
					//     date.push(new Date(d.event_date).getDate());
					//     this.dateList.push(new Date(d.event_date));
					// }
					// this.dateList[0].getDay()
				} catch (e) { }
			}
			var seriesValues = this.dateList;
			seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
			console.log(seriesValues);
			this.dateList = seriesValues;
			console.log('dateList', this.dateList);
			if (Order == 'asc') {
				this.dateList.sort(function (a, b) {
					// Turn your strings into dates, and then subtract them
					// to get a value that is either negative, positive, or zero.
					if (a > b) return 1;
					if (a < b) return -1;
					return 0;
				});
			}
			//  this.mainEventList = res;
			console.log(this.mainEventList);
			for (let item of res) {
				this.mainEventFillterList.push(item);
			}
			this.mainEventList = this.mainEventFillterList;
			//this.EventType=res.itinerarylist;
			// this.mainEventFillterList=this.mainEventList
			console.log('mainlist', this.mainEventFillterList);
			console.log('mainEventList', this.mainEventList);
			if (this.mainEventList.length > 0) {
				//this.currentDay = date[0];
			}
			//	this.filterEvent();
			// this.NextDayfilterEvent();
			//this.eventList = res;
			// console.log('getEventByDate-->', res);

		});


		// }
	}
	openNewsDetail(news) {
		this.navCtrl.push('FishingNewsDetailPage', { news: news });
	}
	Notification() {
		this.navCtrl.push('NotificationsPage');
	}

}
