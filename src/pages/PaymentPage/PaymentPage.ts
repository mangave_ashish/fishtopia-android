import { Component } from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { CardIO } from '@ionic-native/card-io';
import { Stripe } from '@ionic-native/stripe';
import { AlertController, IonicPage, Loading, LoadingController, ModalController, NavController, NavParams, Platform, ToastController, ViewController } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AuthService } from '../../providers/auth-provider';
declare var cordova: any;

@IonicPage()
@Component({
  selector: 'PaymentPage',
  templateUrl: 'PaymentPage.html',
})
export class PaymentPage {
  loading: Loading;
  tripItem: any;
  tripDay: string;
  tripDate: string;
  cardType: any;
  card_Number: string;
  card_Month: number;
  card_Year: number;
  card_CVV: string;
  TripAmt: any;
  payInfo: any;
  masks: any;
  StripeKey: any;
  PaymentForm: FormGroup;
  cardNumber: AbstractControl;
  cardMonth: AbstractControl;
  cardYear: AbstractControl;
  cardCVV: AbstractControl;
  public cardMMExpiry = [/\d/, /\d/,];
  public cardYYExpiry = [/\d/, /\d/,];
  public cardCVVNo = [/\d/, /\d/, /\d/];
  public mask = [/\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/, ' ', /\d/, /\d/, /\d/, /\d/];
  MemberDetails: any;
  title: any;
  errorMsg: any;
  constructor(
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public platform: Platform,
    public toastCtrl: ToastController,
    public navParams: NavParams,
    public loadingCtrl: LoadingController,
    public navCtrl: NavController,
    public stripe: Stripe,
    public cardIO: CardIO,
    public viewCtrl: ViewController,
    public menuCtrl: MenuController,
    public alertCtrl: AlertController,
    public modalCtrl: ModalController,
  ) {

    this.MemberDetails = navParams.get('MemberDetails');
    this.title = navParams.get('Title');
    console.log('MemberDetails', this.MemberDetails + this.title);
    this.UserType = localStorage.getItem("UserType");
    this.getStripeData();
    this.PaymentForm = this.formBuilder.group({
      'cardNumber': ['', Validators.compose([Validators.required])],
      'cardMonth': ['', Validators.compose([Validators.required])],
      'cardYear': ['', Validators.compose([Validators.required])],
      'cardCVV': ['', Validators.compose([Validators.required])],
    });
    // this.stripe.setPublishableKey('pk_live_cJni26iB4BdkWUDKoWlhMa63');
    // this.platform.registerBackButtonAction(() => {
    //   this.viewCtrl.dismiss();
    // });
  }

  getStripeData() {
    this.authService.getStripekey().subscribe(res => {
      if (res.success) {
        this.StripeKey = res.data.stripe_publishable_key;
        this.stripe.setPublishableKey(this.StripeKey);
        console.log('Stripekey', this.StripeKey);
      }
    });
  }
  getAllMyCard() {
    this.authService.Paymentlist = [];
    this.authService.getAllStripePost().subscribe(res => {
      if (res.data.data.length !== 0) {
        this.presentToast("Card Added Successfully.");
        this.authService.Paymentlist = res.data.data;
        this.authService.DataNA = false;
        console.log('data-->', this.authService.Paymentlist);
      } else {
        this.authService.DataNA = true;
        this.navCtrl.push('PaymentPage');
      }
    });
  }
  ionViewWillLeave() {
    this.fabGone = true;
    this.menuCtrl.swipeEnable(true);
    this.viewCtrl.dismiss();
  }
  ionViewDidLeave() {
    this.menuCtrl.swipeEnable(true);
    this.viewCtrl.dismiss();
  }


  // presentLoading(message) {
  //   const loading = this.loadingCtrl.create({
  //     duration: 500
  //   });

  //   loading.onDidDismiss(() => {
  //     const alert = this.alertCtrl.create({
  //       title: 'Success',
  //       subTitle: message,
  //       buttons: ['Dismiss']
  //     });
  //     alert.present();
  //   });

  //   loading.present();
  // }

  loaderDismiss() {
    this.loading.dismiss();
  }

  presentLoading() {
    this.loading = this.loadingCtrl.create({
      duration: 7500,
    });
    this.loading.present();
  }


  private presentToast(text) {
    let toast = this.toastCtrl.create({
      message: text,
      duration: 1500,
      position: 'bottom'
    });
    toast.present();
  }


  // ionViewDidLoad() {
  //   this.stripe.setPublishableKey('pk_test_hYPp4SIIUsem1cySzAoeaIaR');
  // }

  validateCard() {
    let loading = this.loadingCtrl.create({
      spinner: 'hide',
      content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
      duration: 3500
    });
    loading.present();
    let card = {
      number: this.PaymentForm.controls['cardNumber'].value,
      expMonth: this.PaymentForm.controls['cardMonth'].value,
      expYear: this.PaymentForm.controls['cardYear'].value,
      cvc: this.PaymentForm.controls['cardCVV'].value,
    };

    //  alert('card'+ this.cardCVV+  this.cardYear+this.cardMonth+this.cardNumber);
    // Run card validation here and then attempt to tokenise
    //  this.presentLoading();
    this.errorMsg = '';
    this.stripe.createCardToken(card)
      .then(token => {
        loading.dismiss();
        let res = JSON.stringify(token);
        this.payInfo = token.id;
        this.PayReq(loading);
      })
      .catch(error => {
        console.log('error', JSON.stringify(error));

        loading.dismiss();
        // this.presentToast(error)
        this.errorMsg = error;
        //this.presentToast(error)
      });
  }

  PayReq(loading) {
    console.log('this.payInfo', this.payInfo);
    var data = {
      strip_token: this.payInfo
    }
    this.authService.StripeSavePost(data).subscribe(res => {
      loading.dismiss();
      this.viewCtrl.dismiss();
      if (this.title == "Guide Direct" || this.title == "FishTopia Prime") {
        this.PrimeMember(res);
      } else {
        this.getAllMyCard();
      }
      // this.navCtrl.push('PaymentlistPage');
      console.log('data-->', res);
    });
  }

  card = {
    cardType: '',
    cardNumber: '',
    redactedCardNumber: '',
    expiryMonth: null,
    expiryYear: null,
    cvv: '',
    postalCode: ''
  };

  scanCard() {
    //  alert('card'+this.card);
    this.cardIO.canScan()
      .then(
        (res: boolean) => {
          if (res) {
            const options = {
              scanExpiry: true,
              hideCardIOLogo: true,
              scanInstructions: 'Please position your card inside the frame',
              keepApplicationTheme: true,
              requireCCV: true,
              requireExpiry: true,
              requirePostalCode: false
            };
            this.cardIO.scan(options).then(response => {
              console.log('Scan complete');

              const { cardType, cardNumber, redactedCardNumber,
                expiryMonth, expiryYear, cvv, postalCode } = response;


              this.cardType = cardType;
              this.card_Number = cardNumber;
              this.card_Month = expiryMonth;
              this.card_Year = expiryYear;
              this.card_CVV = cvv;


              this.card = {
                cardType,
                cardNumber,
                redactedCardNumber,
                expiryMonth,
                expiryYear,
                cvv,
                postalCode
              };

              //  alert('card'+this.card);
            });
          }
        });
  }

  // Just to animate the fab
  fabGone = false;
  ionViewWillEnter() {
    this.fabGone = false;
  }

  dismiss() {
    this.viewCtrl.dismiss();
  }

  UserType: any;
  msg: any;
  PrimeMember(item: any) {
    this.authService.getPerimeMember().subscribe(res => {
      if (res.success) {
        this.authService.getProfileDetails();
        if (this.UserType == "guide") {
          this.msg = "Welcome To Guide Direct!";
        } else if (this.UserType == "angler") {
          this.msg = "Welcome To Fishtopia Prime!"
          let userid = localStorage.getItem('userId')
          let data4 = {
            "user": userid,
            "subscription": false,
            "angler_prime": true

          }
          console.log(data4)
          this.authService.addpoint(data4).subscribe(res => {
            console.log(res)
            this.authService.getsinguserpoints(null).subscribe((result: any) => {
              console.log("PONTS", result)
              this.authService.points = result.data[0].sum_of_points
            })
          });
        }
        this.alertExtMsg(this.msg);
        console.log(res);
      }
    });
  }

  alertExtMsg(msg: any) {
    let alert = this.alertCtrl.create({
      // title: 'Confirm Trip',
      message: msg,
      buttons: [{
        text: 'Ok',
        handler: (data) => {
          if (this.UserType == "guide") {
            // this.viewCtrl.dismiss();
            //this.navCtrl.setRoot('BusinessformPage');
            //this.modalCtrl.create('BusinessformPage').present();
            this.navCtrl.push('BusinessformPage');
          } else if (this.UserType == "angler") {
            // this.viewCtrl.dismiss();
            // this.navCtrl.setRoot('PrimeOfferPage');
            //this.modalCtrl.create('PrimeOfferPage').present();
            this.navCtrl.setRoot('PrimeOfferPage');
          }
        }
      }
      ]
    });
    alert.present();
  }
  alertMsg(msg: any) {
    let alert = this.alertCtrl.create({
      // title: 'Confirm Trip',
      message: msg,
      buttons: [{
        text: 'Ok',
        handler: (data) => {
          //this.navCtrl.setRoot('HomePage');
        }
      }
      ]
    });
    alert.present();
  }

}
