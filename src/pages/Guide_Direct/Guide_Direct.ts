import { Component } from '@angular/core';
import { IonicPage, ModalController, NavController, NavParams, ToastController } from 'ionic-angular';
import { AlertController } from 'ionic-angular/components/alert/alert-controller';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AuthService } from '../../providers/auth-provider';
/**
 * Generated class for the GuideDetailPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-Guide-Direct',
	templateUrl: 'Guide_Direct.html',
})
export class Guide_DirectPage {
	guideReviewList: any;
	// firstGuideReviewItem: any;
	UserDetails: any;
	Review: any;
	userReviewDetails: any;
	firstGuideReviewItem: any;
	image_url: any;
	GuideDetailsData: any;
	userDetails: any;
	UserType: any;
	MembershipText: any;
	Title: any;
	Amount: any;
	OffersData: any[] = [];
	month: string;
    mainEventList: Event[] = [];
    mainEventFillterList: any[] = [];
    EventType: any[] = [];
    dateList: Date[] = [];
    IPushMessage: any[];
	page = 1;
    perPage = 0;
    totalData = 0;
	totalPage = 0;
	selectedDay: any;
    selectedMonth: any;
    tmwDate: any;
    YearMonth: any;
    SelectDate: any;
	constructor(public toastCtrl: ToastController, public alertCtrl: AlertController, public menuCtrl: MenuController, public modalCtrl: ModalController, public navCtrl: NavController, public navParams: NavParams, public authService: AuthService) {
		this.menuCtrl.swipeEnable(false);
		//this.authService.getProfileDetails();
		this.pageInit();
	}
	pageInit(){
		this.page = 1;
        this.getEventDetails(this.page);
        var v = new Date();
        //this.selectedDay = v.getDate();
        this.selectedDay = ("0" + (v.getDate())).slice(-2);
        this.selectedMonth = ("0" + (v.getMonth() + 1)).slice(-2);
        this.YearMonth = v.getFullYear();
        this.SelectDate = this.YearMonth + '-' + this.selectedMonth + '-' + this.selectedDay;
        console.log('this.SelectDate', this.SelectDate);
		this.UserType = localStorage.getItem("UserType");
		console.log("this.UserType", this.UserType);
		if (this.UserType == "guide") {
			this.Amount = 199;
			this.Title = "Guide Direct";
			this.MembershipText = "Upgrade to Guide Direct";
			this.alertmsg = 'Are you sure you want upgrade to Guide Direct?';
			this.GuideDetailsData = "<p>Guide Direct is a program that offers guides and businesses the opportunity to advertise open seats or open days directly in the Fishing Exchange section of the Alaska FishTopia App. We create a thorough profile of your business, expertise, and more and you can edit this later at any time.</p>" +
				"<p>It comes complete with pictures, up to 3 social media URL's, Phone number, and email contact information. </p>" +
				"<p>Once posted, anglers that have alerts turned on for the areas you post trips in will receive an alert that a new trip has been offered and they can then contact your business directly for booking.</p>";
		}
		else {
			this.Couponlist(1);
			this.Amount = 9.99;
			this.Title = "FishTopia Prime";
			this.MembershipText = "Upgrade to FishTopia Prime";
			this.alertmsg = 'Are you sure you want upgrade to FishTopia Prime?';
			this.GuideDetailsData = "<p>FishTopia Prime is an upgraded membership that gives you discounts at participating sponsors! New sponsors are constantly being added so please continue to check back often. For just $9.99 per year:</p>";
				// "<ul><li>10% Off all purchases at Ken's Alaska Tackle, Soldotna AK.</li><li>10% Off all fish processing at Jolly Wolly's Fish Processing, Soldotna AK.</li>" +
				// "<li>10% Off all boat rentals or guided trips at Alaska Boat Rental & Fishing Academy, Soldotna AK.</li><li>10% Off at Louie's Steak & Seafood, Kenai Alaska.</li></ul>";
		}

	}


	Couponlist(Page: any) {
		var data = {
			page: Page
		}
		this.authService.getCouponlistData(data).subscribe(res => {
			// this.perPage = this.authService.perPage;
			// this.totalData = this.authService.totalData;
			// this.totalPage = this.authService.totalPage;
			if (res.success) {
				for (let item of res.data.items.docs) {
					this.OffersData.push(item);
				}
			}
		});
	}

	ionViewWillLeave() {
		this.menuCtrl.swipeEnable(true);
	}
	ionViewDidLeave() {
		this.menuCtrl.swipeEnable(true);
	}
	alertmsg: any;
	PrimePayNow() {

		//Are you sure you want upgrade to Prime Membership?
		let alert = this.alertCtrl.create({
			message: this.alertmsg,
			buttons: [
				{
					text: 'Cancel',
					role: 'cancel',
					handler: () => {
						console.log('Cancel clicked');
					}
				},
				{
					text: 'Ok',
					handler: (data) => {
						var data1 = data;
						this.PrimeMember(data1);
					}
				}
			]
		});
		alert.present();
	}
	msg:any;
	PrimeMember(item: any) {
		// console.log(JSON.stringify(item));
		// console.log('item', item._id)
		// var data = {
		// 	member: item,
		// }
		this.authService.getPerimeMember().subscribe(res => {
			if (res.success) {
				this.authService.getProfileDetails();
				if (this.UserType == "guide") {
					this.msg = "Welcome To Guide Direct!";
				} else if(this.UserType == "angler"){
					this.msg = "Welcome To Fishtopia Prime!"
				}
				this.alertMsg(this.msg);
			
				console.log(res);
			} else {
				this.presentToast('Please add Payment card.');
				this.navCtrl.push('PaymentlistPage',{MemberDetails:this.UserType,Title:this.Title});
			}

		});
	}
	alertMsg(msg: any) {
		let alert = this.alertCtrl.create({
			// title: 'Confirm Trip',
			message: msg,
			buttons: [{
				text: 'Ok',
				handler: (data) => {
					if (this.UserType == "guide") {
						//this.navCtrl.setRoot('BusinessformPage');
						//this.modalCtrl.create('BusinessformPage').present();
						this.navCtrl.push('BusinessformPage');
					} else {
						//this.navCtrl.setRoot('PrimeOfferPage');
						//this.modalCtrl.create('PrimeOfferPage').present();
						this.navCtrl.push('PrimeOfferPage');
					}
				//	this.navCtrl.setRoot('HomePage');
				}
			}
			]
		});
		alert.present();
	}

	private presentToast(text) {
		let toast = this.toastCtrl.create({
			message: text,
			duration: 2500,
			position: 'bottom'
		});
		toast.present();
	}



	
    doRefresh(refresher) {
        console.log('Begin async operation', refresher);
        setTimeout(() => {
            this.page = 1;
            this.mainEventFillterList = [];
            this.dateList = [];
            this.getEventDetails(this.page);
            console.log('Async operation has ended');
            refresher.complete();
        }, 2000);
    }

    doInfinite(infiniteScroll) {
        if (this.page < this.totalPage) {
            console.log('Begin async operation', infiniteScroll);
            setTimeout(() => {
                this.page++;
                this.getEventDetails(this.page);
                console.log('Async operation has ended');
                infiniteScroll.complete();
            }, 1000);
        }
        else {
            infiniteScroll.enabled(false);
        }
	}
	
	getEventDetails(Page) {
        //this.dateList=[];   
        var data = {
            page: Page,
            sort: "asc",
        }
        this.authService.getCouponlistData(data).subscribe(res => {
            let date: any[] = [];
            this.perPage = this.authService.perPage;
            this.totalData = this.authService.totalData;
            this.totalPage = this.authService.totalPage;
            for (let d of res) {
                try {
                    if (this.SelectDate <= d.coupon_expiry_date) {
                        this.dateList.push(d.coupon_expiry_date);
                    }
                } catch (e) { }
            }
            var seriesValues = this.dateList;
            seriesValues = seriesValues.filter((value, index, seriesValues) => (seriesValues.slice(0, index)).indexOf(value) === -1);
            console.log(seriesValues);
            this.dateList = seriesValues;
            console.log('dateList', this.dateList);
            this.dateList.sort(function (a, b) {
                // Turn your strings into dates, and then subtract them
                // to get a value that is either negative, positive, or zero.
                if (a > b) return 1;
                if (a < b) return -1;
                return 0;
            });
            //  this.mainEventList = res;
            console.log(this.mainEventList);
            for (let item of res) {
                this.mainEventFillterList.push(item);
            }
            this.mainEventList = this.mainEventFillterList;
            console.log('mainlist', this.mainEventFillterList)
            console.log('mainEventList', this.mainEventList);
            if (this.mainEventList.length > 0) {
                //this.currentDay = date[0];
            }
         //   this.filterEvent();
            // this.NextDayfilterEvent();
            //this.eventList = res;
            // console.log('getEventByDate-->', res);

        });


        // }
    }


}
