import { NgModule,CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { Guide_DirectPage } from './Guide_Direct';
import { SharedModule } from '../../app/shared.module';

@NgModule({
  declarations: [
    Guide_DirectPage,
  ],
  imports: [
    IonicPageModule.forChild(Guide_DirectPage),
    SharedModule
  ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class GuideDirectPageModule {}
