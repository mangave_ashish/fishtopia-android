import { regulationsList } from './regulations';
import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';

@NgModule({
  declarations: [
    regulationsList,
  ],
  imports: [
    IonicPageModule.forChild(regulationsList),
  ],
  exports: [
    regulationsList
  ]
})

export class regulationsListPageModule { }
