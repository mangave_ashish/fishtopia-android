import { Component, Input } from '@angular/core';
import { File } from '@ionic-native/file';
import { Transfer, TransferObject } from '@ionic-native/transfer';
import { IonicPage, NavController, Platform } from 'ionic-angular';
import { MenuController } from 'ionic-angular/components/app/menu-controller';
import { AuthService } from '../../providers/auth-provider';
import { DatabaseService, ForecastService, UtilService } from '../../providers/weather';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';

@IonicPage()
@Component({
  selector: 'regulations',
  templateUrl: 'regulations.html'
})

@SideMenuDisplayText('Regulations')
export class regulationsList {
  @Input('progress') progress: number = 0;
  rootPage: any;
  items: Array<{ title: string, page: any }>;
  AppVersion: any;
  AppName: any;
  expand: boolean = false;
  loadedPdf: any;
  totalDwn: any;
  progressBarEnabled: boolean = false;
  storageDirectory: any;
  checkProgress: boolean = false;
  existPDF: boolean = false;
  constructor(
    public menuCtrl: MenuController,
    public navCtrl: NavController,
    public authService: AuthService,
    public platform: Platform,
    private transfer: Transfer,
    private file: File,
    public forecastService: ForecastService,
    public databaseService: DatabaseService,
    public utilService: UtilService,
  ) {
    this.platform.ready().then(() => {
      // make sure this is on a device, not an emulation (e.g. chrome tools device mode)
      if (!this.platform.is('cordova')) {
        return false;
      }

      if (this.platform.is('ios')) {
        this.storageDirectory = this.file.documentsDirectory;
        this.checkExist();
      }
      else if (this.platform.is('android')) {
        this.storageDirectory = this.file.externalDataDirectory + 'Download/';
        this.checkExist();
      }
      else {
        // exit otherwise, but you could add further types here e.g. Windows
        return false;
      }
    });
  }

  pdfview(area: any) {
    var pdfData;
    this.authService.locationlist.forEach(Item => {
      console.log('area', Item);
      if (area == Item.displayText) {
        Item.expand = !Item.expand;
      } else if (area == Item.displayText) {
        Item.expand = !Item.expand;
      } else if (area == Item.displayText) {
        Item.expand = !Item.expand;
      } else if (area == Item.displayText) {
        Item.expand = !Item.expand;
      } else {
        Item.expand = false;
      }
    });
    console.log('area', area, pdfData);
  }


  pdfUrl: any;
  areaView(event: string, area: any, e: any, group: any) {
    group.sub_Options.forEach(Item => {
      console.log('area', Item);
      if (area.displayText == Item.displayText) {
        Item.progrBarEnabled = true;
      } else {
        Item.progrBarEnabled = false;
      }
    });
    if (area.progrBarEnabled) {
      this.pdfUrl = area;
      this.checkFile();
    }
  }




  Notification() {
    this.navCtrl.push('NotificationsPage');
  }


  toggleGroup(group: any) {
    group.expand = !group.expand;
  }

  isGroupShown(group: any) {
    return group.expand;
  }

  checkExist() {
    this.authService.locationlist.forEach(Item => {
      Item.sub_Options.forEach(imgItem => {
        this.file.checkFile(this.storageDirectory, imgItem.displayText + '.jpg')
          .then((result) => {
            if (result) {
              imgItem.checkProgress = true;
            } else {
              imgItem.checkProgress = false;
            }
          }, (error) => {
            console.log('error : ' + JSON.stringify(error));
          });
      })
    })
  }

  checkFile() {
    this.file.checkFile(this.storageDirectory, this.pdfUrl.displayText + '.jpg')
      .then((result) => {
        if (result) {
         // alert('result1 : ' + JSON.stringify(result))

          this.navCtrl.push('pdfViewpage', { data: this.pdfUrl });
        }
      }, (error) => {
       // alert('error1 : ' + JSON.stringify(error))

        this.progressBarEnabled = true;
        this.downloadPDF();
        console.log('error : ' + JSON.stringify(error));
      });
  }

  base64: any;
  downloadPDF() {
    // this.progressBarEnabled = true;
    this.progress = 0;
    this.platform.ready().then(() => {
      const fileTransfer: TransferObject = this.transfer.create();
      fileTransfer.download(this.pdfUrl.pdfURL, this.storageDirectory + this.pdfUrl.displayText + '.jpg').then((entry) => {
      }, (error) => {
      });
      let a = fileTransfer.onProgress((progressEvent: ProgressEvent) => {
        console.log(JSON.stringify(progressEvent) + JSON.stringify(a));
        if (progressEvent.lengthComputable) {
          this.loadedPdf = progressEvent.loaded;
          this.totalDwn = ' / ' + progressEvent.total + 'KB';
          this.progress = Math.floor(progressEvent.loaded / progressEvent.total * 100);
          console.log(JSON.stringify(this.progress));
          if (this.progress < 100) {
            this.progress += 1;
          } else {
            // this.progress = 0;
            this.pdfUrl.checkProgress = true;
            clearInterval(this.progress);
            this.progressBarEnabled = false;
            this.checkFile();
          }
        } else {
          console.log(JSON.stringify(this.progress));

        }
      });
    });
  }
}


