import { Component, Output } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import { AuthService } from '../../providers/auth-provider';
import { SideMenuDisplayText } from '../../shared/side-menu-content/custom-decorators/side-menu-display-text.decorator';
import * as _ from 'lodash';

/**
 * Generated class for the TidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-USGSFlowsPage',
	templateUrl: 'USGSFlowsPage.html',
})
@SideMenuDisplayText('River Flows')
export class USGSFlowsPage {
	waterLocList: any[];
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	FavLocationList: any[] = [];
	FishCountOption: any;
	tabType: any;
	status: boolean;
	GeoLat: any;
	GeoLong: any;
	lat: number = 61.217381;
	lng: number = -149.863129;
	image_url = "assets/img/salamanfish.jpg";
	StartDate: any;
	EndDate: any;
	constructor(public navCtrl: NavController,
		public navParams: NavParams,
		public authService: AuthService,
		public loadingCtrl: LoadingController
	) {
		this.tabType = "FavLocation";
		this.getGeolocationPosition();
		this.StartDate = new Date();
		console.log('this.waterLocList', this.waterLocList);

	}
	// onLinkClick(event: any) {
	// 	console.log('event', event.tab.textLabel);
	// 	this.waterLocList = [];
	// 	this.page = 1;
	// 	if (event.tab.textLabel == "All Stations") {
	// 		this.getwaterFlowsloclist(this.page);
	// 	} else if (event.tab.textLabel == "Favorite") {
	// 		this.getwaterFlowFavlist(this.page);
	// 		//this.waterLocList = this.FavLocationList;
	// 	}
	// }



	ionViewWillLeave() {
		this.EndDate = new Date();
		this.authService.TimeSpan = this.EndDate.getTime() - this.StartDate.getTime();
		this.authService.audit_Page('Fish Counts');
		//console.log('this.authService.TimeSpan', this.authService.TimeSpan);
	}



	getGeolocationPosition() {
		let self = this;
		self.GeoLat = localStorage.getItem('GeoLat');
		self.GeoLong = localStorage.getItem('GeoLong');
		let locationData = localStorage.getItem('GeoLocation');
		let Sub_Location = localStorage.getItem('SubGeoLocation');
		console.log('Geolat');
		if (locationData === "Alaska") {
			self.lat = self.GeoLat;
			self.lng = self.GeoLat;
			// self.zoom = 8;
			self.getwaterFlowFavlist(1);
		} else {
			self.lat = 61.217381;
			self.lng = -149.863129;
			self.getwaterFlowFavlist(1);
		}
	}
	arrayLength: number;
	Output: any[] = [];


	getwaterFlowFavlist(page_no: any) {
		let self = this;
		self.waterLocList = [];
		let loading = this.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 3000
		});
		loading.present();
		var token = localStorage.getItem("token");
		var data = {
			page: page_no,
			latitude: self.GeoLat,
			longitude: self.GeoLong,
			sort: "asc"
		}
		self.authService.getwaterFlowFavlist(data).subscribe(res => {
			console.log('data', res);
			let obj = res.result;
			self.perPage = self.authService.perPage;
			self.totalData = self.authService.totalData;
			self.totalPage = self.authService.totalPage;
			obj.forEach(item => {
				item.favouriteStatus = true;
				item.water_temp = Math.round(JSON.parse(item.water_temp) * 100) / 100;
				//JSON.parse(item.water_temp);
				item.air_temp = Math.round(JSON.parse(item.air_temp) * 100) / 100;
				//JSON.parse(item.air_temp);
				item.gage_height = Math.round(JSON.parse(item.gage_height) * 100) / 100;
				// JSON.parse(item.gage_height);
				item.discharge = Math.round(JSON.parse(item.discharge) * 100) / 100;
				// JSON.parse(item.discharge);

				if (item.water_temp < 1 || item.water_temp == null) {
					item.water_temp = 0;
				}

				if (item.air_temp < 1 || item.air_temp == null) {
					item.air_temp = 0;
				}

				if (item.discharge < 1 || item.discharge == null) {
					item.discharge = 0;

				}

				if (item.gage_height < 1 || item.gage_height == null) {
					item.gage_height = 0;

				}

				self.waterLocList.push(item);
			})
			// loading.dismiss();
			console.log('getfishingcountlocation', self.waterLocList);
		});
	}






	getwaterFlowsloclist(page_no: any) {
		let self = this;
		self.waterLocList = [];
		let loading = self.loadingCtrl.create({
			spinner: 'hide',
			content: `<img width="180px"  height="180px" src="assets/img/GIF.gif" /><div>Fishing for data. Please wait...</div>`,
			duration: 2500
		});
		loading.present();
		var token = localStorage.getItem("token");
		var data = {
			page: page_no,
			latitude: self.GeoLat,
			longitude: self.GeoLong,
			sort: "asc"
		}
		self.authService.getwaterlocation(data).subscribe(res => {
			let obj = res.result;
			var id = localStorage.getItem('userId');
			obj.forEach(item => {
				item.water_temp = Math.round(JSON.parse(item.water_temp) * 100) / 100;
				//JSON.parse(item.water_temp);
				item.air_temp = Math.round(JSON.parse(item.air_temp) * 100) / 100;
				//JSON.parse(item.air_temp);
				item.gage_height = Math.round(JSON.parse(item.gage_height) * 100) / 100;
				// JSON.parse(item.gage_height);
				item.discharge = Math.round(JSON.parse(item.discharge) * 100) / 100;
				// JSON.parse(item.discharge);

				if (item.water_temp < 1 || item.water_temp == null) {
					item.water_temp = 0;
				}

				if (item.air_temp < 1 || item.air_temp == null) {
					item.air_temp = 0;
				}

				if (item.discharge < 1 || item.discharge == null) {
					item.discharge = 0;

				}

				if (item.gage_height < 1 || item.gage_height == null) {
					item.gage_height = 0;

				}
				for (let fav of item.favourites) {
					if (fav.user_id == id) {
						item.favouriteStatus = true;
					} else {
						item.favouriteStatus = false;
					}
				}
				self.FavLocationList = item.favourites
				self.waterLocList.push(item);
			})
			console.log('self.waterLocList', self.waterLocList);
			// loading.dismiss();
		});
	}
	doRefresh(refresher) {
		console.log('Begin async operation', refresher);
		setTimeout(() => {
			this.page = 1;
			this.waterLocList = [];
			if (this.tabType == "AllLocation") {
				this.getwaterFlowsloclist(this.page);
			} else if (this.tabType == "FavLocation") {
				this.getwaterFlowFavlist(1);
				//this.waterLocList = this.FavLocationList;
			}
			console.log('Async operation has ended');
			refresher.complete();
		}, 2000);
	}

	doInfinite(infiniteScroll) {
		//if (this.tabType == "AllLocation") {
		if (this.page < this.totalPage) {
			console.log('Begin async operation', infiniteScroll);
			setTimeout(() => {
				this.page++;
				if (this.tabType == 'FavLocation') {
					this.getwaterFlowsloclist(this.page);
				} else {
					this.getwaterFlowsloclist(this.page);
				}
				console.log('Async operation has ended');
				infiniteScroll.complete();
			}, 1000);
		}
		else {
			infiniteScroll.enabled(false);
		}
	}


	openAreaDetail(area) {
		console.log('area', area);
		this.navCtrl.push('USGS_DetailsPage', { USGS_Details: area });
	}
	Notification() {
		this.navCtrl.push('NotificationsPage');
	}

	addTofavouriteLocList(value: any) {
		// console.log(value);
		if (!value.favouriteStatus) {
			this.status = true;
		} else {
			this.status = false;
		}
		var data = {
			stationNumber: value.stationNumber,
			status: this.status
		};
		value.favouriteStatus = !value.favouriteStatus;
		this.authService.addToWaterFlowFav(data).subscribe(res => {
			console.log('res', res);
			//  this.tabType = "FavLocation";
			//  this.getwaterFlowFavlist(1);
			// added as a favourite.
			//this.getwaterFlowsloclist(1);
		});
	}

	addTofavouriteList(value: any) {
		console.log(value);
		this.waterLocList = [];
		if (!value.favouriteStatus) {
			this.status = true;
		} else {
			this.status = false;
		}
		var data = {
			stationNumber: value.stationNumber,
			status: this.status
		};
		value.favouriteStatus = !value.favouriteStatus;
		this.authService.addToWaterFlowFav(data).subscribe(res => {
			// added as a favourite.
			console.log('res', res);
			if (res.success) {
				//this.getwaterFlowFavlist(1);
				console.log('value', value);
				// var i = this.waterLocList.indexOf(value._id);
				// console.log('id', i)
				// this.waterLocList.splice(i, 1);
			}
		});
	}

	btnSegmentCall(data: any) {
		this.waterLocList = [];
		this.page = 1;
		this.tabType = data;
		if (data == "AllLocation") {
			this.getwaterFlowsloclist(this.page);
		} else if (data == "FavLocation") {
			this.getwaterFlowFavlist(this.page);
			//this.waterLocList = this.FavLocationList;
		}

	}
}
