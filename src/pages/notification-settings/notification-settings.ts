import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
//import { FishCountChartPage } from '../FishCountChart/FishCountChart';
import { AuthService } from '../../providers/auth-provider';
/**
 * Generated class for the TidesPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
	selector: 'page-notification-settings',
	templateUrl: 'notification-settings.html',
})
export class notificationsettingsPage {
	FishCountLocList: any[] = [];
	page = 1;
	perPage = 0;
	totalData = 0;
	totalPage = 0;
	data: any[] = [];
	enableNotifications: boolean = false;
	fishtopianotify: boolean = true;
	OffNotify: any[] = [];
	OffNotifyStatus: any;
	toast: any;
	constructor(private toastCtrl: ToastController, public navCtrl: NavController, public navParams: NavParams, public authService: AuthService) {
		this.getNotifylist();
	}

	onEvent(event: string, item: any, e: any) {

	}

	toggleGroup(group: any) {
		// console.log('group', group)
		group.expand = !group.expand;
	}

	isGroupShown(group: any) {
		// console.log('group', group)
		return group.expand;
	}

	ionViewDidLoad() {
		// console.log('ionViewDidLoad TidesPage');
	}

	doRefresh(refresher) {
		// console.log('Begin async operation', refresher);
		setTimeout(() => {
			this.page = 1;
			this.FishCountLocList = [];
			//this.getNotifylist(this.page);
			// console.log('Async operation has ended');
			refresher.complete();
		}, 2000);
	}

	doInfinite(infiniteScroll) {
		if (this.page < this.totalPage) {
			// console.log('Begin async operation', infiniteScroll);
			setTimeout(() => {
				this.page++;
				//this.getNotifylist(this.page);
				// console.log('Async operation has ended');
				infiniteScroll.complete();
			}, 1000);
		}
		else {
			infiniteScroll.enabled(false);
		}
	}

	cities: any[] = [];
	Sub_cities: any[] = [];
	getNotifylist() {
		//	var token = localStorage.getItem("token");
		// var data = {
		// 	page: page_no
		// }
		this.authService.getNotificationSettings().subscribe(res => {
			//	console.log('item.data.notificationSettingsData.sub_notifications', JSON.stringify(res.data.notificationSettingsData));
			for (let item of res.data.notificationSettingsData) {
				item.expand = false;
				if (item.status === "yes") {
					item.show = true;
				} else {
					item.show = false;
				}
				if (item.sub_notifications !== undefined && item.sub_notifications !== null) {
					this.Sub_cities = [];
					//	console.log('item.data.notificationSettingsData.sub_notifications', JSON.stringify(item.sub_notifications));
					for (let Sub_item of item.sub_notifications) {
						if (Sub_item.status === "yes") {
							Sub_item.show = true;
						} else {
							Sub_item.show = false;
						}
						if (Sub_item.cities !== undefined && Sub_item.cities.length > 0) {
							//console.log('Sub_item.cities', Sub_item.cities);
							for (let cityItem of Sub_item.cities) {
								if (cityItem.name !== undefined) {
									this.Sub_cities.push(cityItem.name);
								}
							}
							Sub_item.cityArray = this.Sub_cities;
						}
					}
				}
				this.data.push(item);
			}
			console.log('this.data', this.data);
			//this.data = res.data.notificationSettingsData;
		});
	}

	// toggleGrpNotifications(group: any, event: any) {
	// 	console.log("data", group.show);
	// 	this.check = group.show;
	// 	if (group.show == false) {
	// 		this.OffNotifyStatus = "yes";
	// 		this.OffNotify = [];
	// 	} else {
	// 		this.OffNotifyStatus = "no";
	// 	}
	// 	var data = {
	// 		notificationSetting: [
	// 			{
	// 				_id: group._id,
	// 				status: this.OffNotifyStatus,
	// 				off_notifications: this.OffNotify
	// 			}
	// 		]
	// 	}
	// 	console.log('data', data);
	// 	this.authService.saveNotificationSettings(data).subscribe(res => {
	// 		this.data = [];
	// 		this.getNotifylist();
	// 	})
	// 	if (this.enableNotifications) {
	// 		this.toastShow('Notifications enabled.');
	// 	} else {
	// 		this.toastShow('Notifications disabled.');
	// 	}
	// }

	check: boolean = false;
	toggleNotifications(group: any, item: any) {
		this.OffNotify = [];
		this.OffNotify = this.removeDups(group.off_notifications);
		group.off_notifications = this.OffNotify;
		if (group.show == false && group.status == "no" && item._id !== undefined) {
			this.OffNotifyStatus = "yes";
			group.show = true;
			group.status = "yes";
			for (let itemSub of group.sub_notifications) {
				if (item._id != itemSub._id) {
					this.OffNotify.push(itemSub._id);
				}
			}
		} else if (group.show == true && group.status == "yes" && item._id !== undefined) {
			this.OffNotifyStatus = group.status;
			if (group.off_notifications.length > 0) {
				console.log('this.OffNotify', this.OffNotify);
				for (let itemOff of group.off_notifications) {
					if (item._id != itemOff && item.show == true) {
						this.OffNotify.push(item._id);
					} else if (item._id == itemOff && item.show == false) {
						var i = this.OffNotify.indexOf(item._id);
						this.OffNotify.splice(i, 1);
					}
				}
			} else {
				this.OffNotify.push(item._id);
			}
		} else if (group.show == true && group.status == "yes" && item._id === undefined) {
			this.OffNotifyStatus = "no";
			group.show = false;
			group.status = "no";
			this.check = true;
			this.OffNotify = [];
		} else if (group.show == false && group.status == "no" && item._id === undefined) {
			this.OffNotifyStatus = "yes";
			this.check = true;
			this.OffNotify = [];
		}
		this.OffNotify = this.removeDups(this.OffNotify);
		group.off_notifications = this.OffNotify;
		if (group.off_notifications.length == group.sub_notifications.length) {
			this.OffNotifyStatus = "no";
			group.show = false;
			group.status = "no";
			this.check = true;
			this.OffNotify = [];
		}


		var data = {
			notificationSetting: [
				{
					_id: group._id,
					status: this.OffNotifyStatus,
					off_notifications: this.OffNotify
				}
			]
		}
		console.log('data', data);
		this.authService.saveNotificationSettings(data).subscribe(res => {
			if (this.check == true) {
				this.data = [];
				this.getNotifylist();
				this.check = false;
			}
			this.toastShow('Notification settings updated successfully.');
		})
		// if (this.enableNotifications) {
		// 	this.toastShow('Notification setting updated successfully.');
		// } else {
		// 	this.toastShow('Notifications disabled.');
		// }
	}

	removeDups(names: any) {
		let unique_array = []
		for (let i = 0; i < names.length; i++) {
			if (unique_array.indexOf(names[i]) == -1) {
				unique_array.push(names[i])
			}
		}
		return unique_array
	}


	toastShow(msg: any) {
		this.toast = this.toastCtrl.create({
			message: msg,
			position: 'bottom',
			duration: 2500
		});
		this.toast.present();
	}
	openAreaDetail(area) {
		// console.log('area', area);
		this.navCtrl.push('FishCountChartPage', { area: area });
	}

}
