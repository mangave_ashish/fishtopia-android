import { Injectable } from '@angular/core';
import { Headers, Http, RequestOptions } from '@angular/http';
import { Storage } from '@ionic/storage';
import { PopoverController } from 'ionic-angular';
import * as _ from 'lodash';
import moment from 'moment';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs/Observable';
import { DatabaseService } from './weather';
@Injectable()
export class AuthService {
    public apiLink = 'http://166.62.118.179:2152/api/';//Live
  // public apiLink = 'http://166.62.118.179:2156/api/';//Test
    private auditUrl = this.apiLink + 'audit/add-audit-data';
    private loginUrl = this.apiLink + 'users/login-after-subscription';

   // private loginUrl = this.apiLink + 'users/login';
    private signupUrl = this.apiLink + 'users/signup';
    private logoutUrl = this.apiLink + 'users/logout';
    private otpverify = this.apiLink + 'users/verify-account';
    private forgotPwd = this.apiLink + 'users/forgot-password';
    private resetPwd = this.apiLink + 'users/reset-password';
    private logOutUrl = this.apiLink + 'users/logout';
    private changePwdUrl = this.apiLink + 'users/change-password';
    private getProfileDetailsUrl = this.apiLink + 'users/me';
    private updateUsersProfileUrl = this.apiLink + 'users/update-profile';
    private addUserLicenseeUrl = this.apiLink + 'users/save-fishing-license';
    private getuserLicenseeUrl = this.apiLink + 'users/get-fishing-license';
    private getuserCSV = this.apiLink + 'admin/exportcsv_users';
    private adduserDetailsLicenseeUrl = this.apiLink + 'users/save-fishing-license-details';
    private getuserLocationUrl = this.apiLink + 'users/update-current-location';
    private getItineraryUrl = this.apiLink + 'event/itinerary';
    // private getEventSearchUrl=this.apiLink + 'users/update-profile';
    private eventByDateUrl = this.apiLink + "event/get-event";
    private addFavouriteEventUrl = this.apiLink + "event/add-to-favourite";
    private addFavouriteUrl = this.apiLink + "news/add-to-favourite";
    private addLikeUrl = this.apiLink + "news/like";
    private getNewsUrl = this.apiLink + "news/get-news";
    private addCmtNewsUrl = this.apiLink + "news/add-comment";
    private getCmtNewsUrl = this.apiLink + "news/";
    private getboattypesUrl = this.apiLink + "masters/get-boat-types";
    private getfishingtypesUrl = this.apiLink + "masters/get-fishing-types";
    private getfishinglocationsUrl = this.apiLink + "masters/get-fishing-locations";
    private addRequestTripUrl = this.apiLink + "trip/add-trip";
    private getFishingExchngeDetailUrl = this.apiLink + 'trip/get-trip';
    private addfilterTripUrl = this.apiLink + "trip/get-filtered-trips-data";
    private getUserDetailsUrl = this.apiLink + 'admin/get_user_detail/';
    private addImgShoutoutActivityUrl = this.apiLink + "activity/get-pending-image";
    private addShoutoutActivityUrl = this.apiLink + "activity/add-activity";
    private getShoutoutActivityUrl = this.apiLink + "activity/get-activity";
    private getCmtShoutoutUrl = this.apiLink + "activity/";
    private addShoutoutToCmtUrl = this.apiLink + "activity/comment";
    private addShoutoutAddToFavUrl = this.apiLink + "activity/favourite";
    //private  getShoutoutAddAbuse=this.apiLink+"/activity/abuse";
    private addShoutoutAddToAbuseUrl = this.apiLink + "activity/abuse";
    private addFishCountFavUrl = this.apiLink + "fish-count/add-favourite-to-count-location";
    private getfishingcountlocationUrl = this.apiLink + "fish-count/get-fishing-count-location";
    private getFishCountUrl = this.apiLink + "fish-count/get-fishing-count/";
    private getfishCountsFavUrl = this.apiLink + "fish-count/get-fishing-count-location-fav"
    private addTripStatusUrl = this.apiLink + "trip/";
    private getTidesUrl = this.apiLink + "admin/get-all-tide-location";
    private getTidesStationDetailsUrl = this.apiLink + "admin/load-tide-location/";
    private getTidesStationMinDetailsUrl = this.apiLink + "admin/get-Tide-Details-Per-Minwith-Date-Filter";
    private getCurrentsUrl = this.apiLink + "admin/get-all-current-location";
    private getCurrentsDetailsUrl = this.apiLink + "admin/get-current-location/";
    //private gettotalData = this.apiLink + "admin/get-fishing-month-data-by-year-location/";
    private gettotalData = this.apiLink + "admin/get-fishing-average-count-data-by-location-year/";
    private getBuoyInfoUrl = this.apiLink + "admin/get-all-bouy-location";
    private getBuoyStationDetailsUrl = this.apiLink + "admin/get-bouy-location/";
    private StripeAnglarUrl = this.apiLink + "trip/"; //5ad097cc4227293567124455/angler/pay
    private StripeGuideReqUrl = this.apiLink + "trip/";//5ad097cc4227293567124455/angler/pay
    private StripeSaveReqUrl = this.apiLink + "users/save-card";
    private StripeDeleteReqUrl = this.apiLink + "users/card/";
    private getStripelistReqUrl = this.apiLink + "users/my-card";
    private addReviewUrl = this.apiLink + "review/submit-review";
    private getReviewUrl = this.apiLink + "review/";
    private getReviewAllUrl = this.apiLink + "review/get-reviews";
    //    private  getTidesUrl=this.apiLink+"admin/get-all-tide-location";
    private getMarineWeatherListUrl = this.apiLink + "admin/get-all-marine-weather-zones";
    private getMarineWeatherLocListUrl = this.apiLink + "admin/get-all-marine-weather-Location";
    private getMarineWeatherDetailsUrl = this.apiLink + "admin/get-marine-weather-zone-details/";
    private getwhEventUrl = this.apiLink + "whevent/get-whevent";
    private getwhEventDetailsUrl = this.apiLink + "whevent/";
    private getwhEventFavUrl = this.apiLink + "whevent/add-to-favourite";
    private getStripekeyUrl = this.apiLink + "admin/get-config";
    private upgradeToPrimeUrl = this.apiLink + "users/upgrade-user";
    private deleteuserDetailsLicenseeUrl = this.apiLink + 'users/delete-Fishing-License';

    private postBusinessDetailsUrl = this.apiLink + "users/save-business-details";
    // private getCouponlistUrl = this.apiLink + "admin/get-coupon-codes";
    private getCouponlistUrl = this.apiLink + "coupon/get-coupon";
    private getCouponDetailsUrl = this.apiLink + "coupon/get-coupon";
    private getNotificationsSetUrl = this.apiLink + "users/get-notification-settings";
    private getNotificationsUrl = this.apiLink + "notification/get-notifications";
    private getNotificationbyIdUrl = this.apiLink + "notification/";
    // private getNotificationReadUrl = this.apiLink + "notification/";
    //private getNotificationReadAllUrl = this.apiLink + "notification/read-all";
    private getNotificationReadAllUrl = this.apiLink + "notification/seen-all-notifications";
    private NotifyDeleteUrl = this.apiLink + "notification/delete/";
    private NotifyUpdatetokenUrl = this.apiLink + "users/update-device-id";
    private NotificationSettingsUrl = this.apiLink + "users/save-notification-settings";
    private resendOTPUrl = this.apiLink + "users/resend-verification-code";
    private googleAccess_tokenUrl = this.apiLink + "admin/createUserforGoogleorFacebook";
    private GoogleandFacebook_tokenUrl = this.apiLink + "users/signupUserTokenforGoogleandFacebook";
    private getWaterFlowUrl = this.apiLink + "admin/get-waterflows";
    private getWaterFlowDetailsUrl = this.apiLink + "admin/get-waterflowsDetails";
    private getWaterFlowDsGraphDetailsUrl = this.apiLink + "admin/get-Graph-Data-for-Discharge";
    private getWaterFlowGHGraphDetailsUrl = this.apiLink + "admin/get-Graph-Data-for-Gage";
    private addWaterFlowFavUrl = this.apiLink + "admin/add-water-flows-favorite";
    private getWaterFlowFavUrl = this.apiLink + "admin/get-water-flow-favorite"
    private referralVerifedUrl = this.apiLink + "users/verify-Referral-Code";
    private postFishingLicenUrl = 'http://107.170.218.205:2156/api/admin/autofill-adfg';
    private postFishingPayLicenUrl = this.apiLink + 'admin/payment-for-license';
    private getFishingLicenUrl = this.apiLink + 'admin/get-fishing-license-detail-as-per-user';
    private getallcitiesUrl = this.apiLink + "admin/get-all-cities";
    private getTidesStationMinDetailsUrlnew = this.apiLink + "admin/get-Tide-Details-Per-Minwith-Date-Filter-new";
    private getCurrentssStationMinDetailsUrlnew = this.apiLink + "admin/get-CurrentLocation-6Min-Prediction-Data";
    private updateAppVersionUrl = this.apiLink + "users/update-User-App-Version";
    private subIAPUrl = this.apiLink + "users/checkSubscriptionforiOS";
    private eventListNew = this.apiLink + "event/get-event-list-venue-entertainer";
    private logCreate = this.apiLink + "admin/Create-Subscription-Status"
    private geteventDetails = this.apiLink + "event/getEvent-ByAttr-with-Venue-Entertainment"
    private addpoints = this.apiLink + "users/add-points-to_users"
    private getpoints =this.apiLink +"users/get-Sum-of-Points"
    public commercial_fishing_location = 'http://166.62.118.179:2152/uploads/kmlfiles/ADFG_Salmon_CookInlet_Select_SetNet_Fishery_StatAreas.shp.json'
    private getcommericalList = this.apiLink + "commercial/getallLocationDetailwithEO"

    // private http: Http = null;
    fishingtypesList: any = [];
    BoatypesList: any = [];
    LocationtypesList: any = [];
    AllLocationtypesList: any = [];
    AllLocationtypesListFilter: any = [];
    isalllocationselected: boolean;

    ShoutOutPageList: any = [] = [];
    Tidelatlonglist: any = [];
    Currentslatlonglist: any = [];
    Buoylatlonglist: any = [];
    userDetails: any;
    Profileimg: any;
    MdleName: any;
    UserType: any;
    authToken: any;
    ReviewAll: any[] = [];
    userDetailsData: any[] = [];
    firstGuideReviewItem: any[] = [];
    selectedDay: any;
    selectedMonth: any;
    dateArray: any = [];
    leftItem: any;
    perPage: number;
    totalData: number;
    totalPage: number;
    Paymentlist: any[] = [];
    DataNA: boolean;
    StripeKey: any;
    MembershipText: any;
    is_featured: string;
    NotifyCount: any = 0;
    FcmToken: any;
    rootPage: any;
    GeoTracklat: any;
    GeoTracklng: any;
    MapEnabled: any;
    title: any;
    iconUrl: any;
  points: any=0;

    pinsDataList: any[] = [];
    INTERNETCHECK: boolean;
    PinsMarked: any[] = [];
    Prev_count: number;
    TimeSpan: any;
    options: any[] = [];
    alertNetwork: boolean = false
    socialUserType: string = ';'
    filterData: any;
    fishinglicenEnabled: boolean = false;
    locationlist: any[] = [
        {
            title: 'Northern Alaska',
            displayText: 'NorthernAlaska',
            pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/2018northern_sfregs_complete-min.pdf',
            sub_Options: [
                {
                    title: 'Cover',
                    displayText: 'NACover',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_cover.jpg'
                },
                {
                    title: 'Northern Alaska Regional Regulations',
                    displayText: 'NARegionalRegulations',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_regionalregs.jpg'
                },
                {
                    title: 'Northern Alaska Shellfish Regulations',
                    displayText: 'NAShellfishRegulations_2',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_shellfish.jpg'
                },
                {
                    title: 'Kuskokwim and Goodnews Drainages',
                    displayText: 'NAKuskokwimandGoodnewsDrainages',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_kusko.jpg'
                },
                {
                    title: 'North Slope Drainages',
                    displayText: 'NANorthSlopeDrainages',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_northslope.jpg'
                },
                {
                    title: 'Northwestern Drainages',
                    displayText: 'NANorthwesternDrainages',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_northwestern.jpg'
                },
                {
                    title: 'Yukon River Drainages',
                    displayText: 'NAYukonRiverDrainages',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_yukon.jpg'
                },
                {
                    title: 'Tanana Drainage',
                    displayText: 'NATananaDrainage',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_tanana.jpg'
                },
                {
                    title: 'Upper Copper & Upper Susitna Area',
                    displayText: 'NAUpperCopperUpperSusitnaArea',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_uppercopper.jpg'
                },
                {
                    title: 'Licensing Information',
                    displayText: 'NALicensingInformation',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_licensing.jpg'
                },
                {
                    title: 'Chitina Personal Use Fishery',
                    displayText: 'NAChitinaPersonalUseFishery',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_chitina_pu_fishery.jpg'
                },
                {
                    title: 'Stocked Lakes',
                    displayText: 'NAStockedLakes',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_stockedwaters.jpg'
                },
                {
                    title: 'Salmon Identification',
                    displayText: 'NASalmonIdentification',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_salmon_id.jpg'
                },
                {
                    title: 'Trout and Other Species Identification',
                    displayText: 'NATroutandOtherSpeciesIdentification',
                    parent_Name: 'NorthernAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/Northern_2019/2019northern_sfregs_trout_other_id.jpg'
                }
            ]
        },
        {
            title: 'Southwest Alaska',
            displayText: 'SouthwestAlaska',
            // pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/2018sw_sfregs_complete-min.pdf',
            sub_Options: [
                {
                    title: 'Cover',
                    displayText: 'SouthwestAlaskaCover',
                    parent_Name: 'SWSouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_cover.jpg'
                },
                {
                    title: 'Southwest Licensing & Regional Regulations',
                    displayText: 'SWSouthwestAlaskaRegionalRegulations',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_regional.jpg'
                },
                {
                    title: 'Bristol Bay Salt & Fresh Waters',
                    displayText: 'SWBristolBaySaltandFreshWaters',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_bristolbay.jpg'
                },
                {
                    title: 'Alaska Peninsula & Aleutian Islands',
                    displayText: 'SWAleutianIslandsandAlaskaPeninsulaFreshWaters',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_akpen_aleutian_freshwater.jpg'
                },
                {
                    title: 'AK Peninsula, Aleutian Islands & Kodiak Island Salt Waters',
                    displayText: 'SWKodiakIslandFreshwaters',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_kodiak_akpen_aleutian_salt.jpg'
                },
                {
                    title: 'Kodiak Island Fresh Waters',
                    displayText: 'SWKodiakIslandRoadZoneSportFishingEnhancementProjects',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_kodiak_fresh.jpg'
                },
                {
                    title: 'ShellFish - Sport & Personal Use Regulations',
                    displayText: 'SWKodiakIslandAlaskaPeninsulaandAleutianIslandsSaltWaters',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_shellfish.jpg'
                },
                {
                    title: 'Fish Identification ',
                    displayText: 'SWSalmonIdentification',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_fishidentification.jpg'
                },
                {
                    title: 'Sport Fishing By Proxy',
                    displayText: 'SWTroutandOtherSpeciesIdentification',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_sportfishing_byproxy.jpg'
                },
                {
                    title: 'ADF&G Trophy Fish Program',
                    displayText: 'SWSouthwestAlaskaSteelheadIdentification',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_trophy_program.jpg'
                },
                {
                    title: 'Fishing Offices & Message From Commissioner',
                    displayText: 'SWRockfishIdentificationandConservation',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2019sw_sfregs_fishing_offices_message_commissioner.jpg'
                },
                {
                    title: 'Southwest Alaska Shellfish Regulations',
                    displayText: 'SWSouthwestAlaskaShellfishRegulations',
                    parent_Name: 'SouthwestAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_west_2019/2018sw_sfregs_shellfish.jpg'
                }
            ]
        },
        {
            title: 'Southcentral Alaska',
            displayText: 'SouthcentralAlaska',
            pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/2018sc_sfregs_complete-min.pdf',
            sub_Options: [
                {
                    title: 'Cover',
                    displayText: 'SCSouthcentralAlaskaCover',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_cover.jpg'
                },
                {
                    title: ' Southcentral Alaska Regional Regulations',
                    displayText: 'SCSouthcentralAlaskaRegionalRegulations',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_regionalregs.jpg'
                },
                {
                    title: 'Southcentral Alaska Shellfish Regulations',
                    displayText: 'SCSouthcentralAlaskaShellfishRegulations',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_shellfish.jpg'
                },
                {
                    title: '2019 Southcentral Alaska Guiding Regulations ',
                    displayText: 'SC2019SouthcentralAlaskaGuidingRegulations ',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_guiding_regulations.jpg'
                },
                {
                    title: 'Personal Use Finfish Fisheries',
                    displayText: 'SCPersonalUseFinfishFisheries',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_personaluse_finfish.jpg'
                },
                {
                    title: 'Cook Inlet Salt Waters',
                    displayText: 'SCCookInletSaltWaters',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_cookinlet_kachemakbay_saltwater.jpg'
                },
                {
                    title: 'West Cook Inlet',
                    displayText: 'SCWestCookInlet',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_west_cook_inlet.jpg'
                },
                {
                    title: 'Susitna River Drainage',
                    displayText: 'SCSusitnaRiverDrainage',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_susitna_river.jpg'
                },
                {
                    title: 'Knik 2019sc_sfregs_knik_arm',
                    displayText: 'SCKnik2019sc_sfregs_knik_arm',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_knik_arm.jpg'
                },
                {
                    title: 'Anchorage Bowl',
                    displayText: 'SCAnchorageBowl',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_anchorage_bowl.jpg'
                },
                {
                    title: 'Kenai River',
                    displayText: 'SCKenaiRiver',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_kenai_river.jpg'
                },
                {
                    title: 'Kenai Peninsula',
                    displayText: 'SCKenaiPeninsula',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_kenai_peninsula.jpg'
                },
                {
                    title: 'North Gulf Coast',
                    displayText: 'SCNorthGulfCoast',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_north_gulf_coast.jpg'
                },
                {
                    title: 'Prince William Sound',
                    displayText: 'SCPrinceWilliamSound',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_prince_william_sound.jpg'
                },
                {
                    title: 'Licensing Information',
                    displayText: 'SCSouthcentralAlaskaLicensingInformation',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_licensing.jpg'
                },
                {
                    title: 'Stocked Lakes',
                    displayText: 'SCSouthcentralAlaskaStockedLakes',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_stockedwaters.jpg'
                },
                {
                    title: 'Salmon Identification',
                    displayText: 'SCSouthcentralAlaskaSalmonIdentification',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_salmon_id.jpg'
                },
                {
                    title: 'Trout and Other Species Identification',
                    displayText: 'SCTroutandOtherSpeciesIdentification',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_trout_other_id.jpg'
                },
                {
                    title: 'Rockfish Identification and Conservation',
                    displayText: 'SCRockfishIdentificationandConservation',
                    parent_Name: 'SouthcentralAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/South_Central/2019sc_sfregs_rockfish_id_conservation.jpg'
                }


            ]
        },
        {
            title: 'Southeast Alaska',
            displayText: 'SoutheastAlaska',
            pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/2018se_sfregs_complete-min.pdf',
            sub_Options: [
                {
                    title: 'Cover',
                    displayText: 'SESoutheastAlaskaCover',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_cover.jpg'
                },
                {
                    title: 'License Details and General Regulations',
                    displayText: 'SELicensesandGeneralRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_licenseandgeneralregs.jpg'
                },
                {
                    title: 'General Seasons and Bag and Size Limits - Fresh Waters',
                    displayText: 'SEGeneralSeasonsandBagandSizeLimitsFreshWaters',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_general_freshwater.jpg'
                },
                {
                    title: 'General Seasons and Bag and Size Limits - Salt Waters',
                    displayText: 'SEGeneralSeasonsandBagandSize',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_general_saltwater.jpg'
                },
                {
                    title: 'Yakutat Area Special Regulations',
                    displayText: 'SEYakutatAreaSpecialRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_yakutat.jpg'
                },
                {
                    title: 'Haines - Skagway Special Regulations',
                    displayText: 'SEHainesSkagwaySpecialRegulationss',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_haines_skagway.jpg'
                },
                {
                    title: 'Juneau - Glacier Bay Special Regulations',
                    displayText: 'SEJuneauGlacierBaySpecialRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_juneau_glacierbay.jpg'
                },
                {
                    title: 'Kodiak Island, Alaska Peninsula, and Aleutian Islands Salt Waters',
                    displayText: 'SEKodiakIslandAlaskaPeninsula',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_juneau_glacierbay.jpg'
                },
                {
                    title: 'Sitka Special Regulations',
                    displayText: 'SESitkaSpecialRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_sitka.jpg'
                },
                {
                    title: 'Petersburg - Wrangell Special Regulations',
                    displayText: 'SEPetersburgWrangelSpecialRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_petersburg_wrangell.jpg'
                },
                {
                    title: 'Prince of Wales Special Regulations',
                    displayText: 'SEPrinceofWalesSpecialRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_princeofwalesisland.jpg'
                },
                {
                    title: 'Ketchikan Special Regulations',
                    displayText: 'SEKetchikanSpecialRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_ketchikan.jpg'
                },
                {
                    title: 'Salmon Identification',
                    displayText: 'SESalmonIdentification',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_salmon_id.jpg'
                },
                {
                    title: 'Atlantic Salmon alert',
                    displayText: 'SEAtlanticSalmonalert',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_alertforatlanticsalmon.jpg'
                },
                {
                    title: 'Trout and Other Species Identification',
                    displayText: 'SETroutandOtherSpeciesIdentification',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_freshwater_fish_id.jpg'
                },
                {
                    title: 'Rockfish Identification and Conservation',
                    displayText: 'SERockfishIdentificationandConservation',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_rockfish_id.jpg'
                },
                {
                    title: 'Southeast Alaska Shellfish Regulations',
                    displayText: 'SESoutheastAlaskaShellfishRegulations',
                    parent_Name: 'SoutheastAlaska',
                    pdfURL: 'http://166.62.118.179:2152/uploads/Regulations/SouthEast/2019se_sfregs_shellfish.jpg'
                }
            ]
        }

    ];
    constructor(public http: Http,
        public storage: Storage,
        public popoverCtrl: PopoverController,
        public databaseService: DatabaseService
    ) {
    }

    audit_Page(page_title: any) {
        if (page_title != null && page_title != undefined &&
            localStorage.getItem("UserName") != undefined &&
            localStorage.getItem("UserName") != null) {
            let self = this;
            let date = new Date();
            var data = {
                date_time: date,
                user_id: localStorage.getItem('userId'),
                user_name: localStorage.getItem("UserName"),
                page_name: page_title,
                duration: self.TimeSpan
            }
            self.auditAPI(data).subscribe((res:any) => {
                console.log('res', JSON.stringify(res));
            })
        }
    }

    auditAPI(data: any): Observable<any> {
        let token1 = localStorage.getItem('token');
        return this.http.request(this.auditUrl, {
            method: 'post',
            body: data,
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            //  console.log(response.json);

        })
    }


    getsinguserpoints(data: any): Observable<any> {
        return this.API_GET(this.getpoints);
    }

    getProfileDetails() {
        localStorage.removeItem('UserType');
        localStorage.removeItem('UserName');
        localStorage.removeItem('Profileimg');
        localStorage.removeItem('is_featured');
        this.getUserProfileDetail(localStorage.getItem("token")).subscribe(res => {
            console.log('message', res.message);
            this.userDetails = res;
            console.log('userDetails', res);
            if (this.userDetails.lastName == null && this.userDetails.lastName == undefined) {
                var UserName = this.userDetails.firstName;
            } else {
                UserName = this.userDetails.firstName + "   " + this.userDetails.lastName;
            }
            localStorage.setItem("UserType", this.userDetails.UserType);
            localStorage.setItem("Profileimg", this.userDetails.image_url);
            localStorage.setItem("UserName", UserName);
            localStorage.setItem("is_featured", this.userDetails.is_featured);
            localStorage.setItem("referral_code", this.userDetails.referral_code);
            let img = localStorage.getItem("Profileimg");
            if (img == "undefined") {
                this.Profileimg = "assets/img/icon/placeholder.png";
            } else {
                this.Profileimg = localStorage.getItem("Profileimg");
            }
            this.MdleName = localStorage.getItem("UserName");
            this.UserType = localStorage.getItem("UserType");
            this.is_featured = this.userDetails.is_featured;
            if (this.UserType == "guide") {
                if (this.is_featured == 'yes' && this.userDetails.Details.length === 0) {
                    this.MembershipText = "Add Business Info";
                } else if (this.userDetails.Details.length !== 0 && this.is_featured == 'yes') {
                    this.MembershipText = "Business Info";
                } else {
                    this.MembershipText = "Upgrade to Guide Direct";
                }
            } else {
                if (this.is_featured == 'yes') {
                    this.MembershipText = "Prime Offers";
                } else {
                    this.MembershipText = "Upgrade to FishTopia Prime";
                }
            }
            console.log('this.userDetails', localStorage.getItem("UserType"));
        }, (err) => {
            var json = JSON.parse(err.text());
            if (json.success == false && json.error.status === 401) {
                localStorage.removeItem('token');
                localStorage.removeItem('UserType');
                localStorage.removeItem('UserName');
                localStorage.removeItem('Profileimg');
                localStorage.removeItem('userId');
                localStorage.removeItem('is_featured');
                localStorage.removeItem('referral_code');
                // this.navCtrl.setRoot('LoginListPage');
            }
            //console.log('message',json.message);
            console.log('res in auth provider-->', json);
            console.log(json._body);
        });
    }

    getNotificationsCount() {
        var data = {
            page: 1,
            status: "pending"
            //"status": "all / pending / seen / read"
            //is_read: false
        }
        this.getNotificationList(data).subscribe(res => {
            this.NotifyCount = res.data.items.total;
            // console.log('NotificationsPage', JSON.stringify(this.notificationList));
        })
    }

    getReviewAll(id: any, page) {
        let self = this;
        var data = {
            page: page,
        }
        this.addReviewaAllPost(data).subscribe(res => {
            //  this.ReviewAll=[];
            if (res.success) {
                for (let item of res.data.items.docs) {
                    if (item.reviewed_to._id == id) {
                        self.ReviewAll.push(item);
                    }
                }
            }
            console.log(' this.Review', self.ReviewAll);
        });
    }

    getTideDetails_New() {
        let self = this;
        self.Tidelatlonglist = [];
        self.getTideData().subscribe(res => {
            if (res.data !== undefined) {
                _.forEach(res.data, (item: any) => {
                    item.iconUrl = "assets/img/plain/blue.png";
                    item.lat = parseFloat(item.lat);
                    item.lon = parseFloat(item.lon);
                    self.Tidelatlonglist.push(item);
                })
                // localStorage.setItem("tbl_TideData", JSON.stringify(self.Tidelatlonglist));
                self.databaseService.addTide_Data(res.data);
                // self.localDBService.addTide(res.data);
            }
        });
    }
    addpoint(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addpoints);
    }
    // getTideMinDetails() {
    //     let self = this;
    //     self.Tidelatlonglist = [];
    //     self.getTidesStationMinDetails().subscribe(res => {
    //         if (res.data !== undefined) {
    //             _.forEach(res.data, (item: any) => {
    //                 item.iconUrl = "assets/img/plain/blue.png";
    //                 item.lat = parseFloat(item.lat);
    //                 item.lon = parseFloat(item.lon);
    //                 self.Tidelatlonglist.push(item);
    //             })
    //             // localStorage.setItem("tbl_TideData", JSON.stringify(self.Tidelatlonglist));
    //             self.databaseService.addTideMinpins(res.data);
    //             // self.localDBService.addTide(res.data);
    //         }
    //     });
    // }

    getCurrentsDetails() {
        let self = this;
        self.Currentslatlonglist = [];
        self.getCurrentsData().subscribe(res => {
            // added as a favourite.
            if (res.data !== undefined) {
                for (let item of res.data) {
                    item.iconUrl = "assets/img/plain/red.png";
                    item.lat = parseFloat(item.lat);
                    item.lon = parseFloat(item.lon);
                    self.Currentslatlonglist.push(item);
                }
                self.databaseService.addCurr_Data(res.data);
            }
        });
    }

    path: any;
    Datapath: any;
    getMarinelistinfo() {
        this.path = [];
        this.getMarineWeatherList().subscribe(res => {
            console.log('data-->', res.data);
            let data = res.data.items;
            data.forEach(item => {
                item.iconUrl = " ";
                // item.iconUrl = "assets/img/plain/marron.png";
                item.lon = item.lng;
                item.name = item.id;
                this.path.push(item);
            });
            this.getMarineLoclistinfo(this.path);
            // localStorage.setItem("tbl_MarineDetails", JSON.stringify(this.Datapath));
        });
    }
    getMarineLoclistinfo(Marin_data: any) {
        var data = {
            page: 1
        }
        this.getMarineWeatherLocationList(data).subscribe(res => {
            console.log('data-->', res.data);
            let data = res.data.items.docs;
            data.forEach(item => {
                if (item._id == "5ba3b95ea47a21332b120250") {
                    var time = moment(item.updated_at).startOf('day').fromNow();
                    var sepTime = time.split(" ");
                    if (sepTime[1] === "hours" || sepTime[1] === "minute" || sepTime[1] === "minutes"
                        && item.wind_speed_in_knots >= 0 && item.wave_height_in_feet >= 0 && item.minimum_boat_length_in_feet >= 0
                    ) {
                        item.EndHours = true;
                        item.wind_speed_in_knots = item.wind_speed_in_knots + " kts";
                        item.wave_height_in_feet = item.wave_height_in_feet + " ft";
                        item.minimum_boat_length_in_feet = item.minimum_boat_length_in_feet + " ft";
                    } else {
                        item.EndHours = false;
                        item.wind_speed_in_knots = "Not Available";
                        item.wave_height_in_feet = "Not Available";
                        item.minimum_boat_length_in_feet = "Not Available";
                    }
                    item.iconUrl = "assets/img/plain/green.png";
                    item.id = item._id;
                    item.name = '';
                    item.lat = parseFloat(item.Lat);
                    item.lon = parseFloat(item.Lng);
                    item.lng = parseFloat(item.Lng);
                } else if (item._id == "5ba4f87977090c022150d2e2") {
                    var time = moment(item.updated_at).startOf('day').fromNow();
                    var sepTime = time.split(" ");
                    if (sepTime[1] === "hours" || sepTime[1] === "minutes" || sepTime[1] === "minute"
                        && item.wind_speed_in_knots >= 0 && item.wave_height_in_feet >= 0 && item.minimum_boat_length_in_feet >= 0
                    ) {
                        item.EndHours = true;
                        item.wind_speed_in_knots = item.wind_speed_in_knots +"MPH";
                        item.wave_height_in_feet = item.wave_height_in_feet + " ft";
                        item.minimum_boat_length_in_feet = item.minimum_boat_length_in_feet + " ft";
                        // item.EndHours = moment(item.updated_at).startOf('day').fromNow();
                    } else {
                        item.EndHours = false;
                        item.wind_speed_in_knots = "Not Available";
                        item.wave_height_in_feet = "Not Available";
                        item.minimum_boat_length_in_feet = "Not Available";
                    }
                    item.iconUrl = "assets/img/plain/yellow.png";
                    item.id = item._id;
                    item.name = '';
                    item.lat = parseFloat(item.Lat);
                    item.lon = parseFloat(item.Lng);
                    item.lng = parseFloat(item.Lng);
                }
                Marin_data.push(item);
            });
            if (Marin_data.length > 0) {
                this.pinsDataList = Marin_data;
                this.databaseService.addMarine_Data(Marin_data);
            } else {
                this.pinsDataList = Marin_data;
                this.databaseService.updateMarine_Data(JSON.stringify(Marin_data));
            }
        });
    }



    getBuoyDetails() {
        let self = this;
        self.Buoylatlonglist = [];
        var data = {
            Page: "1"
        }
        self.getBuoyData(data).subscribe(res => {
            // added as a favourite.
            if (res.data !== undefined) {
                for (let item of res.data.items.docs) {
                    item.iconUrl = "assets/img/plain/orange.png";
                    item.lat = parseFloat(item.lat);
                    item.lon = parseFloat(item.lon);
                    self.Buoylatlonglist.push(item);
                }
                localStorage.setItem("tbl_BuoyData", JSON.stringify(self.Buoylatlonglist));
            }
        });
    }
    getcommercialFishingLOcation(): Observable<any> {
        return this.API_GET(this.getcommericalList);
    }

    API_POST(data: any, Url: any): Observable<any> {
        return this.http.request(Url, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            return json;
        }, (error) => {
            return null
        });
    }

    API_POST_AccesToken(data: any, Url: any): Observable<any> {
        var token = localStorage.getItem("token");
        return this.http.request(Url, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            return json;
        }, (error) => {
            return null
        });
    }

    API_GET(Url: any): Observable<any> {
        var token = localStorage.getItem("token");
        return this.http.request(Url, {
            method: "get",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            return json;
        }, (error) => {
            return null
        });
    }

    login(data): Observable<any> {
        let res = this.http.post(this.loginUrl, data);
        console.log('Responce', res);
        return res;
    }
    doLogin(data: any): Observable<any> {
        return this.API_POST(data, this.loginUrl);
    }

    referralVerifed(data: any): Observable<any> {
        return this.API_POST(data, this.referralVerifedUrl);
    }

    updateAppVersion(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.updateAppVersionUrl);
    }

    otpVerifed(data: any): Observable<any> {
        return this.API_POST(data, this.otpverify);
    }
    otpResend(data: any): Observable<any> {
        return this.API_POST(data, this.resendOTPUrl);
    }

    signup(data: any): Observable<any> {
        return this.API_POST(data, this.signupUrl);
    }
    iosSubIAP(data) {
        return this.API_POST(data, this.subIAPUrl);
    }

    googleAccsess(data: any): Observable<any> {
        return this.API_POST(data, this.googleAccess_tokenUrl);
    }

    googleSignUp(data: any): Observable<any> {
        return this.API_POST(data, this.GoogleandFacebook_tokenUrl);
    }

    resetPassword(data: any): Observable<any> {
        return this.API_POST(data, this.resetPwd);
    }

    forgotPassword(data: any): Observable<any> {
        return this.API_POST(data, this.forgotPwd);
    }
    changePwd(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.changePwdUrl);
    }
    logout(token: any): Observable<any> {
        return this.API_GET(this.logOutUrl);
    }


    getUserLocation(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getuserLocationUrl);
    }

    getUserProfileDetail(token: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getProfileDetailsUrl, {
            method: "get",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('res in auth provider-->', json);
            let res: GetUserProfile;
            if (json.success) {
                res = new GetUserProfile(json.data);
            }
            return res;
        }, (error) => {
            return error;
        });
    }

    updateUsersProfile(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.updateUsersProfileUrl);
    }

    fishingLicen(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.postFishingLicenUrl);
    }


    addfishingLicen(data: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        console.log('token1', token1);
        console.log('data', data);
        return this.http.request(this.postFishingPayLicenUrl, {
            method: "post",
            body: data,
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8"
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('json', json);
            return json;
        }, (error) => {
            // alert(error)
            return null
        });
    }

    fishingLicenlist(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getFishingLicenUrl);
    }

    getUserLicenseDetail(token: any): Observable<any> {
        return this.API_GET(this.getuserLicenseeUrl);
    }

    addLicenseDetails(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.adduserDetailsLicenseeUrl);
    }

    getEventItinerary(): Observable<any> {
        return this.API_GET(this.getItineraryUrl);
    }

    getEventByDate(data: any): Observable<any> {
        var token = localStorage.getItem("token");
        return this.http.request(this.eventByDateUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log(json);
            let res: Event[] = [];
            if (json.success) {
                this.perPage = json.data.items.page;
                this.totalData = json.data.items.total;
                this.totalPage = json.data.items.pages;
                for (let item of json.data.items.docs)
                    res.push(new Event(item));
            }
            return res;
        }, (error) => {
            return null
        });
    }
    getEventByDateNew(data: any): Observable<any> {
        var token = localStorage.getItem("token");
        return this.http.request(this.eventListNew, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
           // console.log(json);
            let res: EventNew[] = [];
            if (json.success) {
                this.perPage = json.data.items.page;
                this.totalData = json.data.items.total;
                this.totalPage = json.data.items.pages;
                for (let item of json.data.items.docs)
                    res.push(new EventNew(item));
            }
            return res;
        }, (error) => {
            return null
        });
    }

    postLog(data: any): Observable<any> {
        return this.API_POST(data, this.logCreate);
    }
    getEventDataSingle(data: any): Observable<any> {
        return this.API_GET(this.geteventDetails + "/" + data);
    }
    getallfishinglocationslist() {
        var token = localStorage.getItem("token");
        this.AllLocationtypesList = [];
        this.AllLocationtypesListFilter = [];
        this.isalllocationselected = true;
        let maxcount = 0
        let count = 0
        this.http.get(this.getallcitiesUrl).subscribe(data => {
            var json = JSON.parse(data.text());
            this.AllLocationtypesListFilter = JSON.parse(localStorage.getItem("filterData"))
            maxcount = json.data.length;
           // console.log('AllLocationtypesListFilter', JSON.stringify(this.AllLocationtypesListFilter) + " " + this.AllLocationtypesListFilter.length);
            if (json.data !== undefined) {
                for (let item of json.data) {
                    console.log(JSON.stringify(item));
                    if (this.AllLocationtypesListFilter) {

                        for (let filteritem of this.AllLocationtypesListFilter) {
                            if (filteritem == item._id) {
                                item.selected = true;
                                break;
                            }
                        }
                        this.AllLocationtypesList.push(item);


                    }
                    else {
                        item.selected = true;
                        this.isalllocationselected = true;
                        this.AllLocationtypesList.push(item);
                    }
                }
               // console.log("this.AllLocationtypesListFilter.length", this.AllLocationtypesListFilter.length)

                if (this.AllLocationtypesListFilter.length == 32)
                    this.isalllocationselected = true;
                else
                    this.isalllocationselected = false
            }
        });
        
        // this.AllLocationtypesList.forEach(element => {
        //     if (element.selected)
        //         ++count;
        // });
        // if (maxcount == count)
        //     this.isalllocationselected = true;
        // else
        //     this.isalllocationselected = false;

    }
    deleteLicenseDetails(): Observable<any> {
        return this.API_GET(this.deleteuserDetailsLicenseeUrl);
    }
    addToFavouriteEvent(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addFavouriteEventUrl);
    }

    getWHEventByDate(data: any): Observable<any> {
        var token = localStorage.getItem("token");
        return this.http.request(this.getwhEventUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log(json);
            let res: WHEvent[] = [];
            if (json.success) {
                this.perPage = json.data.items.page;
                this.totalData = json.data.items.total;
                this.totalPage = json.data.items.pages;
                for (let item of json.data.items.docs)
                    res.push(new WHEvent(item));
            }
            return res;
        }, (error) => {
            return null
        });
    }
    addToFavouriteWHEvent(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getwhEventFavUrl);
    }

    getFishNews(data: any): Observable<any> {
        var token = localStorage.getItem("token");
        console.log('Data', data);
        return this.http.request(this.getNewsUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('getFishNews', json);
            let res: FishNewsList[] = [];
            if (json.success) {
                this.perPage = json.data.items.page;
                this.totalData = json.data.items.total;
                this.totalPage = json.data.items.pages;
                for (let item of json.data.items.docs)
                    res.push(new FishNewsList(item));
            }
            return res;
        }, (error) => {
            return null
        });
    }

    addToFavouriteNews(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addFavouriteUrl);
    }
    addTolikeNews(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addLikeUrl);
    }
    addToCmmtNews(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addCmtNewsUrl);
    }
    // curl -X GET "http://166.62.118.179:2152/api/news/5a7049e8afd56206f4b2c832/comment" -H "accept: application/json" -H "access_token: 1517913749890_kRq3LFMBuKbIcPxo"
    getToCmmtNews(newsId: any): Observable<any> {
        return this.API_GET(this.getCmtNewsUrl + newsId + "/comment");
    }


    addShoutOutPost(data): Observable<any> {
        return this.API_POST_AccesToken(data, this.addShoutoutActivityUrl);
    }

    getShoutOutDetail(data): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getShoutoutActivityUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            this.perPage = json.data.items.page;
            this.totalData = json.data.items.total;
            this.totalPage = json.data.items.pages;
            console.log('res in auth provider-->', json);
            return json;
        }, (error) => {
            return null
        });
    }

    addToFavouriteShoutOut(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addShoutoutAddToFavUrl);
    }

    addToShoutOutAbuse(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addShoutoutAddToAbuseUrl);
    }


    addToCmmtShoutOut(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addShoutoutToCmtUrl);
    }

    getToCmmtShoutOut(activityId: any): Observable<any> {
        var token = localStorage.getItem("token");
        return this.http.request(this.getCmtShoutoutUrl + activityId + "/comment", {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "Accept": "application/json; charset=UTF-8",
                "access_token": token,
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            //  console.log(json);
            return json;
        }, (error) => {
            return null
        });
    }


    getToImgShoutOut(): Observable<any> {
        return this.API_GET(this.addImgShoutoutActivityUrl);
    }


    getboattypeslist() {
        this.BoatypesList = [];
        this.http.get(this.getboattypesUrl).subscribe(data => {
            var json = JSON.parse(data.text());
            console.log('BoatypesList', json);
            if (json.data !== undefined) {
                for (let item of json.data) {
                    item.selected = true;
                    this.BoatypesList.push(item);
                }
                //    console.log('item', this.BoatypesList);
            }
        });
    }
    getfishingtypeslist() {
        this.fishingtypesList = [];
        this.http.get(this.getfishingtypesUrl).subscribe(data => {
            var json = JSON.parse(data.text());
            console.log('fishingtypesList', json);
            if (json.data !== undefined) {
                for (let item of json.data) {
                    item.selected = true;
                    this.fishingtypesList.push(item);
                    //     console.log('item', this.fishingtypesList);
                }
            }
        });
    }
    getfishinglocationslist() {
        this.LocationtypesList = [];
        this.http.get(this.getfishinglocationsUrl).subscribe(data => {
            var json = JSON.parse(data.text());
            console.log('LocationtypesList', json);
            if (json.data !== undefined) {
                for (let item of json.data) {
                    item.selected = true;
                    this.LocationtypesList.push(item);
                    // console.log('item', this.LocationtypesList);
                }
            }
        });
    }
    AddRequestTrip(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addRequestTripUrl);
    }
    AddfilterTrip(data: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.addfilterTripUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('FilterTrip', json);
            let res: FishingExchngEvent[] = [];
            if (json.success) {
                this.perPage = json.data.page;
                this.totalData = json.data.total;
                this.totalPage = json.data.pages;
                for (let item of json.data.items.docs) {
                    res.push(new FishingExchngEvent(item));
                }
            }
            return res;
        }, (error) => {
            return null
        });
    }

    getFishingExchngeDetail(data: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getFishingExchngeDetailUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "Accept": "application/json; charset=UTF-8",
                "access_token": token1,
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('res in auth provider-->', json);
            let res: FishingExchngEvent[] = [];
            if (json.success) {
                this.perPage = json.data.items.page;
                this.totalData = json.data.items.total;
                this.totalPage = json.data.items.pages;
                for (let item of json.data.items.docs) {
                    res.push(new FishingExchngEvent(item));
                }
            }
            console.log('res in auth provider-->', res);
            return res;
        }, (error) => {
            return null
        });
    }

    addTripStatus(data: any, TripId: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addTripStatusUrl + TripId + "/guide/response");
    }

    addTripCancel(TripId: any): Observable<any> {
        var token = localStorage.getItem("token");
        console.log("status", TripId)
        return this.http.request(this.addTripStatusUrl + TripId + "/cancel", {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "Accept": "application/json; charset=UTF-8",
                "access_token": token,
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('json', json);
            return json;
        }, (error) => {
            return null
        });
    }



    getTripDetails(TripId: any): Observable<any> {
        return this.API_GET(this.addTripStatusUrl + TripId);
    }


    getfishingcountlocation(data: any): Observable<any> {
        // return this.API_POST_AccesToken(data, this.getfishingcountlocationUrl);
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getfishingcountlocationUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('AddRequestTrip', json);
            this.perPage = json.data.items.page;
            this.totalData = json.data.items.total;
            this.totalPage = json.data.items.pages;
            // let res: UpdatedUsersProfile;
            // if (json.success) {
            //     res = new UpdatedUsersProfile(json.data);
            // }
            return json;
        }, (error) => {
            return null
        });
    }

    getFavFishCountlist(data: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getfishCountsFavUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('AddRequestTrip', json);
            this.perPage = json.data.items.page;
            this.totalData = json.data.items.total;
            this.totalPage = json.data.items.pages;
            // let res: UpdatedUsersProfile;
            // if (json.success) {
            //     res = new UpdatedUsersProfile(json.data);
            // }
            return json;
        }, (error) => {
            return null
        });
    }

    addToFavouriteFishCount(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addFishCountFavUrl);
    }

    getfishingcount(data: any, LocId: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getFishCountUrl + LocId + "/records");
    }
    getOptionsWithToken() {
        var head = new Headers({
            'Content-Type': 'application/json',
            'access_token': localStorage.getItem("token")
        });
        return new RequestOptions({ headers: head });
    }


    getfishDataByYear(LocationId: any): Observable<any> {
        return this.API_GET(this.gettotalData + LocationId);
    }


    getTideData(): Observable<any> {
        return this.API_GET(this.getTidesUrl);
    }


    getTideStationData(stationId: any): Observable<any> {
        return this.API_GET(this.getTidesStationDetailsUrl + stationId + "?sort=asc");
    }


    getTidesStationMinDetails(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getTidesStationMinDetailsUrl);
    }
    getTidesStationMinDetailsNew(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getTidesStationMinDetailsUrlnew);
    }
    getCurrentsData(): Observable<any> {
        return this.API_GET(this.getCurrentsUrl);
    }

    getUserData(userId: any): Observable<any> {
        return this.API_GET(this.getUserDetailsUrl + userId);
    }

    getCurrentsStationData(stationId: any): Observable<any> {
        return this.API_GET(this.getCurrentsDetailsUrl + stationId + "?sort=asc");
    }
    getCurrentsStationMinDetailsNew(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getCurrentssStationMinDetailsUrlnew);
    }
    getBuoyData(data): Observable<any> {
        return this.API_POST_AccesToken(data, this.getBuoyInfoUrl);
    }


    getBUOYStationDetails(Data: any): Observable<any> {
        return this.API_GET(this.getBuoyStationDetailsUrl + Data._id + "/station/" + Data.stationId);
    }


    getMarineWeatherList(): Observable<any> {
        return this.API_GET(this.getMarineWeatherListUrl);
    }

    getMarineWeatherLocationList(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getMarineWeatherLocListUrl);
    }

    getMarineDetails(stationId: any): Observable<any> {
        return this.API_GET(this.getMarineWeatherDetailsUrl + stationId);
    }

    getwaterlocation(data: any): Observable<any> {
        return this.API_GET(this.getWaterFlowUrl);
    }

    getwaterlocationDetails(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getWaterFlowDetailsUrl);
    }
    getwaterDSGraphDetails(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getWaterFlowDsGraphDetailsUrl);
    }
    getwaterGHGraphDetails(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.getWaterFlowGHGraphDetailsUrl);
    }

    getwaterFlowFavlist(data: any): Observable<any> {
        return this.API_GET(this.getWaterFlowFavUrl);
    }

    addToWaterFlowFav(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.addWaterFlowFavUrl);
    }
    addReviewPost(data): Observable<any> {
        return this.API_POST_AccesToken(data, this.addReviewUrl);
    }
    getReviewPost(reviewId): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getReviewUrl + reviewId, {
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('res in auth provider-->', json);
            return json;
        }, (error) => {
            return null
        });
    }
    addReviewaAllPost(data): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getReviewAllUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('res in auth provider-->', json);
            this.perPage = json.data.items.page;
            this.totalData = json.data.items.total;
            this.totalPage = json.data.items.pages;
            return json;
        }, (error) => {
            return null
        });
    }

    StripeSavePost(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.StripeSaveReqUrl);
    }
    getStripekey(): Observable<any> {
        return this.API_GET(this.getStripekeyUrl);
    }
    getCouponlistData(data: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getCouponlistUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8"
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log(json);
            let res: couponData[] = [];
            if (json.success) {
                this.perPage = json.data.items.page;
                this.totalData = json.data.items.total;
                this.totalPage = json.data.items.pages;
                for (let item of json.data.items.docs)
                    res.push(new couponData(item));
            }
            return res;
        }, (error) => {
            return null
        });
    }

    postBusinessDetails(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.postBusinessDetailsUrl);
    }

    getPerimeMember(): Observable<any> {
        var token1 = localStorage.getItem("token");
        console.log('token1', token1);
        return this.http.request(this.upgradeToPrimeUrl, {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8"
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('json', json);
            return json;
        }, (error) => {
            // alert(error)
            return null
        });
    }

    getAllStripePost(): Observable<any> {
        return this.API_GET(this.getStripelistReqUrl);
    }


    DeleteCardPost(CardID: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.delete(this.StripeDeleteReqUrl + CardID, {
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8"
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('json', json);
            return json;
        }, (error) => {
            console.log('json', error);
            // alert(error)
            return null
        });
    }

    StripeAnglerPost(data: any, tripId: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.StripeAnglarUrl + tripId + "/angler/pay");
    }


    StripeGuideReq(data: any): Observable<any> {
        return this.API_POST(data, this.StripeGuideReqUrl);
    }


    getNotificationSettings(): Observable<any> {
        return this.API_GET(this.getNotificationsSetUrl);
    }

    getNotificationList(data: any): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getNotificationsUrl, {
            body: data,
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('getNotificationsUrl', json);
            this.perPage = json.data.items.page;
            this.totalData = json.data.items.total;
            this.totalPage = json.data.items.pages;
            return json;
        }, (error) => {
            return null
        });
    }
    getNotificationReadList(Notifyid): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getNotificationbyIdUrl + Notifyid + "/read", {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('getNotificationbyIdUrl', json);
            return json;
        }, (error) => {
            return null
        });
    }
    getNotificationReadAllList(): Observable<any> {
        var token1 = localStorage.getItem("token");
        return this.http.request(this.getNotificationReadAllUrl, {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('getNotificationReadAllUrl', json);
            return json;
        }, (error) => {
            return null
        });
    }
    getNotificationDelete(NotifyId: any): Observable<any> {
        console.log('Id', NotifyId)
        var token1 = localStorage.getItem("token");
        return this.http.request(this.NotifyDeleteUrl + NotifyId, {
            method: "post",
            headers: new Headers({
                "Content-Type": "application/json; charset=UTF-8",
                "access_token": token1,
                "Accept": "application/json; charset=UTF-8",
            })
        }).map((response) => {
            var json = JSON.parse(response.text());
            console.log('NotifyDeleteUrl', json);
            return json;
        }, (error) => {
            return null
        });
    }
    UpdateNotificationToken(data: any): Observable<any> {
        return this.API_POST_AccesToken(data, this.NotifyUpdatetokenUrl);
    }

    saveNotificationSettings(data: any) {
        return this.API_POST_AccesToken(data, this.NotificationSettingsUrl);
    }
}

export class GetUserProfile {
    firstName: string;
    MiddleName: string;
    lastName: string;
    phoneNo: any;
    email: string;
    StreetAddr1: string;
    StreetAddr2: string;
    State: string;
    City: string;
    Country: string;
    ZipCode: string;
    PhoneNumber: string;
    Profileimg: any;
    UserType: any;
    image_url: any;
    is_featured: any;
    Details: any[] = [];
    URL1: any;
    URL2: any;
    URL3: any;
    referral_code: any;
    constructor(data?: any) {
        if (data !== undefined) {
            this.firstName = data.first_name;
            this.MiddleName = data.middle_name;
            this.lastName = data.last_name;
            this.phoneNo = data.phone_no;
            this.email = data.email;
            this.Profileimg = data.image;
            this.City = data.city;
            this.State = data.state;
            this.Country = data.country;
            this.ZipCode = data.zip_code;
            this.StreetAddr1 = data.street_address1;
            this.StreetAddr2 = data.street_address2;
            this.UserType = data.user_type;
            this.image_url = data.image_url;
            this.is_featured = data.is_featured;
            this.referral_code = data.referral_code;
            console.log('data.Details', data.details);
            if (data.details != undefined) {
                this.Details = data.details;
            }
            console.log('this.image_url', this.image_url);
        }
    }
}


export class Event {
    likes: number;
    _id: string;
    created_at: string;
    updated_at: string;
    title: string;
    event_date: string;
    description: string;
    start_time: string;
    end_time: string;
    event_date_time: string;
    __v: number;
    status: string;
    favouriteList: Favourite[] = [];
    participantList: any[] = [];
    participantCount: number;
    favouriteStatus: boolean = false;
    joinStatus: boolean = false;
    location: any;
    event_booking_type: string;
    image_url: any;
    image_url_thumb: any;
    amount: any;
    itinerary: any[] = [];
    itinerarylist: any[] = [];
    eventDay: any;
    Day: any;
    constructor(data?: any) {
        if (data !== undefined) {
            this._id = data._id;
            this.created_at = data.created_at;
            this.updated_at = data.updated_at;
            this.description = data.description;
            this.__v = data.__v;
            this.status = data.status;
            this.title = data.title;
            this.start_time = data.event_time;
            this.end_time = data.event_end_time;
            this.event_date = data.event_date;
            this.location = data.location;
            this.event_booking_type = data.event_booking_type;
            if (data.image_url == 'http://166.62.118.179:2152/assets/img/default-event.png') {
                this.image_url = 'assets/img/icon/default-event.png';
                this.image_url_thumb = 'assets/img/event-Thumb.png';
            } else {
                this.image_url = data.image_url;
                this.image_url_thumb = data.image_url;
            }
            this.event_date_time = data.event_date_time;
            var date = moment(data.event_date_time);
            this.eventDay = date.date();
            this.Day = date;
            this.amount = data.amount;
            this.itinerary = data.itinerary;
            var id = localStorage.getItem('userId');
            for (let item of data.favourites) {
                this.favouriteList.push(new Favourite(item));
                if (item.user_id == id) {
                    this.favouriteStatus = true;
                }
            }

        }
    }
    tConvert(time) {
        // Check correct time format and split into components
        time = time.match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
}
export class EventNew {
    _id: string;
    created_at: string;
    updated_at: string;
    description: string;
    status: string;
    title: string;
    start_time: string;
    end_time: string;
    event_date: string;
    event_booking_type: string;
    event_date_time: string;
    eventDay: any;
    Day: any;
    itinerary: any[] = [];
    amount: any;
    favouriteList: any[] = [];
    favouriteStatus: boolean = false;
    created_by: string;
    entertainer_id: string;
    entertainer_name: string;
    event_end_date_time: string;
    event_start_datetime: string;
    location: string
    image_url: string;
    image: string;
    location_id: any;
    updated_by: string;
    venue_id: any[] = []
    venue_name: string;


    constructor(data?: any) {
        if (data !== undefined) {
            this.image = data.image;
            this.location_id = data.location_id;
            this.updated_by = data.updated_by;
            this.venue_id = data.venue_id;
            this.venue_name = data.venue_name;
            this.entertainer_id = data.entertainer_id;
            this.entertainer_name = data.entertainer_name;
            this.event_end_date_time = data.event_end_date_time;
            this.event_start_datetime = data.event_start_datetime;
            this._id = data._id;
            this.created_at = data.created_at;
            this.created_by = data.created_by
            this.updated_at = data.updated_at;
            this.description = data.description;
            this.status = data.status;
            this.title = data.title;
            this.start_time = data.event_time;
            this.end_time = data.event_end_time;
            this.event_date = data.event_date;
            this.location = data.location;
            this.event_booking_type = data.event_booking_type;
            if (data.image_url == 'http://166.62.118.179:2152/assets/img/default-event.png') {
                this.image_url = 'assets/img/icon/default-event.png';
                // this.image_url_thumb = 'assets/img/event-Thumb.png';
            } else {
                this.image_url = data.image_url;
                // this.image_url_thumb = data.image_url;
            }
            this.event_date_time = data.event_date_time;
            var date = moment(data.event_date_time);
            this.eventDay = date.date();
            this.Day = date;
            this.amount = data.amount;
            this.itinerary = data.itinerary;
            var id = localStorage.getItem('userId');
            for (let item of data.favourites) {
                this.favouriteList.push(new Favourite(item));
                if (item.user_id == id) {
                    this.favouriteStatus = true;
                }
            }

        }
    }
    tConvert(time) {
        // Check correct time format and split into components
        time = time.match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
}


export class couponData {
    likes: number;
    _id: string;
    created_at: string;
    updated_at: string;
    title: string;
    coupon_expiry_date: string;
    description: string;
    Short_description: any;
    start_time: string;
    end_time: string;
    coupon_expiry_date_time: string;
    __v: number;
    status: string;
    location: any;
    event_booking_type: string;
    image_url: any;
    image_url_thumb: any;
    constructor(data?: any) {
        if (data !== undefined) {
            this._id = data._id;
            this.created_at = data.created_at;
            this.updated_at = data.updated_at;
            this.description = data.description;
            this.Short_description = data.short_description;
            this.__v = data.__v;
            this.status = data.status;
            this.title = data.title;
            this.coupon_expiry_date = data.coupon_expiry_date;
            this.location = data.location;
            this.event_booking_type = data.event_booking_type;
            if (data.image_url == 'http://166.62.118.179:2152/assets/img/default-event.png') {
                this.image_url = 'assets/img/icon/default-event.png';
                this.image_url_thumb = 'assets/img/event-Thumb.png';
            } else {
                this.image_url = data.image_url;
                this.image_url_thumb = data.image_url;
            }
            this.coupon_expiry_date_time = data.coupon_expiry_date_time;
        }
    }
    tConvert(time) {
        // Check correct time format and split into components
        time = time.match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];

        if (time.length > 1) { // If time format correct
            time = time.slice(1);  // Remove full string match value
            time[5] = +time[0] < 12 ? 'AM' : 'PM'; // Set AM/PM
            time[0] = +time[0] % 12 || 12; // Adjust hours
        }
        return time.join(''); // return adjusted time or original string
    }
}



export class WHEvent {
    likes: number;
    _id: string;
    created_at: string;
    updated_at: string;
    title: string;
    event_date: string;
    description: string;
    start_time: string;
    end_time: string;
    event_date_time: string;
    __v: number;
    status: string;
    favouriteList: Favourite[] = [];
    participantList: any[] = [];
    participantCount: number;
    favouriteStatus: boolean = false;
    joinStatus: boolean = false;
    location: any;
    event_booking_type: string;
    image_url: any;
    image_url_thumb: any;
    amount: any;
    website_url: any;
    constructor(data?: any) {
        if (data !== undefined) {
            this._id = data._id;
            this.created_at = data.created_at;
            this.updated_at = data.updated_at;
            this.description = data.description;
            this.__v = data.__v;
            this.status = data.status;
            this.title = data.title;
            this.event_date = data.event_date;
            this.location = data.location;
            this.website_url = data.website_url;
            this.event_booking_type = data.event_booking_type;
            if (data.image_url == 'http://166.62.118.179:2152/assets/img/default-event.png') {
                this.image_url = 'assets/img/icon/default-event.png';
                this.image_url_thumb = 'assets/img/event-Thumb.png';
            } else {
                this.image_url = data.image_url;
                this.image_url_thumb = data.image_url;
            }
            this.event_date_time = data.event_date_time;
            this.amount = data.amount;
            var id = localStorage.getItem('userId');
            for (let item of data.favourites) {
                this.favouriteList.push(new Favourite(item));
                if (item.user_id == id) {
                    this.favouriteStatus = true;
                }
            }

        }
    }
}



export class Favourite {
    _id: string;
    user_id: string;
    constructor(data?: any) {
        if (data !== undefined) {
            this._id = data._id;
            this.user_id = data.user_id;
        }
    }
}
export class Like {
    _id: string;
    user_id: string;
    constructor(data?: any) {
        if (data !== undefined) {
            this._id = data._id;
            this.user_id = data.user_id;
        }
    }
}

export class FishCountList {
    _id: string;
    title: string;
    updated_at: string;
    news_date_time: any;
    short_description: string;
    likes: number;
    is_featured: string;
    description: string;
    favourites: string;
    created_at: string;
    __v: string;
    thumb_image_url: any;
    image_url: any;
    status: string;
    favouriteList: Favourite[] = [];
    LikeList: Like[] = [];
    participantList: any[] = [];
    participantCount: number;
    favouriteStatus: boolean = false;
    LikeStatus: boolean = false;
    date_time: any;
    constructor(data?: any) {
        if (data !== undefined) {
            this._id = data._id;
            this.title = data.title;
            this.status = data.status;
            this.updated_at = data.updated_at;
            this.news_date_time = data.news_date_time;
            this.short_description = data.short_description;
            this.description = data.description;
            this.created_at = data.created_at;
            this.__v = data.__v;

            this.thumb_image_url = data.thumb_image_url;
            this.image_url = data.image_url;
            var date1 = new Date(data.news_date_time);
            var dt = date1.toUTCString();
            var Day = ("0" + (date1.getDate())).slice(-2);
            var Month = ("0" + (date1.getMonth() + 1)).slice(-2);
            const currentYear = date1.getFullYear();
            var dateFormat1 = currentYear + '-' + Month + '-' + Day;
            console.log(dateFormat1 + '' + dt);
            this.date_time = dateFormat1;
        }
    }
}
export class FishNewsList {
    _id: string;
    title: string;
    updated_at: string;
    news_date_time: any;
    short_description: string;
    likes: number;
    is_featured: string;
    description: string;
    favourites: string;
    created_at: string;
    __v: string;
    thumb_image_url: any;
    image_url: any;
    status: string;
    favouriteList: Favourite[] = [];
    LikeList: Like[] = [];
    participantList: any[] = [];
    participantCount: number;
    favouriteStatus: boolean = false;
    LikeStatus: boolean = false;
    date_time: any;
    constructor(data?: any) {
        console.log('data', data);
        if (data !== undefined) {
            this._id = data._id;
            this.title = data.title;
            this.status = data.status;
            this.updated_at = data.updated_at;
            this.news_date_time = data.news_date_time;
            this.short_description = data.short_description;
            this.description = data.description;
            this.created_at = data.created_at;
            this.__v = data.__v;
            // if(data.thumb_image_url =='http://166.62.118.179:2152/assets/img/default-event.png' && data.image_url =='http://166.62.118.179:2152/assets/img/default-event.png' ){
            //     this.image_url='assets/img/icon/default-event.png';
            //     this.image_url='assets/img/icon/default-event.png';
            // }else{
            //     this.image_url=data.thumb_image_url;
            //     this.image_url=data.image_url;
            // }           
            this.thumb_image_url = data.thumb_image_url;
            this.image_url = data.image_url;
            var date1 = new Date(data.news_date_time);
            var dt = date1.toUTCString();
            var Day = ("0" + (date1.getDate())).slice(-2);
            var Month = ("0" + (date1.getMonth() + 1)).slice(-2);
            const currentYear = date1.getFullYear();
            var dateFormat1 = currentYear + '-' + Month + '-' + Day;
            console.log(dateFormat1 + '' + dt);
            this.date_time = dateFormat1;
            this.favouriteList = data.favourites;
            var id = localStorage.getItem('userId');
            for (let item of data.favourites) {
                // this.favouriteList.push(new Favourite(item));
                if (item.user_id._id == id) {
                    this.favouriteStatus = true;
                }
            }

            for (let item of data.likes) {
                //this.LikeList.push(new Like(item));
                if (item.user_id._id == id) {
                    this.LikeStatus = true;
                }
            }
        }
    }
}

export class ShoutOutActivityList {

    abuse: any;
    status: any;
    _id: any;
    description: any;
    user_id: any;
    updated_at: any;
    created_at: any;
    __v: any;
    images: any = [];
    favourite_count: any;
    comment_count: any;
    favouriteList: Favourite[] = [];
    favouriteStatus: boolean = false;
    likes: number;
    constructor(data?: any) {
        if (data !== undefined) {
            var id = localStorage.getItem('userId');
            for (let item of data.favourites) {
                this.favouriteList.push(new Favourite(item));
                if (item.user_id == id) {
                    this.favouriteStatus = true;
                }
            }
        }
    }
}


export class FishingExchngEvent {
    trip_booking_type: string;
    trip_type: string;
    user_type: string;
    trip_status: string;
    status: string;
    _id: string;
    title: string;
    fishing_type_id: string;
    fishing_location_id: string;
    boat_type_id: string;
    angler_pay: any[] = [];
    guide_accept: any[] = [];
    date: string;
    time: string;
    trip_amount: string;
    member: any;
    total_booking: any;
    total_Available: number = 0;
    created_by: string;
    updated_by: string;
    date_time: string;
    updated_at: string;
    created_at: string;
    __v: string;
    is_trip_request: string;
    favouriteStatus: boolean = false;
    favouriteList: Favourite[] = [];
    likes: number;
    No_member: number = 0;
    imgUrl: any;
    show_membership_details: boolean = false;
    constructor(data?: any) {
        let self = this;
        if (data !== undefined) {
            self._id = data._id;
            self.created_at = data.created_at;
            self.updated_at = data.updated_at;
            self.__v = data.__v;
            self.status = data.status;
            self.title = data.title;
            self.trip_booking_type = data.trip_booking_type;
            self.trip_type = data.trip_type;
            self.user_type = data.user_type;
            self.trip_status = data.trip_status;
            self.fishing_type_id = data.fishing_type_id;
            self.fishing_location_id = data.fishing_location_id;
            self.boat_type_id = data.boat_type_id;
            self.angler_pay = data.angler_pay;
            var id = localStorage.getItem('userId');
            if (data.angler_pay.length !== 0) {
                for (let item of data.angler_pay) {
                    if (item.user_id._id === id) {
                        var num = parseInt(item.member);
                        self.No_member = self.No_member + num;
                    }
                }
            }
            self.guide_accept = data.request;
            self.date = data.date;
            self.time = data.time;
            self.trip_amount = data.trip_amount;
            self.member = data.member;
            self.total_booking = data.total_booking;
            if (self.total_booking !== undefined && self.total_booking !== null) {
                self.total_Available = parseInt(self.member) - parseInt(self.total_booking);
            } else {
                self.total_Available = parseInt(self.member);
            }
            self.created_by = data.created_by;
            self.updated_by = data.updated_by;
            try {

                if (data.created_by && data.created_by.details !== undefined && data.created_by.details !== null) {
                    if (data.created_by.details.image !== undefined && data.created_by.details.image !== '') {
                        let split_string = data.created_by.details.image.split(',');
                        if (split_string[0] == 'data:image/*;charset=utf-8;base64') {
                            self.imgUrl = data.created_by.details.image;
                        } else if (split_string[0] == "data:image/png;base64") {
                            self.imgUrl = data.created_by.details.image;
                        } else {
                            self.imgUrl = 'data:image/jpeg;base64,' + data.created_by.details.image;
                        }
                    } else {
                        self.imgUrl = "assets/img/FishTopia_Guide.png";
                    }
                } else if (data.imgUrl == undefined || !data.show_membership_details) {
                    if (data.user_type == "guide") {
                        self.imgUrl = "assets/img/FishTopia_Guide.png";
                    } else {
                        self.imgUrl = "assets/img/event-Thumb.png";
                    }
                }
            } catch (err) {
                if (data.imgUrl == undefined || !data.show_membership_details) {
                    if (data.user_type == "guide") {
                        self.imgUrl = "assets/img/FishTopia_Guide.png";
                    } else {
                        self.imgUrl = "assets/img/event-Thumb.png";
                    }
                }
            }
            self.date_time = data.date_time;
            self.is_trip_request = data.is_trip_request;
            console.log('data.show_membership_details;', data.show_membership_details);
            if (data.show_membership_details !== undefined) {
                self.show_membership_details = data.show_membership_details;
            }
            // var id = localStorage.getItem('userId');
        }
    }
}
