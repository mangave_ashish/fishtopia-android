import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Storage } from '@ionic/storage';

@Injectable()
export class UserData {

  auth: string;
  constructor(public storage: Storage) { }
  /**
   * This method is used to set value of loggin key into Storage
   */

  setAuthToken(auth: any) {
    this.storage.set('auth', auth);
    console.log("AuthToken in userdata", auth);
  }

  removeAuthToken() {
    this.storage.remove('auth');
    console.log('Auth token is removed');
  }
}
