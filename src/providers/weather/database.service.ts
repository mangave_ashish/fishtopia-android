import { Injectable } from '@angular/core';
import { Sql } from './sql';
import { Forecast, Location } from './model';

@Injectable()
export class DatabaseService {
  private table_forecast = 'forecast';
  private table_world_location = 'world_location';
  private table_Tide = 'tbl_Tide';
  private table_Tide_Min = 'tbl_Tide_Min';
  private table_Regulations = 'tbl_Regulations';
  constructor(public _db: Sql) {
  }

  //
  // table_forecast queries
  //
  addForecast(name: string, forecast: Forecast): Promise<boolean> {
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_forecast} (name, forecast, lastUpdated) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_forecast} (name TEXT PRIMARY KEY, forecast TEXT, lastUpdated TEXT)`;
    let self = this;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, [name, JSON.stringify(forecast), '' + lastUpdated]))
      .then(data => {
        console.debug(name + ' > Inserted with id -> ' + data.res.insertId);
        return true;
      })
      .catch(error => {
        console.error('Saving forecast error -> ' + error.err.message);
        return false;
      });
  }

  getForecast(name: string): Promise<{ forecast: Forecast, lastUpdated: number }> {
    let getQuery: string = `SELECT forecast, lastUpdated FROM ${this.table_forecast} WHERE name = ?`;
    return this._db.query(getQuery, [name])
      .then(data => {
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return {
            forecast: JSON.parse(obj.forecast),
            lastUpdated: +obj.lastUpdated
          };
        }
        return null;
      })
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return null;
      });
  }

  //
  // table_world_location queries
  //
  addWorldLocation(location: Location): Promise<boolean> {
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_world_location} (name, lat, lng) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_world_location} (name TEXT PRIMARY KEY, lat TEXT, lng TEXT)`;
    let self = this;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, [location.name, location.lat, location.lng]))
      .then(data => {
        console.debug(location.name + ' > Inserted with id -> ' + data.res.insertId);
        return true;
      })
      .catch(error => {
        console.error('Saving world location error -> ' + error.err.message);
        return false;
      });
  }

  getWorldLocation(name: string): Promise<Location> {
    let getQuery: string = `SELECT name, lat, lng FROM ${this.table_world_location} WHERE name = ?`;
    return this._db.query(getQuery, [name])
      .then(data => {
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return {
            name: obj.name,
            lat: +obj.lat,
            lng: +obj.lng
          };
        }
        return null;
      })
      .catch(error => {
        console.error('Getting world location error -> ' + error.err.message);
        return null;
      });
  }

  removeWorldLocation(name: string): Promise<boolean> {
    let query: string = `DELETE FROM ${this.table_world_location} WHERE name = ?`;
    let self = this;
    return self._db.query(query, [name])
      .then(() => true)
      .catch(error => {
        console.error('Removing world location error -> ' + error.err.message);
        return false;
      });
  }

  getAllWorldLocations(): Promise<Array<Location>> {
    let getQuery: string = `SELECT name, lat, lng FROM ${this.table_world_location}`;
    let resultArray: Array<Location> = [];
    return this._db.query(getQuery)
      .then(data => {
        for (let i = 0; i < data.res.rows.length; i++) {
          let obj: any = data.res.rows.item(i);
          resultArray.push({
            name: obj.name,
            lat: +obj.lat,
            lng: +obj.lng
          });
        }
        return resultArray;
      })
      .catch(error => {
        console.error('Getting all world locations error -> ' + error.err.message);
        return resultArray;
      });
  }


  //
  // table_Tide_Data queries
  //

  addTide_Data(Tide_Data: any): Promise<any> {
    let self = this;
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Tide} (_id,data,status) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, ['Tide_DB', JSON.stringify(Tide_Data), '' + lastUpdated]))
      .then(data => {
        console.log(' > Inserted with id -> ' + JSON.stringify(data));
        return true;
      })
      .catch(error => {
        console.error('Saving forecast error -> ' + error.err.message);
        return false;
      });
  }


  updateTide_Data(Tide_Data: any): Promise<any> {
    console.log('Update', Tide_Data);
    let getQuery: string = `UPDATE ${this.table_Tide} SET data=? WHERE _id=?`;
    // console.log('getQuery', getQuery)
    return this._db.query(getQuery, [Tide_Data, 'Tide_DB'])
      .then(data => true)
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return false;
      });
  }




  getTide_Data(station: any): Promise<any> {
    let getQuery: string = `SELECT data FROM ${this.table_Tide} WHERE _id = ?`;
    // console.log('getQuery', getQuery)
    return this._db.query(getQuery, [station])
      .then(data => {
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return obj;
        }
      })
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return null;
      });
  }



  addTidepins(Tide_Data: any, station: any): Promise<any> {
    let self = this;
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Tide} (_id,data,status) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, [station, JSON.stringify(Tide_Data), '' + lastUpdated]))
      .then(data => {
        console.log(' > Inserted with id -> ' + JSON.stringify(data));
        return true;
      })
      .catch(error => {
        console.error('Saving forecast error -> ' + error.err.message);
        return false;
      });
  }


  getTidepins(station: any): Promise<any> {
    let getQuery: string = `SELECT data FROM ${this.table_Tide} WHERE _id = ?`;
    // console.log('getQuery', getQuery)
    return this._db.query(getQuery, [station])
      .then(data => {
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return obj;
        }
      })
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return null;
      });
  }

  // mintus Data 
  addTideMinpins(Tide_Data: any, station: any): Promise<any> {
    let self = this;
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Tide_Min} (_id,data,status) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide_Min} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, [station, JSON.stringify(Tide_Data), '' + lastUpdated]))
      .then(data => {
        console.log(' > Inserted with id -> ' + JSON.stringify(data));
        return true;
      })
      .catch(error => {
        console.error('Saving forecast error -> ' + error.err.message);
        return false;
      });
  }


  getTideMinpins(station: any): Promise<any> {
    let getQuery: string = `SELECT data FROM ${this.table_Tide_Min} WHERE _id = ?`;
    // console.log('getQuery', getQuery)
    return this._db.query(getQuery, [station])
      .then(data => {
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return obj;
        }
      })
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return null;
      });
  }


  // table_Currents_Data queries


  addCurr_Data(Curr_Data: any): Promise<any> {
    let self = this;
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Tide} (_id,data,status) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, ['Curr_DB', JSON.stringify(Curr_Data), '' + lastUpdated]))
      .then(data => {
        console.log(' > Inserted with id -> ' + JSON.stringify(data));
        return true;
      })
      .catch(error => {
        console.error('Saving forecast error -> ' + error.err.message);
        return false;
      });
  }





  addCurrentsPins(Currents_Data: any, station: any): Promise<boolean> {
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Tide} (_id,data,status) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
    let self = this;
    console.log(Currents_Data, station);
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, [station, JSON.stringify(Currents_Data), '' + lastUpdated])
      ).then(data => {
        console.debug(name + ' > Inserted with id -> ' + data);
        return true;
      })
      .catch(error => {
        console.error('Saving forecast error -> ' + error.err.message);
        return false;
      });
  }

  getCurrentsPins(station: any): Promise<any> {
    let getQuery: string = `SELECT * FROM ${this.table_Tide} WHERE _id = ?`;
    return this._db.query(getQuery, [station])
      .then(data => {
        //console.log('DBservicedata', JSON.stringify(data.res.rows.item(0)));
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return obj;
        }
        //  return null;
      })
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return null;
      });
  }


  updateCurr_Data(Curr_Data: any): Promise<any> {
    console.log('Update', Curr_Data);
    let getQuery: string = `UPDATE ${this.table_Tide} SET data=? WHERE _id=?`;
    // console.log('getQuery', getQuery)
    return this._db.query(getQuery, [Curr_Data, 'Curr_DB'])
      .then(data => true)
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return false;
      });
  }



  //Marine Add Data
  addMarine_Data(Marine_Data: any): Promise<any> {
    let self = this;
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Tide} (_id,data,status) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, ['Marine_DB', JSON.stringify(Marine_Data), '' + lastUpdated]))
      .then(data => {
        console.log(' > Inserted with id -> ' + JSON.stringify(data));
        return true;
      })
      .catch(error => {
        console.error('Saving forecast error -> ' + error.err.message);
        return false;
      });
  }

  getMarinePins(station: any): Promise<any> {
    let getQuery: string = `SELECT * FROM ${this.table_Tide} WHERE _id = ?`;
    return this._db.query(getQuery, [station])
      .then(data => {
        //console.log('DBservicedata', JSON.stringify(data.res.rows.item(0)));
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return obj;
        }
        //  return null;
      })
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return null;
      });
  }


  updateMarine_Data(Marine_Data: any): Promise<any> {
    console.log('Update', Marine_Data);
    let getQuery: string = `UPDATE ${this.table_Tide} SET data=? WHERE _id=?`;
    // console.log('getQuery', getQuery)
    return this._db.query(getQuery, [Marine_Data, 'Marine_DB'])
      .then(data => true)
      .catch(error => {
        console.error('Getting forecast error -> ' + error.err.message);
        return false;
      });
  }

  //Regulations Add Data
  addRegulationsStateData(state_Data: any, pdfUrl_data: any): Promise<any> {
    let self = this;
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Tide} (_id,data,status) VALUES (?, ?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, ['Regulations_DB' + pdfUrl_data.displayText, JSON.stringify(state_Data), '' + lastUpdated]))
      .then(data => {
        console.log(' > Inserted with id -> ' + JSON.stringify(data));
        return true;
      })
      .catch(error => {
        console.error('Saving Regulations error -> ' + error.err.message);
        return false;
      });
  }


  getRegulationsStateData(state_Data: any): Promise<any> {
    let getQuery: string = `SELECT * FROM ${this.table_Tide} WHERE _id = ?`;
    return this._db.query(getQuery, ['Regulations_DB' + state_Data.displayText])
      .then(data => {
        //console.log('DBservicedata', JSON.stringify(data.res.rows.item(0)));
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return obj;
        }
        //  return null;
      })
      .catch(error => {
        console.error('Getting Regulations error -> ' + error);
        let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Tide} (_id TEXT PRIMARY KEY,data TEXT,status TEXT)`;
        this._db.query(createTableQuery)
          .then(res => {
            console.error('Table Create forecast error -> ' + res);
          })
        return null;
      });
  }

  addRegulations_Data(regulations_Data: any, pdfUrldata: any): Promise<any> {
    let self = this;
    let lastUpdated: number = Date.now();
    let insertQuery: string = `INSERT OR REPLACE INTO ${this.table_Regulations} (_id,data,parentName,status) VALUES (?, ?,?, ?)`;
    let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Regulations} (_id TEXT PRIMARY KEY,data TEXT,parentName TEXT,status TEXT)`;
    return self._db.query(createTableQuery)
      .then(() => self._db.query(insertQuery, ['regulations_' + pdfUrldata.displayText, JSON.stringify(regulations_Data), pdfUrldata.parent_Name, '' + lastUpdated]))
      .then(data => {
        console.log(' > Inserted with id -> ' + JSON.stringify(data));
        return true;
      })
      .catch(error => {
        console.error('Saving Regulations error -> ' + error.err.message);
        return false;
      });
  }


  getRegualtions(pdfUrldata: any): Promise<any> {
    let getQuery: string = `SELECT * FROM ${this.table_Regulations} WHERE _id = ? AND parentName=?`;
    return this._db.query(getQuery, ['regulations_' + pdfUrldata.displayText, pdfUrldata.parent_Name])
      .then(data => {
        if (data.res.rows.length > 0) {
          let obj: any = data.res.rows.item(0);
          return obj;
        }
        //  return null;
      })
      .catch(error => {
        console.error('Getting Regulations error -> ' + error.err.code);
        if (error.err.code == 5) {
          let createTableQuery: string = `CREATE TABLE IF NOT EXISTS ${this.table_Regulations} (_id TEXT PRIMARY KEY,data TEXT,parentName TEXT,status TEXT)`;
          this._db.query(createTableQuery)
            .then(res => {
              console.error('Table Create table_Regulations -> ' + JSON.stringify(res));
            })
            .catch(error => {
              console.error('Table Create table_Regulations error -> ' + error.err.message);
            })
        }
        return null;
      });
  }

  //
  // Shared getter setter
  //
  set(key: string, value: string): Promise<boolean> {
    return this._db.set(key, value)
      .then(() => true)
      .catch(err => {
        console.error('[Error] Saving ' + key + ' - ' + err);
        return false;
      });
  }

  get(key: string): Promise<string> {
    return this._db.get(key)
      .then(value => {
        if (value) {
          return value;
        } else {
          throw new Error('Undefined value');
        }
      })
      .catch(err => {
        console.error('[Error] Getting ' + key + ' - ' + err);
        return null;
      });
  }

  remove(key: string): Promise<boolean> {
    return this._db.remove(key)
      .then(() => true)
      .catch(err => {
        console.error('[Error] Removing ' + key + ' - ' + err);
        return false;
      });
  }

  getJson(key: string): Promise<any> {
    return this.get(key).then(value => {
      try {
        return JSON.parse(value);
      } catch (err) {
        console.error('[Error] getJson(): unable to parse value for key', key, ' as JSON');
        return null;
      }
    });
  }

  setJson(key: string, value: any): Promise<boolean> {
    try {
      return this.set(key, JSON.stringify(value));
    } catch (err) {
      return Promise.resolve(false);
    }
  }
}
